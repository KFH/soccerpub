// IDEA
// create some kind of menu and menu item abstraction
// needs a concept like focus
#include "gamecontrols.hpp"
#include "render.hpp"
#include <stdint.h>
#include <string.h>

namespace m {

struct focus {
	int x, y, select, back;
};

class menuItem {
    public:
	// TODO
	// needs function for handling keypresses too ?
	// and pastes ?
	virtual int render(char* out) = 0;
	virtual int input(const focus& f) { return 0; }
	virtual int keyevent(int keycode, int event, const char* str) { return 0; }
};

struct menu_ctrl_t {
	float   x;
	float   y;
	uint8_t act;

	v3 GetDir() const { return v3{ x, y, 0 }; };

	enum { NONE, SELECT, BACK };
};

class menu {
	char                   text[512];
	int                    in_focus;
	std::vector<menuItem*> items;

    public:
	uint8_t focuscolor;
	uint8_t textcolor;

	menu()
	{
		in_focus   = 0;
		focuscolor = 2;
		textcolor  = 1;
	}
	~menu() {}

	void AddMenu(menuItem* ni) { items.push_back(ni); }

	void Input(const focus& f)
	{

		in_focus += f.y;
		if (items.size() > 0) {
			in_focus = in_focus < 0 ? items.size() - 1 : in_focus;
			in_focus = in_focus % items.size();
			items[in_focus]->input(f);
		}
	}

	int keyevent(int keycode, int event, const char* str)
	{
		items[in_focus]->keyevent(keycode, event, str);
		return 0;
	}

	void Render(Messanger& msger)
	{
		memset(text, 0, 512);
		char* t  = text;
		int   ix = 0;
		for (auto item : items) {
			int s         = item->render(t);
			msger.fgColor = textcolor;
			msger.bgColor = 0;
			if (ix == in_focus)
				msger.fgColor = focuscolor;
			msger.Write(t);
			t += s;
			ix++;
		}
	}
};

class menuManager {
	// TODO
	// hold ptr to multiple menus and switch based on menu requests?
	v3  odir;
	int oact;
	int ix;

    public:
	std::vector<menu*> menuAvail;

	menuManager()
	{
		ix   = 0;
		oact = 0;
	}
	~menuManager()
	{
		// delete all the menus
	}
	void Render(Messanger& msger)
	{
		if (ix >= 0 && ix < menuAvail.size())
			menuAvail[ix]->Render(msger);
	}

	void SelMenu(const menu* tm)
	{
		for (size_t i = 0l; i < menuAvail.size(); i++) {
			if (menuAvail[i] == tm)
				ix = i;
		}
	}

	void Keyevent(int keycode, int event, const char* str)
	{
		menuAvail[ix]->keyevent(keycode, event, str);
	}
	void Input(const menu_ctrl_t& pc)
	{
		focus in  = {};
		v3    dir = pc.GetDir();
		fp32  dx  = dir.x - odir.x;
		fp32  dy  = dir.y - odir.y;
		int   act = pc.act;

		// compare dir with old dir?
		if (std::abs(dir.x) >= 0.99f) {
			if (dx > 0.f && dir.x > 0.f) {
				in.x = 1;
			}
			if (dx < 0.f && dir.x < 0.f) {
				in.x = -1;
			}
		}
		if (std::abs(dir.y) >= 0.99f) {
			if (dy > 0.f && dir.y > 0.f) {
				in.y = 1;
			}
			if (dy < 0.f && dir.y < 0.f) {
				in.y = -1;
			}
		}
		if (act != oact) {
			if (act & menu_ctrl_t::SELECT) {
				in.select = 1;
			}
			if (act & menu_ctrl_t::BACK) {
				in.back = 1;
			}
		}
		odir = dir;
		oact = act;
		if (ix >= 0 && ix < menuAvail.size())
			menuAvail[ix]->Input(in);
	}
};

class menuToggle : public menuItem {
	char mlabel[32];
	int  mlabel_len;
	int* mtog;

    public:
	menuToggle(const char* label, int* tog)
	{
		mlabel_len = sprintf(mlabel, "%s", label);
		mtog       = tog;
	}
	virtual int render(char* out) override
	{
		// dunno if this is good idea
		return sprintf(out, "%s: %s", mlabel, *mtog ? "on" : "off");
	}
	virtual int input(const focus& f) override
	{
		// based on x delta toggle
		return 0;
	}
};

typedef void (*menuFnCallback_t)();

class menuFunc : public menuItem {
	char             mlabel[32];
	int              mlabel_len;
	menuFnCallback_t mcb;

    public:
	menuFunc(const char* label, menuFnCallback_t cb)
	{
		mlabel_len = sprintf(mlabel, "%s", label);
		mcb        = cb;
	}
	virtual int render(char* out) override
	{
		memcpy(out, mlabel, mlabel_len);
		return mlabel_len;
	}
	virtual int input(const focus& f) override
	{
		if (f.select && mcb)
			mcb();
		return 0;
	}
};

class menuSel : public menuItem {
	char         mlabel[32];
	int          mlabel_len;
	const menu*  tm;
	menuManager& manager;

    public:
	menuSel(const char* label, const menu* _tm, menuManager& mm)
	    : manager(mm)
	{
		mlabel_len = sprintf(mlabel, "%s", label);
		tm         = _tm;
	}
	virtual int render(char* out) override
	{
		memcpy(out, mlabel, mlabel_len);
		return mlabel_len;
	}
	virtual int input(const focus& f) override
	{
		if (f.select)
			manager.SelMenu(tm);
		return 0;
	}
};

class menuIP : public menuItem {
	char (&hostaddr)[24];
	int ix = 0;

    public:
	menuIP(char (&addr)[24])
	    : hostaddr(addr)
	{
		ix = strlen(hostaddr) - 1;
	}
	virtual int render(char* out) override
	{
		int len = strlen(hostaddr);
		memcpy(out, hostaddr, len);
		return len;
	}
	virtual int input(const focus& f) override { return 0; }
	virtual int keyevent(int keycode, int event, const char* str)
	{
		if (event != KEY_EVENT_PRESS)
			return 0;
		if (keycode == KEY_BACKSPACE) {
			hostaddr[ix] = 0;
			ix--;
			ix           = ix < 0 ? 0 : ix;
			hostaddr[ix] = '_';
		}
		if (str != nullptr
		    && ((str[0] <= '9' && str[0] >= '0') || str[0] == ':' || str[0] == '.')) {
			hostaddr[ix] = str[0];
			ix++;
			ix           = ix >= 23 ? 22 : ix;
			hostaddr[ix] = '_';
		} else if (keycode == KEY_PERIOD || keycode == KEY_SEMICOLON
		           || (keycode >= KEY_0 && keycode <= KEY_9)) {
			hostaddr[ix] = keycode;
			ix++;
			ix           = ix >= 23 ? 22 : ix;
			hostaddr[ix] = '_';
		}

		return 0;
	}
};
}; // namespace m
