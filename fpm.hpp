#pragma once

#include <cstdint>

typedef double fp64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#define FLOATTYPE

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#ifdef FIXEDTYPE0
#include "Fixed.h"
typedef numeric::Fixed<24, 8> fp32;
inline i32                    signfp(fp32 t) { return t < 0 ? 1 : 0; }
inline fp32                   sqrtfp(fp32 t)
{
	// converting to float and back
	return sqrtf(t);
}
inline fp32 absfp(fp32 t) { return t.abs(); }
inline fp32 fp32epsilon() { return 0; }
#endif

#ifdef FIXEDTYPE1
// TODO/NOTE
// this fixed point lib has strange conversion behaviors
// compile and runs without errors but game does not work
// things do not move correctly
#include <sg14/fixed_point>
// typedef sg14::fixed_point<int32_t,-28> fp32;
typedef sg14::make_fixed<23, 8, int16_t> fp32;

inline i32  signfp(fp32 t) { return t < 0 ? 1 : 0; }
inline fp32 sqrtfp(fp32 t)
{
	fp32       d = sg14::abs(t);
	const fp32 z = std::numeric_limits<fp32>::epsilon();
	if (d <= z) {
		return fp32(0);
	}
	return sg14::sqrt(d);
}
inline fp32 absfp(fp32 t) { return sg14::abs(t); }
inline fp32 fp32epsilon() { std::numeric_limits<fp32>::epsilon(); }
#endif

#ifdef FLOATTYPE
#include <limits>
#include <cmath>
typedef float fp32;
inline i32    signumfp(fp32 t) { return (0 < t) - (t < 0); }
inline i32    signfp(fp32 t) { return t < 0 ? 1 : 0; }
inline fp32   sqrtfp(fp32 t) { return std::sqrt(t); }
inline fp32   absfp(fp32 t) { return abs(t); }
inline fp32   fp32epsilon() { return std::numeric_limits<fp32>::epsilon(); }

inline fp32 smoothstep(fp32 x, fp32 e0, fp32 e1)
{
	fp32 t = std::max(std::min((x - e0) / (e1 - e0), 1.f), 0.f);
	return t * t * (3.f - 2.f * t);
}

#endif

inline i32 rdiv(i32 x, i32 divisor) { return ((x + (divisor / 2)) / divisor) * divisor; }
// make a psuedorandom float -1.f to +1.f
inline float randf() { return (((float)rand()) / (float)RAND_MAX - 0.5f) * 2.f; }
// make psuedorandom float 0.f to 1.f
inline float randf_pos() { return (((float)rand()) / (float)RAND_MAX); }

inline double signof(double x) { return x < 0.0 ? -1.0 : 1.0; }

struct i3 {
	i32      x;
	i32      y;
	i32      z;
	const i3 operator+(const i3& other) const
	{
		return i3{ x + other.x, y + other.y, z + other.z };
	}
	const i3 operator-(const i3& other) const
	{
		return i3{ x - other.x, y - other.y, z - other.z };
	}
};
// TODO
// probably need some fixed point math functions for sinewaves
struct v3 {
	fp32 x;
	fp32 y;
	fp32 z;

	// TODO
	// implement some vector math functions
	// and simd version if possible

	const v3 operator+(const v3& other) const
	{
		return v3{ x + other.x, y + other.y, z + other.z };
	}
	const v3 operator-(const v3& other) const
	{
		return v3{ x - other.x, y - other.y, z - other.z };
	}
	const v3 operator*(const v3& o) const { return v3{ (x * o.x), (y * o.y), (z * o.z) }; }
	const v3 operator*(const fp32 m) const { return v3{ x * m, y * m, z * m }; }
	const v3 operator/(fp32 d) const { return v3{ x / d, y / d, z / d }; }

	const v3 normal() const
	{
		fp32 d = this->len();
		if (d <= fp32epsilon())
			return v3{ 0, 0, 0 };
		return v3{ x / d, y / d, z / d };
	}
	fp32 sum() const { return x + y + z; }
	fp32 len() const { return sqrtfp(x * x + y * y + z * z); }
	fp32 len2() const { return (x * x) + (y * y) + (z * z); }
	fp32 dist2(const v3& o) const
	{
		v3 d = *this - o;
		return d.len2();
	}
	fp32 dist(const v3& o) const
	{
		v3 d = *this - o;
		return d.len();
	}
	fp32 dot(const v3& o) const { return (o.x * x) + (o.y * y) + (o.z * z); }

	v3 cross(const v3& o) const
	{
		return v3{ (y * o.z) - (z * o.y), (z * o.x) - (x * o.z), (x * o.y) - (y * o.x) };
	}

	fp32 minXY() const { return x < y ? x : y; }

	v3 round() const { return v3{ roundf(x), roundf(y), roundf(z) }; }

	i3 toInt() const
	{
		v3 n = round();
		return i3{ i32(n.x), i32(n.y), i32(n.z) };
	}

	void zero()
	{
		x = 0;
		y = 0;
		z = 0;
	}
};

static inline bool InRect(v3 cent, v3 halfdim, v3 pos)
{
	return (pos.x < cent.x + halfdim.x) & (pos.x > cent.x - halfdim.x)
	    & (pos.y < cent.y + halfdim.y) & (pos.y > cent.y - halfdim.y);
}

// based on https://stackoverflow.com/a/14143738/1650183
static inline bool Intersects(v3 s0, v3 e0, v3 s1, v3 e1, v3& ip)
{
	auto isbetween
	    = [](const fp32& x0, const fp32& x, const fp32& x1) { return (x >= x0) && (x <= x1); };

	fp32 xy, ab;
	bool partial = false;
	fp32 denom   = (s1.y - e1.y) * (s0.x - e0.x) - (s0.y - e0.y) * (s1.x - e1.x);
	if (denom == fp32(0)) {
		xy = -1;
		ab = -1;
	} else {
		xy = (s1.x * (e0.y - e1.y) + e1.x * (s1.y - e0.y) + e0.x * (e1.y - s1.y)) / denom;
		partial = isbetween(0, xy, 1);
		if (partial) {
			ab = (e0.y * (s0.x - e1.x) + e1.y * (e0.x - s0.x) + s0.y * (e1.x - e0.x))
			    / denom;
		}
	}
	if (partial && isbetween(0, ab, 1)) {
		ab = fp32(1) - ab;
		xy = fp32(1) - xy;
		// interseciont point
		// fp32 dx = s0.x - e0.x;
		// fp32 dy = s0.y - e0.y;
		// printf("INTERSECTS xy%f ab%f @ %03dx%03d\n",
		//	xy, ab, (s0.x + dx * xy), (s0.y + dy * xy));
		ip.x = s0.x + (s0.x - e0.x) * xy;
		ip.y = s0.y + (s0.y - e0.y) * xy;
		return true;
	} else
		return false;
}
static inline fp32 clamp(fp32 x, fp32 min, fp32 max) { return x > max ? max : x < min ? min : x; }

static inline v3 ClosestPtOnLine(v3 s, v3 e, v3 pt)
{
	const v3   d    = s - e;
	const fp32 half = d.len() / fp32(2.f);
	const v3   n    = (s - e).normal();
	const v3   mid  = (s + e) / fp32(2.f);
	return mid + (n * clamp(n.dot(pt - mid), -half, half));
}

static inline fp32 DistToLine(v3 s, v3 e, v3 pt)
{
	// distance of point to line
	v3 closestPt = ClosestPtOnLine(s, e, pt);
	return pt.dist(closestPt);
}

static inline v3 interceptPT(v3 origin, v3 target, v3 targetvel, fp32 speed)
{

	v3   delta = (target - origin);
	v3   d2    = delta.normal() - targetvel.normal();
	fp32 rr    = d2.y != 0.f ? d2.x / d2.y : 0.f;

	fp32 ang = tanf(rr);

	fp32 div = speed <= 0.1 ? 0.f : delta.len() * std::cos(ang) / speed;
	div      = clamp(div, 0.f, 100.f);
	return target + (targetvel.normal() * div);
}
static inline bool PointInTriangle(v3 pt, v3 p0, v3 p1, v3 p2)
{

	fp32 s = p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * pt.x + (p0.x - p2.x) * pt.y;
	fp32 t = p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * pt.x + (p1.x - p0.x) * pt.y;

	if ((s < 0) != (t < 0))
		return false;

	fp32 a = -p1.y * p2.x + p0.y * (p2.x - p1.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y;

	return a < 0 ? (s <= 0 && s + t >= a) : (s >= 0 && s + t <= a);
}

inline v3 linear(fp32 t, v3 p0, v3 p1) { return p0 * (1.f - t) + p1 * t; }

inline v3 quadratic(fp32 t, v3 p0, v3 p1, v3 p2)
{
	return p0 * std::pow(1.f - t, 2.f) + p1 * 2.f * (1.f - t) * t + p2 * (t * t);
}

inline v3 bezier(fp32 t, v3 p0, v3 p1, v3 p2, v3 p3)
{
	return p0 * std::pow(1.f - t, 3.f) + p3 * (t * t * t) + p2 * 3.f * (t * t) * (1.f - t)
	    + p1 * 3.f * std::pow(1.f - t, 2.f) * t;
}
