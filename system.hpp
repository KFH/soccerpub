
#include <mutex>
#include "gfx.hpp"
#include "game.hpp"
#include "gamesync.hpp"

namespace sim {

template <class input_t> class GameSystem : public snapShotIngestor {

	struct PLAYERCXN_t {
		playerIX   ix;
		net::CXN_h netHandle;

		RingRecordStream<input_t>* netInput; // LOCAL players also have a net input?
		RingRecordStream<input_t>* netOutput;
		InputStream<input_t>*      input;

		RpcDeserializer*     rpc;
		NoOrderDeserializer* rpc_unorder;

		bool operator==(const playerIX& index) const { return ix == index; }
		bool operator==(const net::CXN_h& nh) const { return netHandle == nh; }
	};
	typedef typename vInstance<input_t>::ctrlOverride_t ctrlOverride_t;

	std::vector<PLAYERCXN_t> conns;

    public:
	net::CTX_h nctx   = NET_BADVAL;
	bool       isHost = true;

	Game<input_t>&         game;
	SnapRecorder           snapRec;
	SnapRecorder::snapshot snap;
	timeSlewer             timeslew;
	int                    simInputRem;
	//	std::mutex             mlock;

	GameSystem(Game<input_t>& initGame)
	    : game(initGame)
	    , snapRec()
	{
		simInputRem = 0;
		ResetSlew();
		snapRec.Exec(game);
	}
	~GameSystem()
	{
		// free resources
	}
	void ResetStreams();

	void AddLocalPlayer(playerIX ix, InputStream<input_t>* in);

	playerIX GetLocalPlayer();

	void DelLocalPlayer(playerIX ix);

	void AddRemotePlayer(playerIX ix, net::CXN_h handle);

	void DelRemotePlayer(net::CXN_h handle);

	int GetSimInputRem() { return simInputRem; }

	void ResetSlew();

	double GetSlew() const { return timeslew.GetCoefficient(game.GetFrameCount()); }
	double GetSlewDiff() const { return timeslew.GetTimeDiff(game.GetFrameCount()); }

	void SendAllSync();

	void SendTimeStamp(uint64_t timesent, net::CXN_h conn);

	void ReqTimeStamp(net::CXN_h conn);

	int GetSnapDist() { return game.GetFrameCount() - snap.frame; }

	int GetNumPlayers() { return conns.size(); }

	void ForceSnap();

	int Resimulate();

	void SyncInputs(int simstepstaken);

	int StepGame(double dt);

	void PumpNetwork();

	void SyncConn(net::CXN_h conn);

	void EndConn()
	{
		// send a disconnect message to all ?
	}

	// TODO
	// implement these
	void ReqPause()
	{
		if (isHost)
			game.paused = true;
		// send message to all clients to pause
	}
	void ReqUnPause()
	{
		if (isHost)
			game.paused = false;
		// send message to all clients to unpause
	}

	virtual void ApplySnapShot(ByteBuffer& buff, uint64_t frame) override
	{
		// TODO
		// empty all buffered input
		// apply snapshot
		game.SetState(buff, frame);
		ForceSnap();
	};
};

//  TODO
//  for each local player make a TeeStream
//  setup a input and netInput for playing and resimulation
template <class input_t>
void GameSystem<input_t>::AddLocalPlayer(playerIX ix, InputStream<input_t>* in)
{

	auto* r = new RingRecordStream<input_t>();
	auto* o = new RingRecordStream<input_t>();
	auto* t = new TeeStream<input_t>();
	in->SetID(ix);
	t->SetID(ix);
	r->SetID(ix);
	o->SetID(ix);

	PLAYERCXN_t c = {};
	c.netHandle   = NET_BADVAL;
	c.netInput    = r;
	c.netOutput   = o;
	c.input       = t;
	c.rpc         = nullptr;
	c.rpc_unorder = nullptr;
	c.ix          = ix;

	t->in = in; // expecting a PlayerControllerStream;
	t->rec.push_back(r);
	t->rec.push_back(o);

	conns.emplace_back(c);

	game.SetInputStream(c.ix, c.input);
}

template <class input_t> playerIX GameSystem<input_t>::GetLocalPlayer()
{
	for (auto& c : conns) {
		if (c.netHandle == NET_BADVAL)
			return c.ix;
	}
	return -1;
}

template <class input_t> void GameSystem<input_t>::DelLocalPlayer(playerIX ix)
{
	auto it = std::find(conns.begin(), conns.end(), ix);
	if (it != conns.end()) {

		printf("DELETED PLAYER %d\n", ix);

		delete it->input;
		delete it->netInput;
		delete it->netOutput;
		delete it->rpc;
		delete it->rpc_unorder;
		conns.erase(it);
	}
	game.SetInputStream(ix, nullptr);
}

template <class input_t> void GameSystem<input_t>::AddRemotePlayer(playerIX ix, net::CXN_h handle)
{
	// TODO
	// uncomment make this shit work
	auto remIn       = new RingRecordStream<input_t>();
	auto locIn       = new speculateStream<input_t>(remIn->ID(), remIn);
	auto rpc         = new RpcDeserializer(*locIn, timeslew, *this);
	auto rpc_unorder = new NoOrderDeserializer(sizeof(ctrlOverride_t), &timeslew);

	net::SetSerializer(nctx, handle, rpc, true);
	net::SetSerializer(nctx, handle, rpc_unorder, false);

	PLAYERCXN_t c = {};
	c.netHandle   = handle;
	c.netOutput   = nullptr;
	c.netInput    = remIn;
	c.input       = locIn;
	c.rpc         = rpc;
	c.rpc_unorder = rpc_unorder;
	c.ix          = ix;
	c.netInput->SetID(ix);
	c.input->SetID(ix);
	conns.emplace_back(c);
	game.SetInputStream(ix, c.input);
	// SyncConn(handle);

	if (isHost) {
		// if(false) {

		// expected format:
		// dataDesc_t
		// snapshotDesc_t
		// raw snapshot data

		dataDesc_t     dd         = RPCT::STATE;
		snapshotDesc_t sd         = {};
		size_t         header_len = sizeof(dd) + sizeof(sd);

		snap.Reset();
		// prepend data type and snapshot description
		snap.buff.append(&dd, sizeof(dd));
		snap.buff.append(&sd, sizeof(sd));
		game.SnapShot(snap.buff, snap.frame);

		// write over the empty snapshotDesc_t with final snap shot size and crc
		snapshotDesc_t* dd_ptr = (snapshotDesc_t*)(snap.buff.data() + sizeof(dd));
		dd_ptr->frame          = snap.frame;
		dd_ptr->crc            = Hash32((unsigned char*)snap.buff.data() + header_len,
                                     snap.buff.len() - header_len);
		dd_ptr->uid            = 1234;
		dd_ptr->dsize          = snap.buff.len() - header_len;

		net::Send(nctx, handle, snap.buff.data(), snap.buff.len());
		snap.Reset();
	}
}

template <class input_t> void GameSystem<input_t>::DelRemotePlayer(net::CXN_h handle)
{
	auto it = std::find(conns.begin(), conns.end(), handle);
	if (it != conns.end()) {
		DelLocalPlayer(it->ix);
	}
}

template <class input_t> void GameSystem<input_t>::ResetSlew()
{
	timeslew.Reset();
	uint64_t t = timeslew.GetTimeInt();
	timeslew.SetRef(t, 0.0);
	timeslew.InsertCheckPoint(game.GetFrameCount(), t);
}

template <class input_t> void GameSystem<input_t>::SendAllSync()
{
	uint64_t   lsnap = snapRec.HeadFrame();
	syncDesc_t sd;
	sd.time   = timeslew.GetTimeInt();
	sd.eframe = game.GetFrameCount();
	sd.sframe = lsnap;
	sd.crc    = snapRec.GetCrc(lsnap);
	syncDescChunk sdc(sd);
	net::SendAll(nctx, &sdc, sizeof(sdc));
}

template <class input_t> void GameSystem<input_t>::SendTimeStamp(uint64_t timesent, net::CXN_h conn)
{

	timeStamp_t t = { timesent, timeslew.GetTimeInt() };

	timeStampChunk data(t);
	net::Send(nctx, conn, &data, sizeof(data));
	net::FlushConn(nctx, conn);
}

template <class input_t> void GameSystem<input_t>::ReqTimeStamp(net::CXN_h conn)
{
	timeReqChunk data(timeReq_t{ timeslew.GetTimeInt() });
	net::Send(nctx, conn, &data, sizeof(data));
}

template <class input_t> void GameSystem<input_t>::ForceSnap()
{
	snap.Reset();
	game.SnapShot(snap.buff, snap.frame);
	snapRec.Exec(game);
}

template <class input_t> int GameSystem<input_t>::Resimulate()
{
	// TODO
	// this resimulation should idealy be done in a seperate thread
	//
	// TODO
	// if remote input streams have new input
	// rollback to snapshot
	// resimulate the minimum frames available across all inputstreams
	// retake new snapshot
	// resimulate using recording local inputs up to present frame
	game.m_snap = nullptr;
	if (nctx != NET_BADVAL && conns.size() > 1) {

		// find minimum number of frames to resimulate
		uint64_t fc = game.GetFrameCount();
		simInputRem = fc - snap.frame;
		int rem     = simInputRem;
		for (auto& c : conns) {
			c.netInput->Rewind();
			// find minimum available from remote players
			if (c.netHandle)
				rem = std::min(c.netInput->Remaining(), rem);
		}
		rem = std::max(rem, 0);
		// printf("REM %d MAX %d\n", rem, simInputRem);
		if (rem > 0) {

			// CALL ALL PRE RESIM CALLBACKS HERE

			game.ForcePhase(PhaseCallback::PRERESIM);

			simInputRem -= rem;

			// storing dt accumulator to reapply at end
			double accum = game.accumulator;
			// apply old snapshot only if one exists?
			game.SetState(snap.buff, snap.frame);

			// TODO
			// swap all game.streams[x] with conns.rec inputstream
			// use RingRecordStream for local players
			for (auto& c : conns) {
				c.netInput->Rewind();
				game.SetInputStream(c.ix, c.netInput);
			}
			game.m_snap = &snapRec;
			int lrem    = fc - (snap.frame + rem);
			for (int i = rem - 1; i >= 0; i--) {
				game.SingleStep();
				if (game.GetFrameCount() % 15 == 0) {
					// TODO
					// send a frame num and checksum
					// to everyone to ensure we are in sync
					// still net::SendAll(nctx, &fc,
					// sizeof(uint64_t));
				}
			}

			// CALL ALL POST RESIM CALLBACKS HERE ??
			// OR at end

			game.m_snap = nullptr;
			snap.Reset();
			game.SnapShot(snap.buff, snap.frame);
			// TODO
			// for each local players
			// Rewind() // so read offest is back to head
			// Free(rem) // so the Simulated inputs are discarded
			for (auto& c : conns) {
				c.netInput->Free(rem);
				c.netInput->Rewind();
			}
			// printf("SIM REM %d LREM %d\n", rem, lrem);
			// TODO continue simulating
			// using recording of local inputsstream and a speculative
			// control inputstreams	for remote player
			while (lrem > 0) {
				game.SingleStep();
				lrem--;
			}

			game.ForcePhase(PhaseCallback::POSTRESIM);

			game.accumulator = accum;
		}

		// TODO
		// for all players re attach their local input stream
		for (auto& c : conns) {
			c.netInput->Rewind();
			game.SetInputStream(c.ix, c.input);
		}
	} else {
		simInputRem = 0;
		game.m_snap = &snapRec;
	}
	return 0;
}

template <class input_t> void GameSystem<input_t>::SyncInputs(int simstepstaken)
{

	// TODO FIXME
	// i think this might have an off by 1 error causing divergence?
	// could a better design be found ?
	if (nctx != NET_BADVAL && conns.size() > 1) {
		for (auto& c : conns) {
			if (c.netOutput) {
				ctrlOverride_t co   = { c.ix };
				dataDesc_t     desc = RPCT::PLAYER_INPUT;
				for (int i = simstepstaken - 1; i >= 0; i--) {
					co.ctrl = c.netOutput->buff.pop();
					net::SendAll(nctx, &desc, sizeof(desc));
					net::SendAll(nctx, &co, sizeof(co));
				}
			}
		}
	}
}

template <class input_t> int GameSystem<input_t>::StepGame(double dt)
{
	//		mlock.lock();
	int simstepstaken = 0;
	if (!game.paused) {

		Resimulate();

		// stop simulating if too much input backlogged
		if (simInputRem >= 60)
			goto exit;

		// TODO if online modify dt by a timeslew coefficient
		// timeslew
		if (nctx != NET_BADVAL && conns.size() > 1 && !isHost) {
			dt *= timeslew.GetCoefficient(game.GetFrameCount());
		}

		// TODO
		// update local controller inputs
		simstepstaken = game.Step(dt / 1000.0); // expects dt in seconds
		SyncInputs(simstepstaken);

	} else if (game.doSingleStep) {
		game.doSingleStep = false;
		game.SingleStep();
		simstepstaken = 1;
	}
exit:
	//		mlock.unlock();
	return simstepstaken;
}

template <class input_t> void GameSystem<input_t>::PumpNetwork()
{

	double                                 dt        = 0.0;
	thread_local static gfx::steady_time_t prev_tick = gfx::GetCurrentTime();
	gfx::steady_time_t                     now       = gfx::GetCurrentTime();
	dt                                               = gfx::GetTimeElapsed(now, prev_tick);
	prev_tick                                        = now;

	// mlock.lock();
	// Pump network connections if they exist
	// TODO should pump at 20fps
	// TODO need more state to correctly track rate at which network is being pumped
	//	if game is paused or not running current design stops pumping messages
	//	over the network
	// TODO this may be a bad design
	thread_local static uint64_t lpump    = 1234;
	thread_local static double   netaccum = 0.0;
	thread_local static uint64_t lsnap    = 1234;

	if (net::IsInit() && nctx != NET_BADVAL) {
		netaccum += dt;

		if (!isHost) {
			timeslew.Update(dt);
			if (timeslew.NeedNewStamp()) {
				// TODO
				// replace with function that grabs host/server connection
				for (auto& c : conns) {
					if (c.netHandle) {
						printf("REQUESTING STAMP\n");
						ReqTimeStamp(c.netHandle);
					}
				}
			}
		}

		// have host roughly send a timestamp, crc and frame
		// every so often so clients can check if they are still in sync
		// and adjust simulation speed
		if (isHost && snapRec.HeadFrame() != lsnap) {
			lsnap = snapRec.HeadFrame();
			SendAllSync();
		}

		if (((game.GetFrameCount() != lpump && game.GetFrameCount() % 12 == 0)
		     || netaccum > 500.0)
		    && nctx) {
			netaccum = 0.0;
			lpump    = game.GetFrameCount();
			net::SendAndRecieve(nctx);
		} else {
			net::Recieve(nctx);
		}
		// TODO
		// I would like to send and recieve time stamp requests around here
		// with low latency
		if (isHost) {
			for (auto& c : conns) {
				if (c.rpc && c.rpc_unorder->needStamp) {
					c.rpc_unorder->needStamp = false;
					SendTimeStamp(c.rpc->needStampTime, c.netHandle);
				}
			}
		}
	} else {
		// if(snapRec.HeadFrame() != lsnap) {
		// 	lsnap = snapRec.HeadFrame();
		// 	timeslew.InsertCheckPoint(lsnap, snapRec.GetTime(lsnap));
		// }
	}
	//		mlock.unlock();
}
template <class input_t> void GameSystem<input_t>::SyncConn(net::CXN_h conn)
{
	if (!isHost)
		return;
	// TODO
	// send a pause rpc to all
	// net::SendAll(nctx, pausemessage, sizeof(pausemessage));

	// TODO
	// grab a newest snapshot, where state is certain across clients
	//

	// snap.frame = game.GetFrameCount();
	// snap.Reset();
	// game.SnapShot(snap.buff, snap.frame);
	int a = 0, b = 0;
	snap.crc = RollingHash(a, b, (unsigned char*)snap.buff.head(), snap.buff.unread());
	// TODO
	// encapsulate this whole behavior thing into some kind of serialization api ?
	// TODO
	// write msg type
	// frame
	// crc
	// size
	// then data chunks as they fit
	dataDesc_t     dtype       = RPCT::STATE;
	snapshotDesc_t sd          = { snap.frame, snap.crc, snap.uid, uint16_t(snap.buff.len()) };
	int            off         = 0;
	const int      minsize     = sizeof(dtype) + sizeof(sd);
	int            packetsSent = 0;
	while (off < snap.buff.len()) {
		if (net::GetSpace(nctx, conn) < minsize) {
			net::FlushConn(nctx, conn);
		}
		// send header

		int rem  = net::GetSpace(nctx, conn) - minsize;
		rem      = std::min(rem, int(snap.buff.len()) - off);
		sd.dsize = rem;

		net::Send(nctx, conn, &dtype, sizeof(dtype));
		net::Send(nctx, conn, &sd, sizeof(sd));
		net::Send(nctx, conn, (char*)snap.buff.head() + off, rem);

		off += rem;
		packetsSent++;
	}
	printf("SENT STATE OVER %d PACKETS\n", packetsSent);

	// notify all other clients of the new ip ?
	// send a resume rpc to all
}

} // namespace sim
