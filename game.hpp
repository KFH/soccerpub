#ifndef GAME_HPP
#define GAME_HPP
#include <cmath>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "idtags.hpp"
#include "buff.hpp"
#include "fpm.hpp"
//#include "gamecontrols.hpp"

#include <algorithm>

#define MAXPLAYERS 22
#define MAXUSERS 4

#define FIELD_DIVS 16
namespace sim {

class AbstractGame_t {
	size_t input_type_size;

    public:
	virtual int      Step(double dt, bool freeRate = true)       = 0;
	virtual uint64_t GetFrameCount() const                       = 0;
	virtual void     SetState(ByteBuffer& buff, uint64_t frame)  = 0;
	virtual void     SnapShot(ByteBuffer& buff, uint64_t& frame) = 0;
	size_t           getInputSize() const { return input_type_size; }
};

struct view_t {
	// center of view_t, render world relative to this
	fp32 x, y;
	int  w, h;
	v3   pos;
	v3   vel;
	int  screenX(fp32 px) const
	{
		float v(px - x);
		return roundf(v) + w / 2;
	}
	int screenY(fp32 py) const
	{
		float v(py - y);
		return roundf(v) + h / 2;
	}
	i3 screenXYZ(const v3& v) { return i3{ screenX(v.x), screenY(v.y), 0 }; }

	int screenX(int px) const { return px - roundf(float(x)) + w / 2; }
	int screenY(int py) const { return py - roundf(float(y)) + h / 2; }
	// return world coordinate of screen x y position
	int worldX(int sx) const { return roundf(float(x)) - w / 2 + sx; }
	int worldY(int sy) const { return roundf(float(y)) - h / 2 + sy; }
	// update x y position
	// and some velocity value
	v3   worldPos() const { return pos; }
	void moveTo(v3 npos)
	{
		vel = npos - pos;
		pos = npos;
		x   = pos.x;
		y   = pos.y;
	}
};

// probably not necessary
template <size_t byte_size> struct abstract_input_t {
	static constexpr size_t _size = byte_size;
	unsigned char           _data[byte_size];
};

template <typename input_t> struct abstract_input_override_t {
	playerIX player_id;
	entityID entity_id;
	input_t  ctrl;

	bool operator==(const playerIX& id) { return player_id == id; }
	bool operator==(const entityID& id) { return entity_id == id; }
};

template <typename input_t> class InputStream {
    protected:
	int id;

    public:
	virtual ~InputStream(){};
	// return some kind of entity id for override or something
	int  ID() const { return id; }
	void SetID(int i) { id = i; }

	// expected to set playerCtrl value and return estimate remaining in stream
	// if stream is exhausted playerCtrl need not be set return -1 or 0 ?
	virtual int Get(input_t& out)            = 0;
	virtual int GetConst(input_t& out) const = 0;

	// return estimate remaining, a network stream or local stream could just return 1
	virtual int Remaining() = 0;

	// free from head on recorded streams
	virtual void Free(int x){};

	// rewind reading of stream to head
	virtual void Rewind(){};
};

class BinaryInputRecorder {
    protected:
	size_t _type_size;

    public:
	virtual void push_back(const char* in) = 0;
	size_t       type_size() { return _type_size; };
};

template <typename input_t> class InputRecorder {
    public:
	virtual void push_back(const input_t& in) = 0;
};

template <typename input_t> class vInstance {

    public:
	typedef abstract_input_override_t<input_t> ctrlOverride_t;
	typedef std::vector<ctrlOverride_t>        input_vec_t;

	virtual int  Init()                            = 0;
	virtual void Serialize(ByteBuffer& buff) const = 0;
	virtual void DeSerialize(ByteBuffer& buff)     = 0;

	virtual void Step(int frameNum, input_vec_t& in) = 0;
};

template <typename input_t> class InputStreamExhaustionCB {
    public:
	// callback return 0 break 1 continue ?
	virtual int Callback(AbstractGame_t& me, InputStream<input_t>* stream) = 0;
};

class PhaseCallback {
    public:
	enum { NONE, PRETICK, POSTICK, PRERESIM, POSTRESIM };
	int          phase;
	virtual void Exec(AbstractGame_t& me) = 0;
};

template <typename input_t> class Game : public AbstractGame_t {
	static constexpr double fixedStep = 1.0 / 60.0;
	uint64_t                frameCount;

	typedef typename vInstance<input_t>::ctrlOverride_t ctrlOverride_t;

    public:
	vInstance<input_t>* inst; // IDEA use 2 vInstances and yingyang between each frame
	double              accumulator;
	InputStreamExhaustionCB<input_t>* m_cb;
	PhaseCallback*                    m_snap;
	std::vector<PhaseCallback*>       callbacks;

	bool paused       = false;
	bool doSingleStep = false;

    private:
	InputStream<input_t>* streams[MAXUSERS];
	// std::vector<	vInstance<input_t>::ctrlOverride_t > inputs;
	std::vector<ctrlOverride_t> inputs;

    public:
	Game(vInstance<input_t>* in)
	    : inst(in)
	{
		frameCount  = 0;
		accumulator = 0.0;
		inst->Init();
		m_cb   = nullptr;
		m_snap = nullptr;
		for (auto*& s : streams) {
			s = nullptr;
		}
	}
	void Reset()
	{
		frameCount  = 0;
		accumulator = 0.0;
		inst->Init();
	}
	void ForcePhase(int phase)
	{
		for (auto cb : callbacks) {
			if (cb->phase == phase)
				cb->Exec(*this);
		}
	}

	// advance current frame
	void SingleStep()
	{

		ForcePhase(PhaseCallback::PRETICK);

		inputs.resize(0);
		// TODO
		// for each stream extract input and create override ?
		for (auto* s : streams) {
			if (s) {
				ctrlOverride_t co = { s->ID(), 0 };
				s->Get(co.ctrl);
				inputs.push_back(co);
			}
		}
		inst->Step(frameCount, inputs);

		ForcePhase(PhaseCallback::POSTICK);

		if (m_snap)
			m_snap->Exec(*this);

		frameCount++;
	}
	// advance game based on time step and accumulator
	virtual int Step(double dt, bool freeRate = true) override
	{
		int steps = 0;
		if (freeRate) {

			accumulator += dt;
			if (accumulator > fixedStep) {
				steps = std::min(10.0, accumulator / fixedStep);
				accumulator -= (double)steps * fixedStep;
			}
		} else {
			steps = 1;
		}
		for (int i = 0; i < steps; i++) {
			// TODO
			// if stream exhaustion callback set and streams exhausted break
			// instead of completing loop, set accumulator to 0 ?
			if (m_cb) {
				for (auto* s : streams) {
					if (s && s->Remaining() < 1 && m_cb->Callback(*this, s) < 1)
						return i;
				}
			}
			SingleStep();
		}
		return steps;
	}

	double Tmult() const { return (1.0 - accumulator / fixedStep) * -1.0; }

	vInstance<input_t>& CurInst() { return *inst; };
	vInstance<input_t>& PrevInst() { return *inst; };

	template <typename T> T& CurInst() { return *(static_cast<T*>(inst)); }

	double GetAccumulator() { return accumulator; }

	int RunAhead(uint64_t frames)
	{
		// NOTE
		// coulde be used in conjuction with setstate to
		// resimulate game with new inputs
		for (uint64_t i = 0; i < frames; i++) {
			SingleStep();
		}
		return frames;
	}

	uint64_t GetFrameCount() const { return frameCount; }

	// Sets state and current frame for roll back ?
	// or resimulation ?
	virtual void SetState(ByteBuffer& buff, uint64_t frame) override
	{
		// TODO set inst based on frame modulo
		printf("SETTING STATE FRAME %lu\n", frame);
		frameCount  = frame;
		accumulator = 0.0;
		inst->DeSerialize(buff);
	}
	// serialize the current instance
	virtual void SnapShot(ByteBuffer& buff, uint64_t& frame) override
	{
		// TODO grab inst of two
		inst->Serialize(buff);
		frame = frameCount;
	}
	void SetInputStream(playerIX i, InputStream<input_t>* s)
	{
		if (i < 0 || i >= MAXUSERS)
			return;
		streams[i] = s;
	}
	InputStream<input_t>* GetInputStream(int i)
	{
		if (i < 0 || i >= MAXUSERS)
			return 0;
		return streams[i];
	}
	size_t GetStreamCount() const
	{
		size_t num = 0;
		for (auto s : streams) {
			if (s != nullptr)
				num++;
		}

		return num;
	}
};

} // namespace sim
#endif
