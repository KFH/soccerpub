#define NOMINMAX
#include <algorithm>
#include <chrono>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>
#include <windows.h>

#include "gfx.hpp"
#include "glrend.hpp"
#include "wkeys.hpp"

#include <windowsx.h>

namespace gdi {
#include <Wingdi.h>
}
// TODO
// should probably set some kind of defs to use wchar_t strings in winapi calls?
// or does it even matter?

// v-sync notes
// https://www.codeproject.com/Articles/4436/Tearing-free-drawing-with-GDI
// https://www.compuphase.com/vretrace.htm
// http://andesengineering.com/OSG_ProducerArticles/Synchronization/index.html
// https://gist.github.com/anonymous/4397e4909c524c939bee
// https://msdn.microsoft.com/en-us/library/windows/hardware/ff548441(v=vs.85).aspx

// TODO
// implement clipboard pasting for windows
// https://msdn.microsoft.com/en-us/library/windows/desktop/ms648709(v=vs.85).aspx

namespace gfx {

bool keyHeld[512];
int  xres     = 640;
int  yres     = 480;
int  xres_win = xres;
int  yres_win = yres;

// windows specific crap
HWND             hwnd;
HDC              hdc;
HGLRC            hwglc;
HBITMAP          hbmp;
BITMAPINFO       bmi;
HINSTANCE        hInstance;
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

enum { RENDERMODE_NONE, RENDERMODE_WGL, RENDERMODE_GDI };

int   renderMode = RENDERMODE_WGL;
void* temp;

keyboardCallback keyboardCB;
mouseCallback    mouseCB;
pasteCallback    pasteCB;

mouse_state_t main_mouse_state;

#include <GL/GL.h>
typedef bool (*PFNWGLSWAPINTERVALEXTPROC)(int inverval);
PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT;
// https://msdn.microsoft.com/en-us/library/windows/desktop/dd374293(v=vs.85).aspx
// https://msdn.microsoft.com/en-us/library/windows/desktop/dd374204(v=vs.85).aspx

void SetPalette(uint32_t* pal)
{
	if (renderMode == RENDERMODE_WGL)
		ogl::SetPalette(pal, 32);
}

int InitWGL()
{

	PIXELFORMATDESCRIPTOR pfd
	    = { sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, // Flags
		PFD_TYPE_RGBA, // The kind of framebuffer. RGBA or palette.
		32, // Colordepth of the framebuffer.
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		24, // Number of bits for the depthbuffer
		8, // Number of bits for the stencilbuffer
		0, // Number of Aux buffers in the framebuffer.
		PFD_MAIN_PLANE,
		0,
		0,
		0,
		0 };

	int letWindowsChooseThisPixelFormat;
	letWindowsChooseThisPixelFormat = ChoosePixelFormat(hdc, &pfd);
	SetPixelFormat(hdc, letWindowsChooseThisPixelFormat, &pfd);

	// wglCreateContextAttribsARB();
	hwglc = wglCreateContext(hdc);
	if (hwglc == NULL) {
		printf("failed to create wgl context\n");
		return 0;
	}

	wglMakeCurrent(hdc, hwglc);

	if (!ogl::Init()) {
		printf("failed to initialized ogl\n");
		return 0;
	}

	(void*&)wglSwapIntervalEXT = wglGetProcAddress("wglSwapIntervalEXT");
	// wglSwapIntervalEXT =
	// reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(wglGetProcAddress("wglSwapIntervalEXT"));
	if (wglSwapIntervalEXT == nullptr) {
		printf("WGL failed to get SwapInterval Proc Address\n");
		return 0;
	}
	// wtf
	// wglSwapIntervalEXT(1);
	printf("WGL initialized\n");
	ogl::PrintGLinfo();

	return 1;
}

int ExitWGL()
{
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hwglc);
	return 1;
}

int Init()
{

	initKeycodeTable();
	(void*&)keyboardCB = nullptr;
	(void*&)mouseCB    = nullptr;
	(void*&)pasteCB    = nullptr;
	main_mouse_state   = {};

	hInstance = GetModuleHandle(0);

	// const wchar_t CLASS_NAME[] = L"Sample Window Class";
	const char CLASS_NAME[] = "Mi Window Class";
	WNDCLASS   wc           = {};
	wc.lpfnWndProc          = WindowProc;
	wc.hInstance            = hInstance;
	wc.lpszClassName        = CLASS_NAME;
	wc.style                = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	if (!RegisterClass(&wc)) {
		fprintf(stderr, "RegisterClass failed: %lu\n", GetLastError());
		return 0;
	}

	hwnd = CreateWindow(CLASS_NAME, "soccer", WS_OVERLAPPEDWINDOW, 10, 10, xres, yres, NULL,
	                    NULL, hInstance, NULL);
	if (hwnd == NULL) {
		fprintf(stderr, "Failed to create window\n");
		return 0;
	}

	ShowWindow(hwnd, SW_SHOW);

	memset(keyHeld, 0, sizeof(keyHeld));

	/// --- setup a 32bit surface for drawing

	memset(&bmi, 0, sizeof(bmi));
	bmi.bmiHeader.biSize        = sizeof(bmi);
	bmi.bmiHeader.biWidth       = xres;
	bmi.bmiHeader.biHeight      = -yres; // Order pixels from top to bottom
	bmi.bmiHeader.biPlanes      = 1;
	bmi.bmiHeader.biBitCount    = 32; // last byte not used, 32 bit for alignment
	bmi.bmiHeader.biCompression = BI_RGB;

	hdc = GetDC(hwnd);
	// Create DIB section to always give direct access to pixels

	if (InitWGL()) {
		temp = malloc(xres * yres * sizeof(uint32_t));
	} else {
		renderMode = RENDERMODE_GDI;
		hbmp       = CreateDIBSection(hdc, &bmi, DIB_RGB_COLORS, &temp, NULL, 0);
		ReleaseDC(hwnd, hdc);
	}

	return 1;
}
void Exit()
{
	// TODO
	// free platform specific resources etc
	DestroyWindow(hwnd);
}

int needQuit = 0;
int NeedExit() { return needQuit; }

void GetCurrentBuffer(void** raw32, int* width, int* height)
{
	*raw32  = temp;
	*width  = xres;
	*height = yres;
}

LRESULT CALLBACK WindowProc(HWND h, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	// trying to translate from windows key to a platform independant keycode
	int k = keycodes[HIWORD(lParam) & 0x01FF];

	switch (uMsg) {
	case WM_CREATE:
		printf("Created Window\n");
		break;
	case WM_SIZE: {
		// int width = LOWORD(lParam);  // Macro to get the low-order word.
		// int height = HIWORD(lParam); // Macro to get the high-order word.
		// // Respond to the message:
		// OnSize(hwnd, (UINT)wParam, width, height);
		xres_win = LOWORD(lParam);
		yres_win = HIWORD(lParam);
		printf("RESIZED %dx%d\n", xres_win, yres_win);
	} break;
	case WM_CLOSE:
	case WM_QUIT:
		needQuit = 1;
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		keyHeld[k] = true;
		if (keyboardCB)
			keyboardCB(k, KEY_EVENT_PRESS, nullptr);
		// printf("KEYDOWN %d\n", k);
		break;
	case WM_KEYUP:
	case WM_SYSKEYUP:
		keyHeld[k] = false;
		if (keyboardCB)
			keyboardCB(k, KEY_EVENT_RELEASE, nullptr);
		// printf("KEYUP %d\n", k);
		break;
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms646242(v=vs.85).aspx
	case WM_MOUSEMOVE:
		if (mouseCB)
			mouseCB(MouseAction_t::MOVE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam),
			        MouseButtons_t::NONE);
		break;
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		if (mouseCB)
			mouseCB(MouseAction_t::PRESS, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam),
			        uMsg == WM_LBUTTONDOWN ? MouseButtons_t::LEFT
			                               : MouseButtons_t::RIGHT);
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		if (mouseCB)
			mouseCB(MouseAction_t::RELEASE, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam),
			        uMsg == WM_LBUTTONUP ? MouseButtons_t::LEFT
			                             : MouseButtons_t::RIGHT);
		break;
	default:
		break;
	}
	return DefWindowProc(h, uMsg, wParam, lParam);
}

void PollEvents()
{
	MSG msg;
	while (PeekMessage(&msg, hwnd, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

BOOLEAN nanosleep(LONGLONG ns)
{
	/* Declarations */
	HANDLE        timer; /* Timer handle */
	LARGE_INTEGER li; /* Time defintion */
	/* Create timer */
	if (!(timer = CreateWaitableTimer(NULL, TRUE, NULL)))
		return FALSE;
	/* Set timer properties */
	li.QuadPart = -ns;
	if (!SetWaitableTimer(timer, &li, 0, NULL, NULL, FALSE)) {
		CloseHandle(timer);
		return FALSE;
	}
	/* Start & wait for timer */
	WaitForSingleObject(timer, INFINITE);
	/* Clean resources */
	CloseHandle(timer);
	/* Slept without problems */
	return TRUE;
}
} // namespace gfx
void _swap() { SwapBuffers(gfx::hdc); }
namespace gfx {
void SwapBuffers()
{

	PollEvents();

	switch (renderMode) {

	case RENDERMODE_WGL:
		ogl::Draw(temp, xres, yres, xres_win, yres_win);
		_swap();
		// wglSwapLayerBuffers(hdc,WGL_SWAP_MAIN_PLANE);
		//
		// BOOL res = SwapBuffers(hwglc);

		break;
	case RENDERMODE_GDI: {
		// sleep has ms accuracy ?
		HDC hdc = GetDC(hwnd);
		SetDIBitsToDevice(hdc, 0, 0, xres, yres, 0, 0, 0, yres, temp, &bmi, DIB_RGB_COLORS);
		GdiFlush();
		ReleaseDC(hwnd, hdc);
	}
	case RENDERMODE_NONE: {
		double ct        = GetTimeElapsed();
		double targetFPS = 16.0;
		double ms        = 1e+4;
		int    sleepms   = std::max(0.0, targetFPS - ct);
		nanosleep(sleepms * ms);
		// TODO/NOTE
		// Sleep uses ms units but seems oscilate between 16ms - 33ms intervals, not
		// the single ms precision i want.. ideally i need a vsync mechanism.
		// Sleep(sleepms);
	}
	};
}
const mouse_state_t& GetMouseState() { return main_mouse_state; }
const bool*          GetKeyStates() { return keyHeld; }
/*
using milli = std::chrono::duration<double, std::milli>;
std::chrono::steady_clock::time_point initTime;
void ResetTimer()
{
        // store for comparison global and not thread safe
        initTime = std::chrono::steady_clock::now();
};
// return time elapsed in milliseconds
double GetTimeElapsed()
{
        auto now = std::chrono::steady_clock::now();
        return milli(now - initTime).count();
};
*/
// TODO fill this shit out
void SetKBCallback(keyboardCallback kbcb) { keyboardCB = kbcb; }
void SetMouseCallback(mouseCallback mcb) { mouseCB = mcb; }
void SetPasteCallback(pasteCallback pcb) { pasteCB = pcb; }
void GetScreenDim(int& width, int& height)
{
	width  = xres_win;
	height = yres_win;
}

} // namespace gfx
