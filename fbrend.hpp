
#include <stdint.h>

// TODO
// put frame buffer specific code here
namespace fb {

int  Init();
void Exit();
void GetCurrentBuffer(uint32_t** raw32, int* width, int* height);
void SwapBuffers();
}; // namespace fb
