
#include <algorithm>

#include <cmath>
#include <mutex>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "game.hpp"

#include "gamecontrols.hpp"

#include "gamerend.hpp"

#include "render.hpp"

#include "gfx.hpp"
#include "keys.hpp"
#include "netapi.hpp"

#include "localcontrols.hpp"
#include "snap.hpp"
#include "system.hpp"
#include "threadwork.hpp"
#include "interpolate.hpp"

//#include "menu.hpp"

/// --- game specific types

struct player_ctrl_t {
	float   x;
	float   y;
	uint8_t action;
};

class PlayerControllerStream : public sim::InputStream<player_ctrl_t> {
    public:
	virtual int Get(player_ctrl_t& out) override
	{

		// ihdl.Keyboard(gfx::GetKeyStates());
		//// TODO
		//// ihdl.Joystick();
		// ihdl.ApplyInputs();
		// out = ihdl.GetInput();
		return 1;
	};
	virtual int GetConst(player_ctrl_t& out) const override
	{
		// out = ihdl.GetInput();
		return 1;
	}

	virtual int Remaining() override
	{
		// always return 1
		return 1;
	};
	PlayerControllerStream(int uid)
	{
		this->id = uid;
		// whatever
	};
};

class empty_instance : public sim::vInstance<player_ctrl_t> {
    public:
	virtual int  Init() override { return 0; };
	virtual void Step(int frameNum, empty_instance::input_vec_t& in) override{

	};
	virtual void Serialize(ByteBuffer& buff) const override{};
	virtual void DeSerialize(ByteBuffer& buff) override{};
};

/// --- globals
tw::threadPool_t               threadPool;
empty_instance                 mainInst;
sim::Game<player_ctrl_t>       mainGame(&mainInst);
sim::GameSystem<player_ctrl_t> gs(mainGame);
SnapRecorder*                  snapRec = &gs.snapRec;
sim::view_t                    mainView;

/*
/// --- callbacks
void DrawGame(renderContext& ctx,
                sim::view_t& mainView,
                sim::AbstractGame_t& mainGame
                ) {
        // TODO  Draw game
        ctx.blank(0);
}
*/
void keyboardEvent(int keycode, int event, const char* str) {}
void mouseEvent(gfx::MouseAction_t act, int x, int y, gfx::MouseButtons_t btn) {}
void pasteEvent(char* buff, int len) { printf("PASTED: %.*s\n", len, buff); }

int main(int argc, char* argv[])
{

	/// --- GRAPHICS INIT
	if (!gfx::Init())
		return -1;
	gfx::SetKBCallback(keyboardEvent);
	gfx::SetMouseCallback(mouseEvent);
	gfx::SetPasteCallback(pasteEvent);

	dst_t         screen = {};
	renderContext ctx;
	palette_t     newpalette
	    = { { 0x00000000, 0xff000000, 0xff25009e, 0xffae0083, 0xffff1600, 0xff193ba9,
		  0xff5031c5, 0xffa04000, 0xff534b01, 0xff1ea95c, 0xff145d96, 0xff119464,
		  0xff545753, 0xff7f61df, 0xffffffff, 0xff5486da, 0xff7c8e28, 0xffff6767,
		  0xfff55fd4, 0xff8e918d, 0xfffd8739, 0xff50b48d, 0xffbb94fc, 0xff84b85a,
		  0xffd8ac4e, 0xffd6b638, 0xff86befe, 0xffced2cc, 0xff87f3b7, 0xffffd86f,
		  0xffb5f189, 0xfffffd45 } };
	ctx.palette = newpalette;
	ctx.palette.GenNames();
	gfx::SetPalette(newpalette.color);

	/// --- THREADPOOL INIT
	int pool_size = std::min(8, std::max(4, (int)std::thread::hardware_concurrency() - 1));
	threadPool.CreatePool(pool_size);
	std::vector<renderJob> jobs;

	while (!gfx::NeedExit()) {

		gfx::SwapBuffers();
		// gfx::GetCurrentBuffer((void**)&screen.raw32, &screen.width, &screen.height);

		// DrawGame(ctx, mainView, mainGame);

		/// --- RENDERS/BLIT out all the main sprites
		int numjobs = std::max(2, std::min(12, pool_size - 2));
		jobs.resize(0);
		FillJobList(ctx, numjobs, jobs);
		for (renderJob& rj : jobs) {
			// rj.Exec();
			threadPool.SubJob(&rj, 0, 1);
		}
		threadPool.WaitBatch(1);
	}
	threadPool.Join();

	return 0;
}
