#pragma once
// TODO/IDEA
// main usage pattern for threads is MOSTLY to go very wide then narrow to 1 thread of execution
// eg single thread gameloop -> many thread render frame -> single thread gameloop
// 2nd usage pattern is long running side task, dns resolution and disk io ?
// 3rd sound probably runs for complete duration on seperate thread and communicates through some
// sync method
// only main thread should create jobs/tasks
#include <atomic>
#include <thread>

#if defined(_WIN32)

// std thread is platform dependant and mingw doesnt include a std thread fillin
//#include <mingw.mutex.h>
//#include <mingw.thread.h>
// namespace std {
// using namespace mingw_stdthread;
//}
#endif

//#include <concurrentqueue.h>
#include "job.hpp"
#include <blockingconcurrentqueue.h>

namespace tw {

struct jobDesc_t {
	job_t*  job;
	uint8_t flags;
	uint8_t batchID;
};

enum {
	// flags for handling a job
	LOOP    = 0x01,
	DESTROY = 0x02,
	NOYIELD = 0x04,
	// batches?
	INIT    = 0,
	GAME    = 1,
	AUDIO   = 2,
	RENDER  = 3,
	NETWORK = 4,
};
class threadPool_t {

	class thread_t {
	    public:
		threadPool_t* pool;
		std::thread   t;
		int           id;

		void run()
		{

			jobDesc_t jd;
			while (pool->quit != true) {
				if (pool->pendingjobs.wait_dequeue_timed(
				        jd, std::chrono::milliseconds(16))) {
					// exec job one or in a loop
					if (jd.flags & LOOP) {
						while (pool->quit != true) {
							jd.job->Exec();
							if (!(jd.flags & NOYIELD)) {
								std::this_thread::yield();
							}
						}
					} else {
						jd.job->Exec();
					}

					if (jd.flags & DESTROY)
						delete jd.job;

					pool->batches[jd.batchID]--;
				} else {
					// std::this_thread::sleep_for(std::chrono::milliseconds(2));
					std::this_thread::yield();
				}
			}
		}

		thread_t(threadPool_t* p)
		    : pool(p)
		    , t([&] { run(); })
		{

			// id = t.get_id();
		}
	};

	std::vector<thread_t*> pool;
	// moodycamel::ConcurrentQueue<jobDesc_t> pendingjobs;
	moodycamel::BlockingConcurrentQueue<jobDesc_t> pendingjobs;

	// sure atomic operations to modify these

	std::atomic<bool> quit;
	std::atomic<int>  batches[256]; // int = to threads actively working on batch job

    public:
	threadPool_t() {}

	void CreatePool(int pool_size)
	{
		for (int i = 0; i < pool_size; i++) {
			pool.emplace_back(new thread_t(this));
		}
		printf("THREAD POOL CREATED %d\n", pool_size);
	}

	int SubJob(job_t* job, uint8_t flags, uint8_t batchID)
	{
		// TODO
		// put some flags on jobs like, long running? repeat? no interupting ?
		batches[batchID]++;
		pendingjobs.enqueue(jobDesc_t{ job, flags, batchID });

		return 0;
	}

	int WaitBatch(int batchID)
	{
		// block until some batch of jobs is complete?
		while (batches[batchID] != 0) {
			// sleep ?
			std::this_thread::yield();
		}
		return 0;
	}

	int CheckBatch(int batchID) { return batches[batchID]; }

	void Join()
	{
		quit = true;
		for (auto& thread : pool) {
			thread->t.join();
		}
		pool.resize(0);
		quit = false;
		printf("THREAD POOL JOINED\n");
	}
};
} // namespace tw
