
#include <chrono>
namespace gfx {
using milli = std::chrono::duration<double, std::milli>;
std::chrono::steady_clock::time_point initTime;
void                                  ResetTimer() { initTime = std::chrono::steady_clock::now(); };
// return time elapsed in milliseconds

typedef std::chrono::steady_clock::time_point steady_time_t;
steady_time_t GetCurrentTime() { return std::chrono::steady_clock::now(); };
double        GetTimeElapsed(const steady_time_t& now, const steady_time_t& then)
{
	return milli(now - then).count();
}

double GetTimeElapsed()
{
	auto now = std::chrono::steady_clock::now();
	return milli(now - initTime).count();
}
} // namespace gfx
