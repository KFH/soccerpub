
#include "fbrend.hpp"
namespace gfx {
#include <stdio.h>
#include <string.h>

#ifdef __linux__
#include <fcntl.h>
#include <linux/fb.h>
#include <linux/i2c.h>
#include <linux/types.h>
#include <stropts.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#endif

int               fb_fd;
uint8_t*          fbp;
long              screensize;
fb_var_screeninfo vinfo;
fb_fix_screeninfo finfo;

void printvinfo(fb_var_screeninfo& vin)
{
	printf("res %ux%u virt %ux%u grayscale %u bits %u sync %u\n", vin.xres, vinfo.yres,
	       vin.xres_virtual, vinfo.yres_virtual, vin.grayscale, vin.bits_per_pixel, vin.sync);
}

void setVinfo(fb_var_screeninfo& vin)
{
	int rc = ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vin);
	if (rc == -1) {
		fprintf(stderr, "VAR INFO SETTING FAILED\n");
		printvinfo(vin);
	}
	printf("VINFO RC %d\n", rc);
	ioctl(fb_fd, FBIOGET_VSCREENINFO, &vin);
}

int internalSwapBuffers()
{
	// FBIOGET_VBLANK
	return ioctl(fb_fd, FBIO_WAITFORVSYNC, 0);
}

int Init()
{
	fb_fd = open("/dev/fb0", O_RDWR);
	if (!fb_fd) {
		fprintf(stderr, "Failed to open FrameBuffer\n");
		return -1;
	}
	// Get screen information
	ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);
	ioctl(fb_fd, FBIOGET_FSCREENINFO, &finfo);

	printf("MEM %u TYPE %u AUX %u VIS %u\nXPAN %hu YPAN %hu YWRAP %hu\nLINE %u\n",
	       finfo.smem_len, finfo.type, finfo.type_aux, finfo.visual, finfo.xpanstep,
	       finfo.ypanstep, finfo.ywrapstep, finfo.line_length);

	printvinfo(vinfo);

	// TODO
	// mmap double the resolution for double buffering
	// by doubling the buffers height
	// and then using panning
	// vinfo.grayscale=0;
	// vinfo.bits_per_pixel=32;
	// vinfo.yres_virtual = vinfo.yres_virtual * 2;
	// ioctl(fb_fd, FBIOPUT_VSCREENINFO, &vinfo);
	// ioctl(fb_fd, FBIOGET_VSCREENINFO, &vinfo);
	// printvinfo();

	vinfo.yres         = vinfo.yres * 2;
	vinfo.yres_virtual = vinfo.yres_virtual * 2;
	// vinfo.sync = FB_SYNC_VERT_HIGH_ACT;
	setVinfo(vinfo);

	printvinfo(vinfo);

	screensize = vinfo.yres_virtual * finfo.line_length;
	fbp = (uint8_t*)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fb_fd, (off_t)0);

	int rc = internalSwapBuffers();
	if (!rc)
		fprintf(stderr, "VSYNC FB UNSUPPORTED\n");
	// TODO get rid of this
	// set to middle grey so we know its working
	memset(fbp, 125, screensize);

	return 1;
}

void Exit() { close(fb_fd); }
// run for 3 secs of frames
int fcount_max = 60 * 3;
int fcount     = 0;
int NeedExit()
{
	fcount++;
	return fcount > fcount_max;
};
// TODO
// if panning and double buffering supported return pointer here
void GetCurrentBuffer(uint32_t** raw32, int* width, int* height)
{
	*raw32  = (uint32_t*)fbp;
	*width  = vinfo.xres;
	*height = vinfo.yres;
}

void SwapBuffers() { internalSwapBuffers(); }
} // namespace gfx
