#pragma once
#include <chrono>

// disgusting
class timeSlewer {
	using milli_int = std::chrono::duration<int64_t, std::milli>;
	// using millPointCast = std::chrono::time_point_cast<std::chrono::milliseconds>;
	using milli_dub  = std::chrono::duration<double, std::milli>;
	using milPoint_t = std::chrono::time_point<std::chrono::steady_clock, milli_int>;
	using milPoint_d = std::chrono::time_point<std::chrono::steady_clock, milli_dub>;

	static inline milPoint_t Now()
	{
		auto t = std::chrono::steady_clock::now();
		return std::chrono::time_point_cast<std::chrono::milliseconds>(t);
	}

	milPoint_t reference;

	milli_dub refDelta;

	milPoint_t refPoint;
	uint64_t   refFrame;

	double accum;

    public:
	timeSlewer() { Reset(); }

	void Reset()
	{
		accum     = 5001.0;
		refFrame  = 0;
		refDelta  = milli_dub(0.0);
		refPoint  = Now();
		reference = Now();
	}
	void Update(double dt) { accum += dt; }
	bool NeedNewStamp()
	{
		bool b = accum >= 5000.0;
		if (b)
			accum = 0.0;
		return b;
	}
	static uint64_t GetTimeInt()
	{
		// get some kind of local time units to send to clients
		milPoint_t t = Now();
		return t.time_since_epoch().count();
	}
	void SetRef(uint64_t newRefTime, double latency)
	{
		// set the reference clock of the server
		reference     = milPoint_t(milli_int(newRefTime));
		milPoint_t t2 = Now();
		refDelta      = (reference - t2); // + milli_dub(latency * 0.5);

		// printf("\nTIME DELTA %f\n", refDelta.count());
		// printf("REF %lu\nNEW %lu\nLOC %ld\n\n", reference.time_since_epoch().count(),
		//        newRefTime, t2.time_since_epoch().count());
	}
	uint64_t GetRef() const
	{
		//
		return reference.time_since_epoch().count();
	}

	void InsertCheckPoint(uint64_t frame, uint64_t time)
	{
		refFrame = frame;
		refPoint = milPoint_t(milli_int(time));
	}

	double GetTimeDiff(uint64_t cframe) const
	{

		milPoint_d now  = Now();
		milPoint_d then = refPoint - refDelta;

		double     fd            = (int64_t)cframe - (int64_t)refFrame;
		double     msPerFrame    = 1000.0 / 60.0;
		milPoint_d projectedTime = (then) + milli_dub(fd * msPerFrame);

		/*
		printf("FRAMES %d %d %f\n", cframe, refFrame, fd);
		printf("LTIME %f\nRTIME %f DIFF %f\n\n",
		        milli_dub(now.time_since_epoch()).count(),
		        milli_dub(then.time_since_epoch()).count(),
		        milli_dub(then - now).count());
		*/

		return milli_dub(projectedTime - now).count();
	}

	double GetCoefficient(uint64_t cframe) const
	{

		double msPerFrame = 16.0;

		double co = GetTimeDiff(cframe) / (msPerFrame * 30.0);

		// TODO/IDEA apply some kind of curve to coefficient
		// if co = 0 scale to 1
		// if co > 0 scale towards 0
		// if co < 0 scale towards +infinity
		// make it fairly flat for +- 16ms differences

		// make polynomial that grows toward -+infinity in both dir
		// and intersects at 0,1
		// (-x^3) + 1
		// co = -co * co/2.0 * co/4.0 + 1.0;
		co = co * -co / 2.0 * co / 6.0 - co / 6.0 + 1.0;

		return std::min(2.0, std::max(0.5, co));
	}
};
