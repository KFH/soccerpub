#pragma once
#include "game.hpp"
#include "render.hpp"

void InitVis();

void DrawGame(renderContext& ctx, sim::view_t& mainView, sim::AbstractGame_t& mainGame,
              Messanger& msger, sim::playerIX LocalPlayerIX);
