#pragma once
namespace tw {

// abstract job type
class job_t {
    public:
	virtual ~job_t(){};
	virtual void Exec() = 0;
};
} // namespace tw
