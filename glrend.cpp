
#include <glad/glad.h>
#include <stdio.h>
#include <stdlib.h>
#include "resource.hpp"

namespace ogl {
void PrintGLinfo()
{
	printf("RENDER %s\n", glGetString(GL_RENDERER));
	printf("OPENGL %s\n", glGetString(GL_VERSION));
	printf("GLSL %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
}

#define ARRSIZE(x) sizeof(x) / sizeof(x[0])

GLuint fpal_h = 0;
GLuint ftex_h = 0;
GLuint vs_h   = 0;
GLuint fs_h   = 0;
GLuint prog_h = 0;

GLint u_pic_res_h = 3;
GLint u_win_res_h = 4;

int pic_width  = 640;
int pic_height = 480;

GLuint fbo = 0;
GLuint rbo = 0;

GLint checkShader(GLuint s)
{
	GLint status = 0, InfoLogLength = 0;
	char* shaderErrorMessage;
	glGetShaderiv(s, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {

		glGetShaderiv(s, GL_INFO_LOG_LENGTH, &InfoLogLength);
		shaderErrorMessage = (char*)malloc((InfoLogLength) * sizeof(char));
		glGetShaderInfoLog(s, InfoLogLength, NULL, shaderErrorMessage);
		fprintf(stderr, "SHADER FAILED: %.*s\n", InfoLogLength, shaderErrorMessage);
		free(shaderErrorMessage);
	} else {
		printf("SHADER SUCCESS\n");
	}
	return status;
}

void SetPalette(void* palette, int psize)
{
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, fpal_h);
	// glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, palette);

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 32, 1, GL_RGBA, GL_UNSIGNED_BYTE, palette);
}

int Init()
{
	gladLoadGL();
	// TODO
	// make some buffers compile some shader here
	auto& shaderz = get_resource_manager("shaderz");

	printf("VS SOURCE:%s\n", shaderz.get("vs_src.glsl").c_str());
	printf("FS SOURCE:%s\n", shaderz.get("fs_src.glsl").c_str());

	const GLchar* glsl_src = { 0 };
	std::string   src;
	GLint         glsl_src_length = 0;
	vs_h                          = glCreateShader(GL_VERTEX_SHADER);
	src                           = shaderz.get("vs_src.glsl");
	glsl_src                      = src.c_str();
	glShaderSource(vs_h, 1, &glsl_src, nullptr);
	glCompileShader(vs_h);
	if (checkShader(vs_h) == GL_FALSE) {
		return 0;
	}

	fs_h     = glCreateShader(GL_FRAGMENT_SHADER);
	src      = shaderz.get("fs_src.glsl");
	glsl_src = src.c_str();
	glShaderSource(fs_h, 1, &glsl_src, nullptr);
	glCompileShader(fs_h);
	if (checkShader(fs_h) == GL_FALSE) {
		return 0;
	};

	prog_h = glCreateProgram();
	glAttachShader(prog_h, vs_h);
	glAttachShader(prog_h, fs_h);
	glLinkProgram(prog_h);
	glUseProgram(prog_h);
	GLint linkStatus = 0;
	glGetProgramiv(prog_h, GL_LINK_STATUS, &linkStatus);

	if (linkStatus == GL_FALSE) {
		fprintf(stderr, "failed to link program\n");
		return 0;
	}

	u_pic_res_h = glGetUniformLocation(prog_h, "pic_res");
	u_win_res_h = glGetUniformLocation(prog_h, "win_res");

	// 4.1 feature
	// glProgramUniform1i(prog_h, glGetUniformLocation(prog_h, "tex"), 0);
	// 3.0 es way
	GLint texloc = glGetUniformLocation(prog_h, "tex");
	glUniform1i(texloc, 0);

	glGenTextures(1, &ftex_h);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ftex_h);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, pic_width, pic_height, 0, GL_RED, GL_UNSIGNED_BYTE,
	             NULL);

	texloc = glGetUniformLocation(prog_h, "pal");
	glUniform1i(texloc, 1);

	glGenTextures(1, &fpal_h);
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, fpal_h);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	// glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 32, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB8, 640, 480);

	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rbo);

	GLuint status = glGetError();
	while (status != GL_NO_ERROR) {
		printf("GL_ERROR = %d\n", status);
		status = glGetError();
	}
	// glBindTextureUnit(0, ftex_h);

	printf("OpenGL successfully initialized\n");
	return 1;
}

void Draw(void* pic, int pw, int ph, int w, int h)
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, w, h);
	glClearColor(1.0, 0.0, 0.1, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	glUniform2f(u_pic_res_h, float(pw), float(ph));
	glUniform2f(u_win_res_h, float(w), float(h));

	int ox = 0, oy = 0;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ftex_h);

	if (pw != pic_width || ph != pic_height) {
		// TODO rebuild allocate gl image to match dimensions
		pic_width  = pw;
		pic_height = ph;
		printf("NEW IMAGE %dx%d\n", pic_width, pic_height);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, pic_width, pic_height, 0, GL_RED,
		             GL_UNSIGNED_BYTE, pic);
	} else {
		glTexSubImage2D(GL_TEXTURE_2D, 0, ox, oy, pw, ph, GL_RED, GL_UNSIGNED_BYTE, pic);
	}
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
	// glBlitFramebuffer(0, 0, 640, 480, 0, 0, w, h, GL_COLOR_BUFFER_BIT, GL_LINEAR);

	/*
	glViewport(0, 0, w, h);
	glClearColor(1.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	int ox = 0, oy = 0;
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ftex_h);
	glTexSubImage2D(GL_TEXTURE_2D, 0, ox, oy, pw, ph, GL_RGBA, GL_UNSIGNED_BYTE, pic);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	*/
	GLuint status = glGetError();
	while (status != GL_NO_ERROR) {
		printf("GL_ERROR = %d\n", status);
		status = glGetError();
	}
}
} // namespace ogl
