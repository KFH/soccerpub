#pragma once
#include <algorithm>
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <vector>
#include <chrono>

#include "socket.hpp"
#include "buff.hpp"
#include "netapi.hpp"
#include "random.hpp"

namespace net {
extern char* PublicPunchAddr;

using milli = std::chrono::duration<double, std::milli>;
typedef std::chrono::steady_clock::time_point netTime_t;

#pragma pack(push, 1)
struct message {
	uint16_t proto; // = MAGICNUM
	uint16_t seq; // sequence number
	uint16_t ack; // current ack
	uint16_t exp_size; // expected size of message
	uint64_t ack_bits; // previous acks bits

	// void*    _datums;
	// assume remaining data in msg is THE data
	unsigned char* data() const { return (unsigned char*)this + sizeof(message); }
};

struct submessage {
	uint16_t      proto;
	uint16_t      seq;
	bool          ready;
	size_t        data_s;
	unsigned char data[MAXPACKET_S];
};

#pragma pack(pop)

struct rawPacket {
	Address addr;
	size_t  data_s;
	union {
		unsigned char data[MAXPACKET_S];
		message       msg;
	};

	bool     isFragmented() { return msg.proto == FRAGNUM; }
	uint16_t getSeq() { return msg.seq; }

	size_t size() { return std::max(data_s, sizeof(message)); }
	void*  tail()
	{
		// bad design but if data_s is greater than 0 assume
		// it includes size of message
		size_t ofs = data_s > 0 ? std::max(0, (int)data_s - (int)sizeof(message)) : 0;
		return msg.data() + ofs;
	}
	void append(const void* data, size_t len)
	{
		memcpy(tail(), data, len);
		data_s = size() + len;
	}
};

class Connection;

// contexts wrap a port and buffers incoming recieved packets?
// somehow these packets are distributed to connections or dumped if
// unneeded. Consider a new name for this context doesnt feel right
class Context {
    public:
	static constexpr int ctxBuffMax = BUFFERSIZE;
	Socket               msock;
	unsigned short       mport;
	rawPacket            recvbuff[ctxBuffMax];
	int                  head;
	int                  tail;
	Connection*          conns[NET_MAXCONNECTIONS];
	uint64_t pumpcount; // use to vary rate of certain actions, eg announce every 10 pumps

	bool    punch;
	Address punchAddr;

	bool    publicAddr_known;
	Address publicAddr;

	ConnectionChangeCB OpenCB;
	ConnectionChangeCB CloseCB;

	Context(unsigned short port)
	    : msock()
	    , conns()
	{
		mport = port;
		msock.Open(AF_INET, mport);
		head      = 0;
		tail      = 0;
		pumpcount = 0;
		punchAddr.SetStrIPv4(PublicPunchAddr);
	}

	int  OpenConnections();
	void RemoveConnection(Connection* c);
	void AddConnection(Connection* c);

	void Free();
	void Cleanup();
	int  Recieve();
	void ProcessRecv();

	int Send();

	void Announce();

	typedef rawPacket*       iterator;
	typedef const rawPacket* const_iterator;
	// iterators for range based for loop
	iterator       begin() { return &recvbuff[head]; }
	const_iterator begin() const { return &recvbuff[head]; }
	iterator       end() { return &recvbuff[tail]; }
	const_iterator end() const { return &recvbuff[tail]; }

	void TryOpenCB(Connection* c)
	{
		if (OpenCB)
			OpenCB(this, c);
	}
	void TryCloseCB(Connection* c)
	{
		if (CloseCB)
			CloseCB(this, c);
	}
};

inline bool seq_greater_than(uint16_t s1, uint16_t s2)
{
	return ((s1 > s2) && (s1 - s2 <= 32768)) || ((s1 < s2) && (s2 - s1 > 32768));
}
inline int seq_dist(uint16_t s1, uint16_t s2)
{
	int s = std::min(s1, s2);
	int g = std::max(s1, s2);
	return std::abs(std::max(s - g, g - (USHRT_MAX + s)));
}
inline uint16_t seq_increment(uint16_t seq, uint16_t dt)
{
	int32_t s = seq;
	s += dt;
	return uint16_t(s % USHRT_MAX);
}

inline const char* print_bits(uint32_t bits)
{
	thread_local char str[8 * 5];

	int i;
	for (i = 0; i < 32; i++) {
		str[i] = bits & 1 ? '1' : '0';
		bits >>= 1;
	}
	str[i] = 0;
	return str;
}

typedef uint32_t ACKBITS;

// unique connection
// handles acks and whatever
// could have multiple streams?

template <int S> struct reliableSeq {
	static constexpr uint32_t ACKVAL = 0xffffffffu;

	uint32_t  seq_buff[S];
	rawPacket seq_data[S];
	netTime_t seq_time[S];
	//	uint16_t  seq; // deprecated

	uint16_t seq_head;
	uint16_t seq_tail;

	uint16_t ackseq;

	double latency;

	// uint16_t ack;
	// uint32_t ack_bits;

	uint16_t Oldest(uint16_t sequence) const
	{
		// TODO return sequence number of
		// oldest unacked packet in window
		for (int i = 63; i >= 0; i--) {
			uint16_t s2 = sequence - i;
			if (seq_buff[s2 % S] != ACKVAL)
				return s2;
		}

		return sequence;
	}
	uint16_t Oldest() const { return Oldest(seq_head); }
	int      NextOldest(uint16_t s1, uint16_t s2) const
	{
		// TODO
		for (int i = seq_dist(s1, s2) - 1; i >= 0; i--) {
			uint16_t s3 = s2 - i;
			if (seq_buff[s3 % S] != ACKVAL)
				return s3;
		}
		return s2;
	}
	void IncrHeadSeq()
	{
		seq_head = seq_increment(seq_head, 1);
		seq_head = seq_greater_than(seq_head, seq_tail) ? seq_tail : seq_head;
	}
	void IncrTailSeq() { seq_tail = seq_increment(seq_tail, 1); }
	int  BufferedPackets() { return seq_dist(seq_head, seq_tail); }

	rawPacket* GetPacket(uint16_t sequence)
	{
		int i = sequence % S;
		if (seq_buff[i] == sequence) {
			return &seq_data[i];
		} else {
			fprintf(stderr, "EXPECTED seq %u found seq_buff %u\n", sequence,
			        seq_buff[i]);
		}
		return nullptr;
	}

	rawPacket* GetHead() { return GetPacket(seq_head); }
	rawPacket* GetTail() { return GetPacket(seq_tail); }
	rawPacket* GetOldest() { return GetPacket(Oldest(seq_head)); }

	rawPacket& InsertPacket(uint16_t sequence)
	{
		// printf("INSERTING %hu\n", sequence);
		int i                 = sequence % S;
		seq_buff[i]           = sequence;
		seq_data[i].data_s    = 0;
		seq_data[i].msg.seq   = sequence;
		seq_data[i].msg.proto = MAGICNUM;
		seq_time[i]           = std::chrono::steady_clock::now();
		return seq_data[i];
	}
	rawPacket& AppendPacket()
	{
		IncrTailSeq();
		return InsertPacket(seq_tail);
	}
	void SetSentTime(uint16_t sequence)
	{
		int i       = ((int)sequence) % S;
		seq_time[i] = std::chrono::steady_clock::now();
	}
	void SetSentTime(const rawPacket* rp)
	{
		uint16_t sequence = rp->msg.seq;
		int      i        = ((int)sequence) % S;
		seq_time[i]       = std::chrono::steady_clock::now();
	}
	void CalcLatency(uint16_t sequence)
	{
		int i   = ((int)sequence) % S;
		latency = latency + milli(std::chrono::steady_clock::now() - seq_time[i]).count();
		latency *= 0.5;
	}

	void AckPacket(uint16_t sequence)
	{
		int i = sequence % S;
		if (seq_buff[i] == (uint32_t)sequence) {
			seq_buff[i] = ACKVAL;
			if (seq_greater_than(sequence, ackseq)) {
				ackseq = sequence;
				CalcLatency(ackseq);
			}
		}
	}
	void AckPackets(uint16_t iack, uint32_t bits)
	{
		uint32_t mask = 1;
		// printf("\nCONFIRMED seq %05hu bitfield %s\n", iack, print_bits(bits));
		AckPacket(iack); // ? ? ?
		for (int i = 1; i < 33; i++) {

			uint16_t sequence = iack - i;
			int      h        = sequence % S;

			if ((bits & mask) && seq_buff[h] == (uint32_t)sequence) {
				AckPacket(sequence);

			} else if (seq_buff[h] != ACKVAL) {
				// printf("	S0 %hu S1 %hu\n", seq_buff[h], sequence);
			}
			mask <<= 1;
		}
	}
	void Reset()
	{
		memset(seq_buff, 0xffu, sizeof(seq_buff));

		seq_head = 1;
		seq_tail = 1;
		ackseq   = 0;
		latency  = 0.0;

		InsertPacket(1);
	}

	/*
	void GenAckBits()
	{
	        ack_bits      = 0;
	        uint32_t mask = 1;
	        for (int i = 0; i < 32; i++) {
	                uint16_t sequence = ack - i;
	                if (seq_buff[sequence % S] == (uint32_t)sequence)
	                        ack_bits += mask;
	                mask <<= 1;
	        }
	}
	*/
};

// append a fragment_state_t to track transmission progress for both outgoing and incoming fragments
struct fragment_state_t {
	size_t   byte_size;
	size_t   num_packets;
	uint16_t seq_start;
	uint16_t seq_end;
	uint16_t seq_current;
};

struct fragment_header_t {
	uint32_t byte_size;
	uint16_t num_packets;
	uint16_t current_num;
};

class Connection {
    public:
	Address maddr;

	reliableSeq<BUFFERSIZE>            out;
	RingBuffer<submessage, BUFFERSIZE> inOrder;
	RingBuffer<fragment_state_t, 32>   frag_out;
	RingBuffer<fragment_state_t, 32>   frag_in;

	unsigned char frag_buff[MAXTRANSMISSIBLE];

	double time; // ? ? ?
	double driftAccumulator; // accumulate time drift between connection and local

	Context& ctx;

	deserializer* unorderDst;
	deserializer* orderDst;

	int timeOut;
	enum {
		// conn states
		RESOLVING,
		CONNECTING,
		CONNECTED,
		DISCONNECTED,
		DISCONNECTING
	};
	int state;

	unsigned int debug_seed;
	int          debug_quality;

	// ack stuff for in coming ? ? ?
	struct {
		uint16_t ack;
		uint32_t ack_bits;
		uint16_t edge; // tracks trailing edge of inOrder messages
		uint16_t edge_unordered;
	} in;

	connStats stats;

	Connection(Context& c, Address addr, int uid)
	    : ctx(c)
	    , in{ 0, 0xffffffff, 0 }
	{
		timeOut    = 0;
		maddr      = addr;
		unorderDst = nullptr;
		orderDst   = nullptr;
		Reset();
		stats.Reset();

		out.Reset();

		state = CONNECTING;

		debug_seed    = 1;
		debug_quality = CONNQUALITY;
	}
	void Reset()
	{
		in.ack            = 0;
		in.ack_bits       = 0; // 0xffffffff;
		in.edge           = 1;
		in.edge_unordered = 0;
		out.Reset();
		stats.Reset();

		// out.seq = 1;
		// out.InsertPacket(out.seq);
	}

	transfer_progress_t GetRecieveProgress();
	transfer_progress_t GetSendProgress();

	// return smallest available space left in out reliableSeq
	int GetSpace()
	{
		int overhead         = sizeof(message) + sizeof(fragment_header_t);
		int max_packet_space = (MAXPACKET_S - overhead);
		int fragment_space
		    = int(BUFFERSIZE) - int(stats.oldestUnacked) - out.BufferedPackets();

		return fragment_space * max_packet_space;
	}
	void Close()
	{
		// close the conneciton
		state = DISCONNECTING;
	}
	int AddData(void* data, int len);
	int Send();
	int Recieve();
};
} // namespace net
