

namespace nix {

#include <termios.h>

struct termios orig_termios;
void           reset_terminal_mode() { tcsetattr(0, TCSANOW, &orig_termios); }
void           set_conio_terminal_mode()
{
	struct termios new_termios;

	/* take two copies - one for now, one for later */
	tcgetattr(0, &orig_termios);
	memcpy(&new_termios, &orig_termios, sizeof(new_termios));

	/* register cleanup handler, and set the new terminal mode */
	atexit(reset_terminal_mode);
	cfmakeraw(&new_termios);
	tcsetattr(0, TCSANOW, &new_termios);
}

static inline void Init()
{
	/* Watch stdin (fd 0) to see when it has input. */
	// FD_ZERO(&rfds);
	// FD_SET(0, &rfds);
	// tv.tv_sec = 0;
	// tv.tv_usec = 1000;

	set_conio_terminal_mode();
}
static inline void Exit() { reset_terminal_mode(); }
static inline void PollEvents()
{
	// TODO
	// check keyboard

	fd_set         rfds;
	struct timeval tv;
	FD_ZERO(&rfds);
	FD_SET(0, &rfds);
	tv.tv_sec  = 0;
	tv.tv_usec = 1000;
	int count  = select(1, &rfds, NULL, NULL, &tv);
	if (count > 0) {
		unsigned char c = 0;
		read(0, &c, sizeof(c));
		printf("CHAR %c", c);
		if (count > 0) {
			//	printf(" COUNT %d",count);
		}
		printf("\n");
		if (c == 'q')
			exit(0);
	}
}
} // namespace nix
