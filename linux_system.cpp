#include <algorithm>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "gfx.hpp"
#include "glrend.hpp"

namespace gfx {
// GLFWwindow * window;

#include <time.h>

#include <X11/Xlib.h>
#include <X11/Xos.h>
#include <X11/Xutil.h>

#include <X11/Xatom.h>
#include <X11/extensions/XShm.h>
#include <X11/extensions/sync.h>
#include <sys/shm.h>

#include <GL/glx.h>

// Note: depends on the inclusion of X11/extensions/XShm.h
// #include <X11/extensions/Xv.h>
// #include <X11/extensions/Xvlib.h>

#include "nixkeys.hpp"

// Xlib stuff
// https://www.x.org/releases/X11R7.7/doc/libXext/synclib.html
// http://antony.lesuisse.org/software/xorgsync/
// https://dri.freedesktop.org/wiki/CompositeSwap/

// wm v sync stuff
// http://fishsoup.net/misc/wm-spec-synchronization.html
// https://specifications.freedesktop.org/wm-spec/wm-spec-1.5.html
// http://freedesktop.org/cgi-bin/viewcvs.cgi/xorg/xc/doc/hardcopy/Xext/synclib.PS.gz

// X stuff
Display* xdisplay;
Window   xwindow;
XContext xctx;
Visual*  visual;
XImage*  img;
// Shm stuff
XShmSegmentInfo shminfo;
XSyncCounter    counter;
XSyncCounter    msyncCounters[2];
XSyncValue      v, v1;

// GLX stuff
GLXContext glxctx = 0;
void*      glxmem = NULL;
// Xv stuff
// XvPortID xvPort;
// XvImage*        img;

int       xsync_event  = 0;
int       xsync_error  = 0;
const int xres_default = 640;
const int yres_default = 480;
int       xres_win     = xres_default;
int       yres_win     = yres_default;
int       xres_buff    = xres_default;
int       yres_buff    = yres_default;

int render_mem_size = xres_default * yres_default * sizeof(uint32_t);

bool          keyHeld[512];
mouse_state_t main_mouse; // is it possible to have multiple mice?

enum { RENDERMODE_NONE,
       RENDERMODE_GLX,
       RENDERMODE_SHM,
       RENDERMODE_XV

};

int renderMode = RENDERMODE_GLX;

#define is_aligned(POINTER, BYTE_COUNT) (((uintptr_t)(const void*)(POINTER)) % (BYTE_COUNT) == 0)

Atom selSource;
Atom selTarget;
Atom selProp;

Atom wmDeleteMessage;
Atom wmNetPing;
Atom wmSyncRequest;
Atom wmSyncRequestCounter;
Atom wmFrameDrawn;
Atom xvSyncVBlank;

void GetScreenDim(int& width, int& height)
{
	width  = xres_win;
	height = yres_win;
}

void getBestRes(const int inwidth, const int inheight, int& w, int& h)
{
	// TODO/IDEA
	// take in available width&height of window and pic a rendering resolution
	// where width is 16byte aligned, arbitrary height and less than
	// some max number of pixels like 512x512

	int dx = std::max(1, (inwidth + 256) / 512);

	// no dimension should exceed 640?
	w = std::min(640, inwidth / dx);
	h = std::min(640, inheight / dx);
	w = std::max(16, (w / 16) * 16);
}

void SetPalette(uint32_t* pal)
{
	if (renderMode == RENDERMODE_GLX)
		ogl::SetPalette(pal, 32);
}

int InitGLX()
{
	int          major, minor;
	XVisualInfo* vi               = NULL;
	static int   visual_attribs[] = { GLX_RGBA, GLX_DOUBLEBUFFER, True, None };

	glXQueryVersion(xdisplay, &major, &minor);
	printf("GLX ver %d.%d\n", major, minor);

	vi = glXChooseVisual(xdisplay, DefaultScreen(xdisplay), visual_attribs);
	if (!vi) {
		fprintf(stderr, "no conforming visuals found\n");
		return 0;
	}

	glxctx = glXCreateContext(xdisplay, vi, NULL, true);
	if (!glxctx) {
		fprintf(stderr, "creation of glx context failed\n");
		return 0;
	}

	if (!glXMakeCurrent(xdisplay, xwindow, glxctx)) {
		fprintf(stderr, "failed to set glx context\n");
	}

	glxmem = malloc(render_mem_size);
	if (!ogl::Init())
		return 0;

	printf("GLX initialized\n");
	ogl::PrintGLinfo();

	return 1;
}

int InitShm()
{
	// TODO MOVE THIS
	if (XShmQueryExtension(xdisplay)) {
		printf("Supports shared memory\n");
	} else {
		fprintf(stderr, "No XShm shared memory\n");
		return 0;
	}

	// --- EXPERIMENTAL --- //
	// make a counter and display it in window properties so wm can
	// make game vsync work?
	XSyncIntToValue(&v, 0);
	msyncCounters[0] = XSyncCreateCounter(xdisplay, v);
	msyncCounters[1] = XSyncCreateCounter(xdisplay, v);

	XChangeProperty(xdisplay, xwindow,
	                wmSyncRequestCounter, // Atom prop,
	                XA_CARDINAL, // Atom type,
	                sizeof(msyncCounters[0]), // 32, // int format,
	                PropModeReplace, // int mode
	                (const unsigned char*)msyncCounters, // data*,
	                2 // int elements
	);

	counter = msyncCounters[0];
	XSyncIntToValue(&v, 1);
	XSyncSetCounter(xdisplay, counter, v);

	memset(&main_mouse, 0, sizeof(main_mouse));
	memset(keyHeld, 0, sizeof(keyHeld));
	memset(&shminfo, 0, sizeof(XShmSegmentInfo));
	visual = DefaultVisual(xdisplay, 0);

	/// --- Vanila XShm image setup
	img = XShmCreateImage(xdisplay, visual, 24, ZPixmap, NULL, &shminfo, xres_default,
	                      yres_default);

	// setup shared memory space
	// bytes per width  * height + 4 scratch bytes?
	// maybe need more to ensure 16 byte alignment

	render_mem_size = std::max(render_mem_size, img->bytes_per_line * img->height) + 1;

	shminfo.shmid   = shmget(IPC_PRIVATE, render_mem_size + 4, IPC_CREAT | 0777);
	shminfo.shmaddr = img->data = (char*)shmat(shminfo.shmid, 0, 0);
	if (!is_aligned(shminfo.shmaddr, 16)) {
		fprintf(stderr, "MEMORY IS NOT 16 BYTE ALIGNED\n");
	}
	shminfo.readOnly = False;
	XShmAttach(xdisplay, &shminfo);

	memset(shminfo.shmaddr, 0, render_mem_size);
	printf("shm ALLOCATED %d bytes\n", render_mem_size);
	// setup some kind of vsync
	int rc = XSyncQueryExtension(xdisplay, &xsync_event, &xsync_error);
	if (!rc) {
		fprintf(stderr, "X sync not supported\n");
		return 0;
	}

	int                 ssc_num = 0;
	XSyncSystemCounter* ssc     = NULL;
	ssc                         = XSyncListSystemCounters(xdisplay, &ssc_num);
	for (int i = 0; i < ssc_num; i++) {
		printf("SYNC NAME %s ID %zu VAL %d\n", ssc[i].name, ssc[i].counter,
		       XSyncValueLow32(ssc[i].resolution));
	}

	for (int i = 0; i < ssc_num; i++) {
		// find SERVERTIME counter
		// if (strcmp(ssc[i].name, "SERVERTIME")==0)
		// {
		//     counter = ssc[i].counter;
		//     break;
		// }
	}
	XSyncFreeSystemCounterList(ssc);

	if (counter == None) {
		fprintf(stderr, "Could not find SERVERTIME counter\n");
		return 0;
	}

	// why 2 ?
	XSyncIntToValue(&v1, 8);
	return 1;
}

int InitXv()
{
	/*
	{
	        /// --- XV EXTENSION TEST
	        unsigned int ver, rev, req, ev, err;
	        if (XvQueryExtension(xdisplay, &ver, &rev, &req, &ev, &err) != Success) {
	                fprintf(stderr, "No x video extension\n");
	                return 0;
	        } else {
	                printf("Supports x video ver %u rev %u req %u ev %u err %u\n", ver, rev,
	                       req, ev, err);
	        }
	        unsigned int   ai_num;
	        XvAdaptorInfo* ai;
	        if (XvQueryAdaptors(xdisplay, xwindow, &ai_num, &ai) != Success) {
	                fprintf(stderr, "Failed to query adaptors\n");
	                return 0;
	        }
	        printf("ADAPTERS FOUND %u\n", ai_num);
	        for (int i = (int)ai_num - 1; i >= 0; i--) {
	                printf("ADAPTOR %s\n	SUPPORTS XvImageMask %d\n", ai[i].name,
	                       (ai[i].type & XvImageMask) > 0);
	                xvPort = ai[i].base_id;
	        }

	        if (ai_num > 0) {
	                if (XvGrabPort(xdisplay, xvPort, CurrentTime) == Success) {
	                        printf("GOT Xv Port\n");
	                        // TODO
	                        // we have xv and port
	                        XvSetPortAttribute(xdisplay, xvPort, xvSyncVBlank, 1);
	                } else {
	                        fprintf(stderr, "Failed to grab xv port\n");
	                        return 0;
	                }
	        }
	}

	xvSyncVBlank         = XInternAtom(xdisplay, "XV_SYNC_TO_VBLANK", False);

	/// --- XvShm setup
	{
	        int                  fnum  = 0;
	        XvImageFormatValues* fvals = XvListImageFormats(xdisplay, xvPort, &fnum);
	        printf("Xv formats found %d\n", fnum);
	        fnum--;
	        while (fnum >= 0) {
	                printf("%s TYPE %d\n", fvals[fnum].guid, fvals[fnum].type);
	                fnum--;
	        }
	        XFree(fvals);
	        // img = XvShmCreateImage(xdisplay, xvPort, 24, , NULL, xres_default, yres_default,
	&shminfo);
	}
	*/
	return 1;
}

int Init()
{

	xdisplay = XOpenDisplay(NULL);

	initKeycodeTable(xdisplay);
	Bool detectable = True;
	XkbSetDetectableAutoRepeat(xdisplay, True, &detectable);

	// int screen = DefaultScreen(xdisplay);
	xwindow = XCreateSimpleWindow(xdisplay, DefaultRootWindow(xdisplay), 0, 0, xres_default,
	                              yres_default, 0, 0, 0);
	XSetStandardProperties(xdisplay, xwindow, "Sensual Soccer", "SOC!", None, NULL, 0, NULL);

	long inputmask = StructureNotifyMask | KeyPressMask | KeyReleaseMask | PointerMotionMask
	    | ButtonPressMask | ButtonReleaseMask | ExposureMask | FocusChangeMask
	    | VisibilityChangeMask | EnterWindowMask | LeaveWindowMask | PropertyChangeMask
	    | SelectionNotify;

	selSource = XInternAtom(xdisplay, "CLIPBOARD", False);
	selTarget = XInternAtom(xdisplay, "UTF8_STRING", False);
	selProp   = XInternAtom(xdisplay, "PENGUIN", False);

	XSelectInput(xdisplay, xwindow, inputmask);

	pid_t pid = getpid();
	XChangeProperty(xdisplay, xwindow, XInternAtom(xdisplay, "_NET_WM_PID", False), XA_CARDINAL,
	                32, PropModeReplace, (unsigned char*)&pid, 1);

	wmDeleteMessage      = XInternAtom(xdisplay, "WM_DELETE_WINDOW", False);
	wmNetPing            = XInternAtom(xdisplay, "_NET_WM_PING", False);
	wmFrameDrawn         = XInternAtom(xdisplay, "_NET_WM_FRAME_DRAWN", False);
	wmSyncRequest        = XInternAtom(xdisplay, "_NET_WM_SYNC_REQUEST", False);
	wmSyncRequestCounter = XInternAtom(xdisplay, "_NET_WM_SYNC_REQUEST_COUNTER", False);

	if (wmSyncRequest == None || wmSyncRequestCounter == None)
		printf("SYNC UNSUPPORTED\n");

	Atom protocols[] = { wmDeleteMessage, wmNetPing, wmSyncRequest };
	for (const Atom& p : protocols) {
		printf("PROTOS %lu\n", p);
	}

	XSetWMProtocols(xdisplay, xwindow, protocols, sizeof(protocols) / sizeof(Atom));

	// TODO
	// try to init opengl if that fails
	// fallback to plain x rendering
	if (!InitGLX()) {
		fprintf(stderr, "FALLING BACK TO X rendering\n");
		if (!InitShm())
			return 0;
		renderMode = RENDERMODE_SHM;
	}

	XMapWindow(xdisplay, xwindow);

	return 1;
};
void Exit()
{
	switch (renderMode) {
	case RENDERMODE_GLX:
		glXMakeCurrent(xdisplay, 0, 0);
		glXDestroyContext(xdisplay, glxctx);
		break;
	case RENDERMODE_SHM:
		XShmDetach(xdisplay, &shminfo);
		XSyncDestroyCounter(xdisplay, counter);
		shmctl(shminfo.shmid, IPC_RMID, NULL);
		break;
	}

	XDestroyWindow(xdisplay, xwindow);
	XCloseDisplay(xdisplay);
};
int  needExit = 0;
int  NeedExit() { return needExit; };
void GetCurrentBuffer(void** raw32, int* width, int* height)
{

	double res   = xres_default * yres_default;
	double ratio = double(xres_win) / double(yres_win);
	double w     = sqrt(res * ratio);
	double h     = floor(sqrt(res * (1.0 / ratio)));
	// round down nearest 16 bytes wide to accomidate simd bullshit
	w         = floor(w / 16.0) * 16.0;
	xres_buff = w;
	yres_buff = h;

	switch (renderMode) {
	case RENDERMODE_GLX:
		*raw32  = glxmem;
		*width  = w; // xres_default;
		*height = h; // yres_default;
		break;
	case RENDERMODE_SHM:
		*raw32  = shminfo.shmaddr;
		*width  = xres_default;
		*height = yres_default;
	}
};

int CopyAtomToBuff(Display* dpy, Window w, Atom p, char* buff, size_t maxsize)
{
	Atom           da, type;
	int            di;
	unsigned long  size, dul, rem;
	unsigned char* prop_ret = NULL;

	// Dummy call to get type and size.
	XGetWindowProperty(dpy, w, p, 0, 0, False, AnyPropertyType, &type, &di, &dul, &rem,
	                   &prop_ret);
	XFree(prop_ret);

	size = rem;

	XGetWindowProperty(dpy, w, p, 0, size, False, AnyPropertyType, &da, &di, &dul, &rem,
	                   &prop_ret);

	// printf("di %d dul %lu rem %lu",di,dul,rem);
	switch (di) {
	case 8:
		dul *= 1;
		break;
	case 16:
		dul *= 2;
		break;
	case 32:
		dul *= 4;
		break;
	default: // ? ? ?
		break;
	}

	dul = std::min(dul, maxsize);
	mempcpy(buff, prop_ret, dul);

	XFree(prop_ret);
	// Signal the selection owner that we have successfully read the data.
	XDeleteProperty(dpy, w, p);
	return dul;
}
// TODO
// i need to catch middle mouse button presses or CTRL + V events
// and then request paste
void ReqPaste()
{
	XConvertSelection(xdisplay, selSource, selTarget, selProp, xwindow, CurrentTime);
}
char PasteBuffer[256];

pasteCallback pasteCB = 0;

void SetPasteCallback(pasteCallback pcb) { pasteCB = pcb; }

keyboardCallback keyboardCB = 0;

void SetKBCallback(keyboardCallback kbcb) { keyboardCB = kbcb; }

mouseCallback mouseCB = 0;

void SetMouseCallback(mouseCallback mcb) { mouseCB = mcb; }

const bool* GetKeyStates() { return keyHeld; }

const mouse_state_t& GetMouseState() { return main_mouse; }

char   tempbuff[24];
int    tempbuff_len = 24;
KeySym tempkeysym;

void PollEvents()
{
	// TODO move this somewhere else
	// glfwPollEvents();
	XEvent                     ev;
	XKeyEvent*                 kv      = &ev.xkey;
	const XClientMessageEvent* cmv     = &ev.xclient;
	const XMotionEvent*        mv      = &ev.xmotion;
	const XButtonEvent*        bv      = &ev.xbutton;
	const XConfigureEvent*     cv      = &ev.xconfigure;
	int                        pending = XPending(xdisplay);

	while (pending--) {
		XNextEvent(xdisplay, &ev);
		switch (ev.type) {
		// KeyPressMask
		// KeyReleaseMask
		case KeyPress:
			// emulate paste events
			if (keycodes[kv->keycode] == KEY_V && kv->state == ControlMask)
				ReqPaste();

		case KeyRelease: {
			int  k   = keycodes[kv->keycode];
			bool val = kv->type == KeyPress ? true : false;
			XLookupString(kv, tempbuff, tempbuff_len, &tempkeysym, nullptr);

			if ((val != keyHeld[k]) && keyboardCB)
				keyboardCB(k,
				           kv->type == KeyPress ? KEY_EVENT_PRESS
				                                : KEY_EVENT_RELEASE,
				           tempbuff);
			keyHeld[k] = val;
		} break;
		case ClientMessage:
			printf("CLIENT MESSAGE\n");
			if (cmv->data.l[0] == wmDeleteMessage) {
				printf("NEED EXIT\n");
				needExit = 1;
			} else if (cmv->data.l[0] == wmSyncRequest) {
				printf("VSYNC ? ? "
				       "?\n");
			}
			break;
		case ButtonPress:
			main_mouse.buttons |= uint32_t(mouse_button_maps[bv->button]);
		case ButtonRelease:

			if (ev.type == ButtonRelease)
				main_mouse.buttons &= (~uint32_t(mouse_button_maps[bv->button]));
			if (mouseCB) {
				mouseCB(ev.type == ButtonPress ? MouseAction_t::PRESS
				                               : MouseAction_t::RELEASE,
				        bv->x, bv->y, mouse_button_maps[bv->button]);
			}
			break;
		case MotionNotify:
			main_mouse.win_x = mv->x;
			main_mouse.win_y = mv->y;
			if (mouseCB) {
				mouseCB(MouseAction_t::MOVE, mv->x, mv->y, MouseButtons_t::NONE);
			}
			break;
		case VisibilityNotify:
		case EnterNotify:
		case LeaveNotify:
		case FocusIn:
		case FocusOut:
			// TODO clear keyHeld ?
			break;
		case GraphicsExpose:
		case NoExpose:
		case Expose:
			printf("EXPOSED\n");
			// TODO needs redraw?
			break;
		case PropertyNotify:
			// iconified ?
			break;
		// SubstructureNotifyMask
		case DestroyNotify:
			needExit = 1;
			break;
		case ConfigureNotify:
			xres_win = cv->width;
			yres_win = cv->height;
			// printf("CONFIG NOTIFY\n");
			break;
		case CirculateNotify:
		case GravityNotify:
		case MapNotify:
		case ReparentNotify:
			break;
		case UnmapNotify:
			// NO IDEA WHAT TODO HERE
			printf("unmapped\n");
			break;
		case GenericEvent:
			break;
		case SelectionNotify:
			printf("COPY/PASTE\n");
			{
				XSelectionEvent* sev = (XSelectionEvent*)&ev.xselection;
				if (sev->property == None) {
					printf("Conversion could not be performed.\n");
				} else {

					int len = CopyAtomToBuff(xdisplay, xwindow, selProp,
					                         PasteBuffer, 256);
					if (pasteCB)
						pasteCB(PasteBuffer, len);
				}
			}
			break;
		default:
			// unsupported type
			// fprintf(stderr, "Unsupported XEvent
			// %d\n", ev.type);
			break;
		}
	}
}

void SwapBuffers()
{

	PollEvents();
	switch (renderMode) {
	case RENDERMODE_GLX:
		ogl::Draw(glxmem, xres_buff, yres_buff, xres_win, yres_win);
		glXSwapBuffers(xdisplay, xwindow);
		break;
	case RENDERMODE_SHM:
		XShmPutImage(xdisplay, xwindow, DefaultGC(xdisplay, 0), img, 0, 0, 0, 0,
		             xres_default, yres_default, true);
		// XvShmPutImage(xdisplay, xvPort, xwindow, DefaultGC(xdisplay, 0), img, 0, 0, 0, 0,
		// xres_default,
		//              yres_default, true);
		int rc = XShmGetEventBase(xdisplay);
		if (!rc) {
			fprintf(stderr, "XShmGetEventBase failed %d\n", rc);
		}
		// beginning of frame
		// odd value
		XSyncChangeCounter(xdisplay, msyncCounters[1], v);
		XSync(xdisplay, False);
		double   ct        = gfx::GetTimeElapsed();
		double   ms        = 1e+6;
		double   targetFPS = 16.0; // 60fps
		timespec amount{ 0, int(ms * (targetFPS - ct)) };
		nanosleep(&amount, NULL);
		// end of frame even value
		XSyncChangeCounter(xdisplay, msyncCounters[1], v);
		/*
		Bool b=True;
		XSyncWaitCondition wc;
		wc.trigger.counter = counter;
		wc.trigger.value_type = XSyncAbsolute;
		wc.trigger.test_type = XSyncPositiveComparison;
		XSyncIntToValue(&wc.event_threshold, 0);
		//XSyncIntToValue(&wc.trigger.wait_value, 0);

		XSyncQueryCounter(xdisplay, counter, &v);
		XSyncValueAdd(&wc.trigger.wait_value, v, v1, &b);
		//fprintf(stderr, "Wait for %u\n",XSyncValueLow32(wc.trigger.wait_value));
		XSyncAwait(xdisplay, &wc, 1);
		// */
		break;
	}
}
} // namespace gfx
