#pragma once
#include "game.hpp"
#include "hash.hpp"
#include "timeslew.hpp"

class SnapRecorder : public sim::PhaseCallback {

	// TODO/IDEA
	// snapshot game state every second or so
	// implement somekind of debug interface to roll back to previous snapshots
	// AND implement somekind of roll back and replay for netplay

    public:
	struct snapshot {
		uint64_t   frame;
		uint32_t   crc;
		uint32_t   uid;
		uint64_t   time;
		ByteBuffer buff;
		void       Reset()
		{
			buff.readback(buff.len());
			buff.resize(0);
		}

		uint32_t CalcCRC() const
		{
			int a = 0, b = 0;
			return RollingHash(a, b, (unsigned char*)buff.head(), buff.len());
		}
	};
	static constexpr int maxsnaps = 10;

    private:
	snapshot snaps[maxsnaps];
	bool     enabled;

    public:
	SnapRecorder()
	    : snaps()
	{
		phase   = POSTICK;
		enabled = true;
	}
	~SnapRecorder()
	{
		for (auto s : snaps) {
			s.buff.freeData();
		}
	}
	// why bother?
	void Disable() { enabled = false; }
	void Enable() { enabled = true; }

	size_t MemUsed()
	{
		size_t t = 0;
		for (auto& s : snaps) {
			t += s.buff.cap();
		}
		return t;
	}

	virtual void Exec(sim::AbstractGame_t& game) override
	{
		if (game.GetFrameCount() % 60 != 0 || !enabled)
			return;

		int i          = int(game.GetFrameCount() / 60) % maxsnaps;
		snaps[i].frame = game.GetFrameCount();
		snaps[i].buff.readback(snaps[i].buff.len());
		snaps[i].buff.resize(0);
		game.SnapShot(snaps[i].buff, snaps[i].frame);
		int a = 0, b = 0;
		snaps[i].crc
		    = RollingHash(a, b, (unsigned char*)snaps[i].buff.head(), snaps[i].buff.len());

		snaps[i].time = timeSlewer::GetTimeInt();
		printf("SNAPSHOT %08x-%d FRAME %zu", snaps[i].crc, i, game.GetFrameCount());
		printf(" MemUsed %zu\n", MemUsed());
	}

	void Reset()
	{
		// dump all data and stuff
	}

	uint64_t HeadFrame()
	{
		uint64_t f = 0;
		for (auto& s : snaps) {
			f = s.frame > f ? s.frame : f;
		}
		return f;
	}

	uint32_t GetCrc(uint64_t frame)
	{
		int i = (frame / 60) % maxsnaps;
		return snaps[i].crc;
	}
	uint64_t GetTime(uint64_t frame)
	{
		int i = (frame / 60) % maxsnaps;
		return snaps[i].time;
	}

	bool Compare(uint64_t frame, uint32_t crc) const
	{
		// compare snapshot crc for specific frame
		// if it doesnt exist return false?
		int i = (frame / 60) % maxsnaps;
		return snaps[i].crc == crc;
	}

	void RollBack(sim::AbstractGame_t& game)
	{

		int i = ((game.GetFrameCount() - 1) / 60) % maxsnaps;
		if (snaps[i].frame < game.GetFrameCount() && snaps[i].buff.len() > 0) {
			game.SetState(snaps[i].buff, snaps[i].frame);
		}
	}
};
