
#include <vector>
#include <iostream>
#include <fstream>
#include <filesystem>

#include <unordered_map>

#include "resource.hpp"

#include <sys/stat.h>

int BuildResouceFile(const std::string& conf, const std::string& root, const std::string out)
{

	printf("ROOT %s\n", root.c_str());

	std::vector<resource_t> resource_desc;
	std::vector<size_t>     pts;
	std::string             fn_lim = "filename=\"";
	std::string             output;
	size_t                  pt0 = conf.find(fn_lim);
	resource_index_t        res_index;
	int                     pix           = 0;
	size_t                  all_names_len = 0lu;
	size_t                  all_data_len  = 0lu;
	output.reserve(1024lu * 10000lu); // reserve afew MB to avoid allocating constantly

	while (pt0 != std::string::npos) {
		pts.push_back(pt0 + fn_lim.size());
		pt0 = conf.find(fn_lim, pt0 + fn_lim.size());
	}

	for (const auto& pt : pts) {
		size_t lim = conf.find("\"", pt);

		uint64_t _hash      = 0;
		uint64_t _modified  = 0;
		uint64_t _ptr_data  = 0;
		uint64_t _ptr_name  = pt;
		uint64_t _data_size = 0;
		uint64_t _name_size = lim - pt;

		resource_desc.push_back(resource_t{ resource_type::NONE, _hash, _modified,
		                                    _ptr_data, _ptr_name, _data_size, _name_size });
	}

	// stat each file to get size, last modified and assign a type if possible
	for (auto& res : resource_desc) {

		const std::string fname = conf.substr(res._ptr_name, res._name_size);

		struct stat sbuff;
		int         status;

		status = stat((root + fname).c_str(), &sbuff);
		if (status < 0) {
			fprintf(stderr, "file %s not found\n", fname.c_str());
		}

		res._modified  = sbuff.st_mtim.tv_sec;
		res._data_size = sbuff.st_size;
		res._type      = resource_type::NONE;

		for (const auto& lable : resource_type_labels) {
			if (fname.rfind(lable._str) != std::string::npos) {
				res._type = lable._type;
				break;
			}
		}

		all_names_len += res._name_size;
		all_data_len += res._data_size;
	}

	// calculate the position of all the data
	const size_t offset = sizeof(resource_index_t) + resource_desc.size() * sizeof(resource_t);
	size_t       name_offset = offset;
	size_t       data_offset = offset + all_names_len;
	for (auto& res : resource_desc) {
		res._ptr_data = data_offset;
		res._ptr_name = name_offset;

		data_offset += res._data_size;
		name_offset += res._name_size;
	}

	// calculate res_index member values this should be useful for bound checking and quickly
	// indexing data.
	res_index.res_ptr   = sizeof(res_index);
	res_index.res_len   = resource_desc.size();
	res_index.names_ptr = offset;
	res_index.names_len = all_names_len;
	res_index.data_ptr  = offset + all_names_len;
	res_index.data_len  = all_data_len;

	// write out resource index and table of resource descriptions
	output.append((const char*)&res_index, sizeof(res_index));
	output.append((const char*)resource_desc.data(), resource_desc.size() * sizeof(resource_t));

	// writeout names NOTE: _ptr_name has already been patched to point into name table so
	// pts vector must be used to copy names out of the config
	for (auto& res : resource_desc) {
		output.append(conf.data() + pts[pix], res._name_size);
		pix++;
	}

	for (auto& res : resource_desc) {
		// TODO open each file writeout

		std::ifstream     fs;
		size_t            pt     = res._ptr_data;
		size_t            len    = res._data_size;
		char              filler = 0;
		const std::string fname  = root + output.substr(res._ptr_name, res._name_size);

		fs.open(fname, std::ios::in | std::ios::binary);
		printf("OUTPUT SIZE %zu\n", output.size());
		output.append(std::istreambuf_iterator<char>(fs), std::istreambuf_iterator<char>());
	}
	// TODO
	// compress

	// dirty debug dump table
	if (true)
		for (auto& res : resource_desc) {
			const std::string fname = output.substr(res._ptr_name, res._name_size);
			printf("%s type %zu nlen %zu ptr %zub size %zub mod %zu\n", fname.c_str(),
			       res._type, res._name_size, res._ptr_data, res._data_size,
			       res._modified);
		}

	printf("\nOUTPUT TOTAL SIZE %zu\n\n", output.size());

	resource_manager_t test_rm(output.data(), output.size());

	std::string varname = std::filesystem::path(out).stem();

	std::ofstream fs;
	fs.open(out, std::ios::trunc | std::ios::binary | std::ios::out);
	// output as c file
	if (true) {
		char cbuff[16];
		fs << "#include \"resource.hpp\"" << std::endl;
		fs << "const unsigned char " << varname << " [] = {" << std::endl;
		for (auto c : output) {
			sprintf(cbuff, "%hhu,", (unsigned char)c);
			fs << std::string(cbuff);
		}
		fs << "0};" << std::endl;
		fs << std::endl;

		// init resource manager with this data
		fs << "int werked = init_resource_manager(\"" << varname << "\",(const char*)"
		   << varname << "," << output.size() << "u);" << std::endl;
	}

	return 0;
}
