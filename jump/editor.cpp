#include <string>
#include <fstream>
#include <iostream>

#include "keys.hpp"
#include "editor.hpp"
#include "util.hpp"


void game_editor_t::LoadMap(const std::string &file_name, jmp::jump_instance &inst) {

	printf("LOADING MAP\n");
	inst.map.Reset();

	std::ifstream io(file_name, std::ios::binary);	
	size_t data_size = 0;
	std::vector<jmp::fieldMap_t::entry_t> data;

	io.read((char*)&data_size, sizeof(data_size));
	data.resize(data_size);
	io.read((char*)data.data(), data_size * sizeof(jmp::fieldMap_t::entry_t));

	for(auto& e : data) {	
		inst.map.Insert(e.datum, e.rec);
	}
}

void game_editor_t::SaveMap(const std::string &file_name, const jmp::fieldMap_t &map) {
	printf("SAVING MAP\n");

	std::vector<jmp::fieldMap_t::entry_t> data = map.entries;
	std::vector<int> entries = map.freeEntries;
	std::sort(entries.rbegin(), entries.rend());
	for (auto ix : entries) {
		data.erase(data.begin() + ix);
	}

	std::ofstream io(file_name, std::ios::binary);

	size_t data_size = data.size();
	io.write((char*)&data_size, sizeof(data_size));
	io.write((char*)data.data(), sizeof(jmp::fieldMap_t::entry_t) * data_size);

}

void game_editor_t::keyboardEvent(int keycode, int event, const char* str) {
	if (event == KEY_EVENT_PRESS) {
		switch(keycode) {
		case KEY_END:
			// TODO insert new tile
		case KEY_DELETE:
			// delete everything selected
			for (int ix : inspector.eindex) {
				printf("DELETING %d\n", ix);
				mainInst.map.Delete(ix);
			}	
			inspector.Reset();
		default:
			break;
		}
	}
}

void game_editor_t::mouseEvent(gfx::MouseAction_t act, int x, int y, gfx::MouseButtons_t btn) {

	if(act == gfx::MouseAction_t::PRESS && btn == gfx::MouseButtons_t::RIGHT) {
		// try deleting platform under mouse	
		x = debug_view.worldX(x);
		y = debug_view.worldY(y);
		inspector.Reset();
		mainInst.map.Search( rect_t{ x, y, x+1, y+1}, inspector);
	}
	
	if(act == gfx::MouseAction_t::PRESS && btn == gfx::MouseButtons_t::LEFT) {
		// start drawing platform	
		x = debug_view.worldX(x);
		y = debug_view.worldY(y);
		d_pt0 = v3{fp32(x), fp32(y), 0};
		d_pt1 = d_pt0;
	}

	if(act == gfx::MouseAction_t::RELEASE && btn == gfx::MouseButtons_t::LEFT) {
		// stop drawing platform	
		x = debug_view.worldX(x);
		y = debug_view.worldY(y);
		d_pt1 = v3{fp32(x), fp32(y), 0};

		rect_t rect = {
			std::min(d_pt0.x,d_pt1.x), 
			std::min(d_pt0.y, d_pt1.y),
			std::max(d_pt0.x,d_pt1.x), 
			std::max(d_pt0.y, d_pt1.y)
			};

		if(rect.area() > 10)
			mainInst.map.Insert(jmp::maptile_t{0, jmp::maptile_t::types_t::FLOOR}, rect);	
		d_pt0 = v3{};
		d_pt1 = v3{};
	}

	if(act == gfx::MouseAction_t::MOVE && gfx::GetMouseState().isHeld(gfx::MouseButtons_t::LEFT)) {
		x = debug_view.worldX(x);
		y = debug_view.worldY(y);
		d_pt1 = v3{fp32(x), fp32(y), 0};
	}

}
void game_editor_t::draw(renderContext &ctx) {
	// TODO
	// draw cursor, snap it to 8 - 16 - 32 grid 
	// TODO
	// draw dragged box for selecting or
	const auto& ms = gfx::GetMouseState();
	ctx.drawCircle(ms.win_x, ms.win_y, 8, ctx.palette.RED, false);

	for(auto& r : inspector.rects) {
		rtDrawRect(ctx, debug_view, r, ctx.palette.WHITE);
	}

	rect_t r{
		std::min(d_pt0.x,d_pt1.x), 
			std::min(d_pt0.y, d_pt1.y),
			std::max(d_pt0.x,d_pt1.x), 
			std::max(d_pt0.y, d_pt1.y)
	};
	rtDrawRect(ctx, debug_view, r, ctx.palette.WHITE);
}
