#pragma once
#include "jump.hpp"
#include "gfx.hpp"
#include "render.hpp"


class worldInspect_t : public queryCallback_t<jmp::maptile_t> {
	public:
		std::vector<int>    eindex;
		std::vector<rect_t> rects;
		void Reset() {
			eindex.resize(0);
			rects.resize(0);
		}
		virtual bool callback(int index, const rect_t& ir, const jmp::maptile_t& mt) override
		{
			eindex.push_back(index);
			rects.push_back(ir);
			return 0;
		}
};

class game_editor_t {
	public:

	void keyboardEvent(int keycode, int event, const char* str);
	void mouseEvent(gfx::MouseAction_t act, int x, int y, gfx::MouseButtons_t btn);
	
	void draw(renderContext &ctx);

	static void LoadMap(const std::string &file_name, jmp::jump_instance &inst);

	static void SaveMap(const std::string &file_name, const jmp::fieldMap_t &map);


	private:
	enum class tool_t : uint32_t {
		NONE,
		PLATFORM,
		ITEMS
	};

	int grid;
	tool_t tool;

	// debug cursors
	v3 d_pt0;
	v3 d_pt1;

	worldInspect_t inspector;
	jmp::jump_instance &mainInst;
	sim::view_t &debug_view;


	public:
	game_editor_t(jmp::jump_instance &inst, sim::view_t& view) : mainInst(inst),  debug_view(view) {
		tool = tool_t::PLATFORM;
		d_pt0 = v3{};
		d_pt1 = v3{};
		grid = 8;
	}
};


