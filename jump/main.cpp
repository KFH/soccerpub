
#include <algorithm>

#include <cmath>
#include <mutex>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "game.hpp"
#include "gamecontrols.hpp"
#include "gamerend.hpp"
#include "render.hpp"

#include "gfx.hpp"
#include "keys.hpp"
#include "netapi.hpp"

#include "localcontrols.hpp"
#include "snap.hpp"
#include "system.hpp"
#include "threadwork.hpp"
#include "interpolate.hpp"


#include "jump.hpp"
#include "draw.hpp"
#include "util.hpp"

#include "editor.hpp"


extern "C" {
#include "font4x6.xpm"
}

//#include "menu.hpp"

/// --- game specific types
struct jumpCtrlConf_t {
	sim::ctrlAxis move;
	sim::ctrlAxis jump;
	sim::ctrlAxis grab;
};
class PlayerControllerStream : public sim::InputStream<jmp::player_ctrl_t> {
    public:
	jumpCtrlConf_t kbConf;
	jumpCtrlConf_t mConf;
	jumpCtrlConf_t gpConf;
	jumpCtrlConf_t state;

	virtual int GetConst(jmp::player_ctrl_t& out) const override
	{
		out.x = kbConf.move.value;
		out.action == jmp::player_ctrl_t::action_type::NONE;

		if(kbConf.jump.value > 0.1f) {
			out.action = jmp::player_ctrl_t::action_type::JUMP;
		}
		if(kbConf.grab.value > 0.1f) {
			out.action = jmp::player_ctrl_t::action_type::GRAB;	
		}

		return 1;
	}

	virtual int Get(jmp::player_ctrl_t& out) override
	{
		kbConf.move.ApplyKeys(gfx::GetKeyStates());
		kbConf.jump.ApplyKeys(gfx::GetKeyStates());
		kbConf.grab.ApplyKeys(gfx::GetKeyStates());
		kbConf.move.DrifCenter();

		return GetConst(out);
	};


	virtual int Remaining() override
	{
		// always return 1
		return 1;
	};
	PlayerControllerStream(int uid)
	{
		kbConf.move = sim::ctrlAxis{ sim::ctrlAxis::KB, sim::ctrlAxis::AXIS, KEY_D, KEY_A, 0.25, 0.0, false, false };
		kbConf.jump = sim::ctrlAxis{ sim::ctrlAxis::KB, sim::ctrlAxis::DOWN, KEY_W, -1, 1.0, 0.0, false, false }; 
		kbConf.grab = sim::ctrlAxis{ sim::ctrlAxis::KB, sim::ctrlAxis::DOWN, KEY_Q, -1, 1.0, 0.0, false, false }; 
		state = kbConf;

		this->id = uid;
		// whatever
	};
};

/// --- globals
tw::threadPool_t threadPool;
jmp::jump_instance    mainInst;
PlayerControllerStream                     P1Ctrl(0);
sim::Game<jmp::player_ctrl_t>        mainGame(&mainInst);
sim::GameSystem<jmp::player_ctrl_t>  gs(mainGame);
SnapRecorder*    snapRec = &gs.snapRec;

sim::view_t mainView{ 0, 0, 640, 480 };
sim::view_t debug_view{ 0, 0, 640, 480 };

game_editor_t editor(mainInst, debug_view);

bool edit_mode = false;

float SimSpeedMult    = 1.f;
float SimSpeedMultPos = 1.f;

/// -- network globals

char     hostaddr[24]    = { "127.0.0.1:1234_" };
uint16_t listenPort      = 1234;
int      debugPacketLoss = 0;
net::CTX_h& netctx       = gs.nctx; 

// TODO move this somewhere general cause its same shit for every game
class netPumpJob_t : public tw::job_t {
	public:
	std::mutex             net_lock;
	virtual void Exec() override
	{
		/// --- SEND AND RECEIVE ALL NETWORK STUFF
		//if (net_lock.try_lock()) {
			gs.PumpNetwork();
		//	net_lock.unlock();
		//	std::this_thread::sleep_for(std::chrono::milliseconds(1));
		//}
		return;
	}
} netPumpJob;


void ConnCloseCB(net::CTX_h ctx, net::CXN_h conn)
{
	// TODO handle this
	printf("Connection Closed\n");
	mainGame.paused = true;
	gs.DelLocalPlayer(0);
	gs.DelLocalPlayer(1);
}
void ConnOpenCB(net::CTX_h ctx, net::CXN_h conn)
{
	// TODO
	printf("Connection Open\n");
	// net::SetSerializer(ctx, conn, new sim::RemoteDeserializer(), false);

	gs.DelLocalPlayer(0);
	gs.DelLocalPlayer(1);

	mainGame.paused = false;

	//SnapShot();
//	gs.game.Reset();
	gs.ForceSnap();
	gs.ResetSlew();

	if (gs.isHost) {
		printf("HOSTING\n");
		//LocalPlayerIX = 0;
		gs.AddLocalPlayer(0, &P1Ctrl);
		gs.AddRemotePlayer(1, conn); // rr, spec, deserializer);
	} else {
		printf("CLIENTING\n");
		//LocalPlayerIX = 1;
		gs.AddLocalPlayer(1, &P1Ctrl);
		gs.AddRemotePlayer(0, conn); // rr, spec, deserializer);
	}


	gs.SendAllSync();

	printf("GAME HAS %zu PLAYERS\n", mainGame.GetStreamCount());

//	*/
}


void ToggleNet() {
	if (netctx == NET_BADVAL) {
		netctx = net::OpenContext(listenPort, net::IPV4PROTO);
		if (netctx != NET_BADVAL) {
			net::SetConnOpenCallback(netctx, ConnOpenCB);
			net::SetConnCloseCallback(netctx, ConnCloseCB);
		} else {
			fprintf(stderr, "FAILED TO OPEN NETWORK\n");
		}
	} else {
		net::FreeContext(netctx);
		netctx = NET_BADVAL;
	}
}

void ConnectToHost()
{
	if (netctx == NET_BADVAL)
		ToggleNet();

	if (netctx == NET_BADVAL)
		return;
	gs.isHost     = false;
	//LocalPlayerIX = 1;
	if (net::ConnectTo(netctx, hostaddr) < 0) {
		printf("Failed to init connection\n");
	}
}



void keyboardEvent(int keycode, int event, const char* str) {
	
	if (event == KEY_EVENT_PRESS) {
		switch(keycode) {
		case KEY_F1:
			edit_mode ^= true;
			break;
		case KEY_F2:// take snapshot
		case KEY_F3:// apply snapshot
		case KEY_F4:// init replay
		case KEY_F5:// rollback
			break;

		case KEY_F7:
			game_editor_t::SaveMap("alice.map", mainInst.map);
			break;
		case KEY_F8:
			game_editor_t::LoadMap("alice.map", mainInst);
			break;
		case KEY_F10:
			// FORCE SEND snapshot ?
			break;
		case KEY_F11:
			ConnectToHost();
			break;
		case KEY_F12:
			ToggleNet();
			break;
		case KEY_P:
			mainGame.paused       ^= 1;
			break;	
		case KEY_O:
			mainGame.doSingleStep = true;
			mainGame.paused       = true;
			break;
		case KEY_LEFT_BRACKET:
			SimSpeedMultPos = std::max(0.f, SimSpeedMultPos - 0.1f);
			break;
		case KEY_RIGHT_BRACKET:
			SimSpeedMultPos = std::min(2.f, SimSpeedMultPos + 0.1f);
			break;

			break;
		default:
			break;	
		}
	}
}
void mouseEvent(gfx::MouseAction_t act, int x, int y, gfx::MouseButtons_t btn) {
	editor.mouseEvent(act, x, y, btn);
}

void pasteEvent(char* buff, int len) { printf("PASTED: %.*s\n", len, buff); }

int main(int argc, char* argv[]) {

	if(argc > 1) {
		for (int i = 1; i + 1 < argc; i += 2) {
			if (strcmp("-p", argv[i]) == 0) {
				// set listen port
				sscanf(argv[i + 1], "%hu", &listenPort);
			} else if (strcmp("-h", argv[i]) == 0) {
				// set a host ip
				snprintf(hostaddr, 24, "%s", argv[i + 1]);
			} else if (strcmp("-q", argv[i]) == 0) {
				// set packet loss out of 100
				sscanf(argv[i + 1], "%d", &debugPacketLoss);
				debugPacketLoss  = std::min(100, std::max(0, debugPacketLoss));
				net::CONNQUALITY = debugPacketLoss;
			}
		}
	}


	/// --- THREADPOOL INIT
	int pool_size = std::min(8, std::max(4, (int)std::thread::hardware_concurrency() - 1));
	threadPool.CreatePool(pool_size);
	std::vector<renderJob> jobs;


	/// --- NETWORK INIT
	// TODO/NOTE
	// lack of net connection should not end play, if netinit failes
	// retry when/if player attempts to start online game
	// TODO
	// Init can block for several seconds while resolving names
	// call it from a seperate thread ?
	if (!net::Init()) {
		fprintf(stderr, "netplay not available\n");
	}
//	threadPool.SubJob(&netPumpJob, tw::LOOP, 0);

	/// --- GRAPHICS INIT
	if (!gfx::Init())
		return -1;
	gfx::SetKBCallback(keyboardEvent);
	gfx::SetMouseCallback(mouseEvent);
	gfx::SetPasteCallback(pasteEvent);

	dst_t screen = {}; 
	renderContext ctx;
	palette_t newpalette
		= { { 0x00000000, 0xff000000, 0xff25009e, 0xffae0083, 0xffff1600, 0xff193ba9,
			0xff5031c5, 0xffa04000, 0xff534b01, 0xff1ea95c, 0xff145d96, 0xff119464,
			0xff545753, 0xff7f61df, 0xffffffff, 0xff5486da, 0xff7c8e28, 0xffff6767,
			0xfff55fd4, 0xff8e918d, 0xfffd8739, 0xff50b48d, 0xffbb94fc, 0xff84b85a,
			0xffd8ac4e, 0xffd6b638, 0xff86befe, 0xffced2cc, 0xff87f3b7, 0xffffd86f,
			0xffb5f189, 0xfffffd45 } };
	ctx.palette = newpalette;
	ctx.palette.GenNames();
	gfx::SetPalette(newpalette.color);

	sprite_t     fntsrc = { 1, 1, NULL };
	paletteMap_t fntmap = {};
	if (!pixmapToSprite((const unsigned char**)font4x6_xpm, ARRSIZE(font4x6_xpm), ctx.palette,
				fntsrc, fntmap)) {
		fprintf(stderr, "Failed to parse font\n");
		return -1;
	}
	printf("fntsrc %d %d\n", fntsrc.width, fntsrc.height);
	monofont fnt{ fntsrc, ctx.palette, {}, 4, 6, 32 };
	Messanger              msger(ctx, fnt);

	mainGame.paused = false;
	mainGame.Reset();
	gs.AddLocalPlayer(0, &P1Ctrl);
	// important to reset timer before starting
	gfx::ResetTimer();

	while(!gfx::NeedExit()) {


		//netPumpJob.net_lock.lock();

		gfx::SwapBuffers();
		gfx::GetCurrentBuffer((void**)&screen.raw32, &screen.width, &screen.height);
		mainView.w = screen.width;
		mainView.h = screen.height;
		debug_view.w = screen.width;
		debug_view.h = screen.height;
		ctx.dst = screen;

		/// --- STEP SIMULATION
		double frameTime = gfx::GetTimeElapsed();
		gfx::ResetTimer();
		SimSpeedMult      = std::pow(SimSpeedMultPos, 3.f);
		int simstepstaken = gs.StepGame(frameTime * SimSpeedMult);




		// update view to follow local player
		auto local_uid = mainInst.findPlayer(0);
		if(local_uid != sim::NULL_ID && !edit_mode) {
			auto* p = mainInst.players.Get(local_uid);
			v3 vp = mainView.worldPos();
			v3 dir = p->vel.normal();
			fp32 dist = p->pos.dist(vp  - dir * 10.f  );
			fp32 mul = smoothstep(dist, 20.f, 90.f);
			mainView.moveTo(vp * (1.f-mul) + (p->pos + dir * 10.f) * mul    );
		} else {
			bool yp = gfx::GetKeyStates()[KEY_KP_8];
			bool yn = gfx::GetKeyStates()[KEY_KP_2];
			bool xp = gfx::GetKeyStates()[KEY_KP_6];
			bool xn = gfx::GetKeyStates()[KEY_KP_4];
			debug_view.y -= yp * 4;
			debug_view.y += yn * 4;
			debug_view.x += xp * 4;
			debug_view.x -= xn * 4;
			mainView = debug_view;
		}

		/// --- RENDERS
		DrawGame(ctx, mainView, mainGame, msger);

		if(edit_mode) 
			editor.draw(ctx);


		int numjobs = std::max(2, std::min(12, pool_size - 2));
		jobs.resize(0);
		FillJobList(ctx, numjobs, jobs);
		for (renderJob& rj : jobs) {
			// rj.Exec();
			threadPool.SubJob(&rj, 0, 1);
		}
		threadPool.WaitBatch(1);

		// purpose?
		ctx.reset();

		if (netctx) {

			msger.BeginMsg(220, 10, 10, 200);
			int y = 10;
			// int               x     = 460;
			int               i     = net::GetNumConnecitons(netctx) - 1;
			const net::CXN_h* conns = net::GetConnections(netctx);
			net::connStats    cs    = {};

			msger.Write("ONLINE: %s", gs.isHost ? "HOST" : "CLIENT" );
			msger.Write(net::GetPublicAddress(netctx).c_str());
			y += 10;
			static char address[48] = {};
			for (; i >= 0; i--) {
				cs = net::GetConnStats(netctx, conns[i]);

				msger.Write("------------", cs.recvPackets);
				net::GetConnAddr(netctx, conns[i], address);
				msger.Write(address);
				msger.Write("ltnc %f", cs.latency);
				msger.Write("rate %d", cs.bitRate);
				msger.Write("recv %d", cs.recvPackets);
				msger.Write("sent %d", cs.sentPackets);
				msger.Write("oldest %d", cs.oldestUnacked);
				msger.Write("uackv %d", cs.unackedPacket);
				msger.Write("o_smsg %d", cs.subMessagesOUT);
				msger.Write("i_smsg %d", cs.subMessagesIN);
				msger.Write("drop %d", cs.droppedPackets);
				msger.Write("stall %d", cs.stall);

				msger.Write("ordbff %d", cs.orderBuffSize);
				msger.Write("snap %d", gs.GetSnapDist());
				msger.Write("slew %0.03f", gs.GetSlew());
				msger.Write("simr %d", gs.GetSimInputRem());

				msger.Write("edge %d", cs.edge);
				msger.Write("%05d %05d %05d %05d", cs.orderEdge[0], cs.orderEdge[1],
						cs.orderEdge[2], cs.orderEdge[3]);
				msger.Write("%05d %05d %05d %05d", cs.orderEdgeRdy[0],
						cs.orderEdgeRdy[1], cs.orderEdgeRdy[2],
						cs.orderEdgeRdy[3]);
			}
		}

		/// --- DISPLAY TEXT
		msger.WriteOut();
		msger.Clear();

		netPumpJob.Exec();
		//netPumpJob.net_lock.unlock();
	}
	threadPool.Join();
	net::CloseAllConnections(netctx);


	return 0;
}
