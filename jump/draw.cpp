

#include "jump.hpp"
#include "render.hpp"
#include "interpolate.hpp"
#include "draw.hpp"
#include "util.hpp"

#include "alice.xpm"
#include "alice.xpm.hpp"

typedef entityQuant<jmp::player_t> playerQuant_t;
std::vector<playerQuant_t> player_pos;

class debugTreeDrawer : public queryCallback_t<jmp::maptile_t> {
    public:
	sim::view_t*   gview;
	rect_t         view;
	renderContext* ctx    = 0;
	uint8_t        color  = 0;
	int            margin = 0;

	debugTreeDrawer() {}
	virtual bool callback(int index, const rect_t& ir, const jmp::maptile_t& mt) override
	{
		rect_t r = ir;
		r.expand(margin);
		rtDrawRect(*ctx, *gview, r, color);
		return 0;
	}
};
debugTreeDrawer mapDrawer;

bool rendering_init = false;
uint8_t sprite_ix = 0;
uint8_t map_ix    = 0;

void InitDrawing(renderContext& ctx) {

	rendering_init = true;

	sprite_ix = ctx.sprites.size();
	map_ix    = ctx.maps.size();

	ctx.sprites.emplace_back();
	ctx.maps.emplace_back();

	ctx.tileDesc.push_back(arr_view<tileDesc_t>(alice_tiles, FIXARRLEN(alice_tiles)));
	if (!pixmapToSprite((const unsigned char**)alice_xpm, ARRSIZE(alice_xpm), ctx.palette,
	                    ctx.sprites.back(), ctx.maps.back())) {
		fprintf(stderr, "Failed to parse pixmap 1\n");
		return;
	}
}

void drawSprite(renderContext& ctx, const tileDesc_t& td, i3 pt) {
	ctx.blit(sprite_ix, map_ix, 
		td.x0, td.y0, td.x1, td.y1,
		pt.x, pt.y);
}

void DrawGame(renderContext& ctx, 
		sim::view_t& mainView, 
		sim::Game<jmp::player_ctrl_t>& mainGame, 
		Messanger& msger) {

	
	if(!rendering_init)
		InitDrawing(ctx);

	jmp::jump_instance& inst = mainGame.CurInst<jmp::jump_instance>();



	ctx.blank(0);	
	msger.BeginMsg(10, 10, 12, 500);
	msger.Write("JUMP GAME: %s",APP_VERSION);
	msger.Write("FRAME %lu MULT %f", mainGame.GetFrameCount(), mainGame.Tmult());
	msger.Write("MAP %zu", inst.map.Size());
	msger.Write("PLAYERS %zu", inst.players.size());
	for(const auto&map : inst.ctrl_map) {
		msger.Write("    %u => %u", map.player_id, map.entity_id.id);
	}
	for(auto player = inst.players.begin(); player != inst.players.end(); player++) {
	
		msger.Write("P%u %dx%d", player.Key().id, (int)player->pos.x, (int)player->pos.y);
	}


	// draw the map
	rect_t viewRect = GetViewRect(mainView);
	mapDrawer.gview  = &mainView;
	mapDrawer.view   = viewRect;
	mapDrawer.ctx    = &ctx;
	mapDrawer.color  = ctx.palette.RED;
	mapDrawer.margin = 0;
	inst.map.Search(viewRect, mapDrawer);

	// draw players
	player_pos.resize(0);
	InterpolateViewPos<jmp::player_t, decltype(inst.players)>(mainView, inst.players, player_pos, mainGame.Tmult());
	int z = 0;

	for (const auto& eq : player_pos) {
		
		uint8_t mycolors[4] = {ctx.palette.RED, ctx.palette.GREEN, ctx.palette.WHITE, ctx.palette.YELLOW};
		uint8_t offset = eq.uid.id % 4u;
		int radius =  9 + offset + z;
		const auto* p = inst.players.Get(eq.uid);

		/*
		ctx.drawCircle(eq.x, eq.y, radius, mycolors[offset] , false);
		z++;

		i3 hp = mainView.screenXYZ(p->hit_pt);
		ctx.drawCircle(hp.x, hp.y, 2, mycolors[offset] , false);
	
		v3 d = p->hit_dir;
		fp32 s = 8.f;
		ctx.drawLine(hp.x, hp.y, hp.x + d.x * s, hp.y + d.y * s, ctx.palette.WHITE);
		*/

		i3 off= i3{-8,-18,0};
		i3 pt = i3{eq.x, eq.y};
		float threshold = 0.2;
		uint8_t sel = idle_R;

		if(p->vel.y < -threshold || !p->grounded) 
			sel = jump_R;
		if(p->vel.y > threshold)
			sel = fall_R;
	

		if(absfp(p->vel.x) > threshold && p->grounded ) {
			int i = mainGame.GetFrameCount() % 16u;
			sel = i < 7 ? walk0_R : idle_R;
			sel = i > 9 ? walk1_R : sel;
		}

		if(p->vel.x < 0)
			sel  += 7;



			
		drawSprite(ctx, alice_tiles[sel], pt + off );
		


	}



}
