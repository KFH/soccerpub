#pragma once
#include "game.hpp"
#include "map.hpp"
#include "binarytree.hpp"
#include "buff.hpp"

namespace jmp {

struct maptile_t {
	enum class types_t : uint8_t { 
		NONE = 0x0, 
		FLOOR = 0x01 
	};
	
	uint8_t  gfx;
	types_t  mtype;
};

typedef rtree<maptile_t, 8, 16> fieldMap_t;

struct player_ctrl_t {
	enum class action_type : uint8_t  {
		NONE,
		JUMP,
		GRAB,
		THROW
	};
	float x;
	action_type action;

};

struct ctrl_map_t {
	sim::playerIX player_id;
	sim::entityID entity_id;
	player_ctrl_t ctrl;

	bool     operator==(const ctrl_map_t& other) const { return player_id == other.player_id; }
	bool     operator==(const sim::playerIX& id)   const { return player_id == id; }
};

struct player_t {

	enum class state_t : uint32_t {
		NONE,
		THROWING,
		INVINCIBLE,
		DEAD,	
	};

	v3 pos;
	v3 vel;
	v3 hit_pt;
	v3 hit_dir;
	v3 p0;

	//  could use state transition count down?
	state_t state;
	uint32_t state_count;
	bool grounded;
};

struct cake_t {
	v3 pos;
	v3 vel;
};

struct connect_t {
	enum class connection_type : uint32_t {
		NONE,
		CARRIED	
	};
	sim::entityID 	e0;
	sim::entityID 	e1;
	v3              pos;

	bool     operator==(const sim::entityID& ent) const { return e0 == ent || e1 == ent; }
};


class jump_instance : 
	public sim::vInstance<player_ctrl_t> 
{

    public:
	sim::g_entityTracker            IDs;
	tree_t<sim::entityID, player_t> players;
	std::vector<ctrl_map_t>         ctrl_map;
	std::vector<connect_t>          connections;

	fieldMap_t                      map;

	virtual int  Init()                                             override;
	virtual void Step(int frameNum, jump_instance::input_vec_t& in) override;
	virtual void Serialize(ByteBuffer& buff) const                  override;
	virtual void DeSerialize(ByteBuffer& buff)                      override;

	sim::entityID findPlayer(sim::playerIX pix) {
		auto it = std::find(ctrl_map.begin(), ctrl_map.end(), pix);
		if(it != ctrl_map.end())
			return it->entity_id;

		return sim::NULL_ID;
	}
};

}
