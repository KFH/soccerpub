#pragma once
static tileDesc_t const alice_tiles[] = {
	{2,1,19,28,0,0},
	{1,30,20,44,0,0},
	{7,47,23,69,0,0},
	{1,73,25,96,0,0},
	{1,102,19,128,0,0},
	{5,132,23,157,0,0},
	{1,161,24,191,0,0},

	{34,1,52,28,0,0},
	{35,30,54,44,0,0},
	{31,47,48,69,0,0},
	{29,73,53,96,0,0},
	{33,103,53,128,0,0},
	{31,132,49,157,0,0},
	{31,161,53,191,0,0}
};
enum alice_names {
	idle_R = 0,
	crawl_R = 1,
	crouch_R = 2,
	jump_R = 3,
	walk0_R = 4,
	walk1_R = 5,
	fall_R = 6,
	idle_L = 7,
	crawl_L = 8,
	crouch_L = 9,
	jump_L = 10,
	walk0_L = 11,
	walk1_L = 12,
	fall_L = 13,
};
