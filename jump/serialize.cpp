#include "jump.hpp"

#include <cstring>

namespace jmp {

#pragma pack(push, 1)
struct serialDefinition_t {
	enum class types : uint8_t {
		UIDS,
		PLAYERS,
		CTRLMAP,
		FIELD,
	};
	types mtype;
	uint16_t     len;
};
#pragma pack(pop)


#define SERIALIZE_TREE(sd, vtype, stree, buff)                                                     \
	{                                                                                          \
		sd.mtype = vtype;                                                                  \
		sd.len   = stree.SerializeReqSize();                                               \
		buff.append(&sd, sizeof(sd));                                                      \
		size_t old_buff_len = buff.len();                                                  \
		buff.resize(buff.len() + sd.len);                                                  \
		stree.Serialize(buff.data() + old_buff_len);                                       \
	}

#define SERIALIZE_ARR(sd, vtype, arr, buff)  \
{\
	sd.mtype = vtype; \
	sd.len   = sizeof(decltype(arr)::value_type) * arr.size(); \
	buff.append(&sd, sizeof(sd));\
	buff.append(arr.data(), sd.len); \
}\


void jump_instance::Serialize(ByteBuffer& buff) const {


//	serialDefinition_t sd{ serialDef::INSTANCE, 0 };
//	buff.append(&sd, sizeof(sd));

/*
//	sim::g_entityTracker             IDs;
//	tree_t<sim::entityID, player_t>  players;
//	tree_t<sim::entityID, connect_t> connections;
//	fieldMap_t                  map;
*/
	serialDefinition_t sd;
	SERIALIZE_TREE(sd, serialDefinition_t::types::UIDS,        IDs, buff)

	SERIALIZE_TREE(sd, serialDefinition_t::types::PLAYERS, players, buff)

	SERIALIZE_ARR(sd, serialDefinition_t::types::CTRLMAP, ctrl_map, buff)

};

#define DESERIALIZE_TREE(tree, tname)                                                              \
	{                                                                                          \
		if (buff.unread() >= sd.len) {                                                     \
			tree.Deserialize(buff.head(), sd.len);                                     \
			buff.read(nullptr, sd.len);                                                \
		} else {                                                                           \
			printf("failed to deserialize %s\n", tname);                               \
		}                                                                                  \
	}

#define DESERIALIZE_ARR(arr, tname) \
	{                                                                                          \
		if (buff.unread() >= sd.len) {                                                     \
			arr.resize(sd.len /  sizeof(decltype(arr)::value_type));                   \
			memcpy(arr.data(), buff.head(), sd.len);				   \
			buff.read(nullptr, sd.len);                                                \
		} else {                                                                           \
			printf("failed deserialize %s\n", tname);                                  \
		}                                                                                  \
	}


void jump_instance::DeSerialize(ByteBuffer& buff) {

	printf("DESERIALZING\n");
	serialDefinition_t sd;
	// read ahead until INSTANCE is encountered
//	while (buff.unread()) {
//		buff.read(&sd, sizeof(sd));
//		if (sd.stype == serialDef::INSTANCE)
//			break;
//	}

	while (buff.unread()) {
		buff.read(&sd, sizeof(sd));

		switch(sd.mtype) {
			case serialDefinition_t::types::UIDS:
				DESERIALIZE_TREE(IDs, "IDs")
				break;
			case serialDefinition_t::types::PLAYERS:
				DESERIALIZE_TREE(players, "players")
				break;
			case serialDefinition_t::types::CTRLMAP:
				DESERIALIZE_ARR(ctrl_map, "ctrl_map");
				break;
			case serialDefinition_t::types::FIELD:
			default:
				break;
		}

	}

	for(auto player = players.begin(); player != players.end(); player++) {
		printf("P%u POS %0.3fx%0.3fx%0.3f VEL %0.3fx%0.3fx%0.3f\n", 
			player.Key().id, 
			player->pos.x, player->pos.y, player->pos.z,
			player->vel.x, player->vel.y, player->vel.z
			);
	}

};

};
