#include "jump.hpp"
#include <cmath>
#include <cassert>

namespace jmp {

int  jump_instance::Init() {
	// TODO
	// create some players randomly

	IDs.Reset();
	players.Reset();
	ctrl_map.resize(0);

	if(map.Size() < 1) {

		map.Insert(maptile_t{0, maptile_t::types_t::FLOOR}, {-200, 40, -120, 60 });	
		map.Insert(maptile_t{0, maptile_t::types_t::FLOOR}, {-120, 100, -50, 120 });	

		map.Insert(maptile_t{0, maptile_t::types_t::FLOOR}, {-50, 150, -10, 160 });	

		map.Insert(maptile_t{0, maptile_t::types_t::FLOOR}, {-170, 180, -150, 200} );
		map.Insert(maptile_t{0, maptile_t::types_t::FLOOR}, {-200, 200, 200, 210 } );
	}

	auto id = IDs.Create();
	v3 pos = {100,0,0};
	v3 vel = {0,0,0};
	players.Insert(id, {pos, vel, v3{}, v3{}, v3{}, player_t::state_t::NONE, false});

	return 0;
};
inline rect_t rtBoxPoint(v3 p, int margin)
{
	v3  np = p.round();
	int x  = int(np.x);
	int y  = int(np.y);
	return rect_t{ x - margin, y - margin, x + margin, y + margin };
}

inline v3 rectCenter(const rect_t& ir) {
	return  v3{fp32(ir.x0 + ir.x1), fp32(ir.y0 + ir.y1), 0.f} * 0.5f;
}
class playerCollisionQuery : public queryCallback_t<maptile_t> {
    public:
	player_t& me;
	int     x, y;
	v3      npos;
	fp32    mx, my;
	fp32    margin;
	bool    hasHit;
	playerCollisionQuery(player_t& player)
	    : me(player)
	{
		margin = fp32(8.f);
		x           = !signfp(me.vel.x);
		y           = !signfp(me.vel.y);
		npos        = me.pos + me.vel;
		mx          = fp32(x);
		my          = fp32(y);
		npos.x += mx;
		npos.y += my;
		npos = npos + me.vel.normal() * margin;
		hasHit = false;
		me.grounded = false;
	}
	// TODO should determine a deflection point and plane/planes
	// based on all returned intersections
	virtual bool callback(int index, const rect_t& ir, const maptile_t& i) override
	{
		//if (hasHit)
		//	return 0;
		// determine wich edges to test against	
		fp32 x0  = x ? fp32(ir.x0) : fp32(ir.x1);
		fp32 x1  = x ? fp32(ir.x1) : fp32(ir.x0);
		fp32 y0  = y ? fp32(ir.y0) : fp32(ir.y1);
		fp32 y1  = y ? fp32(ir.y1) : fp32(ir.y0);
		v3   p0  = v3{ x0, y0, 0 };

		v3 hit_pt0 = ClosestPtOnLine(p0, v3{ x1, y0, 0 }, me.pos);
		v3 hit_pt1 = ClosestPtOnLine(p0, v3{ x0, y1, 0 }, me.pos);
		v3 np0, np1;

		v3 cent_dir = (rectCenter(ir) - me.pos).normal();

		bool updown = (me.pos.dist(hit_pt0) < margin);
		bool leftright = (me.pos.dist(hit_pt1) < margin);

		/*
		if(updown & leftright) {

			v3 dir = (hit_pt0 - me.pos).normal();
			np0 = hit_pt0 + dir * -margin;	

			dir = (hit_pt1 - me.pos).normal();
			np1 = hit_pt1 + dir * -margin;			

			me.pos = (np0 + np1) * 0.5f;
			me.vel = v3{0,0,0};

			me.hit_pt = (hit_pt0 + hit_pt1) * 0.5;
			printf("HIT CORNER\n");

		} else */
		if(updown) {

			v3 dir = (hit_pt0 - me.pos).normal();
			dir = dir.dot(cent_dir) < 0.f ? dir * -1.f : dir;

			me.vel = me.vel * v3{ 1.f, 0.f, 0.f };
			me.pos = hit_pt0 + (dir * -margin);	
			me.hit_pt = hit_pt0;
			me.hit_dir = dir;
			
			if(dir.dot(v3{0,1,0}) > 0.f) {
				me.grounded = true;
			} else {
				printf("HIT CEILING\n");
			}

		} 
		if(leftright) {

			v3 dir = (hit_pt1 - me.pos).normal();
			dir = dir.dot(cent_dir) < 0.f ? dir * -1.f : dir;

			me.pos = hit_pt1 + dir * -margin;
			me.vel = me.vel * v3{ 0.f, 1.f, 0.f};
			me.hit_pt = hit_pt1;
			me.hit_dir = dir;
		}
		

		hasHit |= updown | leftright;

		return 0;
	}
};

class respawnCB : public queryCallback_t<maptile_t> {
	public:
	rect_t r;
	bool hashit = false;
	virtual bool callback(int index, const rect_t& ir, const maptile_t& i) override {
		hashit = true;
		r = ir;
		return 0;	
	}
};

void jump_instance::Step(int frameNum, jump_instance::input_vec_t& in) {

	// TODO
	//
	// map players id input to an entity id
	//
	// check collision against map for all players
	//
	// apply all player actions + movement if possible
	// apply connections
	//
	// check collision again

	for(auto &p : players) {
		if(p.pos.y > 500) {
			respawnCB cb;
			map.Search(rect_t{i32(p.hit_pt.x)-3, i32(p.hit_pt.y)-3, i32(p.hit_pt.x)+3, i32(p.hit_pt.y)+3}, cb);
			if(cb.hashit) {
				float x= (cb.r.x0 + cb.r.x1) * 0.5f;
				float y= (cb.r.y0 + cb.r.y1) * 0.5f;
				v3 dir = (p.hit_pt - v3{x,y,0.f} ).normal();

				p.pos = p.hit_pt - v3{dir.x * 8.f, 40.f, 0};
			}else {
				p.pos = p.hit_pt - v3{0,20.f,0};
			}
		}
	}


	// map all inputs to a player entity for control
	for (auto& ctrl : in) {
		auto it = std::find(ctrl_map.begin(), ctrl_map.end(), ctrl.player_id);
		if(it != ctrl_map.end()) {
			// player input is mapped to an entity
			it->ctrl = ctrl.ctrl;	
		} else {
			printf("MAKING PLAYER\n");
			// player is unmapped create entity or something
			auto id = IDs.Create();
			v3 pos = {0,0,0};
			v3 vel = {0,0,0};
			players.Insert(id, {pos, vel, v3{}, v3{}, v3{}, player_t::state_t::NONE, false});
			ctrl_map.push_back(ctrl_map_t{ctrl.player_id, id, ctrl.ctrl});
		}
	}

	// apply all mapped controls to entities
	for(const auto& mapping : ctrl_map) {
		auto& player = *players.Get(mapping.entity_id);
		float x = mapping.ctrl.x;
		if(!std::isfinite(x)) {
			fprintf(stderr, "PLAYER INPUT IS OUT OF BOUNDS\n");
			assert(false);
		}

		player.vel = player.vel + v3{x * 0.13f, 0.f, 0.f};
		// TODO
		// if being held break bond by jumping
		if(mapping.ctrl.action == player_ctrl_t::action_type::JUMP && player.grounded) {
			player.vel = player.vel + v3{0.f,-20.f,0.f};
			printf("JUMP\n");
		}

		if(mapping.ctrl.action == player_ctrl_t::action_type::GRAB && player.state_count == 0) {

			// if holding something throw it	
			auto con = std::find(connections.begin(), connections.end(), mapping.entity_id);
			if(con != connections.end()) {
				// erase connection and throw
				if(con->e0 == mapping.entity_id) {
					v3 dir = (player.vel.normal() + v3{0,-1,0}).normal();
					players.Get(con->e1)->vel = dir * 8.f;
				}
				connections.erase(con);
				player.state_count = 5;


			// if holding nothing try to grab something
			} else if(player.grounded) {
				for(auto it = players.begin(); it != players.end(); it++) {
					if(it.Key() == mapping.entity_id)
						continue;
					if(it->pos.dist(player.pos) < 40.f)  {
						connections.push_back({mapping.entity_id, it.Key(), v3{0, -30, 0}});
						player.state_count = 5;
					}

				}	

			}	
		}
	}

	// apply gravity
	for(auto& player : players) {
		player.vel = player.vel * 0.93f;
		player.vel = player.vel + v3{0.f, 0.5f, 0.f};
	}

	float radius = 10.f;

	// for all entities move and collision test

	for(auto& player : players) {
		if(player.state_count > 0)
			player.state_count--;
		player.pos = player.pos + player.vel;	
		playerCollisionQuery cb(player);
		map.Search(rtBoxPoint(player.pos, int(player.vel.len()) + radius), cb);
	}

	for(const auto& con : connections) {
		auto* p0 = players.Get(con.e0);
		auto* p1 = players.Get(con.e1);

		p1->pos = p0->pos + con.pos;
		p1->vel = p0->vel;
	}

};

};
