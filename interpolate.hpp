
#pragma once

// quantized screen relative position
template <typename entityType> struct entityQuant {
	int           x, y, yz, dir;
	sim::entityID uid;
	// entityType* me;
};

#define FIXEDSTEPVAL 1.0 / 60.0

enum DIRECTIONS {
	DIR_EE = 0, // 6 heading limit to game
	DIR_WW,
	DIR_NN,
	DIR_SS,
	DIR_SW,
	DIR_NW,
	DIR_SE,
	DIR_NE
};

inline int quantizeDirection(const v3& vel)
{
	int frame = 0;
	if (vel.len2() > fp32(0.15)) {
		// categorize players vel into one of 6 quadrants to pick sprite that faces
		// correct direciton, need only 4 bits, 16 cases?
		v3            vn     = vel.normal();
		int           dirsel = 0;
		fp32          absx   = std::abs(vn.x);
		fp32          absy   = std::abs(vn.y);
		constexpr int MY     = 0;
		constexpr int MX     = 0x01; // x is largest value
		constexpr int PX     = 0x02;
		constexpr int PY     = 0x04;
		constexpr int DIAG   = 0x08;
		dirsel |= absx > absy;
		dirsel |= (vn.x > 0) << 1;
		dirsel |= (vn.y > 0) << 2;
		dirsel |= ((absx < 0.92) & (absx > 0.38)) | ((absy < 0.92) & (absy > 0.38)) << 3;

		switch (dirsel) {
		case MX | PX | PY:
		case MX | PX:
			frame = DIR_EE; // right
			break;
		case MX | PY:
		case MX:
			frame = DIR_WW; // left
			break;
		case MY:
		case MY | PX:
			frame = DIR_NN; // up
			break;
		case MY | PY:
		case MY | PY | PX:
			frame = DIR_SS; // down
			break;
		case PY | DIAG | MX:
		case PY | DIAG:
			frame = DIR_SW; // down left
			break;
		case DIAG | MX:
		case DIAG:
			frame = DIR_NW; // up left
			break;
		case PX | PY | DIAG:
		case PX | PY | DIAG | MX:
			frame = DIR_SE; // down right
			break;
		case PX | DIAG:
		case PX | DIAG | MX:
			frame = DIR_NE; // up right
		}
	}
	return frame;
}

template <typename entityType>
int InterpolateViewPos(const sim::view_t& v, const entityType& e, entityQuant<entityType>& eqs,
                       double tmult, bool zoff = true)
{

	v3   vpos  = v3{ v.x, v.y, 0 }.round();
	fp32 xh    = v.w / 2;
	fp32 yh    = v.h / 2;
	v3   np    = e.pos + (e.vel * tmult);
	v3   delta = np.round() - vpos;
	if (zoff)
		;
	int xd = delta.x + xh;
	int yd = delta.y + yh;

	eqs.x   = xd;
	eqs.y   = yd;
	eqs.yz  = yh + (delta.y - delta.z);
	eqs.dir = quantizeDirection(e.vel);
	eqs.uid = sim::NULL_ID;

	return 1;
}

template <typename entityType, typename iterable_t>
int InterpolateViewPos(const sim::view_t& v, iterable_t& ents,
                       std::vector<entityQuant<entityType>>& eqs, double tmult, bool zoff = true)
{
	// IDEA
	// use inverse velocity * accumulator / fixed step size to create an interpolated position
	// TODO/ALTERNATE
	// store entities previous frame and current frame position to construct direction of
	// movement
	// independant of any velocity

	// if new position is in view_t bounding box, quantize pos
	int  insize = eqs.size();
	v3   vpos   = v3{ v.x, v.y, 0 }.round();
	fp32 xh     = v.w / 2;
	fp32 yh     = v.h / 2;
	for (auto it = ents.begin(); it != ents.end(); it++) {
		entityType& e     = *it;
		v3          np    = e.pos + (e.vel * tmult);
		v3          delta = np.round() - vpos;
		// if(zoff)
		// 	delta.y -= delta.z;
		int xd = delta.x + xh;
		int yd = delta.y + yh;
		// TODO only if in view_t
		if ((xd < v.w) & (yd < v.h)) {
			eqs.push_back(
			    entityQuant<entityType>{ xd, yd, int(yh + (delta.y - delta.z)),
			                             quantizeDirection(e.vel), it.Key() });
		}
	}
	return (int)eqs.size() - insize;
}

static v3 InterpolatePos(const v3& pos, const v3& vel, double tmult)
{
	//
	return pos + (vel * tmult);
}
