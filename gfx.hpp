#pragma once
#include <cstdint>
#include <chrono>

// TODO
// rename/reorganize this into general platform layer
namespace gfx {
int         Init();
void        Exit();
int         NeedExit();
void        SetPalette(uint32_t* pal);
void        GetCurrentBuffer(void** raw32, int* width, int* height);
void        SwapBuffers();
const bool* GetKeyStates();
void        ResetTimer();
double      GetTimeElapsed();
void        GetScreenDim(int& width, int& height);

typedef std::chrono::steady_clock::time_point steady_time_t;
steady_time_t                                 GetCurrentTime();
double GetTimeElapsed(const steady_time_t& now, const steady_time_t& then);

typedef void (*keyboardCallback)(int keycode, int event, const char* str);
void SetKBCallback(keyboardCallback kbcb);

enum class MouseAction_t : uint8_t {
	// these should probably go somewhere else
	MOVE,
	PRESS,
	RELEASE
};

enum class MouseButtons_t : uint8_t {
	NONE       = 0x00,
	LEFT       = 0x01,
	MIDDLE     = 0x02,
	RIGHT      = 0x04,
	SCROLLUP   = 0x08,
	SCROLLDOWN = 0x10,
	FORWARD    = 0x20,
	BACKWARD   = 0x40
};

struct mouse_state_t {
	double   win_x;
	double   win_y;
	uint32_t buttons;
	bool     isHeld(MouseButtons_t btn) const { return (buttons & uint32_t(btn)) > 0; }
};
const mouse_state_t& GetMouseState();

// typedef void (*mouseCallback)(MouseButtons_t act, int x, int y, MouseButtons_t btn);
typedef void (*mouseCallback)(MouseAction_t act, int x, int y, MouseButtons_t btn);
void SetMouseCallback(mouseCallback mcb);

typedef void (*pasteCallback)(char* data, int length);
void SetPasteCallback(pasteCallback pcb);
} // namespace gfx
