
#include <algorithm>

#include <cmath>
#include <mutex>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "gamecontrols.hpp"

#include "game.hpp"
#include "instance.hpp"
#include "gamerend.hpp"
#include "render.hpp"

#include "gfx.hpp"
#include "keys.hpp"
#include "netapi.hpp"
//#include "platform.hpp"
#include "localcontrols.hpp"
#include "map.hpp"
#include "rtree.hpp"
#include "snap.hpp"
#include "system.hpp"
#include "threadwork.hpp"
#include "util.hpp"
#include "menu.hpp"
#include "interpolate.hpp"
#include "geditor.hpp"

#include "binarytree.hpp"

// sprite sheets
extern "C" {
#include "font4x6.xpm"
#include "pic.xpm"
#include "tiles.xpm"
#include "title.xpm"
}
#include "pic.xpm.hpp"
#include "tiles.xpm.hpp"

int needExit = 0;
int wantQuit() { return needExit | gfx::NeedExit(); }

tw::threadPool_t threadPool;
std::mutex       gs_lock;
sim::Instance    mainInst;
sim::Game        mainGame(&mainInst);
sim::GameSystem  gs(mainGame);
SnapRecorder*    snapRec = &gs.snapRec;

m::menuManager mainMenuManager;

bool displayDebug = false;

// TODO delete this
bool             testTreeAddNode  = false;
bool             testTreeDelNode  = false;
bool             testTreeTraverse = false;
int              testTreeCursor   = 0;
tree_t<int, int> testTree;
int              treecounter = 0;

// ByteBuffer buff;
ByteBuffer snapshotBuff[2];
uint64_t   frame;

PlayerControllerStream P1Menu(0);
PlayerControllerStream P1Ctrl(0);
LocalRecordedStream    P1Rec(&P1Ctrl);
sim::playerIX          LocalPlayerIX = 0;
sim::playerID          LocalPlayerID = 0;
net::CTX_h&            netctx        = gs.nctx; /// { NET_BADVAL };

char     hostaddr[24]    = { "127.0.0.1:1234_" };
uint16_t listenPort      = 1234;
int      debugPacketLoss = 0;

sim::view_t mainView{ 0, 0, 640, 480 };

gameEditor editor(mainView);
v3         debugViewPos = { 0, 0, 0 };
bool       debugView    = false;

map_t      mapSpaces;
fieldMap_t testmap;

extern uint64_t defaultMap asm("defaultMap");
extern int      defaultMap_size asm("defaultMap_size");

float SimSpeedMult    = 1.f;
float SimSpeedMultPos = 1.f;

bool doDisplayMenu() { return gs.GetNumPlayers() < 1 && debugView == false; }

class EndReplayStreamCalllback : public sim::InputStreamExhaustionCB {
	virtual int Callback(sim::Game& me, sim::InputStream* stream) override
	{
		printf("EXHAUSTION CALLBACK\n");
		me.m_cb         = 0;
		P1Rec.target    = &P1Ctrl;
		mainGame.paused = true;
		return -1;
	}
} endReplayCB;

ByteBuffer& GetSnapshotBuffer()
{
	int i = 0; // ((mainGame.GetFrameCount()-1) / 10) % 2;
	return snapshotBuff[i];
}

void SnapShot()
{
	ByteBuffer& buff = GetSnapshotBuffer();
	buff.resize(0);
	mainGame.SnapShot(buff, frame);
	P1Rec.free();
	printf("Snapshot size %zu bytes\n", buff.len());
}
void ApplySnapshot()
{
	ByteBuffer& buff = GetSnapshotBuffer();
	if (buff.len() < 1)
		return;
	printf("Reload\n");
	buff.readback(buff.len());
	P1Rec.free();
	mainGame.SetState(buff, frame);
	mainGame.paused = false;
}
void InitReplay()
{
	ByteBuffer& buff = GetSnapshotBuffer();
	if (buff.len() < 1)
		return;
	printf("REPLAY\n");
	buff.readback(buff.len());
	P1Rec.reset();
	P1Rec.target = 0;
	mainGame.SetState(buff, frame);
	mainGame.m_cb   = &endReplayCB;
	mainGame.paused = false;
}

void StartNewGame()
{
	P1Rec.free();
	mainGame.Reset();
}
void TogglePlayerOne()
{
	if (mainGame.GetInputStream(0)) {
		// mainGame.SetInputStream(0, NULL);
		gs.DelLocalPlayer(0);
	} else {
		// mainGame.SetInputStream(0, &P1Rec);
		gs.AddLocalPlayer(0, &P1Ctrl);
	}
}

void ConnCloseCB(net::CTX_h ctx, net::CXN_h conn)
{
	// TODO handle this
	printf("Connection Closed\n");
	mainGame.paused = true;
	gs.DelLocalPlayer(0);
	gs.DelLocalPlayer(1);
}
void ConnOpenCB(net::CTX_h ctx, net::CXN_h conn)
{
	// TODO
	printf("Connection Open\n");
	// net::SetSerializer(ctx, conn, new sim::RemoteDeserializer(), false);

	gs.game.Reset();
	gs.DelLocalPlayer(0);
	mainGame.paused = false;
	SnapShot();
	gs.ForceSnap();
	gs.ResetSlew();
	gs.SendAllSync();

	if (gs.isHost) {
		LocalPlayerIX = 0;
		gs.AddLocalPlayer(0, &P1Ctrl);
		gs.AddRemotePlayer(1, conn); // rr, spec, deserializer);
	} else {
		LocalPlayerIX = 1;
		gs.AddLocalPlayer(1, &P1Ctrl);
		gs.AddRemotePlayer(0, conn); // rr, spec, deserializer);
	}
}

class netPumpJob_t : public tw::job_t {
	virtual void Exec() override
	{
		// TODO acquire gs mutex
		/// --- SEND AND RECEIVE ALL NETWORK STUFF

		if (gs_lock.try_lock()) {
			gs.PumpNetwork(1.0);
			gs_lock.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		return;
	}
} netPumpJob;

void ToggleNet()
{
	if (netctx == NET_BADVAL) {
		netctx = net::OpenContext(listenPort, net::IPV4PROTO);
		if (netctx != NET_BADVAL) {
			net::SetConnOpenCallback(netctx, ConnOpenCB);
			net::SetConnCloseCallback(netctx, ConnCloseCB);
		} else {
			fprintf(stderr, "FAILED TO OPEN NETWORK\n");
		}
	} else {
		net::FreeContext(netctx);
	}
}
void ConnectToHost()
{
	if (netctx == NET_BADVAL)
		ToggleNet();

	if (netctx == NET_BADVAL)
		return;
	gs.isHost     = false;
	LocalPlayerIX = 1;
	if (net::ConnectTo(netctx, hostaddr) < 0) {
		printf("Failed to init connection\n");
	}
}

void StartHosting()
{
	StartNewGame();
	TogglePlayerOne();
	ToggleNet();
}

void DoNetStuff() { return; }

void RollBack()
{
	mainGame.paused = true;
	uint64_t f      = mainGame.GetFrameCount();
	snapRec->RollBack(mainGame);
	int n = f - mainGame.GetFrameCount();
	// dump n recorded inputs
	P1Rec.pop_back(n);
}

void ToggleKill()
{
	printf("kill ai\n");
	static bool isKill = false;
	isKill ^= 1;
	if (isKill) {
		printf("AI off\n");
		for (auto& team : mainGame.CurInst<sim::Instance>().teamManager) {
			team.teamid = 123;
		}
		for (auto& p : mainGame.CurInst<sim::Instance>().players) {

			p.cmd = sim::aiCMD_t{ sim::aiCMD_t::NONE, v3{}, 0 };
		}
	} else {
		printf("AI on\n");
		uint8_t teamid = 0;
		for (auto& team : mainGame.CurInst<sim::Instance>().teamManager) {
			team.teamid = teamid;
			teamid += 1;
		}
	}
}

void keyboardEvent(int keycode, int event, const char* str)
{

	if (doDisplayMenu()) {
		mainMenuManager.Keyevent(keycode, event, str);
	}

	if (debugView)
		editor.keyboardEvent(keycode, event);
	// put debug key reaction crap here
	if (event == KEY_EVENT_PRESS) {

		switch (keycode) {
		case KEY_T:
			testTreeAddNode = true;
			// add node to tree
			break;
		case KEY_R:
			testTreeDelNode = true;
			break;
		case KEY_Y:
			testTreeTraverse = true;
			// traverse tree in order
			break;
		case KEY_J:
			testTree.GetLeft(testTreeCursor);
			break;
		case KEY_L:
			testTree.GetRight(testTreeCursor);
			break;
		case KEY_K:
			testTree.GetParent(testTreeCursor);
			break;
		case KEY_BACKSPACE:
			testTree.Delete(testTreeCursor);
			testTreeCursor = testTree.GetRoot();
			break;
		case KEY_F1:
			displayDebug ^= 1;
			break;
		case KEY_F2:
			SnapShot();
			break;
		case KEY_F3:
			ApplySnapshot();
			break;
		case KEY_F4:
			InitReplay();
			break;
		case KEY_F5:
			RollBack();
			break;
		case KEY_F6:
			TogglePlayerOne();
			break;
		case KEY_F7:
			ToggleKill();
			break;
		case KEY_F8:
			debugView ^= 1;
			break;
		case KEY_F10:
			gs.ResetSlew();
			break;
		case KEY_F11:
			ConnectToHost();
			break;
		case KEY_F12:
			ToggleNet();
			break;
		case KEY_ESCAPE:
			needExit = 1;
			break;
		case KEY_P:
			mainGame.paused ^= 1;
			printf("PAUSED\n");
			break;
		case KEY_LEFT_BRACKET:
			mainGame.doSingleStep = true;
			mainGame.paused       = true;
			break;
		case KEY_MINUS:
			SimSpeedMultPos = std::max(SimSpeedMultPos - 0.1f, 0.f);
			break;
		case KEY_EQUAL:
			SimSpeedMultPos = std::min(SimSpeedMultPos + 0.1f, 2.f);
			break;
		default:
			// unhandled
			break;
		}

	} else {
	}
}

struct {
	int act = 0, x = 0, y = 0, btn = 0;

} mousePos;
void (*mousePressCB)(int act, int x, int y, int btn)   = 0;
void (*mouseReleaseCB)(int act, int x, int y, int btn) = 0;

void mouseEvent(int act, int x, int y, int btn)
{

	if (btn == 1) {
		if (act == gfx::MBTN_PRESS && mousePressCB)
			mousePressCB(act, x, y, btn);
		if (act == gfx::MBTN_RELEASE && mouseReleaseCB)
			mouseReleaseCB(act, x, y, btn);
	}

	if (debugView)
		editor.mouseEvent(act, x, y, btn);

	mousePos.act = act;
	mousePos.x   = x;
	mousePos.y   = y;
	mousePos.btn = btn;
};

struct {
	int  x0 = 0, y0 = 0, x1 = 0, y1 = 0, val = map_t::ALL_WALL;
	bool active = false;
} dbgRtreeSqr;

int mapUnitSize = 32;

void dbgRtreeStartDraw(int act, int x, int y, int btn)
{

	int w, h;
	gfx::GetScreenDim(w, h);
	x = (float)x / (float)w * (float)mainView.w;
	y = (float)y / (float)h * (float)mainView.h;

	// TODO
	// determine mouse to world x y coord
	dbgRtreeSqr.x0     = rdiv(mainView.worldX(x), mapUnitSize);
	dbgRtreeSqr.y0     = rdiv(mainView.worldY(y), mapUnitSize);
	dbgRtreeSqr.active = true;

	printf("VIEW @ %f %f\n", mainView.x, mainView.y);
	printf("VIEW X %d %d\n", x, mainView.worldX(x));
	printf("START INSERT %d %d\n", dbgRtreeSqr.x0, dbgRtreeSqr.y0);
}
void dbgRtreeApplyDraw(int act, int x, int y, int btn)
{
	if (!dbgRtreeSqr.active)
		return;

	int w, h;
	gfx::GetScreenDim(w, h);
	x = (float)x / (float)w * (float)mainView.w;
	y = (float)y / (float)h * (float)mainView.h;

	// TODO
	// determine mouse to world x y coord
	// insert cube from p0 to p1 into map spaces
	dbgRtreeSqr.x1 = rdiv(mainView.worldX(x), mapUnitSize);
	dbgRtreeSqr.y1 = rdiv(mainView.worldY(y), mapUnitSize);
	printf("APPLY INSERT %d %d\n", dbgRtreeSqr.x1, dbgRtreeSqr.y1);

	int    minx = std::min(dbgRtreeSqr.x0, dbgRtreeSqr.x1);
	int    miny = std::min(dbgRtreeSqr.y0, dbgRtreeSqr.y1);
	int    maxx = std::max(dbgRtreeSqr.x0, dbgRtreeSqr.x1);
	int    maxy = std::max(dbgRtreeSqr.y0, dbgRtreeSqr.y1);
	rect_t r    = rect_t{ minx, miny, maxx, maxy };

	printf("RECT %d %d %d %d\n", r.x0, r.y0, r.x1, r.y0);

	mapSpaces.Merge(dbgRtreeSqr.val, r);
	// mapSpaces.Insert(dbgRtreeSqr.val, r);

	dbgRtreeSqr.active = false;
}

void pasteEvent(char* buff, int len) { printf("PASTED: %.*s\n", len, buff); }

void printHelp()
{
	printf("COMMANDS:\n");
	printf("-p set network port\n");
	printf("-h set network host 'ip:port'\n");
	printf("-q set network quality 0-100");
	printf("-s set seed\n");
}

// menu callbacks

void Quit_cb() { needExit = 1; }

void SinglePlayerGame_cb()
{

	StartNewGame();
	gs.DelLocalPlayer(0);
	gs.DelLocalPlayer(1);
	gs.AddLocalPlayer(0, &P1Ctrl);
}

int main(int argc, char* argv[])
{
	if (argc > 2) {
		for (int i = 0; i < argc; i++) {
			printf("%s\n", argv[i]);
		}
		for (int i = 1; i + 1 < argc; i += 2) {
			if (strcmp("-p", argv[i]) == 0) {
				// set listen port
				sscanf(argv[i + 1], "%hu", &listenPort);
			} else if (strcmp("-h", argv[i]) == 0) {
				// set a host ip
				snprintf(hostaddr, 24, "%s", argv[i + 1]);
			} else if (strcmp("-q", argv[i]) == 0) {
				// set packet loss out of 100
				sscanf(argv[i + 1], "%d", &debugPacketLoss);
				debugPacketLoss  = std::min(100, std::max(0, debugPacketLoss));
				net::CONNQUALITY = debugPacketLoss;
			} else if (strcmp("-s", argv[i]) == 0) {
				// set seed
				int seedval = 1;
				sscanf(argv[i + 1], "%d", &seedval);
				srand(seedval);
			} else {
				printHelp();
				return 0;
			}
		}
	} else if (argc > 1) {
		printHelp();
		return 0;
	}

	printf("INIT\n");

	// Minimum number of threads
	// 1 network, 1 render, 1 audio, 1 extra?
	int pool_size = std::min(8, std::max(4, (int)std::thread::hardware_concurrency() - 1));
	threadPool.CreatePool(pool_size);
	printf("THREADS %d\n", pool_size);

	if (!gfx::Init())
		return -1;

	// TODO/NOTE
	// lack of net connection should not end play, if netinit failes
	// retry when/if player attempts to start online game
	// TODO
	// Init can block for several secods while resolving names
	// call it from a seperate thread ?

	if (!net::Init()) {
		fprintf(stderr, "NETPLAY NOT AVAILABLE\n");
	}

	gfx::SetKBCallback(keyboardEvent);
	gfx::SetMouseCallback(mouseEvent);
	gfx::SetPasteCallback(pasteEvent);

	// mousePressCB   = dbgRtreeStartDraw;
	// mouseReleaseCB = dbgRtreeApplyDraw;

	// gs.AddLocalPlayer(0, &P1Ctrl);

	dst_t screen = {}; // = {0,0,640,480,0,NULL,(uint32_t*)malloc(640*480*4)};
	// gfx::GetCurrentBuffer(&screen.raw32, &screen.width, &screen.height);

	renderContext ctx;

	palette_t newpalette
	    = { { 0x00000000, 0xff000000, 0xff25009e, 0xffae0083, 0xffff1600, 0xff193ba9,
		  0xff5031c5, 0xffa04000, 0xff534b01, 0xff1ea95c, 0xff145d96, 0xff119464,
		  0xff545753, 0xff7f61df, 0xffffffff, 0xff5486da, 0xff7c8e28, 0xffff6767,
		  0xfff55fd4, 0xff8e918d, 0xfffd8739, 0xff50b48d, 0xffbb94fc, 0xff84b85a,
		  0xffd8ac4e, 0xffd6b638, 0xff86befe, 0xffced2cc, 0xff87f3b7, 0xffffd86f,
		  0xffb5f189, 0xfffffd45 } };
	ctx.palette = newpalette;
	ctx.palette.GenNames();

	gfx::SetPalette(newpalette.color);

	// printf("MATCH TEST\n");
	// printf("%u\n", ctx.palette.Match(0xffffffff));
	// printf("%u\n", ctx.palette.Match(0xfffa8025));
	// printf("screenSize %d\n", 640 * 480 * 4);

	ctx.sprites.emplace_back();
	ctx.maps.emplace_back();
	ctx.tileDesc.push_back(arr_view<tileDesc_t>(pic_xpm_tiles, FIXARRLEN(pic_xpm_tiles)));
	if (!pixmapToSprite((const unsigned char**)pic_xpm, ARRSIZE(pic_xpm), ctx.palette,
	                    ctx.sprites.back(), ctx.maps.back())) {
		fprintf(stderr, "Failed to parse pixmap 1\n");
		return -1;
	}

	// TODO/NOTE make a map for 2nd teams jersey colors
	// the xpm color positions are not constant having a seemingly random order
	// every time it is saved,
	ctx.maps.push_back(ctx.maps.back());
	ctx.maps.back().offset[4] = 3;
	ctx.maps.back().offset[5] = 17;

	ctx.maps.push_back(ctx.maps.back());
	auto& dmgmap     = ctx.maps.back();
	dmgmap.offset[1] = 31;
	dmgmap.offset[2] = 29;
	dmgmap.offset[3] = 29;
	dmgmap.offset[4] = 14;
	dmgmap.offset[5] = 14;
	dmgmap.offset[6] = 27;

	ctx.sprites.emplace_back();
	ctx.maps.emplace_back();
	ctx.tileDesc.push_back(arr_view<tileDesc_t>(tiles_xpm_tiles, FIXARRLEN(tiles_xpm_tiles)));
	if (!pixmapToSprite((const unsigned char**)tiles_xpm, ARRSIZE(tiles_xpm), ctx.palette,
	                    ctx.sprites.back(), ctx.maps.back())) {
		fprintf(stderr, "Failed to parse pixmap 2\n");
		return -1;
	}

	ctx.sprites.emplace_back();
	ctx.maps.emplace_back();
	if (!pixmapToSprite((const unsigned char**)title_xpm, ARRSIZE(title_xpm), ctx.palette,
	                    ctx.sprites.back(), ctx.maps.back())) {
		fprintf(stderr, "Failed to parse pixmap 3\n");
		return -1;
	}

	sprite_t     fntsrc = { 1, 1, NULL };
	paletteMap_t fntmap = {};
	if (!pixmapToSprite((const unsigned char**)font4x6_xpm, ARRSIZE(font4x6_xpm), ctx.palette,
	                    fntsrc, fntmap)) {
		fprintf(stderr, "Failed to parse font\n");
		return -1;
	}
	printf("fntsrc %d %d\n", fntsrc.width, fntsrc.height);
	monofont fnt{ fntsrc, ctx.palette, {}, 4, 6, 32 };

	std::vector<renderJob> jobs;
	Messanger              msger(ctx, fnt);

	// mainGame.SetInputStream(0, &P1Rec);
	// snapRec = new SnapRecorder();
	// mainGame.callbacks.push_back(snapRec);

	SnapShot();

	double       frameTime = 0.0;
	double       simTime   = 0.0;
	double       rendTime  = 0.0;
	double       orendTime = 0.0;
	unsigned int frameNum  = 0;
	gfx::ResetTimer();

	{
		int   end  = std::min(defaultMap, defaultMap_size / sizeof(fieldMap_t::entry_t));
		auto* data = (fieldMap_t::entry_t*)((&defaultMap) + 1); // lol

		for (int i = 0; i < end; i++) {
			auto& e = data[i];
			testmap.Insert(e.datum, e.rec);
			printf("rec %d %d %d %d\n", e.rec.x0, e.rec.y0, e.rec.x1, e.rec.y1);
		}
		printf("defaultMap_size %d entries %d\n", defaultMap_size, end);
	}

	mainGame.CurInst<sim::Instance>().map = &testmap;

	// start paused
	mainGame.paused = false;

	gs.ForceSnap();
	gs.ResetSlew();

	threadPool.SubJob(&netPumpJob, tw::LOOP, 0);

	m::menu TestMenu;
	m::menu netMenu;

	TestMenu.focuscolor = ctx.palette.WHITE;
	TestMenu.textcolor  = ctx.palette.BLUE;
	TestMenu.AddMenu(new m::menuFunc("PLAY", SinglePlayerGame_cb));
	TestMenu.AddMenu(new m::menuSel("ONLINE", &netMenu, mainMenuManager));
	// TestMenu.AddMenu(new m::menuFunc("OPTIONS", nullptr));
	TestMenu.AddMenu(new m::menuFunc("QUIT", Quit_cb));

	netMenu.focuscolor = ctx.palette.WHITE;
	netMenu.textcolor  = ctx.palette.BLUE;
	netMenu.AddMenu(new m::menuIP(hostaddr));
	netMenu.AddMenu(new m::menuFunc("CONNECT", ConnectToHost));
	netMenu.AddMenu(new m::menuFunc("HOST", StartHosting));
	netMenu.AddMenu(new m::menuSel("EXIT", &TestMenu, mainMenuManager));

	mainMenuManager.menuAvail.push_back(&TestMenu);
	mainMenuManager.menuAvail.push_back(&netMenu);

	testTree.Insert(treecounter, treecounter);

	class treedrawer_t : public tree_t<int, int>::debugCallback {

		Messanger&     m;
		renderContext& c;

	    public:
		treedrawer_t(Messanger& _m, renderContext& _c)
		    : m(_m)
		    , c(_c){};
		virtual void callback(int key, int x, int y, bool c0, bool c1, int lvl) override
		{
			// print characters here
			int xw = 9;
			m.BeginMsg(x * xw, y * 20, 10, 20);
			m.fgColor = 26;
			m.bgColor = c.palette.BLACK;
			if (key == testTreeCursor) {
				m.fgColor = c.palette.YELLOW;
				m.bgColor = c.palette.BLUE;
			}
			m.Write("%d:%d", key, lvl);

			if (c0)
				c.drawLine(x * xw, y * 20, (x - 1) * xw, (y + 1) * 20,
				           c.palette.WHITE);
			if (c1)
				c.drawLine(x * xw, y * 20, (x + 1) * xw, (y + 1) * 20,
				           c.palette.WHITE);
		}
	};
	treedrawer_t treedrawer(msger, ctx);

	editor.map = &testmap;

	while (wantQuit() == 0) {

		if (testTreeAddNode) {
			testTreeAddNode = false;
			treecounter--;
			testTree.Insert(treecounter, treecounter);
		}
		if (testTreeDelNode) {
			testTreeDelNode = false;
			// TODO delete a random node
		}

		if (testTreeTraverse) {
			testTreeTraverse = false;
			// TODO
			// traverse the tree printf number of iterator hops between nodes or
			// something for(auto &v : testTree) { 	printf("%d\n", v);
			// }
		}

		frameNum++;

		gfx::SwapBuffers();
		gfx::GetCurrentBuffer((void**)&screen.raw32, &screen.width, &screen.height);

		/// --- SEND AND RECEIVE ALL NETWORK STUFF
		// gs.PumpNetwork(frameTime);

		frameTime = gfx::GetTimeElapsed();
		gfx::ResetTimer();

		ctx.dst = screen;
		jobs.resize(0);

		gs_lock.lock();
		/// --- STEP SIMULATION
		double t0         = gfx::GetTimeElapsed();
		SimSpeedMult      = std::pow(SimSpeedMultPos, 3.f);
		int simstepstaken = gs.StepGame(frameTime * SimSpeedMult);
		gs_lock.unlock();

		simTime  = gfx::GetTimeElapsed();
		rendTime = simTime;
		simTime  = simTime - t0;

		sim::Instance& inst = mainGame.CurInst<sim::Instance>();

		// TODO
		// temporary hack to move camera
		static int ofcount = -1; //
		if (debugView) {
			editor.Input();
			const bool* keys = gfx::GetKeyStates();
			float       mspd = 5.0f;
			if (keys[KEY_LEFT_SHIFT]) {
				mspd = 30.f;
			}
			if (keys[KEY_UP]) {
				debugViewPos.y -= mspd;
			} else if (keys[KEY_DOWN]) {
				debugViewPos.y += mspd;
			}
			if (keys[KEY_RIGHT]) {
				debugViewPos.x += mspd;
			} else if (keys[KEY_LEFT]) {
				debugViewPos.x -= mspd;
			}
			mainView.moveTo(debugViewPos);
		} else if (ofcount != mainGame.GetFrameCount()) {
			const auto& inst = mainGame.CurInst<sim::Instance>();
			if (mainGame.CurInst<sim::Instance>().GetPlayerEnt(LocalPlayerIX)) {
				auto          inst = mainGame.CurInst<sim::Instance>();
				auto*         ent  = inst.GetPlayerEnt(LocalPlayerIX);
				fp32          dist;
				sim::entityID id;
				auto*         b = inst.closestBallPtr(ent->pos, dist, id);
				if (b) {
					fp32 d    = std::min(fp32(120),
                                                          ent->pos.dist(b->pos) * fp32(0.80));
					v3   norm = (b->pos - ent->pos).normal();
					mainView.moveTo(ent->pos + (norm * d));
				} else {
					mainView.moveTo(ent->pos);
				}

			} else if (inst.balls.size() > 0) {
				auto b        = inst.balls.begin();
				auto ball_ent = inst.ent.Get(b.Key());
				mainView.moveTo(ball_ent->pos);
			}
		}

		ofcount    = mainGame.GetFrameCount();
		v3 np      = InterpolatePos(mainView.pos, mainView.vel, mainGame.Tmult());
		mainView.x = np.x;
		mainView.y = np.y;

		// TODO
		// rework this interface/construction
		DrawGame(ctx, mainView, mainGame, msger, LocalPlayerIX);

		if (debugView) {
			editor.Draw(ctx, msger);
		}
		if (!displayDebug) {
			msger.Clear();
		}

		{
			int w, h, x, y;
			gfx::GetScreenDim(w, h);
			x = (float)mousePos.x / (float)w * (float)mainView.w;
			y = (float)mousePos.y / (float)h * (float)mainView.h;

			// TODO
			// remove debug visuals for editing/seeing mouse pos
			ctx.drawCircle(x, y, 2, ctx.palette.RED, true);
		}
		// ctx.drawCircle(ctx.dst.width / 2, ctx.dst.height / 2, 1, 0xffff0000, false);
		if (dbgRtreeSqr.active) {

			int w, h, x, y;
			gfx::GetScreenDim(w, h);
			x = (float)mousePos.x / (float)w * (float)mainView.w;
			y = (float)mousePos.y / (float)h * (float)mainView.h;

			rect_t r = { dbgRtreeSqr.x0, dbgRtreeSqr.y0,
				     rdiv(mainView.worldX(x), mapUnitSize),
				     rdiv(mainView.worldY(y), mapUnitSize) };
			rtDrawRectX(ctx, mainView, r, ctx.palette.YELLOW);
			rtDrawRect(ctx, mainView, r, ctx.palette.TEAL);
		}

		// draw title screen here
		if (doDisplayMenu()) {
			uint8_t swatch[]
			    = { ctx.palette.BLUE,  ctx.palette.RED,    ctx.palette.YELLOW,
				ctx.palette.BLACK, ctx.palette.PURPLE, ctx.palette.WHITE,
				ctx.palette.TEAL,  ctx.palette.PINK };

			ctx.maps[5].offset[1] = swatch[(frameNum / 4) % 8];
			ctx.maps[5].offset[2] = ctx.palette.WHITE;
			ctx.blit(2, 5, // sprite and palette
			         0, 0, 384, 64, // title dim
			         128, 128); // dst
		}

		// TODO
		// somehow give jobs to thread pool

		int numjobs = std::max(2, std::min(12, pool_size - 2));
		// int tilesize = 480 / numjobs;
		// for (int i = 0; i < numjobs; i++) {
		// 	ctx.drawLine(100, tilesize * i, 540, tilesize * i, ctx.palette.WHITE);
		// }

		// draw the color palette on bottom of screen
		if (displayDebug) {
			int cx = 12;
			for (uint8_t c = 0; c < 32; c++) {
				ctx.drawCircle(cx, 460, 12, c, true);
				cx += 18;
			}
		}

		msger.fgColor = 26;
		msger.bgColor = 1;
		msger.BeginMsg(100, 5, 16, 100);
		msger.Write("tree=%zu rix=%d", testTree.size(), testTree.rix);
		testTree.KnuthDebug(treedrawer);

		/// --- RENDERS/BLIT out all the main sprites
		FillJobList(ctx, numjobs, jobs);
		for (renderJob& rj : jobs) {
			// rj.Exec();
			threadPool.SubJob(&rj, 0, 1);
		}
		threadPool.WaitBatch(1);

		/// --- menus handled here
		if (doDisplayMenu()) {
			msger.BeginMsg(132, 240, 16, 100);
			sim::playerCtrl menuctrls;
			P1Ctrl.Get(menuctrls);
			mainMenuManager.Input(menuctrls);
			mainMenuManager.Render(msger);
			// write out
			msger.BeginMsg(484, 200, 16, 100);
			msger.fgColor = 1; // swatch[(frameNum / 20) % 8];
			msger.bgColor = 0;
			msger.Write("MMXVIII");
			msger.WriteOut();
			msger.Clear();
		}

		ctx.reset();
		msger.BeginMsg(10, 10, 10, 200);
		msger.fgColor = 26;
		msger.bgColor = 1;
		rendTime      = gfx::GetTimeElapsed() - rendTime;
		orendTime     = (rendTime * 0.2 + orendTime * 0.8);
		msger.Write("SCORE %d - %d", inst.team[0].score, inst.team[1].score);
		msger.Write("FTIME %03.3fms SIM %03.3fms STEPS %d REND %03.3fms", frameTime,
		            simTime, simstepstaken, orendTime);
		msger.Write("THREADS %d", pool_size);
		msger.Write("FRAME %d", mainGame.GetFrameCount());
		msger.Write("OFFSET %dx%d", mainView.screenX(0), mainView.screenY(0));
		msger.Write("CO %03.3f DT %03.3f", gs.GetSlew(), gs.GetSlewDiff());
		msger.Write("EVENTS %zu", inst.events.size());
		msger.Write("RECYCLE %zu", inst.entIDs.RecyclingCount());
		msger.Write("IDS %zu", inst.entIDs.IdCount());
		msger.Write("SPEED %f", SimSpeedMultPos);

		msger.BeginMsg(210, 10, 10, 200);
		msger.Write("F2 snapshot");
		msger.Write("F3 apply snap");
		msger.Write("F4 replay");
		msger.Write("F5 rollback");
		msger.Write("F6 TOG P1");
		msger.Write("F7 KILL AI");
		msger.Write("F8 DEBUG VIEW");
		msger.Write("F9 SAVE");
		msger.Write("F10 LOAD");
		msger.Write("F11 CONNECT");
		msger.Write("F12 HOST");

		msger.BeginMsg(280, 10, 10, 200);
		msger.Write("P pause");
		msger.Write("[ step");
		msger.Write("- slowdown");
		msger.Write("+ speedup");
		if (displayDebug)
			msger.WriteOut();
		msger.Clear();

		gs_lock.lock();
		if (netctx) {

			msger.BeginMsg(460, 10, 10, 200);
			int y = 10;
			// int               x     = 460;
			int               i     = net::GetNumConnecitons(netctx) - 1;
			const net::CXN_h* conns = net::GetConnections(netctx);
			net::connStats    cs    = {};

			msger.Write("ONLINE");
			y += 10;
			static char address[48] = {};
			for (; i >= 0; i--) {
				cs = net::GetConnStats(netctx, conns[i]);

				msger.Write("------------", cs.recvPackets);
				net::GetConnAddr(netctx, conns[i], address);
				msger.Write(address);
				msger.Write("ltnc %f", cs.latency);
				msger.Write("rate %d", cs.bitRate);
				msger.Write("recv %d", cs.recvPackets);
				msger.Write("sent %d", cs.sentPackets);
				msger.Write("oldest %d", cs.oldestUnacked);
				msger.Write("uackv %d", cs.unackedPacket);
				msger.Write("o_smsg %d", cs.subMessagesOUT);
				msger.Write("i_smsg %d", cs.subMessagesIN);
				msger.Write("drop %d", cs.droppedPackets);
				msger.Write("stall %d", cs.stall);

				msger.Write("ordbff %d", cs.orderBuffSize);
				msger.Write("snap %d", gs.GetSnapDist());
				msger.Write("slew %0.03f", gs.GetSlew());
				msger.Write("simr %d", gs.GetSimInputRem());

				msger.Write("edge %d", cs.edge);
				msger.Write("%05d %05d %05d %05d", cs.orderEdge[0], cs.orderEdge[1],
				            cs.orderEdge[2], cs.orderEdge[3]);
				msger.Write("%05d %05d %05d %05d", cs.orderEdgeRdy[0],
				            cs.orderEdgeRdy[1], cs.orderEdgeRdy[2],
				            cs.orderEdgeRdy[3]);
			}
		}

		gs_lock.unlock();
		msger.WriteOut();
		msger.Clear();
	}

	free(fntsrc.raw32);

	threadPool.Join();

	gfx::Exit();
	net::Exit();
	// nix::Exit();
	printf("EXIT\n");
	return 0;
}
