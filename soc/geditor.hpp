#include "render.hpp"
#include "instance.hpp"

class gameEditor {

	int state;
	int sel_tile;
	int sel_tool;

	rect_t pending;
	bool   pendingrdy;

	const sim::view_t& mainView;

	int mx, my;

	uint8_t tileType = 0;

    public:
	fieldMap_t* map;
	gameEditor(const sim::view_t& mv)
	    : mainView(mv)
	{
		sel_tile = 0;
		sel_tool = 0;
		state    = 0;
		mx       = 0;
		my       = 0;
	};

	void mouseEvent(int act, int x, int y, int btn);
	void keyboardEvent(int keycode, int event);

	void SaveMap();
	void LoadMap();

	void Input();

	void Draw(renderContext& ctx, Messanger& msger);
};
