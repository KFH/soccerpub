// instance of the game
//
#pragma once
#include <limits>

#include "gamecontrols.hpp"

#include "game.hpp"
#include "map.hpp"
#include "binarytree.hpp"


typedef rtree<uint32_t, 8, 16> worldMap_t;

struct maptile_t {
	uint8_t gfx;
	uint8_t type;
	enum maptiletypes { floor = 0x0, wall = 0x01 };
};
typedef rtree<maptile_t, 8, 16> fieldMap_t;
extern fieldMap_t               testmap;

namespace sim {

const v3 drag{ 0.93, 0.93, 0.93 };
const v3 bdrag{ 0.95, 0.95, 0.97 };
const v3 adrag{ 0.995, 0.995, 0.97 };

const fp32 playerRadius       = 8.f;
const fp32 playerAttackRadius = 13.f;



struct playerCtrl {
	// quantized player controls for gameplay
	uint8_t action;
	int8_t  dirx; // 1.0 -1.0
	int8_t  diry; // 1.0 -1.0
	int8_t  dirz;

	enum { BNONE   = 0x00,
	       BTACKLE = 0x01,
	       BKICK   = 0x02,
	       BPASS   = 0x04,
	       BSWITCH = 0x08,
	       BPOWER  = 0x10,

	       BTN_NONE  = 0x00,
	       BTN_SHOOT = 0x01
	};

	v3 GetDir() const
	{
		return v3{ fp32(dirx) / fp32(128.0), fp32(diry) / fp32(128.0), 0 }.normal();
	}
	double GetX() {
		return fp32(dirx) / fp32(128.0);
	}
	double GetY() {
		return fp32(diry) / fp32(128.0);
	}
	double GetZ() {
		return fp32(dirz) / fp32(128.0);
	}
	void SetZ(double z) {
		dirz = std::min(127.0, std::max(-127.0, z * 127.0));
	}
	void SetDir(double x, double y)
	{
		dirx = std::min(127.0, std::max(-127.0, x * 127.0));
		diry = std::min(127.0, std::max(-127.0, y * 127.0));
	}

	int  GetAct(void) const { return action; }
	void SetAct(int newaction) { action = newaction; }

	bool operator==(const playerCtrl& other)
	{
		// cough
		return (action == other.action) & (dirx == other.dirx) & (diry == other.diry);
	}
};

#pragma pack(push, 1)
struct serialDef {
	enum serialtypes {
		INSTANCE,
		UIDS,
		ENTITY,
		PLAYERS,
		CTRLMAP,
		BALLS,
		FIELD,
		TEAM,
		EVENTS,
	};
	uint8_t  stype;
	uint16_t len;
};
#pragma pack(pop)

// forward declares
class Instance;
class eventRecorder;
struct entity;

inline rect_t rtBoxPoint(v3 p, int margin)
{
	v3  np = p.round();
	int x  = int(np.x);
	int y  = int(np.y);
	return rect_t{ x - margin, y - margin, x + margin, y + margin };
}
#pragma pack(push, 1)
struct ballState {

	enum {
		// balls boundary states
		INBOUNDS = 0,
		OUTBOUNDS,
		INGOAL_1,
		INGOAL_2,
		// ball action state, combinable
		ROLBALL = 0x00,
		HITBALL = 0x01,
		AIRBALL = 0x02,
	};
	uint8_t  state;
	uint8_t  actstate;
	entityID playerUid;
	entityID playerUid_prev;
	uint8_t  teamid;

	void Step(const Instance& inst, eventRecorder& er, entity& me);

	bool HasHit() { return actstate & HITBALL; }

	void SetPossessor(entityID uid, uint8_t tid)
	{
		teamid = tid;
		if (playerUid != NULL_ID) {
			playerUid_prev = playerUid;
		}
		playerUid = uid;
	}
	uint8_t  PossessorTeam() const { return teamid; }
	entityID PossessorPrev() const { return playerUid_prev; }
	entityID Possessor() const { return playerUid; }
	int      Val() const { return state; };
};
struct refState {
	// TODO
	// implement referie behavior/state machine
	void Step(){};
};

// TODO/NOTE
// i want to quantize the normal of movement somehow
// into a really small number of bits like 8
// and then expand it back up to a valid normal
// try and stay away from trig, causes problems when switching
// between fixedpoint and floating point
// v3   GetDir() const { return v3{ fp32(dirx) / 128.0, fp32(diry) / 128.0, 0 }; }
// void SetDir(v3 n)
//{
//	dirx = (n.x * 128.0);
//	diry = (n.y * 128.0);
//}

struct aiCMD_t {
	uint8_t  type;
	v3       vec;
	entityID target;
	enum {
		// command types
		NONE,
		GOTO,
		PASS,
		SHOOT,
		ATTACK,
		FORMATION,
	};
};

constexpr char* aiCMD_str[] = { "NONE", "GOTO", "PASS", "SHOOT", "ATTACK", "FORMATION" };

struct playerState {

	// TODO
	// i think i need some kind of action transition counter?
	// for actions that take time to enter/exit

	uint8_t cstate;
	uint8_t ctime;
	uint8_t hp;
	uint8_t stamina;
	uint8_t invincible;
	uint8_t teamid;
	v3 fpos; // field position;
	aiCMD_t cmd;

	enum {
		// 4 bits movement state
		WALK       = 0,
		SPRINT     = 1,
		TACKLE     = 2,
		KICK       = 3,
		HOLDING    = 4,
		CARRY      = 5,
		JUMP       = 6,
		FALL       = 7,
		PASS       = 8,
		ACTIONMASK = WALK | SPRINT | TACKLE | KICK | HOLDING | CARRY | JUMP | FALL | PASS,
		// 2 bits for role
		GOALIE   = 0 << 4,
		DEFENSE  = 1 << 4,
		MIDFIELD = 2 << 4,
		FORWARD  = 3 << 4,
		ROLEMASK = GOALIE | DEFENSE | MIDFIELD | FORWARD,

	};
	// TODO
	// implement player behavior/state machine
	void Step(const Instance& inst, const entity& me, playerCtrl& ctrl); // const;

	// x = 0-63 and y = 0-3
	void SetFieldPos(int x, int y) { 
		fpos.x = x;
	 	fpos.y = y;	
	}
	v3 GetFieldPos(int& x, int& y) const
	{
		x = fpos.x;
		y = fpos.y;
		return fpos;
	}

	int  GetAct() const { return cstate & ACTIONMASK; }
	void SetAct(int action) { cstate = (cstate & (~ACTIONMASK)) | action; }

	int  GetRole() const { return cstate & ROLEMASK; }
	void SetRole(int role) { cstate = (cstate & (~ROLEMASK)) | role; }

	int  GetStamina() const { return stamina; }
	void AddStamina(int v) { stamina = std::min(255, std::max(0, (int)stamina + v)); };

	int  GetHP() const { return hp; }
	void SetHP(int v) { hp = std::min(255, std::max(0, v)); }
	void AddHP(int v) { hp = std::min(255, std::max(0, (int)hp + v)); };

	void AddInvicible(int v) { invincible = std::min(255, std::max(0, (int)invincible + v)); }

	int CanTackle() const { return stamina > 125; }
	int IsDead() const { return hp < 1; }
	int IsVincible() const { return (invincible < 1) & (GetAct() != TACKLE); }

	int Val() const { return cstate & ACTIONMASK; }
};

constexpr char* playerStateRoles_str[] = { "GOALIE", "DEFENSE", "MIDFIELD", "FORWARD" };

union eState {
	int64_t     v;
	ballState   ball;
	playerState player;
	/*
	eState()
	    : v(0){};
	eState(int x)
	    : v(x){};
	eState(playerState ps)
	    : player(ps){};
	    */
};

// NOTE
// id like to keep state size as small as possible
// for quicker syncronization if needed
// static_assert(sizeof(eState) <= sizeof(uint64_t), "eState is too large");

struct entity {
	v3 pos;
	v3 vel;
	// TODO
	// move this else where?
	/*union {
	        uint64_t    v;
	        playerState player;
	        ballState   ball;
	};*/
	int16_t frame;
	// entityID uid;

	// uint32_t _padding_;
	// bool operator==(const entity& other) const { return this == &other;}
	// bool operator==(const entityID& id) const { return uid == id; }
};
// static_assert(sizeof(entity) % 16 == 0, "entity wrongly sized");
#pragma pack(pop)

#pragma pack(push, 1)
namespace EV {
	enum {
		// enumerate event types here 
		NONE,
		KICK,
		PASS,
		SLIDETACKLE,
		STEAL,
		GOAL,
		PLAYERHIT,
		OUTBOUNDS,
	};
	constexpr char* _str[]
	    = { // enumerate event type strings, unless some automated way exists?
		"NONE", 
		"KICK", 
		"PASS", 
		"SLIDETACKLE", 
		"STEAL", 
		"GOAL", 
		"PLAYERHIT", 
		"OUTBOUNDS"};

	struct Kick_t {
		v3       dir;
		fp32     pow;
		entityID player;
	};
	struct Pass_t {
		v3 p0;
		v3 p1;	
	};
	struct Steal_t {
		entityID player;
	};
	struct PlayerHit_t {
		entityID attacker;
		entityID reciever;
		fp32     pow;
	};
	struct Goal_t {
		entityID player;
		int      teamid;
		int      score;
	};
	union Meta_u {
		Pass_t      pass;
		Kick_t      kick;
		Steal_t     steal;
		PlayerHit_t playerhit;
		Goal_t      goal;
	};
}; // namespace EV

struct gEvent_t {
	uint32_t   type;
	entityID   id;
	int        life;
	int        x;
	int        y;
	EV::Meta_u meta;
};
#pragma pack(pop)
// Not sure if this is a good idea
class eventRecorder {
    public:
	virtual gEvent_t& AddEvent(uint32_t type, int life, const v3& pos) = 0;
};
struct socField {
	int hWidth;
	int hHeight;
	int hGoal;

	v3 GetPos(int team, int x, int y) const
	{
		v3 pos = {};
		pos.x  = (x - 32) * hWidth / 32;
		pos.y  = hHeight - y * hHeight / 3;
		if (team == 0)
			pos.y *= -1.f;

		return pos;
	}
};

struct team_t {
	char     name[64];
	uint32_t color;
	int      score;
};

class teamManager_t {
    public:
	uint8_t teamid;
	// map player fpos to v3 field position
	std::vector<v3> formation;

	void Step(Instance& in);
	void setFieldFormation(Instance& inst);

	v3 opponentGoalPos(Instance &inst) const;
	v3 myGoalPos(Instance & inst) const;
};
namespace phases {
	enum {
		//
		NONE,
		PLAY,
		PENALTY,
		KICKIN,
		FACEOFF,
	};
};
class Instance : public vInstance, public eventRecorder { // game instace
    public:
	// TODO
	// store some kind of mapping of entity ID to states or components
	// eg:
	// map<g_entityID,entity> entities; // everything should have one of these
	// map<g_entityID,ballState> balls; // only create mapping for ents that are balls
	// map<g_entityID,playerState> players; // only create for ents that are players
	// map<g_entityID,playerAiGoal> player_ai_goal; // only create for players that have an AI
	// goal?
	//
	// this map type needs to be quickly serialized, deserialized, and indexed
	// it possibly needs to de/serialize every 16ms and execute thousands of indexes

	g_entityTracker entIDs;

	tree_t<entityID, entity>      ent;
	tree_t<entityID, ballState>   balls;
	tree_t<entityID, playerState> players;

	socField field;

	std::vector<gEvent_t> events;
	uint32_t              evId;

	uint32_t gamePhase;

	fieldMap_t* map;

	team_t team[2];

	teamManager_t teamManager[2];

#pragma pack(push, 1)
	struct ctrlMap {
		playerIX player_id;
		entityID entity_id;
		bool     isPaired;
		bool     operator==(const ctrlMap& other) const { return player_id == other.player_id; }
		bool     operator==(const playerIX& id) const { return player_id == id; }
	};
#pragma pack(pop)
	struct {
		int Possessor; // CONCEPT: use this to pass control to next input stream
		int Player; // CONCEPT: use this to iterate through ents not controlling the ball
	} nextIx;
	std::vector<ctrlMap> inmap;

	Instance()
	    : field{ 0, 0, 0 }
	    , team{ team_t{}, team_t{} }
	{
		nextIx.Possessor = 0;
		nextIx.Player    = 0;
		entIDs.Reset();
	}

	const entity* closestBallPtr(v3 p, fp32& dist, entityID& id) const
	{
		id                 = NULL_ID;
		const entity* ball = nullptr;
		fp32          d    = std::numeric_limits<float>::max(); // will break on fixed point
		dist               = d;
		for (auto bs = balls.begin(); bs != balls.end(); bs++) {
			ball    = ent.Get(bs.Key());
			fp32 nd = ball->pos.dist2(p);
			if (nd < d) {
				d    = nd;
				id   = bs.Key();
				dist = ball->pos.dist(p);
			}
		}
		return ball;
	}

	v3 closestNet(const entityID& eid) const
	{
		auto p = players.Get(eid);
		if (p->teamid == 0) {
			return v3{ 0, fp32(field.hHeight), 0 };
		} else {
			return v3{ 0, fp32(-field.hHeight), 0 };
		}
		return v3{};
	}

	entity*       GetEntity(entityID uid) { return ent.Get(uid); }
	const entity* GetEntity(entityID uid) const { return ent.Get(uid); }

	const entity* ClosestOfTeam(int teamid, const v3& pos, entityID& target,
	                            fp32 mindist = -1.f) const
	{
		const entity* e    = nullptr;
		fp32          dist = std::numeric_limits<float>::max();
		if (mindist > 0) // unsquare distance, so dist2 can be compared
			mindist = mindist * mindist;
		for (auto p = players.begin(); p != players.end(); p++) {
			if (p->teamid == teamid) {
				auto t  = ent.Get(p.Key());
				fp32 nd = t->pos.dist2(pos);
				if ((nd < dist) && (nd > mindist)) {
					e      = &(*t);
					dist   = nd;
					target = p.Key();
				}
			}
		}
		return e;
	}

	int PlayersInZone(int teamid, v3 tl, v3 br) const
	{
		fp32 x0    = tl.x;
		fp32 y0    = tl.y;
		fp32 x1    = br.x;
		fp32 y1    = br.y;
		int  count = 0;
		for (auto p = players.begin(); p != players.end(); p++) {
			if (p->teamid == teamid) {
				auto t = ent.Get(p.Key());
				if ((t->pos.x < x0) & (t->pos.x > x1) & (t->pos.y < y0)
				    & (t->pos.y > y1))
					count++;
			}
		}
		return count;
	}

	virtual gEvent_t& AddEvent(uint32_t type, int life, const v3& pos) override
	{
		i3 p = pos.toInt();
		events.push_back(gEvent_t{ type, entIDs.Create(), life, p.x, p.y, {} });
		return *events.rbegin();
	}

	// int GetTeam(const entity& e) const { return e.player.teamid; }

	// TODO
	// find way to retreve players ent from a handle
	entity*  GetPlayerEnt(playerIX ix);
	entityID GetPlayerEntityID(playerIX ix);

	virtual int  Init() override;
	virtual void Step(int frameNum, std::vector<ctrlOverride>& in) override;
	virtual void Serialize(ByteBuffer& buff) const override;
	virtual void DeSerialize(ByteBuffer& buff) override;
};

} // namespace sim
