
#include "gamecontrols.hpp"
#include "gamerend.hpp"
#include "idtags.hpp"


#include "instance.hpp"
#include "render.hpp"
#include "util.hpp"
#include "interpolate.hpp"

#include <time.h>

#include "names.h"

#include "pic.xpm.hpp"
#include "tiles.xpm.hpp"

struct lfsr_t {
	uint16_t lfsr; // init to non zero number
	uint16_t mask; //

	void reset()
	{
		lfsr = 1;
		mask = 0x057Du; // 11bit period
	}
	uint16_t rand()
	{
		unsigned lsb = lfsr & 1; /* Get LSB (i.e., the output bit). */
		lfsr >>= 1; /* Shift register */
		if (lsb) /* If the output bit is 1, apply toggle mask. */
			lfsr ^= mask;
		return lfsr;
	}
};

class worldDrawer_t : public queryCallback_t<maptile_t> {
    public:
	renderContext* ctx       = 0;
	sim::view_t*   gview     = 0;
	uint8_t        spritesrc = 1;
	uint8_t        colormap  = 4;

	worldDrawer_t(){};

	virtual bool callback(int index, const rect_t& ir, const maptile_t& mt) override
	{
		int         px0 = gview->screenX(ir.x0), py0 = gview->screenY(ir.y0);
		int         px1 = gview->screenX(ir.x1), py1 = gview->screenY(ir.y1);
		spriteLoc_t sl = tiles_xpm_tiles[mt.gfx].spriteL();
		ctx->tile(spritesrc, colormap, sl, px0, py0, px1, py1);
		return 0;
	}
};
worldDrawer_t worldDrawer;

class debugTreeDrawer : public queryCallback_t<rect_t> {
    public:
	sim::view_t*   gview;
	rect_t         view;
	renderContext* ctx    = 0;
	uint8_t        color  = 0;
	int            margin = 0;

	debugTreeDrawer() {}
	virtual bool callback(int index, const rect_t& ir, const rect_t&) override
	{
		rect_t r = ir;
		r.expand(margin);
		rtDrawRect(*ctx, *gview, r, color);
		return 0;
	}
};
debugTreeDrawer mapDrawer;

class wallDrawer_t : public queryCallback_t<uint32_t> {
    public:
	renderContext* ctx   = 0;
	sim::view_t*   gview = 0;

	spriteLoc_t twall;
	spriteLoc_t bwall;
	spriteLoc_t lwall;
	spriteLoc_t rwall;

	uint8_t spritesrc = 1;
	uint8_t colormap  = 4;
	wallDrawer_t()
	    : twall{ tiles_xpm_tiles[wall_top].spriteL() }
	    , bwall{ tiles_xpm_tiles[wall_bottom].spriteL() }
	    , lwall{ tiles_xpm_tiles[wall_left].spriteL() }
	    , rwall{ tiles_xpm_tiles[wall_right].spriteL() } {

	    };

	virtual bool callback(int index, const rect_t& ir, const uint32_t& i) override
	{
		int px0 = gview->screenX(ir.x0), py0 = gview->screenY(ir.y0);
		int px1 = gview->screenX(ir.x1), py1 = gview->screenY(ir.y1);

		if (i & map_t::L_WALL)
			ctx->tile(spritesrc, colormap, lwall, px0 - lwall.width(), py0, px0, py1);
		if (i & map_t::T_WALL)
			ctx->tile(spritesrc, colormap, twall, px0, py0 - twall.height(), px1, py0);
		if (i & map_t::R_WALL)
			ctx->tile(spritesrc, colormap, rwall, px1, py0, px1 + rwall.width(), py1);
		if (i & map_t::B_WALL)
			ctx->tile(spritesrc, colormap, bwall, px0, py1, px1, py1 + bwall.height());

		return 0;
	}
};
wallDrawer_t wallDrawer;

class floorDrawer_t : public queryCallback_t<uint32_t> {
    public:
	renderContext* ctx   = 0;
	sim::view_t*   gview = 0;
	uint8_t        c1    = 9;
	uint8_t        c2    = 11;

	floorDrawer_t(){

	};

	virtual bool callback(int index, const rect_t& ir, const uint32_t& i) override
	{
		int px0 = gview->screenX(ir.x0), py0 = gview->screenY(ir.y0);
		int px1 = gview->screenX(ir.x1), py1 = gview->screenY(ir.y1);

		ctx->checker(px0, py0, px1, py1, 32, 32, gview->screenX(0), gview->screenY(0), c1,
		             c2);

		return 0;
	}
};
floorDrawer_t floorDrawer;
class debugQueryMessanger : public queryCallback_t<uint32_t> {
    public:
	rect_t rec;

	struct hit_t {
		rect_t   rec;
		uint32_t v;
	};
	std::vector<hit_t> hits;
	debugQueryMessanger(){};
	virtual bool callback(int index, const rect_t& ir, const uint32_t& i) override
	{
		hits.push_back(hit_t{ ir, i });
		return 0;
	}
};
debugQueryMessanger mapTester;

typedef entityQuant<sim::entity> gEntQuant;

std::vector<gEntQuant> entityPos;
lfsr_t                 randomName = {};
sim::entityID          entity_id       = sim::NULL_ID;
bool                   init       = false;
int                    playername = 0;
int                    num_names  = sizeof(people_names) / sizeof(people_names[0]);
uint8_t                colorcycle[8];

void DrawGame(renderContext& ctx, sim::view_t& mainView, sim::Game& mainGame, Messanger& msger,
              sim::playerIX LocalPlayerIX)
{
	// TODO quick hack setup stuff on first run
	// not sure where name stuff should go
	if (!init) {
		init = true;
		randomName.reset();
		randomName.lfsr      = uint16_t(time(NULL) % num_names);
		uint8_t newcolors[8] = { // make obnoxious rainbow
			                 ctx.palette.BLUE,  ctx.palette.RED,    ctx.palette.YELLOW,
			                 ctx.palette.BLACK, ctx.palette.PURPLE, ctx.palette.WHITE,
			                 ctx.palette.TEAL,  ctx.palette.PINK
		};
		std::copy_n(newcolors, 8, colorcycle);
	}

	const uint64_t CURRENTFRAME = mainGame.GetFrameCount();

	entityPos.resize(0);
	sim::Instance& inst = mainGame.CurInst<sim::Instance>();
	// worldMap_t&    mapSpaces = *inst.map;

	/*
	// inst.GetEntsInViewPos(mainView, inst.players, entityPos);
	InterpolateViewPos<sim::entity>(mainView, inst.ent, entityPos, mainGame.Tmult());

	// sort lowest to heighest
	sort(entityPos.begin(), entityPos.end(),
	     [](const auto& a, const auto& b) { return a.y < b.y; });
	*/

	// memset(ctx.dst.raw32, 0, ctx.dst.width * ctx.dst.height * sizeof(uint32_t));

	ctx.blank(0);

	// TODO/NOTE
	// tree debugging here remove this eventually
	int    vx     = roundf(mainView.x);
	int    vy     = roundf(mainView.y);
	int    hvx    = mainView.w / 2;
	int    hvy    = mainView.h / 2;
	int    margin = 64;
	rect_t viewRect{ vx - hvx - margin, vy - hvy - margin, vx + hvx + margin,
		         vy + hvy + margin };

	// TODO
	// draw floor tiles here
	// then later draw wall tiles
	worldDrawer.ctx   = &ctx;
	worldDrawer.gview = &mainView;
	testmap.Search(viewRect, worldDrawer);

	// floorDrawer.ctx   = &ctx;
	// floorDrawer.gview = &mainView;
	// mapSpaces.Search(viewRect, floorDrawer);

	// draw ball shadow and ring and vel line
	/*
	entityPos.resize(0);
	InterpolateViewPos<sim::entity>(mainView, inst.balls, entityPos, mainGame.Tmult(), false);
	for (gEntQuant& eq : entityPos) {
	        const v3& vel = eq.me->vel;
	        ctx.drawLine(eq.x, eq.y, eq.x + roundf(vel.x) * -1, eq.y + roundf(vel.y) * -1,
	                        ctx.palette.RED);
	        ctx.drawCircle(eq.x, eq.y, std::max(20.f, roundf(vel.len()) * 3), ctx.palette.WHITE,
	                        false);
	        ctx.drawCircle(eq.x, eq.y + 3, 4,
	                       ctx.palette.BLACK, true);
	}
	*/

	int sx = mainView.screenX(0);
	int sy = mainView.screenY(0);

	// circles at center of field or 0,0,0 world coord
	ctx.drawCircle(sx, sy, 5, ctx.palette.WHITE);
	ctx.drawCircle(sx, sy, 30, ctx.palette.WHITE);
	ctx.drawCircle(sx, sy, 60, ctx.palette.WHITE);
	ctx.drawCircle(sx, sy, 90, ctx.palette.WHITE);

	// center line
	ctx.drawLine(sx - inst.field.hWidth, sy, sx + inst.field.hWidth, sy, ctx.palette.WHITE);
	// left right lines
	ctx.drawLine(sx - inst.field.hWidth, sy - inst.field.hHeight, sx - inst.field.hWidth,
	             sy + inst.field.hHeight, ctx.palette.WHITE);
	ctx.drawLine(sx + inst.field.hWidth, sy - inst.field.hHeight, sx + inst.field.hWidth,
	             sy + inst.field.hHeight, ctx.palette.WHITE);
	// top bottom lines
	ctx.drawLine(sx - inst.field.hWidth, sy - inst.field.hHeight, sx + inst.field.hWidth,
	             sy - inst.field.hHeight, ctx.palette.WHITE);
	ctx.drawLine(sx - inst.field.hWidth, sy + inst.field.hHeight, sx + inst.field.hWidth,
	             sy + inst.field.hHeight, ctx.palette.WHITE);
	// goals
	ctx.drawLine(sx - inst.field.hGoal, sy - inst.field.hHeight, sx + inst.field.hGoal,
	             sy - inst.field.hHeight, ctx.palette.RED);
	ctx.drawLine(sx - inst.field.hGoal, sy + inst.field.hHeight, sx + inst.field.hGoal,
	             sy + inst.field.hHeight, ctx.palette.RED);

	int spriteW = 16;
	int spriteH = 16;
	int shw     = spriteW / 2;
	int shh     = spriteH / 2;

	// wallDrawer.ctx   = &ctx;
	// wallDrawer.gview = &mainView;
	// mapSpaces.Search(viewRect, wallDrawer);

	entityPos.resize(0);
	InterpolateViewPos<sim::entity, decltype(inst.ent)>(mainView, inst.ent, entityPos,
	                                                    mainGame.Tmult());
	// sort lowest to heighest
	sort(entityPos.begin(), entityPos.end(),
	     [](const gEntQuant& a, const gEntQuant& b) { return a.y < b.y; });

	auto*         playerEnt = inst.GetPlayerEnt(LocalPlayerIX);
	sim::entityID playerID  = inst.GetPlayerEntityID(LocalPlayerIX);
	auto*         strmptr   = mainGame.GetInputStream(LocalPlayerIX);
	if (playerEnt && strmptr) {
		// draw circle around main player
		gEntQuant eqs;
		InterpolateViewPos<sim::entity>(mainView, *playerEnt, eqs, mainGame.Tmult());
		ctx.drawCircle(eqs.x, eqs.y, 16, ctx.palette.WHITE, false);
		// draw line of joystick dir

		sim::playerCtrl pc;
		strmptr->GetConst(pc);

		fp32          ball_dist;
		sim::entityID ball_id;
		auto*         b = inst.closestBallPtr(playerEnt->pos, ball_dist, ball_id);
		if (b) {
			if (!InRect(mainView.pos,
			            v3{ fp32(mainView.w / 2), fp32(mainView.h / 2), 0 }, b->pos)) {
				i3 bpos = mainView.screenXYZ(b->pos);

				v3 norm = (b->pos - playerEnt->pos).normal();

				ctx.drawLine(eqs.x + (norm.x * 200), eqs.y + (norm.y * 200), bpos.x,
				             bpos.y, ctx.palette.WHITE);
			}
		}
		if (b && inst.balls.Get(ball_id)->Possessor() == playerID) {
			v3       p  = inst.closestNet(playerID);
			v3       vn = playerEnt->vel.normal();
			v3       h  = v3{ fp32(inst.field.hGoal), 0, 0 };
			v3       ip;
			uint32_t selcolor = ctx.palette.WHITE;
			if (Intersects(p + h, p - h, playerEnt->pos,
			               playerEnt->pos + (vn * fp32(1000)), ip))
				selcolor = ctx.palette.RED;

			ctx.drawLine(eqs.x, eqs.y, eqs.x + vn.x * 3000, eqs.y + vn.y * 3000,
			             selcolor);
		}

		ctx.drawLine(eqs.x, eqs.y, eqs.x + pc.dirx / 8, eqs.y + pc.diry / 8,
		             ctx.palette.WHITE);

		if (entity_id != playerID) {
			entity_id       = playerID;
			playername = randomName.rand() % num_names;
		}

		v3 pt0 = playerEnt->pos;
		v3 n   = playerEnt->vel.normal() * fp32(256);
		v3 cn  = playerEnt->vel.cross(v3{ 0, 0, 1 }).normal();
		v3 pt1 = pt0 + n + cn * fp32(120);
		v3 pt2 = pt0 + n + cn * fp32(-120);

		ctx.drawLine(eqs.x, eqs.y, mainView.screenX(pt1.x), mainView.screenY(pt1.y),
		             ctx.palette.BLACK);
		ctx.drawLine(eqs.x, eqs.y, mainView.screenX(pt2.x), mainView.screenY(pt2.y),
		             ctx.palette.BLACK);
		ctx.drawLine(mainView.screenX(pt1.x), mainView.screenY(pt1.y),
		             mainView.screenX(pt2.x), mainView.screenY(pt2.y), ctx.palette.BLACK);

		const auto* ps = inst.players.Get(playerID);
		// DEBUGGING draw lines to all valid passes
		for (auto otherplayer = inst.players.begin(); otherplayer != inst.players.end();
		     otherplayer++) {
			if (otherplayer->teamid != ps->teamid || otherplayer.Key() == playerID)
				continue;
			const auto* op = inst.ent.Get(otherplayer.Key());

			if (PointInTriangle(op->pos, pt0, pt1, pt2)) {
				i3 opd = mainView.screenXYZ(op->pos);
				ctx.drawLine(eqs.x, eqs.y, opd.x, opd.y, ctx.palette.BLUE);
			}
		}

		// TODO
		// this doesnt seem like a good interface for positioning single words
		msger.BeginMsg(eqs.x - 6, eqs.y - 24, 10, 200);
		msger.fgColor = 14;
		msger.bgColor = 0;
		msger.Write(people_names[playername]);
	}

	// draw shadows
	for (const gEntQuant& eq : entityPos) {
		ctx.drawCircle(eq.x, eq.y + 6, 5, ctx.palette.BLACK, true);

		// ctx.drawCircle(eq.x, eq.y, 12, colorcycle[eq.me->player.cmd.type], false);
		// msger.BeginMsg(eq.x - 16, eq.y - 24, 10, 200);
		// msger.fgColor = 14;
		// msger.bgColor = 1;
		// msger.Write("%d", eq.me->uid);
	}
	for (const gEntQuant& eq : entityPos) {

		// psuedorandom shift current frame in run cycle so
		// players dont make strides in sync
		//
		int     foffset = spriteH * abs(eq.dir);
		int     rix     = eq.uid.Ix();
		uint8_t pmap    = rix % 2 == 0 ? 1 : 2;
		// offx to cycle between up and down frame unless standing still
		int offx = ((CURRENTFRAME + rix) / 5) % 2 == 0 ? spriteW : 0;

		int  eqyoff = 0;
		auto p      = inst.players.Get(eq.uid);
		if (p == nullptr) {
			ctx.drawCircle(eq.x, eq.yz, 3, ctx.palette.WHITE, true);
			continue;
		}
		// if still stop running animation
		if (inst.ent.Get(eq.uid)->vel.len() < 0.4)
			offx = spriteW;

		if (!p->IsVincible() && (CURRENTFRAME / 5) % 2 == 0)
			pmap = 3;

		switch (p->GetAct()) {
		case sim::playerState::TACKLE:
			offx   = spriteW * 2;
			eqyoff = 4;
			break;
		case sim::playerState::PASS:
		case sim::playerState::KICK:
			offx   = spriteW * 2;
			eqyoff = -2;
			break;
		default:
			break;
		}

		ctx.blit(0, pmap, 0 + offx, foffset, spriteW + offx, spriteH + foffset, eq.x - shw,
		         eq.yz - shh + eqyoff);

		msger.BeginMsg(eq.x - 16, eq.y - 24, 6, 200);
		msger.fgColor = 14;
		msger.bgColor = 1;
		msger.Write("%d", eq.uid);

		msger.Write("%s-%s", sim::playerStateRoles_str[p->GetRole() >> 4],
		            sim::aiCMD_str[p->cmd.type]);
	}
	// draw balls debug visual stuff
	entityPos.resize(0);
	for (auto ball = inst.balls.begin(); ball != inst.balls.end(); ball++) {
		auto      bent = inst.ent.Get(ball.Key());
		gEntQuant bq;
		InterpolateViewPos<sim::entity>(mainView, *bent, bq, mainGame.Tmult());
	}

	for (auto& e : inst.events) {
		int x = mainView.screenX(e.x);
		int y = mainView.screenY(e.y);

		uint8_t evColor = ctx.palette.BLACK;
		msger.BeginMsg(x, y, 10, 200);
		msger.fgColor = 14;
		msger.bgColor = 0;
		switch (e.type) {
		case sim::EV::GOAL:
			evColor       = ctx.palette.RED;
			msger.fgColor = colorcycle[(CURRENTFRAME / 3) % 8];
			// draw rectangle?
			ctx.checker(0, 0, 32, 480, 32, 64, 0, (CURRENTFRAME * 4) % 64,
			            colorcycle[(CURRENTFRAME / 3) % 8],
			            colorcycle[(CURRENTFRAME / 3 + 1) % 8]);
			ctx.checker(640 - 32, 0, 640, 480, 32, 64, 0, (CURRENTFRAME * 4) % 64,
			            colorcycle[(CURRENTFRAME / 3) % 8],
			            colorcycle[(CURRENTFRAME / 3 + 1) % 8]);
			{
				auto* ent = inst.GetEntity(e.meta.goal.player);
				;

				if (ent) {
					i3 ipos = mainView.screenXYZ(ent->pos);
					ctx.drawCircle(ipos.x, ipos.y, 20,
					               colorcycle[CURRENTFRAME % 8], false);
					ctx.drawCircle(ipos.x, ipos.y, 18,
					               colorcycle[CURRENTFRAME % 8], false);
				}
			}
			break;
		case sim::EV::PASS:
			evColor = ctx.palette.PURPLE;
			{
				fp32 x0 = e.meta.pass.p0.x;
				fp32 y0 = e.meta.pass.p0.y;
				fp32 x1 = e.meta.pass.p1.x;
				fp32 y1 = e.meta.pass.p1.y;
				ctx.drawLine(mainView.screenX(x0), mainView.screenY(y0),
				             mainView.screenX(x1), mainView.screenY(y1),
				             ctx.palette.RED);
			}
			break;
		case sim::EV::KICK:
			evColor = ctx.palette.YELLOW;
			{
				fp32 t  = (fp32)e.life / 60.f;
				fp32 x0 = e.x + (e.meta.kick.dir.x * e.meta.kick.pow * 10.f);
				fp32 y0 = e.y + (e.meta.kick.dir.y * e.meta.kick.pow * 10.f);
				fp32 x1 = ((t) * (fp32)e.x) + ((1.f - t) * x0);
				fp32 y1 = ((t) * (fp32)e.y) + ((1.f - t) * y0);
				ctx.drawLine(mainView.screenX(x0), mainView.screenY(y0),
				             mainView.screenX(x1), mainView.screenY(y1),
				             ctx.palette.BLUE);
			}
			break;
		case sim::EV::PLAYERHIT:
			evColor = ctx.palette.PINK;
			break;
		case sim::EV::STEAL:
			evColor = ctx.palette.BLUE;
			break;
		}
		msger.Write("%d:%s",e.id.Ix(), sim::EV::_str[e.type]);
		ctx.drawCircle(mainView.screenX(e.x), mainView.screenY(e.y), 16, evColor, false);
	}

	bool drawDebugCrap = true;
	if (drawDebugCrap) {
		// draw debug rectangles
		mapDrawer.gview  = &mainView;
		mapDrawer.view   = viewRect;
		mapDrawer.ctx    = &ctx;
		mapDrawer.color  = ctx.palette.RED;
		mapDrawer.margin = 0;
		// mapSpaces.DebugSearch(viewRect, mapDrawer, 2);
		// mapDrawer.margin = 2;
		// mapDrawer.color  = 0xffffff00;
		// mapSpaces.DebugSearch(viewRect, mapDrawer, 1);
		// mapDrawer.margin = 3;
		// mapDrawer.color  = 0xff0000ff;
		// mapSpaces.DebugSearch(viewRect, mapDrawer, 0);

		mapTester.rec = rect_t{ vx - 30, vy - 30, vx + 30, vy + 30 };
		// rtDrawRect(ctx, mainView, mapTester.rec, 0xffee0055);
		mapTester.hits.resize(0);
		// mapSpaces.Search(mapTester.rec, mapTester);
		for (auto hit : mapTester.hits) {
			rtDrawRectX(ctx, mainView, hit.rec, ctx.palette.BLUE);
		}
	}
}
