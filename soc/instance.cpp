
#include "instance.hpp"
namespace sim {

int Instance::Init()
{

	balls.Reset();
	players.Reset();
	ent.Reset();

	inmap.resize(0);
	nextIx.Possessor = 0;
	nextIx.Player    = 0;

	const auto ball_id = entIDs.Create();
	balls.Insert(ball_id, {});
	balls[0].SetPossessor(NULL_ID, 123);
	ent.Insert(ball_id, entity{ v3{}, v3{}, 0 });

	gamePhase = phases::FACEOFF;

	// fields half dimensions
	this->field.hGoal   = 50;
	this->field.hWidth  = 608;
	this->field.hHeight = 576;

	sprintf(team[0].name, "TIGERDOGS\n");
	team[0].color = 0;
	team[0].score = 0;
	sprintf(team[1].name, "APPLEPIE\n");
	team[1].color = 0;
	team[1].score = 0;

	float inc = M_PI * 2.0 / (double)MAXPLAYERS / 2.0;
	float x   = 0.f;
	float d   = 300.f;
	for (int i = MAXPLAYERS - 1; i >= 0; i--) {

		uint8_t state = 0;
		uint8_t fquadrant
		    = (FIELD_DIVS / 2 * FIELD_DIVS) + FIELD_DIVS / 2; // field position
		uint8_t    stamina    = 200;
		uint8_t    hp         = 5;
		float      p          = i % 2 == 0 ? -x : x;
		const auto player_id  = entIDs.Create();
		const v3   player_pos = v3{ cos(p) * d, sin(p) * d * 0.5f, 0 };
		const v3   player_vel = v3{};
		uint8_t    teamid     = uint8_t(i % 2);
		uint8_t    ctime      = 0;

		players.Insert(player_id,
		               playerState{ state, ctime, hp, stamina, 0, teamid, {} });
		ent.Insert(player_id, entity{ player_pos, player_vel, 0 });

		x += inc;
	}

	auto positionTeam = [&](int ofs) {
		int num  = players.size() / 2;
		int xinc = 44 / (num / 2);
		int x    = 32;
		int y    = 0;
		for (int i = 0; i < (int)players.size(); i++) {
			auto& p = players[i];
			if (p.teamid == ofs) {
				p.SetFieldPos(x, y);
				x = std::max(10, (x + xinc) % 54);
				y = std::max(1, ((y + 1) % 3));
				p.SetRole(playerState::FORWARD);
				if (y == 1)
					p.SetRole(playerState::DEFENSE);
			}
		}
	};



	// TODO finish this positioning
	positionTeam(0);
	positionTeam(1);
	players[0].SetRole(playerState::GOALIE);
	players[1].SetRole(playerState::GOALIE);
	teamManager[0].teamid = 0;
	teamManager[1].teamid = 1;

	teamManager[0].setFieldFormation(*this);
	teamManager[1].setFieldFormation(*this);


	return 0;
}

class playerCollisionQuery : public queryCallback_t<maptile_t> {
    public:
	entity& me;
	playerCollisionQuery(entity& target)
	    : me(target){};
	virtual bool callback(int index, const rect_t& ir, const maptile_t& i) override
	{
		if (i.type == maptile_t::floor)
			return 0;
		// for each intersecting rectangle check distance from pos to wall
		// if intersects with wall position adjacent too it
		// redirect velocity if angle less than 45 or use some exponential curve
		v3   l0 = { fp32(ir.x0), fp32(ir.y0), 0 }, l1 = { fp32(ir.x0), fp32(ir.y1), 0 };
		v3   l2 = { fp32(ir.x1), fp32(ir.y0), 0 }, l3 = { fp32(ir.x1), fp32(ir.y1), 0 };
		fp32 r   = 9.f;
		bool inX = (me.pos.x > ir.x0) & (me.pos.x < ir.x1);
		bool inY = (me.pos.y > ir.y0) & (me.pos.y < ir.y1);

		// TODO
		// redirect a portion of velocity that intersects wall
		if (DistToLine(l0, l1, me.pos) < r & inY) {
			me.pos.x = l0.x - r;
		} else if (DistToLine(l2, l3, me.pos) < r & inY) {
			me.pos.x = l2.x + r;
		}
		if (DistToLine(l0, l2, me.pos) < r & inX) {
			me.pos.y = l2.y - r;
		} else if (DistToLine(l1, l3, me.pos) < r & inX) {
			me.pos.y = l3.y + r;
		}
		return 0;
	}
};

// TODO
// entity behaviors to current state of game
// could be parralelized, assusming no use of rand
void Instance::Step(int frameNum, std::vector<ctrlOverride>& in)
{

	// TODO
	// run some highlevel team AI that sets a general plan for each player
	//

	if (balls.size() < 1)
		gamePhase = phases::NONE;

	// remove expired events
	// for (auto it = events.rbegin(); it != events.rend(); it++) {
	for (int i = (int)events.size() - 1; i >= 0; i--) {
		auto& ev = events[i];
		ev.life--;
		if (ev.life < 0) {
			entIDs.Destroy(ev.id);
			ev = *events.rbegin();
			events.pop_back();
		}
	}

	// team management AI issues commands to players

	// static bool runOnce = false;
	// if(!runOnce) {
	// 	runOnce = true;
	for (auto& m : teamManager) {
		m.Step(*this);
	}
	//}

	// TODO/HACK
	// round robin set input stream X to control player in possession of ball
	// CONCEPT/HACK
	// remap input ctrlOverride ids into player ids
	for (auto& c : in) {
		auto it = std::find(inmap.begin(), inmap.end(), c.player_id);
		if (it != inmap.end()) {
			// convert input ctrl uid to from player_id to the corresponding entity_id cause
			// i cant think of a better design atm
			c.entity_id       = it->entity_id;
			it->isPaired = true;
		} else {
			// ctrlOverride is unmapped, find a free player for them to control
			for (int i = players.size(); i > 0; i--) {
				nextIx.Player = (nextIx.Player + 1) % players.size();

				entityID newplayeruid = players.GetKey(nextIx.Player);

				auto itm = std::find_if(inmap.begin(), inmap.end(),
				                        [&newplayeruid](auto const& cm) {
					                        return cm.entity_id == newplayeruid;
				                        });
				if (itm == inmap.end()) {
					inmap.push_back(ctrlMap{ c.player_id, newplayeruid, true });
					c.entity_id = newplayeruid;
					break;
				}
			}
		}
	}

	for (int i = (int)inmap.size() - 1; i >= 0; i--) {
		if (!inmap[i].isPaired) {
			inmap.erase(inmap.begin() + i);
		} else {
			inmap[i].isPaired = false;
		}
	}

	if (inmap.size() > 0) {
		// switch control to a player closest to the ball
		// if switch button is pressed
		for (auto c : in) {
			if (c.ctrl.GetAct() == playerCtrl::BSWITCH) {
				auto     it      = players.Get(c.entity_id);
				auto     mymap   = std::find(inmap.begin(), inmap.end(), c.player_id);
				uint8_t  teamid  = it->teamid;
				fp32     mindist = 9999999.0;
				int      ix;
				entityID ballId = NULL_ID;
				auto*    b = closestBallPtr(ent.Get(c.entity_id)->pos, mindist, ballId);
				if (!b)
					break;
				printf("SWITCHING\n");
				v3 ballPos = ent.Get(ballId)->pos;
				for (auto p = players.begin(); p != players.end(); p++) {
					if (p->teamid != teamid)
						continue;

					fp32     nl = (ent.Get(p.Key())->pos - ballPos).len2();
					entityID newplayeruid = p.Key();
					// make sure someone doesnt already control player
					if (nl < mindist
					    && std::find_if(inmap.begin(), inmap.end(),
					                    [&newplayeruid](auto const& cm) {
						                    return cm.entity_id == newplayeruid;
					                    })
					        == inmap.end()) {
						mindist     = nl;
						c.entity_id      = p.Key();
						mymap->entity_id = p.Key();
					}
				}
				printf("SWITCHED TO %d\n", c.entity_id);
			}
		}

		// TODO
		// review this
		// move control to player with ball
		for (auto& b : balls) {
			entityID id = b.Possessor();
			if (id != NULL_ID) {
				auto it
				    = std::find_if(inmap.begin(), inmap.end(),
				                   [&id](auto const& cm) { return cm.entity_id == id; });
				if (it != inmap.end()) {
					// someone already control the player that possesses this
					// ball ?
					continue;
				} else {
					nextIx.Possessor
					    = (nextIx.Possessor + 1) % int(inmap.size());
					// TODO
					// check ctrlMap/inmap for their team id
					if (b.PossessorTeam() == 0) {
						inmap[nextIx.Possessor].entity_id = id;
					}
				}
			}
		}
	}

	// for each AI player get its controller inputs
	thread_local std::vector<playerCtrl> ctrls;
	ctrls.resize(0);
	for (auto it = players.begin(); it != players.end(); it++) {
		playerCtrl pc;
		auto       k = it.Key();
		auto       e = ent.Get(k);
		it->Step(*this, *e, pc);

		// TODO
		// override controlled by human player ctrl inputs here
		// with ctrlOverrid value
		for (int h = int(in.size()) - 1; h >= 0; h--) {
			if (k == in[h].entity_id) {
				pc = in[h].ctrl;
				break;
			}
		}
		ctrls.push_back(pc);
	}

	// figure out frame of animation based on state
	// and applying drag
	// TODO should probably move drag crap else where
	auto pitr = players.begin();
	auto citr = ctrls.begin();
	while (pitr != players.end()) {

		entity& e = *ent.Get(pitr.Key());

		int  es     = pitr->GetAct();
		bool inMove = false;

		if (pitr->ctime > 0) {
			pitr->ctime--;
			inMove = true;
		}

		if (es == playerState::TACKLE && e.vel.len() > 0.1) {
			if ((*citr).GetDir().len() > 0)
				e.vel = (e.vel * 0.75
				         + (*citr).GetDir().normal() * e.vel.len() * 0.25);
			inMove = true;
		}

		if (citr->GetAct() == playerCtrl::BPOWER) {
			// citr->GetDir().
			e.vel = (e.vel.normal() * 0.95 + citr->GetDir().normal() * 0.05)
			    * (e.vel.len() + 0.5);
			inMove = true;
		}

		if (!inMove)
			e.vel = e.vel + citr->GetDir().normal() * 0.35;
		if (!inMove)
			switch (citr->GetAct()) {
			case playerCtrl::BKICK:
				pitr->SetAct(playerState::KICK);
				e.vel       = e.vel * fp32(0.9f);
				pitr->ctime = 16;
				break;
			case playerCtrl::BPASS:
				pitr->SetAct(playerState::PASS);
				e.vel       = e.vel * fp32(0.9f);
				pitr->ctime = 16;
				break;
			case playerCtrl::BTACKLE:
				pitr->SetAct(playerState::TACKLE);
				e.vel = e.vel * 2;
				pitr->AddStamina(-125);
				pitr->ctime = 16;
				break;
			default:
				pitr->SetAct(playerState::WALK);
				break;
			}
		e.vel = e.vel * drag;
		pitr->AddStamina(1);
		pitr->AddInvicible(-1);
		pitr++;
		citr++;
	}

	// lambda for striking
	auto strike = [&](auto me, auto mepos, auto other, auto otherpos, v3 d) {
		auto& ev          = AddEvent(EV::PLAYERHIT, 60, otherpos->pos);
		ev.meta.playerhit = EV::PlayerHit_t{ me.Key(), other.Key(), 1 };
		otherpos->vel     = d.normal() * 2 + mepos->vel;
		other->AddHP(-1);
		other->AddInvicible(200);
		return ev;
	};

	/// --- resolve inter entity interactions here ?
	// TODO
	// iterate entities THEN if they overlap check if their
	// both players etc
	for (auto me = players.begin(); me != players.end(); me++) {
		auto mp    = ent.Get(me.Key());
		auto other = me;
		for (other++; other != players.end(); other++) {
			auto op = ent.Get(other.Key());
			v3   d  = op->pos - mp->pos;
			if (me->GetAct() == playerState::TACKLE | me->GetAct() == playerState::KICK
			    && other->IsVincible()) {
				if (mp->vel.len() > 0.1 && (d.len() < 13)) {
					strike(me, mp, other, op, d);
				}
			} else if (other->GetAct() == playerState::TACKLE
			               | other->GetAct() == playerState::KICK
			           && me->IsVincible()) {
				if (op->vel.len() > 0.1 && (d.len() < 13)) {
					strike(other, op, me, mp, d);
				}
			} else if (d.len2() < 128) {
				fp32 l = d.len();
				if (l < 16.f) {
					v3 n = d.normal();
					if (n.len2() == 0)
						n = v3{ 0.5, 0.5, 0 };
					fp32 vl0 = op->vel.len();
					fp32 vl1 = mp->vel.len();

					fp32 d0 = std::max(0.f, op->vel.normal().dot(n));
					fp32 d1 = std::max(0.f, mp->vel.normal().dot(n));

					v3 f0 = (n * vl0 * -0.3 * d0);
					v3 f1 = (n * vl1 * 0.3 * d1);

					op->vel = (op->vel * (0.7 + 0.3 * (1.f - d0))) + f1;
					mp->vel = (mp->vel * (0.7 + 0.3 * (1.f - d1))) + f0;

					op->vel = op->vel + n * (l - 16.f) * -0.05;
					mp->vel = mp->vel + n * (l - 16.f) * 0.05;
				}
			}
		}
	}

	// resolve position here
	for (auto p = players.begin(); p != players.end(); p++) {
		// check against map bounding boxes
		auto& e = *ent.Get(p.Key());
		e.pos   = e.pos + e.vel;
		playerCollisionQuery cb(e);
		this->map->Search(rtBoxPoint(e.pos, int(e.vel.len()) + 8.0), cb);
	}

	for (auto b = balls.begin(); b != balls.end(); b++) {

		auto me = ent.Get(b.Key());
		b->Step(*this, *this, *me);

		switch (b->state) {
		case ballState::OUTBOUNDS:
			gamePhase = phases::KICKIN;
			break;
		case ballState::INBOUNDS:
			if (gamePhase == phases::PENALTY)
				gamePhase = phases::PLAY;
			if (gamePhase == phases::FACEOFF) {
				if (me->pos.dist(v3{}) < 4.f) {
					gamePhase = phases::PLAY;
				}
			}
			break;
		case ballState::INGOAL_1: {
			team[0].score++;
			auto& ev = AddEvent(EV::GOAL, 120, me->pos);
			ev.meta.goal.player
			    = b->Possessor() != NULL_ID ? b->Possessor() : b->PossessorPrev();
			ev.meta.goal.score = team[0].score;
			gamePhase          = phases::FACEOFF;
		} break;
		case ballState::INGOAL_2: {
			team[1].score++;
			auto& ev = AddEvent(EV::GOAL, 120, me->pos);
			ev.meta.goal.player
			    = b->Possessor() != NULL_ID ? b->Possessor() : b->PossessorPrev();
			ev.meta.goal.score = team[1].score;
			gamePhase          = phases::FACEOFF;
		} break;
		default:
			break;
		}
	}
}

#define SERIALIZE_TREE(sd, vtype, stree, buff)                                                     \
	{                                                                                          \
		sd.stype = vtype;                                                                  \
		sd.len   = stree.SerializeReqSize();                                               \
		buff.append(&sd, sizeof(sd));                                                      \
		size_t old_buff_len = buff.len();                                                  \
		buff.resize(buff.len() + sd.len);                                                  \
		stree.Serialize(buff.data() + old_buff_len);                                       \
	}

void Instance::Serialize(ByteBuffer& buff) const
{
	// mark start of instance
	serialDef sd{ serialDef::INSTANCE, 0 };
	buff.append(&sd, sizeof(sd));

	// control mappings
	sd.stype = serialDef::CTRLMAP;
	sd.len   = sizeof(nextIx) + inmap.size() * sizeof(decltype(inmap)::value_type);
	buff.append(&sd, sizeof(sd));
	buff.append(&nextIx, sizeof(nextIx));
	buff.append(inmap.data(), sizeof(ctrlMap) * inmap.size());

	// TODO
	// seems like events leak memory  or something?
	sd.stype = serialDef::EVENTS;
	sd.len   = sizeof(decltype(events)::value_type) * events.size();
	buff.append(&sd, sizeof(sd));
	buff.append(events.data(), sd.len);

	SERIALIZE_TREE(sd, serialDef::UIDS, entIDs, buff)
	SERIALIZE_TREE(sd, serialDef::ENTITY, ent, buff)
	SERIALIZE_TREE(sd, serialDef::PLAYERS, players, buff)
	SERIALIZE_TREE(sd, serialDef::BALLS, balls, buff)

	// serialize field
	// sd.stype = serialDef::FIELD;
	// sd.len   = sizeof(socField);
	// buff.append(&sd, sizeof(sd));
	// buff.append(&field, sd.len);
	// serialize both teams
	// sd.stype = serialDef::TEAM;
	// sd.len   = sizeof(team);
	// buff.append(&sd, sizeof(sd));
	// buff.append(team, sd.len);
}
#define DESERIALIZE_TREE(tree, tname)                                                              \
	{                                                                                          \
		if (buff.unread() >= sd.len) {                                                     \
			printf("deserialized %s\n", tname);                                        \
			tree.Deserialize(buff.head(), sd.len);                                     \
			buff.read(nullptr, sd.len);                                                \
		} else {                                                                           \
			printf("failed deserialize name\n");                                       \
		}                                                                                  \
	}

void Instance::DeSerialize(ByteBuffer& buff)
{
	printf("DESERIALIZING\n");
	serialDef sd;
	// read ahead until INSTANCE is encountered
	while (buff.unread()) {
		buff.read(&sd, sizeof(sd));
		if (sd.stype == serialDef::INSTANCE)
			break;
	}
	while (buff.unread()) {
		buff.read(&sd, sizeof(sd));

		switch (sd.stype) {
		case serialDef::UIDS:
			DESERIALIZE_TREE(entIDs, "serialDef::UIDS")
			break;
		case serialDef::ENTITY:
			DESERIALIZE_TREE(ent, "serialDef::ENTITY")
			break;
		case serialDef::PLAYERS:
			DESERIALIZE_TREE(players, "serialDef::PLAYERS")
			break;
		case serialDef::BALLS:
			DESERIALIZE_TREE(balls, "serialDef::BALLS")
			break;
		case serialDef::FIELD:
			buff.read(&field, std::min(sizeof(socField), (size_t)sd.len));
			break;
		case serialDef::TEAM:
			buff.read(team, std::min(sizeof(team), (size_t)sd.len));
			break;
		case serialDef::CTRLMAP: {
			buff.read(&nextIx, sizeof(nextIx));
			int n = (int(sd.len) - int(sizeof(nextIx))) / sizeof(ctrlMap);
			inmap.resize(n);
			if (n > 0)
				buff.read(inmap.data(), n * sizeof(ctrlMap));
		} break;
		case serialDef::EVENTS:
			events.resize(sd.len / sizeof(gEvent_t));
			buff.read(events.data(), sd.len);
			break;
		default:
			// unhandled type
			buff.read(nullptr, sd.len);
			return;
		}
	}
}

entity* Instance::GetPlayerEnt(playerIX uid)
{
	entity* e  = nullptr;
	auto    im = std::find(inmap.begin(), inmap.end(), uid);
	if (im != inmap.end())
		e = ent.Get(im->entity_id);
	return e;
}
entityID Instance::GetPlayerEntityID(playerIX ix)
{
	auto im = std::find(inmap.begin(), inmap.end(), ix);
	if (im != inmap.end())
		return im->entity_id;
	return NULL_ID;
}

/*
// TODO
// this doesnt belong here
int Instance::GetEntsInViewPos(const view_t& v, std::vector<entity>& ents,
                               std::vector<entityQuant>& eqs)
{

        int  insize = eqs.size();
        v3   vpos   = v3{ v.x, v.y, 0 }.round();
        fp32 xh     = v.w / 2;
        fp32 yh     = v.h / 2;

        for (entity& e : ents) {

                v3 delta = e.pos.round() - vpos;
                delta.y -= delta.z;
                int xd = delta.x + xh;
                int yd = delta.y + yh;
                // TODO only if in view_t
                if ((xd < v.w) & (yd < v.h)) {
                        eqs.push_back(entityQuant{ xd, yd, &e });
                }
        }
        return (int)eqs.size() - insize;
}
*/
} // namespace sim
