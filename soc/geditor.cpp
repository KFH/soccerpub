#include "gfx.hpp"
#include "geditor.hpp"
#include "keys.hpp"

#include "tiles.xpm.hpp"

int mapUnits = 16;

//((x + (divisor / 2)) / divisor) * divisor; }
class worldInspect_t : public queryCallback_t<maptile_t> {
    public:
	std::vector<int> eindex;
	virtual bool     callback(int index, const rect_t& ir, const maptile_t& mt) override
	{
		eindex.push_back(index);
		return 0;
	}
};
worldInspect_t inspector;

void gameEditor::mouseEvent(int act, int x, int y, int btn)
{

	int w, h;
	gfx::GetScreenDim(w, h);

	x = std::round((float)x / (float)w * (float)mainView.w);
	y = std::round((float)y / (float)h * (float)mainView.h);

	mx = rdiv(mainView.worldX(x), mapUnits);
	my = rdiv(mainView.worldY(y), mapUnits);

	switch (act) {
	case gfx::MMOVE:
		// update bounds of drawing
		if (pendingrdy) {
			pending.x1 = mx;
			pending.y1 = my;
		}
		break;
	case gfx::MBTN_PRESS:
		// start trying to draw
		if (btn == gfx::MBTN_LEFT) {
			inspector.eindex.resize(0);
			pending.x0 = mx;
			pending.y0 = my;
			pending.x1 = pending.x0;
			pending.y1 = pending.y0;
			pendingrdy = true;
		}
		break;
	case gfx::MBTN_RELEASE:
		// if rect drawn has 0 area, try selecting tile
		// else create new rect with artwork
		if (pendingrdy && btn == gfx::MBTN_LEFT) {
			pending.x1 = mx;
			pending.y1 = my;
			pending.normalize();
			pendingrdy = false;
			if (pending.area() > 0) {
				// insert tile into map
				map->Insert(maptile_t{ uint8_t(sel_tile), tileType }, pending);
			} else {
				// sel area 0 try selecting entry from map
				mx = mainView.worldX(x);
				my = mainView.worldY(y);
				map->Search(rect_t{ mx, my, mx, my }, inspector);
			}
		}
	}
}

void gameEditor::keyboardEvent(int keycode, int event)
{
	if (event == KEY_EVENT_PRESS) {
		switch (keycode) {
		case KEY_PAGE_UP:
			sel_tile++;
			break;
		case KEY_PAGE_DOWN:
			sel_tile--;
			break;
		case KEY_HOME:
			tileType = (tileType + 1) % 2;
			break;
		case KEY_F9:
			SaveMap();
			break;
		case KEY_F10:
			LoadMap();
			break;
		case KEY_DELETE:
			if (inspector.eindex.size() > 0) {
				for (int ix : inspector.eindex) {
					map->Delete(ix);
				}
				inspector.eindex.resize(0);
			}
		}
	}
}

void gameEditor::Input()
{
	// proces inputs to modify editor state
	// mouse, keyboard

	// make mouse wheel cycle tiles?
}

void gameEditor::SaveMap()
{
	FILE* fp;
	fp = fopen("test.map", "w");
	if (fp == nullptr)
		return;

	printf("SAVING\n");

	// write num entries 64bit
	// write out all rect_t
	// write out all entries

	// duplicate and then delete free/empty entries so only useful data written out
	std::vector<fieldMap_t::entry_t> data = map->entries;
	std::sort(map->freeEntries.rbegin(), map->freeEntries.rend());
	for (auto ix : map->freeEntries) {
		data.erase(data.begin() + ix);
	}

	uint64_t len = data.size();
	fwrite(&len, sizeof(len), 1, fp);
	fwrite(data.data(), sizeof(fieldMap_t::entry_t), len, fp);
	fclose(fp);
}
void gameEditor::LoadMap()
{

	FILE*                            fp;
	uint64_t                         len;
	std::vector<fieldMap_t::entry_t> data;
	fp = fopen("test.map", "r");
	if (fp == nullptr)
		return;

	printf("LOADING\n");

	map->Reset();
	fread(&len, sizeof(len), 1, fp);
	data.resize(len);
	fread(data.data(), sizeof(fieldMap_t::entry_t), len, fp);
	fclose(fp);
	for (auto& e : data) {
		map->Insert(e.datum, e.rec);
		printf("rec %d %d %d %d\n", e.rec.x0, e.rec.y0, e.rec.x1, e.rec.y1);
	}
}

void gameEditor::Draw(renderContext& ctx, Messanger& msger)
{
	// TODO draw current selected tile type

	if (ctx.tileDesc.size() > 1 && ctx.tileDesc[1].size() > 0) {
		const auto& ts = ctx.tileDesc[1];

		sel_tile              = sel_tile < 0 ? ts.size() - 1 : sel_tile % (int)ts.size();
		const auto& td        = ts[sel_tile];
		int         sel_spite = 1, sel_map = 4;

		int x = 1, y = 1;

		ctx.blit(sel_spite, sel_map, td.x0, td.y0, td.x1, td.y1, x, y);
		ctx.drawLine(0, 0, td.width() + 2, 0, ctx.palette.BLUE);
		ctx.drawLine(0, td.height() + 2, td.width() + 2, td.height() + 2, ctx.palette.BLUE);
	}

	// draw cursor + current tool
	ctx.drawCircle(mainView.screenX(mx), mainView.screenY(my), 8, ctx.palette.BLUE);
	int px0 = mainView.screenX(mx) - mapUnits;
	int py0 = mainView.screenY(my);
	int px1 = px0 + mapUnits * 2;
	int py1 = py0;
	ctx.drawLine(px0, py0, px1, py1, ctx.palette.BLUE);
	px0 = mainView.screenX(mx);
	py0 = mainView.screenY(my) - mapUnits;
	px1 = px0;
	py1 = py0 + mapUnits * 2;
	ctx.drawLine(px0, py0, px1, py1, ctx.palette.BLUE);

	// draw bounding box on selected if relavent
	if (inspector.eindex.size() > 0) {
		for (int ix : inspector.eindex) {
			auto&   temp = map->entries[ix].rec;
			int     px0 = mainView.screenX(temp.x0), py0 = mainView.screenY(temp.y0);
			int     px1 = mainView.screenX(temp.x1), py1 = mainView.screenY(temp.y1);
			uint8_t color = ctx.palette.BLUE;
			ctx.drawLine(px0, py0, px0, py1, color);
			ctx.drawLine(px0, py0, px1, py0, color);
			ctx.drawLine(px1, py1, px1, py0, color);
			ctx.drawLine(px0, py1, px1, py1, color);
		}
	}
	int yp = tiles_xpm_tiles[sel_tile].spriteL().height();
	msger.BeginMsg(0, yp, 12, 100);
	msger.Write(tileType == maptile_t::wall ? "wall" : "floor");

	// draw bounding box if relavent
	if (pendingrdy) {
		rect_t temp = pending;
		temp.normalize();
		int     px0 = mainView.screenX(temp.x0), py0 = mainView.screenY(temp.y0);
		int     px1 = mainView.screenX(temp.x1), py1 = mainView.screenY(temp.y1);
		uint8_t color = ctx.palette.BLUE;
		ctx.drawLine(px0, py0, px0, py1, color);
		ctx.drawLine(px0, py0, px1, py0, color);
		ctx.drawLine(px1, py1, px1, py0, color);
		ctx.drawLine(px0, py1, px1, py1, color);
		uint8_t     spritesrc = 1;
		uint8_t     colormap  = 4;
		spriteLoc_t sl        = tiles_xpm_tiles[sel_tile].spriteL();
		ctx.tile(spritesrc, colormap, sl, px0, py0, px1, py1);
	}
}
