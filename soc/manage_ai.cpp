#include "instance.hpp"
#include "keymap.hpp"
#include <algorithm>
#include <bitset>

// TODO
// look into SHOP2 JSHOP2 HTN (Hierarchical Ordered Planner)
// htn recursively decompose tasks into sub tasks, stopping
// when it reaches primitives that can be executed by operators
//
namespace sim {
v3 teamManager_t::opponentGoalPos(Instance &inst) const {
	v3 oGoalPos = v3{ 0,  fp32(-inst.field.hHeight), 0 };
	if (teamid != 0) {
		oGoalPos = v3{ 0, fp32( inst.field.hHeight), 0 };
	} 
	return oGoalPos;
};
v3 teamManager_t::myGoalPos(Instance & inst) const {
	v3 oGoalPos = v3{ 0,  fp32(inst.field.hHeight), 0 };
	if (teamid != 0) {
		oGoalPos = v3{ 0, fp32(-inst.field.hHeight), 0 };
	} 
	return oGoalPos;
};
enum {

	// GOALS
	IDEL    = 0x00,
	GOAL    = 0x01,
	DEFENSE = 0x02,
	SUPPORT = 0x04,
	IDLE    = 0x08,

	// PRECONDITIONS & EFFECTS
	POSSESSION = 0x10,
	PRESSURE   = 0x20, // ball on opponents side of field
	NEARPOINT  = 0x40, // special case?
	NEARTARGET = 0x80,

	PATHTARGET     = 0x0100,
	PATHPOINT      = 0x0200,
	NEARBALL       = 0x0400,
	NOBALL         = 0x0800,
	SAFETY         = 0x1000,
	NOPOSSESION    = 0x2000,
	TEAMPOSSESION  = 0x4000,
	INPLAY         = 0x8000,

	BALLNEEDSCOVER = 0x010000,
	ISKICKER       = 0x020000,// if player is designated kicker flag is set 

};

union costMeta_u {
	v3 pos;
 	entityID target;
};
typedef float   (*cost_fn)( v3 cpos, costMeta_u meta);
typedef aiCMD_t (*make_cmd_fn)(const Instance& in, const entity& e, entityID uid, costMeta_u meta);

struct Action_t {
	uint64_t   type;
	uint64_t   precon;
	uint64_t   effect;
	v3 	   atPos;

	costMeta_u meta;

	make_cmd_fn cmd;
	cost_fn     cost;
	
	bool operator==(const int& goal) const { return effect & goal; }
};
struct state_t {
	uint64_t   effect;
	v3 	   atPos;
};

// NOTHING
float doNothingCost(v3 cpos, costMeta_u meta)
{
	return 0.f;
}
aiCMD_t doNothing(const Instance& inst, const entity& e, entityID uid, costMeta_u meta)
{
	return aiCMD_t{ aiCMD_t::NONE };
}
// ATTACK
float doAttackCost( v3 cpos, costMeta_u meta)
{
	return 10.f;
}
aiCMD_t doAttack(const Instance& inst, const entity& e, entityID uid, costMeta_u meta)
{
	fp32     bd;
	entityID bid;
	inst.closestBallPtr(e.pos, bd, bid);
	return aiCMD_t{ aiCMD_t::ATTACK, v3{}, bid };
}
// GOTO
float doGotoPointCost( v3 cpos, costMeta_u meta)
{
	return meta.pos.dist(cpos); 
}
aiCMD_t doGotoPoint(const Instance& inst, const entity& e, entityID uid, costMeta_u meta)
{
	return aiCMD_t{ aiCMD_t::GOTO, meta.pos, 0 };
}
// PASS
float doPassCost( v3 cpos, costMeta_u meta)
{
	// TODO
	// create some passing metric, favour middle distance over far or near players?
	return 10.f;
}
aiCMD_t doPass(const Instance& inst, const entity& e, entityID uid, costMeta_u meta)
{
	auto* t = inst.ent.Get(meta.target);
	return aiCMD_t{ aiCMD_t::PASS, (t->pos - e.pos).normal(), meta.target };
}
// SHOOT
float doShootCost(v3 cpos, costMeta_u meta)
{
	return 10.f;
}
aiCMD_t doShoot(const Instance& inst, const entity& e, entityID uid, costMeta_u meta)
{
	v3 pos = inst.closestNet(uid);
	return aiCMD_t{ aiCMD_t::SHOOT, (pos - e.pos).normal(), 0 };
}
// TODO
// make command to COVER opponent eg position player between opponent and net
// make command to SUPPORT teamate eg position self laterally from teammate or ahead

enum {	// action types
	NONE,
	SHOOT,
	PASS,
	DEFEND, // similar to idle?
	GOTOTARGET, // intercepting pathing?
	ATTACK,
	KICKIN,
};
// clang-format off
Action_t  defaultActions[] = {
{ NONE,0,0,0},
{ SHOOT,     POSSESSION|INPLAY,   	   PRESSURE|GOAL,          v3{},{},doShoot,  doShootCost},
{ PASS,      POSSESSION|INPLAY,            SUPPORT|SAFETY,          v3{},{},doPass,   doPassCost},
{ DEFEND,    NONE,                         DEFENSE,                 v3{},{},doNothing,doNothingCost},
{ GOTOTARGET,NEARBALL,    		   POSSESSION|NEARTARGET,   v3{},{},doAttack, doAttackCost},
{ ATTACK,    NOPOSSESION|NEARTARGET|INPLAY,TEAMPOSSESION|POSSESSION,v3{},{},doAttack, doAttackCost},
{ KICKIN,    ISKICKER|POSSESSION, 	   INPLAY,                    v3{},{},doShoot,doShootCost},
};
// { GOTOPOINT,    NONE,                NEARPOINT, v3{}, doGotoPoint,doGotoPointCost },
// clang-format on

// give each ai a list of ranked goals?
struct GoalReqPair_t {
	uint64_t goal;
	uint64_t req;    // world state that must be true
	uint64_t reqnot; // world state that must be false
};
#define MAXGOALS 7
GoalReqPair_t defenseGoals[MAXGOALS] = {
	// make some kind of prep goal?
	{ INPLAY, NONE, INPLAY},
	{ TEAMPOSSESION, NEARBALL | BALLNEEDSCOVER , TEAMPOSSESION},
	{ SAFETY, POSSESSION|INPLAY, SAFETY },
	{ PRESSURE, POSSESSION|INPLAY, NONE },
	{ DEFENSE, NONE, NONE},
	{ NONE, NONE, NONE }


};

GoalReqPair_t forwardGoals[MAXGOALS] = {
	{ INPLAY, NONE, INPLAY},
	{ TEAMPOSSESION, NEARBALL | BALLNEEDSCOVER, TEAMPOSSESION }, 
	{ SAFETY, POSSESSION | INPLAY, SAFETY },
	{ GOAL, POSSESSION | INPLAY, NONE },
	{ SUPPORT, NEARBALL | TEAMPOSSESION | INPLAY | BALLNEEDSCOVER, NONE },
	{ DEFENSE, NONE, NONE },
	{ NONE, NONE },
};

// passto, defend, attack, shoot dir, formation

float Astar(uint8_t start, uint64_t goal, 
		const std::vector<Action_t> acts, 
		std::vector<uint8_t>& seq)
{

	int itr     = 0;
	int max_itr = acts.size() + 1;

	thread_local std::vector<uint8_t>     closedset;
	thread_local std::vector<uint8_t>     openset;
	thread_local keymap<uint8_t, uint8_t> camefrom;
	thread_local keymap<uint8_t, float>   gscore;
	thread_local keymap<uint8_t, uint64_t>     geffect;
	thread_local keymap<uint8_t, float>   fscore;

	closedset.resize(0);
	openset.resize(0);
	camefrom.resize(0);
	gscore.resize(0);
	geffect.resize(0);
	fscore.resize(0);

	openset.push_back(start);
	gscore.insert(start, 0.f);
	geffect.insert(start, acts[start].effect);
	fscore.insert(start, 100.f);

	while (openset.size() > 0 && itr < max_itr) {
		itr++;
		uint8_t cix    = 0;
		float   thresh = 9999.f;
		// fprintf(stdout,"	testing %d: ", openset.size());
		for (auto oix : openset) {
			// fprintf(stdout, "%d-", oix);
			float nfscore = fscore.getVal(fscore.find(oix));

			if (nfscore < thresh) {
				thresh = nfscore;
				cix    = oix;
			}
		}
		// fprintf(stdout,"\n");

		if (acts[cix].effect & goal) {
			// reached goal reconstruct path
			seq.push_back(cix);
			int nc = camefrom.find(cix);
			while (nc >= 0) {
				cix = camefrom.getVal(nc);
				seq.push_back(cix);
				nc = camefrom.find(cix);
			}
			// fprintf(stdout,"ITR %d\n", itr);
			return 1.f;
		}
		// makesure cix is in openset when erasing
		auto it = std::find(openset.begin(), openset.end(), cix);
		if (it != openset.end()) {
			openset.erase(it);
		}

		closedset.push_back(cix);
		float cscore = gscore.getVal(gscore.find(cix));
		uint64_t cstate = geffect.getVal(geffect.find(cix));

		for (uint8_t nix = 1; nix < acts.size(); nix++) {

			// TODO
			// do cix nodes cumulative effects meet preconditions of nix node?
			// ifnot skip

			if ((acts[nix].precon & (~cstate)) == 0) {
				if (std::find(closedset.begin(), closedset.end(), nix)
				    != closedset.end()) {
					continue;
				}
			} else {
				continue;
			}
			// fprintf(stdout,"	%s-%s\n", actionNames[cix], actionNames[nix]);
			// fprintf(stdout,"	 %s\n	 %s\n",
			//  	std::bitset<16>(cstate).to_string().c_str(),
			//  	std::bitset<16>(acts[nix].precon).to_string().c_str());
	
			// cacluate cost to executino action
			float td = cscore + acts[nix].cost(acts[cix].atPos, acts[nix].meta);
			if (std::find(openset.begin(), openset.end(), nix) == openset.end()) {
				openset.push_back(nix);
			}
			// not a better path
			int ix = gscore.find(nix);
			if (ix >= 0) {
				if (td >= gscore.getVal(ix)) {
					// fprintf(stdout,"	skipping\n");
					continue;
				}
			}
			uint64_t newstate = (cstate | acts[nix].effect);
			camefrom.insert(nix, cix);
			gscore.insert(nix, td);
			geffect.insert(nix, newstate);
			fscore.insert(nix,
			              (td + 10.f)); // td + heuristic_cost_estimate(nix, goal));
		}
	}
	// fprintf(stdout,"ITER %d openset %d\n", itr, openset.size());
	return -1.f;
}

void teamManager_t::Step(Instance& inst)
{


	// TODO/IDEA
	// teamManager_t needs its own hierarchy of goals for team. determined by games phase
	// or state
	/*
	switch(inst.gamePhase) {
	case phases::FACEOFF:
		// make formation depending on if attacking or recieving
		// send forwards to center
		// have one carry ball to center
		// once ball crosses center line change phase

		break;
	case phases::PENALTY:
		// make formation depending on if attacking or recieving
		// choose player to take shot

		break;
	case phases::KICKIN:
		// make formation depending on if attack or recieving
		// choose player to kick ball back in

		break;
	case phases::PLAY:
	case phases::NONE:
	default:
		break;
	}
	*/



	// TODO/IDEA
	// determine state of world, in possesison, players in position?
	// for each player determin their individual current state and goal?
	// for each player determin a sequence of actions too arrive at goal

	// TODO
	// how do we determine if an action completed successfully

	thread_local std::vector<Action_t> availableActions;
	availableActions.resize(0);

	// first action will hold current state
	availableActions.push_back(defaultActions[NONE]);

	// TODO
	// push_back SHOOT actions for all available shooting points on opponent nets
	{	
		v3 p0 = opponentGoalPos(inst);
		v3 p1 = myGoalPos(inst);
		v3 dir = (p0 - p1).normal(); 
		v3 wdir = dir.cross(v3{0,0,1}).normal();
		availableActions.push_back(defaultActions[SHOOT]);
		availableActions.back().atPos = p0 + dir * 150.f ;
		availableActions.push_back(defaultActions[SHOOT]);
		availableActions.back().atPos = p0 + dir * 150.f + wdir * -150.f;
		availableActions.push_back(defaultActions[SHOOT]);
		availableActions.back().atPos = p0 + dir * 150.f + wdir * 150.f;
	
	}

	for (auto ps = inst.players.begin(); ps != inst.players.end(); ps++) {
		if (ps->teamid != teamid)
			continue;

		availableActions.resize(4);

		// TODO
		// push_back DEFEND action for current players field position
		availableActions.push_back(defaultActions[DEFEND]);
		availableActions.back().atPos = ps->fpos; // defend field position

		auto& p = *inst.ent.Get(ps.Key());
		uint64_t cstate = NONE;
		fp32     bd;
		entityID bid;
		auto     b = inst.closestBallPtr(p.pos, bd, bid);
		if (b != nullptr) {
			auto bs = inst.balls.Get(bid);

			if (bs->Possessor() != NULL_ID && bs->PossessorTeam() == teamid) {
				cstate |= TEAMPOSSESION;
			} else {
				cstate |= NOPOSSESION;
			}

			if (bs->state == ballState::INBOUNDS)
				cstate |= INPLAY;

			if (bs->Possessor() == ps.Key()) 
				cstate |= POSSESSION;

			if (bd < 420) {
				cstate |= NEARBALL;
			} else {
				cstate |= NOBALL;
			}

			v3  hdim  = v3{ 50, 50, 0 };
			int count = inst.PlayersInZone(ps->teamid, b->pos + hdim, b->pos - hdim);
			if (count < 2 || bd < 50)
				cstate |= BALLNEEDSCOVER;

			// check if player is designated kicker	for ball
			if((cstate & INPLAY) == 0) {
				entityID closestsID = NULL_ID;
				inst.ClosestOfTeam(ps->teamid, b->pos, closestsID, 0.f);
				if(closestsID == ps.Key()) {
					cstate |= ISKICKER;
					availableActions.push_back(defaultActions[KICKIN]);
					availableActions.back().atPos = b->pos;
				}
			}
			if(cstate & NOPOSSESION) {
				// TODO
				// push_back GOTOTARGET action for all balls not in team possession 
				// AND nearby
				availableActions.push_back(defaultActions[GOTOTARGET]);	
				availableActions.back().atPos = b->pos;
				// TODO
				// push_back ATTACK action for all opponents in possession of ball 
				// AND nearby or just reuse previous ball entries for lazyness
				availableActions.push_back(defaultActions[ATTACK]);	
				availableActions.back().atPos = b->pos;
			}
		}

		cstate |= SAFETY;
		if (cstate & POSSESSION) {
			// if opponent is close remove safety
			entityID    eid    = NULL_ID;
			const auto* e      = inst.ClosestOfTeam(ps->teamid ^ 1, p.pos, eid);
			v3          netpos = inst.closestNet(ps.Key());
			fp32        d      = e->pos.dist(p.pos);

			fp32 dot = (netpos - p.pos).normal().dot((e->pos - p.pos).normal());
			if (d > 30.f && d < 150.f && dot > 0.6) {
				cstate = cstate & (~SAFETY);
			}
		}
		if(cstate & POSSESSION) {
			// TODO
			// push_back PASS actions for all open players if current player 
			// has ball 
			// atPos = current player position?
			entityID target;
			auto* t = inst.ClosestOfTeam(ps->teamid, p.pos, target, 160.f);
			if (t != nullptr) {
				availableActions.push_back(defaultActions[PASS]);
				availableActions.back().atPos = p.pos;
				availableActions.back().meta.target = target;
			}
		}

		for (const auto& e : inst.events) {
			if (e.type == EV::GOAL && e.meta.goal.teamid == ps->teamid) {
				cstate |= GOAL;
				// TODO hack
				cstate |= PRESSURE;
				cstate = cstate & (~INPLAY);
				break;
			}
		}

		// if (inst.closestNet(ps.Key()).dist(p.pos) < 150.f)
		// 	cstate |= PRESSURE;

		// look at previous command ? ? ?
		// to determine if moving succeeded?
		if (ps->cmd.type == aiCMD_t::GOTO) {
			if (ps->cmd.vec.dist(p.pos) < 10.f)
				cstate |= NEARPOINT;
		}
		if (ps->cmd.type == aiCMD_t::ATTACK) {
			fp32     dist = 0;
			entityID bid;
			inst.closestBallPtr(p.pos, dist, bid);
			if (dist < 16.f)
				cstate |= NEARTARGET;
		}	
		if (ps->fpos.dist(p.pos) < 100.f)
			cstate |= DEFENSE;






		// TODO
		// set first available action as current state
		availableActions.front().effect = cstate;
		availableActions.front().atPos = p.pos;


		// NOTES
		// try find first unsatisfied goal then astar a plan to achieve goal
		// if astar fails try next goal until achievable goal/path is found
		ps->cmd = aiCMD_t{};
		int tgoal = 0;
		int reqnot = 0;
		thread_local std::vector<uint8_t> seq;
		GoalReqPair_t(&goalList)[MAXGOALS]
		    = ps->GetRole() == playerState::FORWARD ? forwardGoals : defenseGoals;

		for (const auto& g : goalList) {
			seq.resize(0);
			if ((g.reqnot & cstate) == 0) {
				if (g.req == 0 || (cstate & g.req) == g.req) {
					tgoal  = g.goal;
					reqnot = g.reqnot;
				} else {
					continue;
				}
			}
			if (tgoal == NONE) {
				// nothing to do
				continue;
			} else if (Astar(0, tgoal, availableActions, seq) < 0.f) {
				// path finding failed continue to next goal
				continue;
			} else if (seq.size() < 2) {
				// astar found no actions needed ?
				break;
			} else {
				// ignore first action
				auto it = seq.rbegin();
				it++;
				if (it == seq.rend())
					continue;
				if(!availableActions[*it].cmd)
			       		continue;
				auto& act = availableActions[*it];
				if(act.atPos.dist(p.pos) > 36.f ) {
					// make a goto command if distance to action
					// is too great
					ps->cmd = aiCMD_t{ aiCMD_t::GOTO, act.atPos, 0 };
				} else {
					ps->cmd = availableActions[*it].cmd(
						inst, 
						p, 
						ps.Key(),
						availableActions[*it].meta
						);
				}
				break;
			}
		}
	}
};


void teamManager_t::setFieldFormation(Instance& inst) {
	
	// TODO
	// move elsewhere later
	std::vector<v3> formation;
	v3 mGoalPos = myGoalPos(inst);
	v3 oGoalPos = opponentGoalPos(inst);

	v3 dist = oGoalPos - mGoalPos;
	v3 dir = (dist).normal();
	v3 wdir = dir.cross(v3{0,0,-1}).normal();
	
	// goal line
	v3 g0 = mGoalPos +(dir * 46.f) + wdir * -inst.field.hGoal; 
	v3 g1 = mGoalPos +(dir * 46.f);
	v3 g2 = mGoalPos +(dir * 46.f) + wdir * inst.field.hGoal; 

	// defense line
	fp32 d_margin = 160.f;
	v3 d0 = mGoalPos + (dir * (dist.len() * 5.f/16.f)) + wdir * (inst.field.hWidth - d_margin); 
	v3 d1 = mGoalPos + (dir * (dist.len() * 2.f/16.f)); 
	v3 d2 = mGoalPos + (dir * (dist.len() * 5.f/16.f)) + wdir * -(inst.field.hWidth - d_margin);

	// forward line
	fp32 f_width = 256.f;
	v3 f0 = wdir*8.f + mGoalPos + (dir * (dist.len() * 7.f/16.f)) + wdir * f_width;
	v3 f1 = wdir*8.f + mGoalPos + (dir * (dist.len() * 8.f/16.f));
	v3 f2 = wdir*8.f + mGoalPos + (dir * (dist.len() * 7.f/16.f)) + wdir * -f_width;

	//
	fp32 goalies = 0.f;
	fp32 defense = 0.f;
       	fp32 forwards = 0.f;	
	fp32 tg = 0.f;
	fp32 td = 0.f;
	fp32 tf = 0.f;
	for (auto ps = inst.players.begin(); ps != inst.players.end(); ps++) {
		if (ps->teamid != teamid)
			continue;
		switch(ps->GetRole()) {
		case playerState::GOALIE:
			goalies++;
			break;
		case playerState::DEFENSE:
			defense++;
			break;
		case playerState::FORWARD:
			forwards++;
			break;
		default:
			//
			break;
		}
	}
	tg = 1.f / goalies  * 0.5f;
	td = 1.f / defense  * 0.5f;
	tf = 1.f / forwards * 0.5f;

	for (auto ps = inst.players.begin(); ps != inst.players.end(); ps++) {
		if (ps->teamid != teamid)
			continue;
		auto& p = *inst.ent.Get(ps.Key());
		v3 pos;
		switch(ps->GetRole()) {
		case playerState::GOALIE:
			pos = quadratic(tg, g0, g1, g2);
			tg += 1.f / goalies;
			break;
		case playerState::DEFENSE:
			pos = quadratic(td, d0, d1, d2);
			td += 1.f / defense;
			break;
		case playerState::FORWARD:
			pos = quadratic(tf, f0, f1, f2);
			tf += 1.f / forwards;
			break;
		default:
			//
			break;
		}
		p.pos = pos;	
		ps->SetFieldPos(p.pos.x,p.pos.y);
	}
}
}; // namespace sim
