#include "instance.hpp"

namespace sim {

// TODO
// can this be broken into GOAP? assign actions costs and preconditions
// use some kind of cost minimizing to achieve goal?
//
// TODO
// look into Hierarchical Task Network planning (HTN)
// for a team coordinator that plans moves for players to execute in parallel
// to achieve a goal
//
// ACTIONS
// hang out at field position between ball and goal
// pass ball to teamates
// carry ball down field
// slide tackle opponents with ball
// jump for ball in air
//
// GOALIE
// grab ball
//
// TODO
// idea for design of AI systems
//
// coordinator class sets each player on a teams goal, DEFEND, ATTACK, PASS, IDLE
// ai chooses action and movement direction based on DEFEND ATTACK etc, position and its
// surroundings?

/*
v3 Avoid(const Instance& inst, const entity& me)
{
        // boid style avoidance of nearby players personal space
        v3 ndir = { 0, 0, 0 };
        for (const entity& e1 : inst.players) {
                fp32 mindist = 45*45; // compare sqr distances cause its faster
                if (e1.uid != me.uid) {
                        v3   dir     = e1.pos - me.pos;
                        fp32 l       = dir.len2();
                        if (l < mindist) {
                                ndir = ndir
                                    + dir.normal() * fp32(-(fp32(1) - sqrtfp(l) / mindist) *
fp32(0.2f));
                        }
                }
        }
        if (ndir.len() > 1)
                ndir = ndir.normal();
        return ndir;
}
*/
void playerState::Step(const Instance& inst, const entity& me, playerCtrl& ctrl) /*const*/
{
	v3   jsdir = { 0, 0, 0 };
	fp32 d     = 0;
	ctrl.SetAct(0);

	switch (cmd.type) {
	case aiCMD_t::NONE:
		break;
	case aiCMD_t::GOTO:
		d = me.pos.dist(cmd.vec);
		if (d > 6.f) {
			jsdir = (cmd.vec - me.pos).normal() * std::max(d / 16.f, 1.f);
			// jsdir = jsdir + (Avoid(inst, me) * fp32(8.0));
		} else {
			// goal complete?
		}
		break;
	case aiCMD_t::PASS:
		jsdir = cmd.vec;
		if (me.vel.normal().dot(cmd.vec) > 0.75f)
			ctrl.SetAct(playerCtrl::BPASS);
		break;
	case aiCMD_t::SHOOT:
		jsdir = cmd.vec;
		ctrl.SetAct(playerCtrl::BKICK);
		break;
	case aiCMD_t::FORMATION: {

		auto* e = inst.ent.Get(cmd.target);
		// TODO rotate formation offset from target based on velocity direction
		// e.vel.normal()
		// cmd.vec.normal()

		jsdir = ((e->pos + cmd.vec) - me.pos).normal();
		// jsdir = jsdir + (Avoid(inst, me) * fp32(8.0));
	} break;
	case aiCMD_t::ATTACK: {

		auto it = inst.ent.Get(cmd.target);
		if (it != nullptr) {

			v3   delta = (it->pos - me.pos);
			v3   d2    = delta.normal() - it->vel.normal();
			fp32 rr    = d2.y != 0.f ? d2.x / d2.y : 0.f;

			fp32 s = me.vel.len();

			fp32 ang = tanf(rr);

			fp32 div = s <= 0.1 ? 0.f : delta.len() * std::cos(ang) / s;
			div      = clamp(div, 0.f, 100.f);
			cmd.vec  = (it->pos + (it->vel.normal() * div * 1.f));

			jsdir = (cmd.vec - me.pos).normal();

			// jsdir = ((it->pos ) - me.pos).normal();

			d = me.pos.dist(it->pos);

			auto bs = inst.balls.Get(cmd.target);
			if (d < 36 && bs && bs->Possessor() != NULL_ID
			    && bs->PossessorTeam() != teamid && CanTackle())
				ctrl.SetAct(playerCtrl::BTACKLE);
		}
	} break;
	}
	if (jsdir.len2() > 0.1)
		jsdir = jsdir.normal();
	ctrl.SetDir(jsdir.x, jsdir.y);
}
} // namespace sim
