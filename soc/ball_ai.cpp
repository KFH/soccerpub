#include "instance.hpp"
// #include "buff.hpp"

namespace sim {

class ballCollisionQuery : public queryCallback_t<maptile_t> {
    public:
	entity& me;
	int     x, y;
	v3      npos;
	fp32    mx, my, zpos;
	bool    hasHit;
	ballCollisionQuery(entity& ball)
	    : me(ball)
	{
		fp32 margin = fp32(0.f);
		x           = !signfp(me.vel.x);
		y           = !signfp(me.vel.y);
		npos        = me.pos + me.vel;
		mx          = fp32(x) * margin;
		my          = fp32(y) * margin;
		npos.x += mx;
		npos.y += my;
		hasHit = false;
		zpos   = npos.z;
	}
	// TODO should determine a deflection point and plane/planes
	// based on all returned intersections
	virtual bool callback(int index, const rect_t& ir, const maptile_t& i) override
	{
		if (hasHit)
			return 0;
		if (i.type == maptile_t::floor)
			return 0;

		fp32 x0  = x ? fp32(ir.x0) : fp32(ir.x1);
		fp32 x1  = x ? fp32(ir.x1) : fp32(ir.x0);
		fp32 y0  = y ? fp32(ir.y0) : fp32(ir.y1);
		fp32 y1  = y ? fp32(ir.y1) : fp32(ir.y0);
		v3   p0  = v3{ x0, y0, 0 };
		v3   hp0 = v3{}, hp1 = v3{};
		bool b0 = false, b1 = false;

		// left right
		if (Intersects(me.pos, npos, p0, v3{ x0, y1, 0 }, hp0)) {
			// invert x velocity
			// me.vel.x = me.vel.x * fp32(-1);
			b0 = true;
		}

		// up down
		if (Intersects(me.pos, npos, p0, v3{ x1, y0, 0 }, hp1)) {
			// invert y velocity
			// me.vel.y = me.vel.y * fp32(-1);
			b1 = true;
		}
		// determine time of impact
		if (b0 & b1) {
			// fp32 d = hp0.dist(me.pos);
			// fp32 m = (d - npos.dist(me.pos)) / d;
			me.vel = me.vel * v3{ fp32(-1), fp32(-1), fp32(1) };

			me.pos = hp0; // + (me.vel * m);

			me.pos.x += mx;
			me.pos.y += my;
		} else if (b0) {

			// fp32 d = hp0.dist(me.pos);
			// fp32 m = (d - npos.dist(me.pos)) / d;
			me.vel = me.vel * v3{ fp32(-1), fp32(1), fp32(1) };

			me.pos = hp0; // + (me.vel * m);

			me.pos.x += mx;

		} else if (b1) {
			// fp32 d = hp0.dist(me.pos);
			// fp32 m = (d - npos.dist(me.pos)) / d;
			me.vel = me.vel * v3{ fp32(1), fp32(-1), fp32(1) };
			me.pos = hp1; // + (me.vel * m);

			me.pos.y += my;
		}
		if (b0 | b1) {
			me.vel   = me.vel * fp32(0.95f);
			me.pos.z = zpos;
		}

		hasHit |= b0 | b1;

		return 0;
	}
};
void ballState::Step(const Instance& inst, eventRecorder& er, entity& me)
{

	v3 npos = me.pos + me.vel;
	// constrain ball, bounce of walls
	ballCollisionQuery bq(me);
	if (inst.map) {
		inst.map->Search(rtBoxPoint(me.pos, int(me.vel.len()) + 3), bq);
	}

	if (me.pos.x > inst.field.hWidth || me.pos.x < -inst.field.hWidth
	    || me.pos.y > inst.field.hHeight || me.pos.y < -inst.field.hHeight) {
		// out of bounds

		if(state != OUTBOUNDS) {
			auto& ev     = er.AddEvent(EV::OUTBOUNDS, 60, me.pos);
		}
		state = OUTBOUNDS;
	} else {
		state = INBOUNDS;
		v3 filler;
		if (Intersects(me.pos, npos,
		               v3{ fp32(-inst.field.hGoal), fp32(inst.field.hHeight), 0 },
		               v3{ fp32(inst.field.hGoal), fp32(inst.field.hHeight), 0 }, filler)) {
			state = INGOAL_1;
		} else if (Intersects(me.pos, npos,
		                      v3{ fp32(-inst.field.hGoal), fp32(-inst.field.hHeight), 0 },
		                      v3{ fp32(inst.field.hGoal), fp32(-inst.field.hHeight), 0 },
		                      filler)) {
			state = INGOAL_2;
		}
	}

	// move
	if (!bq.hasHit)
		me.pos = npos;
	v3 nvel = me.vel;
	// only apply gravity if in the air
	if (me.pos.z > fp32(0.1f)) {
		nvel.z -= 0.3;
	}
	// invert z velocity on ground bounce
	if (me.pos.z < fp32(0))
		nvel.z = nvel.z * fp32(-0.8f);
	// drag
	if (me.pos.z > fp32(0.2f)) {
		nvel = nvel * adrag;
	} else {
		nvel = nvel * bdrag;
	}
	me.vel = nvel;

	// TODO/IDEA
	// mark ents in contention for control on ball
	// then do some kind of resolution step
	// ALTERNATE IDEA
	// if there is contention for possession player with
	// greatest velocity wins

	uint8_t oldteamid = teamid;
	SetPossessor(NULL_ID, 123);
	fp32 ovel = -1.f;
	if (me.pos.z < 6) {
		for (auto p = inst.players.begin(); p != inst.players.end(); p++) {
			auto e     = inst.ent.Get(p.Key());
			fp32 pdist = e->pos.dist(me.pos);
			if (pdist < 20.f) {
				fp32 nvel = e->vel.len();
				if (nvel > ovel) {
					ovel = nvel;
					SetPossessor(p.Key(), p->teamid);
				}
			}
		}
	}

	if (Possessor() == NULL_ID)
		return;
	auto p = inst.players.Get(Possessor());
	if (p != nullptr) {

		const entity& e = *inst.ent.Get(Possessor());
		// whats the point of this ?!?
		SetPossessor(Possessor(), p->teamid);

		if (oldteamid != teamid) {
			er.AddEvent(EV::STEAL, 60, e.pos);
		}
		if (p->GetAct() == playerState::PASS) {
			auto target = inst.ent.Get(p->cmd.target);
			v3 targetpt;
			if (p->cmd.target != NULL_ID && target != nullptr) {

				fp32 speed    = e.vel.len() + 12.f;
				targetpt = interceptPT(e.pos, target->pos, target->vel, speed);
				me.vel        = (targetpt - me.pos).normal() * speed;

			} else {
				me.vel = (e.vel.normal() * std::max(2.f, me.vel.len())) + e.vel;
				targetpt = me.pos + me.vel * 20.f;
			}
			// TODO i dont think this is necessary  next frame ball should be out of
			// players grasp
			// SetPossessor(NULL_ID, 123);

			auto& ev     = er.AddEvent(EV::PASS, 60, e.pos);
			ev.meta.pass = EV::Pass_t{ me.pos, targetpt};

		} else if ((p->GetAct() == playerState::KICK)
		           | (p->GetAct() == playerState::TACKLE)) {

			if (state == ballState::OUTBOUNDS) {
				// shoot into bounds
				// if out of bounds
				me.vel = me.pos.normal() * -6;
			} else {
				// kick in diection
				// of players
				// movement
				v3 n   = me.pos - e.pos;
				me.vel = me.vel + n.normal() * 3;
				me.vel = me.vel * 0.5 + e.vel;
			}
			auto& ev     = er.AddEvent(EV::KICK, 60, e.pos);
			ev.meta.kick = EV::Kick_t{ me.vel.normal(), me.vel.len(), 0 };

			// TODO z vel should vary based on kick
			// type
			// and direction etc
			me.vel.z = 6;
			// TODO i dont think this is necessary  next frame ball should be out of
			// players grasp
			// SetPossessor(NULL_ID, 123);
		} else {
			// gravitate towards and infront of players vel?

			// v3   dp  = (me.pos - e.pos).normal();
			v3   vn   = e.vel.normal();
			fp32 mag  = 12.f;
			v3   tpos = e.pos + (vn * mag);

			fp32 oz = me.vel.z;
			me.vel  = (me.vel * 0.9) + ((tpos - me.pos) * 0.1);

			me.vel.z = oz;

			/*
			fp32 dot = dp.dot(vn);
			v3   nd  = dp.cross(vn) * dot;
			vn       = vn * dot;

			// printf("ND %f %f %f\n", nd.x, nd.y, nd.z);
			// printf("BA %f %f %f\n", me.vel.x, me.vel.y, me.vel.z);

			fp32 oz  = me.vel.z;
			me.vel   = (me.vel * 0.2) + (e.vel * 0.8) + nd + vn;
			me.vel.z = oz;
			*/
		}
	}
}
} // namespace sim
