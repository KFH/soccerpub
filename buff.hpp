#ifndef BUFF_HPP
#define BUFF_HPP
#include <algorithm>
#include <stdlib.h>
#include <string.h>

// abstract interface for piping fixed sized data around
class sized_pipe_t {
    protected:
	size_t mtype_size;

    public:
	virtual void append(const char* in) = 0; // append to pipe/stream
	virtual int  read(char* out) = 0; // copy mtype_size to out and advances head of stream
	virtual int  read_const(char* out) const = 0;
	virtual int  remaining()                 = 0; // size / type_size;
	virtual void free(int x)                 = 0; // release whatever resources this has
	virtual void rewind()                    = 0; // reset head

	size_t type_size() const { return mtype_size; }
};

// TODO
// i might not want a hard cap in the future?
template <typename T, unsigned int CAP> class RingBuffer {
    private:
	int head;
	int tail;
	T   data[CAP];

    public:
	RingBuffer()
	    : data{}
	{
		head = 0;
		tail = 0;
	}

	void reset()
	{
		head = 0;
		tail = 0;
	}
	int dist(int s, int e) const { return s <= e ? e - s : e + (CAP - s); }
	int size() const { return dist(head, tail); };
	int push(const T& in)
	{
		// push back
		if (size() >= CAP)
			return -1;
		data[tail] = in;
		tail++;
		tail = tail % CAP;
		return size();
	}
	// insert relative to head
	void relInsert(int ofs, const T& in)
	{

		int i = (head + ofs) % CAP;
		if (dist(head, tail) <= dist(head, i)) {
			tail = i;
			tail++;
			tail = tail % CAP;
		}
		data[i] = in;
	}

	// goofy interfaces for easier use in message ring buffer
	void expandRel(int ofs)
	{
		int i = (head + ofs) % CAP;
		if (dist(head, tail) <= dist(head, i)) {
			tail = i;
			tail++;
			tail = tail % CAP;
		}
	}
	T& getRel(int ofs)
	{
		ofs   = std::max(0, ofs);
		int i = (head + ofs) % CAP;
		return data[i];
	}
	T getRel(int ofs) const
	{
		std::max(0, ofs);
		int i = (head + ofs) % CAP;
		return data[i];
	}
	T& pop()
	{
		int i = head;
		if (size() > 0) {
			head++;
			head = head % CAP;
		}
		return data[i];
	}

    public:
	struct ringiter_t {
		int         ix;
		int         head;
		int         tail;
		T*          data;
		ringiter_t& operator++(int)
		{
			ix = (ix + 1) % CAP;
			return *this;
		}
		ringiter_t& operator++()
		{
			ix = (ix + 1) % CAP;
			return *this;
		}
		T* operator->() { return &data[ix]; }
		T& operator*() { return data[ix]; }

		bool operator==(const ringiter_t& other) const { return ix == other.ix; }
		bool operator!=(const ringiter_t& other) const { return ix != other.ix; }
	};

	ringiter_t begin() { return ringiter_t{ head, head, tail, data }; }
	ringiter_t end() { return ringiter_t{ tail, head, tail, data }; }
};

// NOTE must manually freeData()  to deallocte
class ByteBuffer {
	char*  m_data;
	size_t m_off;
	size_t m_size;
	size_t m_cap;

    public:
	void append(const void* in, size_t in_s)
	{
		size_t newSize = in_s + m_size;
		if (newSize >= m_cap) {
			m_cap *= 2;
			m_cap  = newSize > m_cap ? newSize : m_cap;
			m_data = (char*)realloc(m_data, m_cap);
		}
		memcpy(m_data + m_size, in, in_s);
		m_size += in_s;
	}
	void resize(size_t newsize)
	{
		m_off = 0;
		if (newsize < m_size) {
			m_size = newsize;
			return;
		}
		m_size = newsize;
		if (newsize < m_cap)
			return;

		m_cap  = newsize;
		m_data = (char*)realloc(m_data, m_cap);
		return;
	}
	void*  head() const { return m_data + m_off; }
	void*  tail() const { return m_data + m_size; }
	size_t unread() const { return m_size - m_off; }
	size_t len() const { return m_size; }
	size_t cap() const { return m_cap; }
	char*  data() const { return m_data; }
	void   resetReader() { m_off = 0; }
	void   reset() { resize(0); }

	void freeData()
	{
		free(m_data);
		m_data = NULL;
		m_size = 0;
		m_cap  = 0;
		m_off  = 0;
	}
	void setData(const void* nd, int len)
	{
		m_data = (char*)nd;
		m_off  = 0;
		m_size = len;
		m_cap  = len;
	}

	void newBuffer(void* in, size_t in_s)
	{
		free(m_data);
		m_off  = 0;
		m_data = (char*)in;
		m_size = in_s;
		m_cap  = in_s;
	}
	// copy from m_off into out
	// out_s bytes or upto m_size
	// return amount copied
	// if out is null just advance read head offset
	size_t read(void* out, size_t out_s)
	{
		size_t copyamount = m_off + out_s > m_size ? m_size - m_off : out_s;
		if (out)
			memcpy(out, m_data + m_off, copyamount);
		m_off += copyamount;
		return copyamount;
	}
	size_t readback(size_t n)
	{
		if ((int)m_off - (int)n <= 0) {
			n     = m_off;
			m_off = 0;
		} else {
			m_off -= n;
		}
		return n;
	}
	size_t peek(void* out, size_t out_s) const
	{
		size_t copyamount = m_off + out_s > m_size ? m_size - m_off : out_s;
		if (out)
			memcpy(out, m_data + m_off, copyamount);
		return copyamount;
	}

	ByteBuffer()
	{
		m_data = 0;
		m_off  = 0;
		m_size = 0;
		m_cap  = 0;
	};
};
#endif
