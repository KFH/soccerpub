#pragma once

#include <cstring>
#include <vector>

namespace sim {

struct g_entityID {
	uint32_t id;
	uint32_t Ix() const { return id & 0x00ffffff; }
	uint32_t Gen() const { return id >> 24; }
	void     IncrGen()
	{
		uint32_t g = Gen() + 1;
		id         = Ix() | (g << 24);
	};
	bool operator<(const g_entityID& other) const { return id < other.id; }
	bool operator==(const g_entityID& other) const { return id == other.id; }
	bool operator!=(const g_entityID& other) const { return id != other.id; }
	bool operator==(const unsigned int& val) const { return id == val; }
};

static constexpr g_entityID NULL_ID = { 0 }; // reserve 0 for null id value

// dish out and verify whether entity exists
class g_entityTracker {

	std::vector<g_entityID> ents;
	std::vector<uint16_t>   recycling;
	g_entityID              incr; // = 0;

	// TODO
	// implement serialize deserialize function

    public:
	size_t RecyclingCount() { return recycling.size(); }
	size_t IdCount() { return ents.size(); }

	void Reset()
	{
		recycling.resize(0);
		ents.resize(0);
		ents.push_back(g_entityID{ 0xffffffff });
		incr.id = 0;
	}
	bool Exists(g_entityID e)
	{
		if (e.Ix() < ents.size())
			if (ents[e.Ix()] == e)
				return true;
		return false;
	}
	g_entityID Create()
	{
		if (recycling.size() > 64) {
			uint16_t r = recycling.back();
			recycling.pop_back();
			return ents[r];
		} else {
			// TODO
			//
			incr.id++;
			ents.push_back(incr);
		}
		return incr;
	}
	bool Destroy(g_entityID e)
	{
		if (Exists(e)) {
			e.IncrGen();
			ents[e.Ix()] = e;
			recycling.push_back(e.Ix());
			return true;
		}
		return false;
	}

	struct description_t {
		uint16_t   ent_len;
		uint16_t   recycling_len;
		g_entityID incr;
	};
	int SerializeReqSize() const
	{
		size_t data_len = 0;
		data_len += sizeof(description_t);
		data_len += ents.size() * sizeof(g_entityID);
		data_len += recycling.size() * sizeof(uint16_t);
		data_len += sizeof(incr);
		return data_len;
	}
	int Serialize(void* dst) const
	{

		size_t        ofs0 = sizeof(description_t);
		size_t        ofs1 = ents.size() * sizeof(g_entityID);
		size_t        ofs2 = recycling.size() * sizeof(uint16_t);
		description_t desc = { (uint16_t)ents.size(), (uint16_t)recycling.size(), incr };

		memcpy((char*)dst, &desc, ofs0);
		memcpy((char*)dst + ofs0, ents.data(), ofs1);
		memcpy((char*)dst + ofs0 + ofs1, recycling.data(), ofs2);
		return ofs0 + ofs1 + ofs2;
	}
	int Deserialize(void* src, size_t n)
	{
		// TODO
		// add checks
		size_t        ofs = sizeof(description_t);
		description_t desc;
		memcpy(&desc, src, ofs);
		incr = desc.incr;

		ents.resize(desc.ent_len);
		recycling.resize(desc.recycling_len);

		memcpy(ents.data(), (char*)src + ofs, desc.ent_len * sizeof(g_entityID));
		ofs += desc.ent_len * sizeof(g_entityID);
		memcpy(recycling.data(), (char*)src + ofs, desc.recycling_len);

		return 1;
	}
};

typedef int        playerIX;
typedef g_entityID entityID;
typedef uint16_t   playerID;

// #define NULLID 65535

} // namespace sim
