
#include "keys.hpp"

// key mapping taken from glfw.h
//
// acts as a translation table
short int keycodes[512];

void initKeycodeTable()
{
	memset(keycodes, 0, sizeof(keycodes));

	keycodes[0x00B] = KEY_0;
	keycodes[0x002] = KEY_1;
	keycodes[0x003] = KEY_2;
	keycodes[0x004] = KEY_3;
	keycodes[0x005] = KEY_4;
	keycodes[0x006] = KEY_5;
	keycodes[0x007] = KEY_6;
	keycodes[0x008] = KEY_7;
	keycodes[0x009] = KEY_8;
	keycodes[0x00A] = KEY_9;
	keycodes[0x01E] = KEY_A;
	keycodes[0x030] = KEY_B;
	keycodes[0x02E] = KEY_C;
	keycodes[0x020] = KEY_D;
	keycodes[0x012] = KEY_E;
	keycodes[0x021] = KEY_F;
	keycodes[0x022] = KEY_G;
	keycodes[0x023] = KEY_H;
	keycodes[0x017] = KEY_I;
	keycodes[0x024] = KEY_J;
	keycodes[0x025] = KEY_K;
	keycodes[0x026] = KEY_L;
	keycodes[0x032] = KEY_M;
	keycodes[0x031] = KEY_N;
	keycodes[0x018] = KEY_O;
	keycodes[0x019] = KEY_P;
	keycodes[0x010] = KEY_Q;
	keycodes[0x013] = KEY_R;
	keycodes[0x01F] = KEY_S;
	keycodes[0x014] = KEY_T;
	keycodes[0x016] = KEY_U;
	keycodes[0x02F] = KEY_V;
	keycodes[0x011] = KEY_W;
	keycodes[0x02D] = KEY_X;
	keycodes[0x015] = KEY_Y;
	keycodes[0x02C] = KEY_Z;

	keycodes[0x028] = KEY_APOSTROPHE;
	keycodes[0x02B] = KEY_BACKSLASH;
	keycodes[0x033] = KEY_COMMA;
	keycodes[0x00D] = KEY_EQUAL;
	keycodes[0x029] = KEY_GRAVE_ACCENT;
	keycodes[0x01A] = KEY_LEFT_BRACKET;
	keycodes[0x00C] = KEY_MINUS;
	keycodes[0x034] = KEY_PERIOD;
	keycodes[0x01B] = KEY_RIGHT_BRACKET;
	keycodes[0x027] = KEY_SEMICOLON;
	keycodes[0x035] = KEY_SLASH;
	keycodes[0x056] = KEY_WORLD_2;

	keycodes[0x00E] = KEY_BACKSPACE;
	keycodes[0x153] = KEY_DELETE;
	keycodes[0x14F] = KEY_END;
	keycodes[0x01C] = KEY_ENTER;
	keycodes[0x001] = KEY_ESCAPE;
	keycodes[0x147] = KEY_HOME;
	keycodes[0x152] = KEY_INSERT;
	keycodes[0x15D] = KEY_MENU;
	keycodes[0x151] = KEY_PAGE_DOWN;
	keycodes[0x149] = KEY_PAGE_UP;
	keycodes[0x045] = KEY_PAUSE;
	keycodes[0x146] = KEY_PAUSE;
	keycodes[0x039] = KEY_SPACE;
	keycodes[0x00F] = KEY_TAB;
	keycodes[0x03A] = KEY_CAPS_LOCK;
	keycodes[0x145] = KEY_NUM_LOCK;
	keycodes[0x046] = KEY_SCROLL_LOCK;
	keycodes[0x03B] = KEY_F1;
	keycodes[0x03C] = KEY_F2;
	keycodes[0x03D] = KEY_F3;
	keycodes[0x03E] = KEY_F4;
	keycodes[0x03F] = KEY_F5;
	keycodes[0x040] = KEY_F6;
	keycodes[0x041] = KEY_F7;
	keycodes[0x042] = KEY_F8;
	keycodes[0x043] = KEY_F9;
	keycodes[0x044] = KEY_F10;
	keycodes[0x057] = KEY_F11;
	keycodes[0x058] = KEY_F12;
	keycodes[0x064] = KEY_F13;
	keycodes[0x065] = KEY_F14;
	keycodes[0x066] = KEY_F15;
	keycodes[0x067] = KEY_F16;
	keycodes[0x068] = KEY_F17;
	keycodes[0x069] = KEY_F18;
	keycodes[0x06A] = KEY_F19;
	keycodes[0x06B] = KEY_F20;
	keycodes[0x06C] = KEY_F21;
	keycodes[0x06D] = KEY_F22;
	keycodes[0x06E] = KEY_F23;
	keycodes[0x076] = KEY_F24;
	keycodes[0x038] = KEY_LEFT_ALT;
	keycodes[0x01D] = KEY_LEFT_CONTROL;
	keycodes[0x02A] = KEY_LEFT_SHIFT;
	keycodes[0x15B] = KEY_LEFT_SUPER;
	keycodes[0x137] = KEY_PRINT_SCREEN;
	keycodes[0x138] = KEY_RIGHT_ALT;
	keycodes[0x11D] = KEY_RIGHT_CONTROL;
	keycodes[0x036] = KEY_RIGHT_SHIFT;
	keycodes[0x15C] = KEY_RIGHT_SUPER;
	keycodes[0x150] = KEY_DOWN;
	keycodes[0x14B] = KEY_LEFT;
	keycodes[0x14D] = KEY_RIGHT;
	keycodes[0x148] = KEY_UP;

	keycodes[0x052] = KEY_KP_0;
	keycodes[0x04F] = KEY_KP_1;
	keycodes[0x050] = KEY_KP_2;
	keycodes[0x051] = KEY_KP_3;
	keycodes[0x04B] = KEY_KP_4;
	keycodes[0x04C] = KEY_KP_5;
	keycodes[0x04D] = KEY_KP_6;
	keycodes[0x047] = KEY_KP_7;
	keycodes[0x048] = KEY_KP_8;
	keycodes[0x049] = KEY_KP_9;
	keycodes[0x04E] = KEY_KP_ADD;
	keycodes[0x053] = KEY_KP_DECIMAL;
	keycodes[0x135] = KEY_KP_DIVIDE;
	keycodes[0x11C] = KEY_KP_ENTER;
	keycodes[0x037] = KEY_KP_MULTIPLY;
	keycodes[0x04A] = KEY_KP_SUBTRACT;
}
