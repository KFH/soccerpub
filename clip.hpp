// taken from:
// Bresenham’s Line Generation Algorithm with Built-in Clipping
// Yevgeny P. Kuzmin
// 10.1111/1467-8659.1450275
// clang-format off
#pragma once
#define XCHG(x, y){t=x;x=y;y=t;}
// clang-format on
template <typename T>
static inline int clipped_line(int sx1, int sy1, int sx2, int sy2, int wx1, int wy1, int wx2,
                               int wy2, T* img, T color)
{
	int  dsx, dsy, stx, sty, xd, yd, dx2, dy2, rem, term, e, *d1, *d2, t, wm;
	long tmp;

	/*
	t = std::min(sx1, sx2);
	sx2 = std::max(sx1,sx2);
	sx1 = t;
	t = std::min(sy1, sy2);
	sy2 = std::max(sy1,sy2);
	sy1 = t;
	*/

	wm  = wx2 - wx1;
	stx = 1;
	sty = 1;
	if (sx1 < sx2) {
		if ((sx1 > wx2) | (sx2 < wx1))
			return 0;
	} else {
		if ((sx2 > wx2) | (sx1 < wx1))
			return -1;
		stx = -1;
		sx1 = -sx1;
		sx2 = -sx2;
		wx1 = -wx1;
		wx2 = -wx2;
		XCHG(wx1, wx2);
	}

	if (sy1 < sy2) {
		if ((sy1 > wy2) | (sy2 < wy1))
			return -2;
	} else {
		if ((sy2 > wy2) | (sy1 < wy1))
			return -3;
		sty = -1;
		sy1 = -sy1;
		sy2 = -sy2;
		wy1 = -wy1;
		wy2 = -wy2;
		XCHG(wy1, wy2);
	}

	dsx = sx2 - sx1;
	dsy = sy2 - sy1;
	if (dsx < dsy) {
		d1 = &yd;
		d2 = &xd;
		XCHG(sx1, sy1);
		XCHG(sx2, sy2);
		XCHG(dsx, dsy);
		XCHG(wx1, wy1);
		XCHG(wx2, wy2);
		XCHG(stx, sty);
	} else {
		d1 = &xd;
		d2 = &yd;
	}
	/* Bresenham's set up */
	dx2  = 2 * dsx;
	dy2  = 2 * dsy;
	xd   = sx1;
	yd   = sy1;
	e    = 2 * dsy - dsx;
	term = sx2;

	if (sy1 < wy1) { /* window horizontal entry */
		tmp = (long)dx2 * (wy1 - sy1) - dsx;
		xd += tmp / dy2;
		rem = tmp % dy2;
		if (xd > wx2)
			return -4;
		if (xd + 1 > wx1) {
			yd = wy1;
			e -= (rem + dsx);
			if (rem > 0) {
				xd++;
				e += dy2;
			}
			goto SetExit;
		}
	}
	if (sx1 < wx1) { /* window vertical entry */
		tmp = (long)dy2 * (wx1 - sx1);
		yd += tmp / dx2;
		rem = tmp % dx2;
		if ((yd > wy2) | ((yd == wy2) & (rem >= dsx)))
			return -5;
		xd = wx1;
		e += rem;
		if (rem >= dsx) {
			yd++;
			e -= dx2;
		}
	}
SetExit:
	if (sy2 > wy2) { /* window exit */
		tmp  = (long)dx2 * (wy2 - sy1) + dsx;
		term = sx1 + tmp / dy2;
		rem  = tmp % dy2;
		if (rem == 0)
			term--;
	}
	if (term > wx2)
		term = wx2;
	term++; // whats the point ?

	if (sty == -1)
		yd = -yd;
	if (stx == -1) {
		xd   = -xd;
		term = -term;
	}
	dx2 -= dy2;

	while (xd != term) { /* Bresenham's line drawing */
		img[(*d1) + (*d2) * wm] = color;
		xd += stx;
		if (e >= 0) {
			yd += sty;
			e -= dx2;
		} else {
			e += dy2;
		}
	}
	return 1;
}
