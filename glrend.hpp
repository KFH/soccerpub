
namespace ogl {
void PrintGLinfo();
void SetPalette(void* palette, int psize);
int  Init();
void Draw(void* pic, int pw, int ph, int w, int h);
} // namespace ogl
