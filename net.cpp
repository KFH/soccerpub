#include "net.hpp"

namespace net {

#pragma pack(push, 1)
struct Announce_t {
	uint32_t proto;
	uint32_t addr;
	uint16_t port;
};
#pragma pack(pop)
#define PROTO_HOSTING 123456
#define PROTO_CONNECT 654321

int Context::OpenConnections()
{
	int open = 0;
	for (auto c : conns) {
		if (c && c->state == Connection::CONNECTED) {
			open++;
		}
	}
	return open;
}
void Context::RemoveConnection(Connection* c)
{
	for (auto& conn : conns) {
		if (conn == c)
			conn = NULL;
	}
}
void Context::AddConnection(Connection* c)
{
	// TODO
	// try using nat punch through here if enabled to resolve
	// a real port
	for (auto& conn : conns) {
		if (!conn) {
			conn     = c;
			c->state = Connection::CONNECTING;
			return;
		}
	}
}

void Context::Free()
{
	for (Connection* c : conns) {
		if (c)
			c->Close();
	}
	Cleanup();
	msock.Close();
}
void Context::Cleanup()
{
	for (auto& c : conns) {
		if (c) {
			message msg  = {};
			msg.proto    = CLOSENUM;
			msg.exp_size = 0; // sizeof(msg);
			switch (c->state) {
			case Connection::DISCONNECTING:
				msock.Send(c->maddr, &msg, sizeof(msg));
			case Connection::DISCONNECTED:
				TryCloseCB(c);
				delete c;
				c = nullptr;
			default:
				// do nothing
				break;
			}
		}
	}
}

int Context::Recieve()
{
	int packetcount = 0;
	head            = 0;
	tail            = 0;
	int openconns   = OpenConnections();

	while (packetcount < ctxBuffMax) {
		rawPacket& packet = recvbuff[tail];
		int rb = msock.Receive(recvbuff[tail].addr, recvbuff[tail].data, MAXPACKET_S);
		if (!rb)
			break;

		packet.data_s = rb;

		if (rb < sizeof(message)) {

			if (packet.addr == punchAddr && packet.data_s == sizeof(Announce_t)) {
				auto* a = (Announce_t*)packet.data;
				printf("ANNOUNCE %u %u %u\n", a->proto, a->addr, a->port);
				publicAddr       = Address(a->addr, a->port, true);
				publicAddr_known = true;

			} else {
				printf("UNSUPPORTED RESP size %d %zu\n", rb, sizeof(Announce_t));
			}
			continue;
		}

		// skip to next message if unsupported proto detected
		switch (packet.msg.proto) {
		case MAGICNUM:
		case CLOSENUM:
		case FRAGNUM:
			break;
		default:
			printf("UNSUPPORTED PROTO %u\n", packet.msg.proto);
			continue;
		}

		// TODO
		// if any connection is still CONNECTING check for match & switch state to CONNECTED
		// else
		// if new unknown address create connection
		if (openconns < NET_MAXCONNECTIONS) {
			bool known = false;
			for (auto c : conns) {
				if (c && packet.addr == c->maddr) {
					known = true;
					if (c->state != Connection::CONNECTED) {
						c->state = Connection::CONNECTED;
						TryOpenCB(c);
					}
					break;
				}
			}

			if (!known) {
				int         uid = 1234;
				Connection* nc  = new Connection(*this, packet.addr, uid);
				AddConnection(nc);
				openconns = OpenConnections();
				nc->state = Connection::CONNECTED;
				TryOpenCB(nc);
			}
		}
		tail++;
		packetcount++;
	}

	for (auto c : conns) {
		if (c)
			c->Recieve();
	}
	Cleanup();
	return packetcount;
}

void Context::ProcessRecv()
{

	// TODO
	// match packet addr in buffer against connections
	// connections then apply connection logic
	// acks, deserialize?
}

int Context::Send()
{

	// for all open connections send their messages out
	for (auto& c : conns) {
		if (c) {
			if (c->state == Connection::CONNECTING) {
				message msg  = {};
				msg.proto    = MAGICNUM;
				msg.exp_size = 0; // sizeof(msg);
				msock.Send(c->maddr, &msg, sizeof(msg));
			} else {
				c->Send();
			}
			// RemoveConnection
			if (c->timeOut > MAXCOUNT) {
				if (CloseCB)
					CloseCB(this, c);
				delete c;
				c = nullptr;
			}
		}
	}

	return 0;
}

void Context::Announce()
{
	pumpcount++;
	if (pumpcount % 10 == 0) {
		Announce_t msg = { PROTO_HOSTING, 0, mport };
		// printf("punchAddr %u\n", punchAddr.ip.v4[0]);
		// punchAddr.PrintV4();
		// printf("\n");
		msock.Send(punchAddr, &msg, sizeof(msg));
	}

	if (punch) {
		for (auto& c : conns) {
			if (c->state == Connection::CONNECTING) {
				// send some kind of connecting / blank packet?
			}
		}
	}
}
int Connection::AddData(void* data, int len)
{
	// IF data larger than one packet can hold create a outgoing fragmented message
	if (len > (MAXPACKET_S - sizeof(message))) {

		int overhead             = sizeof(message) + sizeof(fragment_header_t);
		int fragment_num_packets = (len - 1) / (MAXPACKET_S - overhead) + 1;
		int fragment_space
		    = int(BUFFERSIZE) - int(stats.oldestUnacked) - 1 - out.BufferedPackets();

		if (fragment_num_packets < fragment_space) {
			// IDEA
			// store initial out.seq number
			// pack data into out.GetPacket() in for loop
			uint16_t seq_start  = out.seq_tail;
			uint16_t seq_end    = seq_increment(seq_start, fragment_num_packets);
			int      chunk_size = MAXPACKET_S - overhead;
			int      rem        = len;
			for (int i = 0; i < fragment_num_packets; i++) {

				rawPacket* rp = &out.AppendPacket();
				rp->msg.proto = FRAGNUM;
				// copy in chunk
				size_t            offest = (i * chunk_size);
				fragment_header_t fh     = { len, fragment_num_packets, i };

				rp->append(&fh, sizeof(fh));
				rp->append((char*)data + offest,
				           rem > chunk_size ? chunk_size : rem);
				rem -= chunk_size;
			}

			fragment_state_t fs = {};
			fs.byte_size        = len;
			fs.num_packets      = fragment_num_packets;
			fs.seq_start        = seq_start;
			fs.seq_end          = seq_end;
			fs.seq_current      = seq_start;
			frag_out.push(fs);

			return 1;
		}
		return -1;
	}

	rawPacket* rp = out.GetTail();
	if (rp && rp->size() + len < MAXPACKET_S) {
		rp->append(data, len);
		return 1;
	}
	// TODO
	// somethings gone wrong, unsure if this case still relavent
	printf("FAILED TO COPY seq %u  %u %d \n", out.seq_head, out.seq_tail, rp != nullptr);
	return 0;
}

int Connection::Send()
{
	enum {
		// enums for flow strategy
		FLOWSTALL,
		FLOWSLOW,
		FLOWNORMAL,
	};
	int        strat        = FLOWNORMAL;
	uint16_t   oldest       = out.Oldest(out.seq_head);
	int        dist         = seq_dist(out.seq_head, oldest);
	int        packet_count = 0;
	rawPacket* rp           = nullptr;
	stats.oldestUnacked     = dist;
	stats.unackedPacket     = oldest;
	stats.subMessagesOUT    = 0;
	stats.stall             = timeOut;

	// TODO
	// these strats will probably just cripple connection ???
	if (dist > 31)
		strat = FLOWSLOW;
	if (dist >= 50)
		strat = FLOWSTALL;

	switch (strat) {
	case FLOWSTALL:
		Close();
	case FLOWSLOW:
		timeOut++;
		printf("	SLOW\n");
		rp = out.GetOldest();
		ctx.msock.Send(maddr, rp->data, rp->size());
		stats.sentPackets++;
		break;
	case FLOWNORMAL:

		timeOut      = 0;
		packet_count = std::min(MAX_BURST, out.BufferedPackets());

		while (packet_count--) {

			rp = out.GetHead();
			out.IncrHeadSeq();

			// TODO
			// Something must be wrong with connection setup logic? ? ?
			// sometimes rp is returning null on brand new connections that should
			// have initilized with a default seq and outbound packets ? ? ?
			if (!rp) {
				fprintf(stderr,
				        "SOMETHINGS GONE WRONG WITH RELIABLE SEQUENCE %u %u\n",
				        out.seq_head, out.seq_tail);
				rp = &out.AppendPacket();
			}

			out.SetSentTime(rp->msg.seq);
			int osize = rp->size();

			// IDEA
			// append old un acked messages to current message if there is space
			if (dist > MAXWAIT && !rp->isFragmented()) {
				rawPacket* op = out.GetPacket(oldest);
				while (op->size() + rp->size() < MAXPACKET_S && dist > MAXWAIT) {
					memcpy(rp->data + rp->size(), op->data, op->size());
					rp->data_s = rp->size() + op->size();
					oldest     = out.NextOldest(oldest, out.seq_head);
					dist       = seq_dist(out.seq_head, oldest);
					stats.subMessagesOUT++;
					op = out.GetPacket(oldest);
				}
			}
			// acknowledge packets recieved
			rp->msg.ack      = in.ack;
			rp->msg.ack_bits = in.ack_bits;

			rp->msg.exp_size = std::max(0, osize - (int)sizeof(message));
			rp->data_s       = rp->size(); // ensure min size is set?

			// roll dice, if value less than debug connection quality drop
			// packet
			int d = rand_r_fill(&debug_seed) % 100;
			if (d >= debug_quality) {
				ctx.msock.Send(maddr, rp->data, rp->size());
			} else {
				printf("DROPPING %d\n", rp->msg.seq);
			}

			stats.sentPackets++;
			// reset size incase unacked it can be more easily appended
			rp->data_s = osize;
		}
		if (out.BufferedPackets() == 0 || out.GetTail()->isFragmented()) {
			out.AppendPacket();
		}
	}

	return 0;
};

int Connection::Recieve()
{
	int      count       = 0;
	uint16_t newack      = in.ack;
	uint32_t newack_bits = in.ack_bits;
	for (const rawPacket& p : ctx) {
		if (p.addr == maddr) {
			count++;
			stats.recvPackets++;
		} else {
			continue;
		}

		// process the packet
		int rem             = p.data_s;
		stats.subMessagesIN = 0;
		// printf("PROCESSING\n");
		while (rem >= (int)sizeof(message)) {

			const unsigned char* ptr = p.data + (p.data_s - rem);
			message*             m   = (message*)ptr;
			int                  dist, d2;

			if (p.msg.proto == CLOSENUM) {
				// TODO close connection
				state = DISCONNECTED;
				goto exit;
			} else if (m->proto != MAGICNUM) {
				// TODO scan ahead by 1 byte instead?
				printf("BAD DATA\n");
				rem -= sizeof(message);
				continue;
			}
			ptr += sizeof(message);
			rem -= sizeof(message);
			stats.subMessagesIN++;
			out.AckPackets(m->ack, m->ack_bits);

			// IDEA
			// update ack number and bitfield for incoming packets
			dist = seq_dist(m->seq, newack);
			d2   = seq_dist(in.edge, m->seq);

			if ((d2 < MAXCOUNT) & (seq_greater_than(m->seq, in.edge)) || d2 == 0) {
				inOrder.expandRel(d2);
				submessage& nmsg = inOrder.getRel(d2);
				// dont copy unnecessary retransmitted packets
				if (nmsg.seq != m->seq) {
					// printf("%hu, ", m->seq);
					nmsg.seq    = m->seq;
					nmsg.proto  = m->proto;
					nmsg.data_s = std::min((int)m->exp_size, rem);
					nmsg.ready  = true;
					memcpy(nmsg.data, m->data(), nmsg.data_s);
				}
				if (unorderDst && seq_greater_than(m->seq, in.edge_unordered)) {
					in.edge_unordered = m->seq;
					unorderDst->read(this, nmsg.data, nmsg.data_s);
				}
			}

			if (seq_greater_than(m->seq, newack)) {
				newack = m->seq;
				newack_bits <<= dist;
				newack_bits |= 1 << (dist - 1);
				// TODO shift newack_bits ?
			} else if (dist > 0) {
				uint32_t mask = 1 << (dist - 1);
				newack_bits |= mask; // right shift dist - 1 ?
			}
			rem -= m->exp_size;
		}
		// printf("\n");
	}
exit:

	in.ack        = newack;
	in.ack_bits   = newack_bits;
	stats.latency = out.latency;

	// find range of in order packets ready to ingestion
	int rel_start = 0;
	int rel_end   = inOrder.size();
	int rel_ix    = 0;
	while (rel_ix < rel_end && inOrder.getRel(rel_ix).ready &&
	       // inOrder.getRel(rel_ix) == FRAGNUM &&
	       seq_dist(in.edge + rel_ix, inOrder.getRel(0).seq) <= 1) {
		rel_ix++;
	}

	rel_end = rel_ix;
	rel_ix  = rel_start;

	while (rel_ix < rel_end) {
		auto& sm = inOrder.getRel(rel_ix);

		if (sm.proto == FRAGNUM && frag_in.size() < 1) {

			// TODO fill out and bound check within reason
			fragment_state_t fs = {

			};

			frag_in.push(fs);

		} else if (sm.proto == FRAGNUM && sm.seq == frag_in.getRel(0).seq_end) {
			printf("DESERIALIZING FRAGMENT %u to %u", rel_start, rel_end);

			// copy fragments into contigous buffer
			size_t frag_buff_offset = 0;
			for (int i = rel_start; i < rel_end; i++) {
				// copy out
				auto& sm = inOrder.pop();
				memcpy(frag_buff + frag_buff_offset, sm.data, sm.data_s);
				frag_buff_offset += sm.data_s;
				// TODO reseting this shit after poping is bad, RingBuffer should
				// handle this somehow
				sm.data_s = 0;
				sm.ready  = false;

				// advance edge to keep track of ingested packets
				in.edge++;
			}

			if (orderDst)
				orderDst->read(this, frag_buff, frag_buff_offset);

		} else {
			auto& sm = inOrder.pop();
			if (orderDst)
				orderDst->read(this, sm.data, sm.data_s);
			// TODO reseting this shit after poping is bad, RingBuffer should handle
			// this somehow
			sm.data_s = 0;
			sm.ready  = false;
			in.edge++;
		}
		rel_ix++;
	}

	{
		stats.orderEdgeSize = std::min(inOrder.size(), 10);
		for (int i = 0; i < stats.orderEdgeSize; i++) {
			stats.orderEdge[i]    = inOrder.getRel(i).seq;
			stats.orderEdgeRdy[i] = inOrder.getRel(i).ready;
		}
	}
	stats.edge          = in.edge;
	stats.orderBuffSize = inOrder.size();

	// TODO
	// tracking output transfer progress here
	// assuming fragmented message sequence was transfered if oldest unacked packet greater
	// than end of fragment sequence
	while (frag_out.size() > 0 && seq_greater_than(out.Oldest(), frag_out.getRel(0).seq_end)) {
		frag_out.pop();
	}

	return count;
};

} // namespace net
