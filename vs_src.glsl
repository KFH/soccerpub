
#version 300 es
#define x 1.0
#define y 1.0
vec4 verts[4] = 
	vec4[4](vec4(-x, y,0.0,1.0), vec4( x, y,0.0,1.0), vec4( x,-y,0.0,1.0), vec4(-x,-y,0.0,1.0));
vec2 uvs[4] = 
	vec2[4](vec2(0,0),vec2(1,0),vec2(1,1),vec2(0,1));

out vec2 vUV;

void main(void) {
	vUV = uvs[gl_VertexID];
	gl_Position = verts[gl_VertexID];
}

