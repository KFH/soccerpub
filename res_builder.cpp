
#include <fstream>
#include <string>
#include <filesystem>
#include "resource.hpp"

//#include "out_test.hpp"

int main(int argc, const char** argv)
{

	if (argc < 3) {
		fprintf(stderr,
		        "require input and out put filename eg ./res_build in.conf out.bin\n");
		return -1;
	}

	std::fstream mfile;
	std::string  fname_in  = argv[1];
	std::string  fname_out = argv[2];

	mfile.open(fname_in, std::ios::in | std::ios::binary);
	mfile.seekg(0, std::ios::end);
	size_t fend = mfile.tellg();
	mfile.seekg(0, std::ios::beg);
	std::string conf;
	conf.reserve(fend);

	conf.assign((std::istreambuf_iterator<char>(mfile)), std::istreambuf_iterator<char>());

	mfile.close();

	// extract path from conf file
	std::filesystem::path path = fname_in;

	printf("PACKING RESOURCES FROM %s INTO %s ROOT %s\n", argv[1], argv[2],
	       std::filesystem::absolute(path).parent_path().c_str());

	return BuildResouceFile(conf, std::filesystem::absolute(path).parent_path() / "",
	                        fname_out);
}
