#pragma once
// checksum, hash, crc32
inline uint32_t RollingHash(int& a, int& b, const unsigned char* data, size_t data_s)
{
	// adler32
	const int            MOD_ADLER = 65521;
	const unsigned char* end       = data + data_s;
	size_t               dlen      = data_s;
	size_t               i         = 0;
	while (data < end) {
		size_t len = std::min((size_t)5552, dlen);
		for (i = 0; i < len; i++) {
			a = (a + data[i]);
			b = (b + a);
		}
		a = a % MOD_ADLER;
		b = b % MOD_ADLER;
		dlen -= len;
		data = &data[i];
	}
	return (b << 16) | a;
}

inline uint32_t Hash32(const unsigned char* data, size_t data_size)
{
	int a = 0, b = 0;
	return RollingHash(a, b, data, data_size);
}

inline uint32_t fletcher32(const uint16_t* data, size_t len)
{
	uint32_t     c0, c1;
	unsigned int i;
	for (c0 = c1 = 0; len >= 360; len -= 360) {
		for (i = 0; i < 360; ++i) {
			c0 = c0 + *data++;
			c1 = c1 + c0;
		}
		c0 = c0 % 65535;
		c1 = c1 % 65535;
	}
	for (i = 0; i < len; ++i) {
		c0 = c0 + *data++;
		c1 = c1 + c0;
	}
	c0 = c0 % 65535;
	c1 = c1 % 65535;
	return (c1 << 16 | c0);
}
