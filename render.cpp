#include "render.hpp"
#include "clip.hpp"
#include <algorithm>
#include <cmath>
#include <math.h>
#include <stdint.h>

template <typename T>
inline void Checker(renderTarget<T>& dst, int x0, int y0, int x1, int y1, int cw, int ch, int offx,
                    int offy, T c0, T c1)
{

	auto fillLine = [&dst](int cx0, int cx1, int y, T color) {
		int s = cx0 + (y * dst.width);
		int e = cx1 + (y * dst.width);
		if (e - s > 16) {
			__m128i ci = _mm_set1_epi8(color);
			for (; s + 16 <= e; s += 16) {
				_mm_storeu_si128((__m128i*)(dst.raw32 + s), ci);
			}
		}
		for (; s < e; s++) {
			dst.raw32[s] = color;
		}
	};

	x0 = std::max(x0, 0);
	y0 = std::max(y0, 0);
	x1 = std::min(x1, dst.width);
	y1 = std::min(y1, dst.height);

	offx -= x0;

	int ybit = std::signbit((float)offy); //(offy > 0) - (offy < 0);
	int xbit = std::signbit((float)offx); //(offx > 0) - (offx < 0);

	bool ytog     = ((offy - y0 + ybit) / ch) % 2 == 0 ? true : false;
	bool xtoginit = ((offx) / cw) % 2 == 0 ? false : true;

	xtoginit ^= xbit;
	xtoginit ^= ybit;

	offx   = offx < 0 ? cw - std::abs(offx) % cw : std::abs(offx) % cw;
	offy   = offy < 0 ? ch - std::abs(offy) % ch : std::abs(offy) % ch;
	int xd = x1 / cw;
	xd     = std::min(xd * cw + (x1 % cw), x1);

	// starts first chunk safely
	int sx = std::min(x0 + offx, x1);

	for (int y = y0; y < y1; y++) {

		if ((y - offy) % ch == 0)
			ytog ^= 1;

		bool xtog   = ytog ^ xtoginit;
		T    fcolor = xtog ? c0 : c1;
		int  x      = x0;
		fillLine(x, sx, y, fcolor);
		for (x = x0 + offx; (x + cw) <= xd; x += cw) {
			xtog ^= 1;
			fcolor = xtog ? c0 : c1;
			fillLine(x, x + cw, y, fcolor);
		}
		xtog ^= 1;
		fcolor = xtog ? c0 : c1;
		fillLine(x, x1, y, fcolor);
	}
}

inline void Blank(renderTarget<uint32_t>& target, uint32_t color)
{

	// assert((target.width * target.height * 4) % sizeof(uint32_t) == 0);

	__m128i   v;
	uint32_t* vc = (uint32_t*)&v;
	vc[0]        = color;
	vc[1]        = color;
	vc[2]        = color;
	vc[3]        = color;

	__m128i* d
	    = (__m128i*)target.raw32 + (target.width * target.height * sizeof(uint32_t)) / 16;
	__m128i* p = (__m128i*)target.raw32;

	while (p != d) {
		_mm_storeu_si128(p, v);
		p++;
	}
}

inline void BlankALIGNED(dst_t& target, dstFormat color)
{

	// assert((target.width * target.height * 4) % sizeof(uint32_t) == 0);

	__m128i v = _mm_set1_epi8(color);

	__m128i* d
	    = (__m128i*)target.raw32 + (target.width * target.height * sizeof(dstFormat)) / 16;
	__m128i* p = (__m128i*)target.raw32;

	while (p != d) {
		_mm_store_si128(p, v);
		p++;
	}
}

inline void BlitSet(int di, int si, dst_t& dst, const sprite_t& src, const paletteMap_t& map)
{
	uint8_t val = map[src.raw32[si]];
	if (val)
		dst.raw32[di] = val;
	//	dst.raw32[di] = pal.color[val];
}

#include <emmintrin.h>
#include <tmmintrin.h>

union simd_t {
	__m128i       m128i;
	unsigned char i8[16];
};

inline __m128i simd_mask_i8(const __m128i& mask)
{
	__m128i z = _mm_setzero_si128();
	return _mm_cmpgt_epi8(mask, z);
}

inline __m128i simd_compl_i8(const __m128i& a)
{
	__m128i mask = _mm_set1_epi8(0xff);
	return _mm_xor_si128(a, mask);
}
// filler for non existant blend function on older cpu
inline __m128i simd_blender(const __m128i& a, const __m128i& b, const __m128i& mask)
{
	__m128i c0 = _mm_and_si128(a, mask);
	__m128i c1 = _mm_and_si128(b, simd_compl_i8(mask));
	return _mm_or_si128(c0, c1);
}

inline void BlitTiledLine(int ssx, int sx1, int sx2, int dx1, int dx2, dst_t& dst,
                          const sprite_t& src, const paletteMap_t& map)
{
}

inline void BlitLine(int sx1, int sx2, int dx, dst_t& dst, const sprite_t& src,
                     const paletteMap_t& map)
{

	if (sx2 - sx1 < 16) {
		for (; sx1 < sx2; sx1++) {
			BlitSet(dx, sx1, dst, src, map);
			dx++;
		}
		return;
	}
	// IDEA
	// simd load line of src into a __m128i
	// to index palletteMap in simd way use two shuffle passes? since 256bytes in size?
	// make mask of src indexs < 16 = 1, index >= 16 = 0 using simd compare
	// mask src = m0
	// suffle pallette using m0, store in p1
	// invert mask
	// mask src = m1
	// subtract 16 from m1
	// suffle pallette using m1, store p2
	// merge p1 & p2 using mask m1 = nl     _mm_blendv_epi8 ?

	// make new mask nl > 0 = m2
	// merge dst & nl using mask m2

	__m128i *  d, lim = _mm_set1_epi8(16);
	const auto zero = _mm_setzero_si128();
	d               = (__m128i*)(dst.raw32 + dx);

	// main loop

	for (; sx1 + 16 <= sx2; sx1 += 16) {
		const __m128i s    = _mm_loadu_si128((__m128i const*)(src.raw32 + sx1));
		__m128i       mask = _mm_cmplt_epi8(s, lim);
		__m128i       p1   = _mm_shuffle_epi8(map.m128i[0], s);
		__m128i       p2   = _mm_shuffle_epi8(map.m128i[1], _mm_subs_epi8(s, lim));
		__m128i       nl   = simd_blender(p1, p2, mask);
		mask               = _mm_cmpgt_epi8(nl, zero);
		p1                 = _mm_loadu_si128(d);
		__m128i nv         = simd_blender(nl, p1, mask);
		_mm_storeu_si128(d, nv);
		d++;
		dx += 16;
	}
	// fixup remainder
	for (; sx1 < sx2; sx1++) {
		uint8_t val = map[src.raw32[sx1]];
		if (val)
			dst.raw32[dx] = val;
		dx++;
	}
}

inline void Blit(dst_t& dst, const sprite_t& src, const paletteMap_t& map, const palette_t& pal,
                 int srcx0, int srcy0, int srcx1, int srcy1, int dstx0, int dsty0)
{

	// TODO
	// safely copy straight from 1 to other
	int sw = srcx1 - srcx0;
	int sh = srcy1 - srcy0;

	int dx0 = std::max(dstx0, 0);
	int dy0 = std::max(dsty0, 0);

	int sx0
	    = std::min(src.width,
	               srcx0 - std::min(0, dstx0)); // srcx0;//std::max(srcx0 + dstx0 * -1 , srcx0);
	int sy0 = std::min(src.height,
	                   srcy0 - std::min(0, dsty0)); // std::max(srcy0 + dsty0 * -1 , srcy0);
	int sx1 = srcx0 + (std::min(dstx0 + sw, dst.width) - dstx0);
	int sy1 = srcy0 + (std::min(dsty0 + sh, dst.height) - dsty0);

	// height
	for (int y = sy0; y < sy1; y++) {
		// width
		int dyoff = dst.width * dy0;
		int syoff = src.width * y;
		int dx    = dx0;

		BlitLine(sx0 + syoff, sx1 + syoff, dx + dyoff, dst, src, map);
		// for (int x = sx0; x < sx1; x++) {
		//	BlitSet(dx + dyoff, x + syoff, dst, src, map, pal);
		//	dx++;
		//}
		dy0++;
	}
}

inline void BlitTiled(dst_t& dst, const sprite_t& src, const paletteMap_t& map,
                      const palette_t& pal, int srcx0, int srcy0, int srcx1, int srcy1, int dstx0,
                      int dsty0, int dstx1, int dsty1)
{
	int sw  = srcx1 - srcx0;
	int sh  = srcy1 - srcy0;
	int sdx = (sw > 0) ? 1 : -1;
	int sdy = (sw > 0) ? 1 : -1;
	sw      = abs(sw);
	sh      = abs(sh);

	int x0 = std::min(std::max(dstx0, 0), dst.width);
	int y0 = std::min(std::max(dsty0, 0), dst.height);
	int x1 = std::min(std::max(dstx1, 0), dst.width);
	int y1 = std::min(std::max(dsty1, 0), dst.height);

	int offx = (dstx0 - x0) % sw;
	int offy = (dsty0 - y0) % sh;

	int sy = 0;
	for (int y = y0; y < y1; y++) {

		int dyoff = dst.width * y;
		int syoff = src.width * abs(srcy0 + ((sy - offy) % sh));
		int sx    = 0;

		for (int x = x0; x < x1;) {
			int sp = abs(srcx0 + (sx - offx) % sw) + syoff;
			int se = std::min((srcx1 + syoff), sp + (x1 - x));
			int dp = x + dyoff;
			BlitLine(sp, se, dp, dst, src, map);
			sx += se - sp;
			x += se - sp;
		}

		/*
		for (int x = x0; x < x1; x++) {
		        int sp = abs(srcx0 + (sx - offx) % sw) + syoff;
		        int dp = x + dyoff;
		        BlitSet(dp, sp, dst, src, map);
		        sx += sdx;
		}*/
		sy += sdy;
	}
}

template <typename T> inline void Circle(renderTarget<T>& src, int cx, int cy, int r, T color)
{

	auto setpixel = [&src](int x, int y, T color) {
		// TODO/NOTE
		// 4 checks per pixel
		// if bounding box of pixel is assured to be within range
		// this can be skipped
		if ((x >= src.width) | (x < 0) | (y >= src.height) | (y < 0))
			return;
		int p        = x + y * src.width;
		src.raw32[p] = color;
	};

	int x   = r;
	int y   = 0;
	int err = 0;
	while (x >= y) {

		setpixel(cx + x, cy + y, color);
		setpixel(cx + x, cy - y, color);
		setpixel(cx + y, cy + x, color);
		setpixel(cx + y, cy - x, color);
		setpixel(cx - x, cy - y, color);
		setpixel(cx - x, cy + y, color);
		setpixel(cx - y, cy - x, color);
		setpixel(cx - y, cy + x, color);

		y += 1;
		if (err <= 0)
			err += 2 * y + 1;
		if (err > 0) {
			x -= 1;
			err -= 2 * x + 1;
		}
	}
}
template <typename T> inline void CircleFill(renderTarget<T>& src, int cx, int cy, int r, T color)
{
	auto fillLine = [&src](int x0, int x1, int y, T col) {
		if ((x0<0 | x1> src.width | y<0 | y> src.height))
			return;
		x0    = std::max(0, std::min(src.width, x0));
		x1    = std::max(0, std::min(src.width, x1));
		y     = std::max(0, std::min(src.height - 1, y));
		int s = x0 + y * src.width;
		int e = x1 + y * src.width;
		for (; s < e; s++) {
			src.raw32[s] = col;
		}
	};
	int x   = r;
	int y   = 0;
	int err = 0;
	while (x >= y) {

		fillLine(cx - x, cx + x, cy + y, color);
		fillLine(cx - x, cx + x, cy - y, color);
		fillLine(cx - y, cx + y, cy + x, color);
		fillLine(cx - y, cx + y, cy - x, color);

		y += 1;
		if (err <= 0)
			err += 2 * y + 1;
		if (err > 0) {
			x -= 1;
			err -= 2 * x + 1;
		}
	}
}

struct pixmaxdef_t {
	int width;
	int height;
	int colors;
	int characters;
};

int pixmapToSprite(const unsigned char* pixmap[], int pixmapSize, const palette_t& p, sprite_t& tex,
                   paletteMap_t& map)
{

	pixmaxdef_t pd;
	int res = sscanf((const char*)pixmap[0], "%d %d %d %d", &pd.width, &pd.height, &pd.colors,
	                 &pd.characters);
	if (!res || pd.colors < 1 || pd.characters > 1)
		return res;

	tex.raw32  = (uint8_t*)malloc(pd.width * pd.height);
	tex.width  = pd.width;
	tex.height = pd.height;

	struct mapping_t {
		uint32_t color;
		uint8_t  index;
	};

	static mapping_t table[256];

	int i;
	int h = 0;
	for (i = 0; i < pd.colors; i++) {
		// fill out character defs
		h++;
		unsigned char k = pixmap[h][0];
		table[k].index  = i;
		switch (pixmap[h][4]) {
		case '#':
			table[k].color = strtol((const char*)&pixmap[h][5], NULL, 16);
			break;
		case 'N': // None
			table[k].color = 0;
			break;
		default:
			return -1;
		}

		map.offset[i] = p.Match(table[k].color);
	}

	i = 0;
	for (h = h + 1; h < pixmapSize; h++) {
		const unsigned char* line = pixmap[h];
		while (line[0] != 0) {
			tex.raw32[i] = table[line[0]].index;
			i++;
			line++;
		}
	}

	return 1;
}

int pixmapToU32(const char* pixmap[], int pixmapSize, uint32_t** out, int* out_w, int* out_h)
{
	// parse first line of pixmap
	// width height colors charactersPerColorIWillIgnore?

	struct charColorPair {
		unsigned char key;
		uint32_t      color;
	};
	pixmaxdef_t pd;

	// TODO
	// dynamic size array
	static charColorPair defs[16];

	int res
	    = sscanf(pixmap[0], "%d %d %d %d", &pd.width, &pd.height, &pd.colors, &pd.characters);
	if (!res || pd.colors < 1 || pd.characters > 1)
		return res;

	*out = (uint32_t*)malloc(pd.width * pd.height * 4);

	int i;
	int h = 0;
	for (i = 0; i < pd.colors; i++) {
		// fill out character defs
		h++;
		defs[i].key = (unsigned char)pixmap[h][0];
		switch (pixmap[h][4]) {
		case '#':
			defs[i].color = strtol(&pixmap[h][5], NULL, 16);
			break;
		case 'N': // None
			defs[i].color = 0;
			break;
		default:
			return -1;
		}
	}

	static uint32_t colorTable[256];
	for (i = 0; i < pd.colors; i++) {
		printf("DEF %c = %X\n", defs[i].key, defs[i].color);
		colorTable[defs[i].key] = defs[i].color;
	}

	// TODO
	// for rest of array fillout
	i = 0;
	for (h = h + 1; h < pixmapSize; h++) {
		const char* line = pixmap[h];
		while (line[0] != 0) {
			(*out)[i] = colorTable[(unsigned char)line[0]];
			i++;
			line++;
		}
	}
	*out_w = pd.width;
	*out_h = pd.height;
	return 1;
}

void monofont::WriteTo(dst_t& dst, uint8_t fg, uint8_t bg, int x, int y, const char* msg)
{

	// TODO
	// correctly use real paletteMap_t
	int i         = 0;
	map.offset[0] = bg;
	map.offset[1] = fg;
	while (msg[i] != 0) {
		int c     = msg[i];
		int srcx0 = (c % lchars) * fwidth;
		int srcx1 = srcx0 + fwidth;
		int srcy0 = (c / lchars) * fheight;
		int srcy1 = srcy0 + fheight;

		Blit(dst, src, map, pal, srcx0, srcy0, srcx1, srcy1, x, y);

		x += fwidth;
		i += 1;
	}
}

void renderContext::proxieProcess(const renderProxie& rp)
{
	// process all the tokens rewrite dst
	// to be relative for a job thread?

	dst_t rtp = { dst.width, rp.height, rp.raw32 };
	for (cmdtoken& ct : commands) {
		switch (ct.ctype) {
		case cmdtoken::BLIT: {
			const blitcmd& cmd = ct.cmd.blit;
			Blit(rtp, sprites[cmd.sprite_i], maps[cmd.map_i], palette, cmd.srcx0,
			     cmd.srcy0, cmd.srcx1, cmd.srcy1, cmd.dstx0, cmd.dsty0 - rp.yoff);
		} break;
		case cmdtoken::TILE: {
			const tilecmd& cmd = ct.cmd.tile;
			BlitTiled(rtp, sprites[cmd.sprite_i], maps[cmd.map_i], palette, cmd.sl.x0,
			          cmd.sl.y0, cmd.sl.x1, cmd.sl.y1, cmd.dstx0, cmd.dsty0 - rp.yoff,
			          cmd.dstx0 + cmd.dw, cmd.dsty0 - rp.yoff + cmd.dh);

		} break;
		case cmdtoken::LINE: {
			const linecmd& cmd = ct.cmd.line;
			clipped_line<uint8_t>(cmd.x0, cmd.y0 - rp.yoff, cmd.x1, cmd.y1 - rp.yoff, 0,
			                      0, rtp.width, rtp.height - 1, rtp.raw32, cmd.color);
		} break;
		case cmdtoken::BLNK: {
			const blnkcmd& cmd = ct.cmd.blnk;
			BlankALIGNED(rtp, palette.color[cmd.color]);

		} break;
		case cmdtoken::CIRC: {
			const circcmd& cmd = ct.cmd.circ;
			if (cmd.fill) {
				CircleFill<uint8_t>(rtp, cmd.cx, cmd.cy - rp.yoff, cmd.r,
				                    cmd.color);
			} else {
				Circle<uint8_t>(rtp, cmd.cx, cmd.cy - rp.yoff, cmd.r, cmd.color);
			}
		} break;
		case cmdtoken::CHKR: {
			// TODO checker pattern drawing here
			const chkrcmd& cmd = ct.cmd.chkr;
			Checker<uint8_t>(rtp, cmd.x0, cmd.y0 - rp.yoff, cmd.x1, cmd.y1 - rp.yoff,
			                 cmd.cw, cmd.ch, cmd.offx, cmd.offy - rp.yoff, cmd.c0,
			                 cmd.c1);

		} break;
		default:
			fprintf(stderr,
			        "UNSUPPORTE"
			        "D TYPE\n");
		}
	}
}
