#pragma once

#include "rtree.hpp"

struct mapTiles_t {
	uint8_t grafx;
	uint8_t offX;
	uint8_t offY;
	uint8_t flags;
};

// TODO/IDEA
// extend rtree with function to merge and split overlapping squares
// and divide intersecting squares
// preseving some kind of connectivity?
class map_t : public rtree<uint32_t, 8, 16> {

	// http://programming.sirrida.de/calcperm.php
	static inline uint32_t bit_permute_step_simple(uint32_t x, uint32_t m, uint32_t shift)
	{
		return ((x & m) << shift) | ((x >> shift) & m);
	}

    public:
	enum {
		// bit flags if edge of node is a wall or not
		L_WALL   = 0x01,
		R_WALL   = 0x02,
		T_WALL   = 0x04,
		B_WALL   = 0x08,
		NO_WALL  = 0,
		ALL_WALL = L_WALL | R_WALL | T_WALL | B_WALL,
	};

	// NOTE
	// assumes no overlapping entries, insert only via Merg function

	class overlapCB_t : public queryCallback_t<uint32_t> {
		std::vector<int>& ents;
		rect_t            mrec;

	    public:
		bool isEnclosed;

		overlapCB_t(std::vector<int>& e, rect_t in)
		    : ents(e)
		{
			mrec       = in;
			isEnclosed = false;
		};

		virtual bool callback(int index, const rect_t& rec, const uint32_t& data) override
		{
			isEnclosed |= rec.enclose(mrec);
			ents.push_back(index);
			return 0;
		}
	};

	void Merge(uint32_t v, rect_t newrect)
	{
		auto splity = [](entry_t& in, entry_t& out, int y) {
			out.rec   = rect_t{ in.rec.x0, in.rec.y0, in.rec.x1, y };
			out.datum = in.datum;
			in.rec.y0 = y;
			out.datum &= ~B_WALL;
			in.datum &= ~T_WALL;
		};
		auto splitx = [](entry_t& in, entry_t& out, int x) {
			out.rec   = rect_t{ in.rec.x0, in.rec.y0, x, in.rec.y1 };
			out.datum = in.datum;
			out.datum &= ~R_WALL;
			in.datum &= ~L_WALL;
			in.rec.x0 = x;
		};

		printf("MERGING\n");
		// find all overlapping entries, store index in oent
		std::vector<int> oent;
		oent.resize(0);
		overlapCB_t cb(oent, newrect);
		Search(newrect, cb);

		if (cb.isEnclosed)
			return;

		if (oent.size() < 1) {
			Insert(v, newrect);
			return;
		}

		// for each overlap do an intersection of the two rects such that no
		// new rectangles overlap
		std::vector<entry_t> nr;
		nr.resize(0);

		entry_t r = entry_t{ newrect, v };

		printf("OVERLAPS %zu\n", oent.size());

		for (auto i : oent) {
			entry_t& e = entries[i];
			if (r.rec.enclose(e.rec)) {
				printf("	ENCLOSED 2\n");
				Delete(i);
				continue;
			}

			// inner rectangle of overlapping area
			rect_t ovr = { std::max(e.rec.x0, r.rec.x0), std::max(e.rec.y0, r.rec.y0),
				       std::min(e.rec.x1, r.rec.x1), std::min(e.rec.y1, r.rec.y1) };

			// NOTE
			// 3 scenarios of corner enclosure 0, 1, 2
			// always 4 split lines defined by overlapping area
			int     c = 0, c2 = 0;
			entry_t r0, r1;
			bool    b0, b1, b2;

			c += e.rec.enclose(r.rec.x0, r.rec.y0);
			c += e.rec.enclose(r.rec.x0, r.rec.y1);
			c += e.rec.enclose(r.rec.x1, r.rec.y1);
			c += e.rec.enclose(r.rec.x1, r.rec.y0);

			c2 += r.rec.enclose(e.rec.x0, e.rec.y0);
			c2 += r.rec.enclose(e.rec.x0, e.rec.y1);
			c2 += r.rec.enclose(e.rec.x1, e.rec.y1);
			c2 += r.rec.enclose(e.rec.x1, e.rec.y0);

			c = std::max(c, c2);
			switch (c) {
			case 0:
				printf("MERGE CASE 0\n");
				r0 = ovr.y0 == e.rec.y0 ? r : e;
				// split vertically
				splity(r0, r1, ovr.y0);
				nr.push_back(r1);
				splity(r0, r1, ovr.y1);
				r1.datum = NO_WALL;
				nr.push_back(r1);
				nr.push_back(r0);
				// split horizontally
				r0 = ovr.x0 == e.rec.x0 ? r : e;
				splitx(r0, r1, ovr.x0);
				nr.push_back(r1);
				splitx(r0, r1, ovr.x1);
				nr.push_back(r0);
				break;
			case 1:
				printf("MERGE CASE 1\n");
				// select highest rect y0
				// split vert then horz = 3 new rects
				b0 = r.rec.y0 < e.rec.y0;
				r0 = b0 ? r : e;
				b1 = ovr.x0 == r0.rec.x0;

				splity(r0, r1, ovr.y0);
				nr.push_back(r1);
				splitx(r0, r1, b1 ? ovr.x1 : ovr.x0);

				b2       = r0.rec == ovr;
				r0.datum = b2 ? NO_WALL : r0.datum;
				r1.datum = b2 ? r1.datum : NO_WALL;
				nr.push_back(r0);
				nr.push_back(r1);

				// selec lower rec
				// split vert, then split top half horz
				// discard duplicat peice
				r0 = b0 ? e : r;
				splity(r0, r1, ovr.y1);
				nr.push_back(r0);

				splitx(r1, r0, b1 ? ovr.x0 : ovr.x1);
				r1 = b1 ? r0 : r1;
				nr.push_back(r1);
				break;
			case 2:
				// determine which axis is optimal for splitting along
				b0 = (r.rec.x0 > e.rec.x0) == (r.rec.x1 < e.rec.x1);
				if (b0)
					goto xoptimal;
				// yoptimal:
				printf("MERGE CASE 2Y\n");
				b0 = r.rec.y0 < e.rec.y0;
				r0 = b0 ? r : e;

				splity(r0, r1, ovr.y0);
				nr.push_back(r1);
				splity(r0, r1, ovr.y1);
				nr.push_back(r0);

				r0 = b0 ? e : r;
				b1 = r0.rec.x0 == ovr.x0;
				r1.datum &= b1 ? ~R_WALL : ~L_WALL;
				nr.push_back(r1);

				splitx(r0, r1, b1 ? ovr.x1 : ovr.x0);
				r0 = b1 ? r0 : r1;
				nr.push_back(r0);
				break;
			// */
			xoptimal:
				printf("MERGE CASE 2X\n");
				b0 = r.rec.x0 < e.rec.x0;
				r0 = b0 ? r : e;

				splitx(r0, r1, ovr.x0);
				nr.push_back(r1);
				splitx(r0, r1, ovr.x1);
				nr.push_back(r0);

				r0 = b0 ? e : r;
				b1 = r0.rec.y0 == ovr.y0;
				r1.datum &= b1 ? ~B_WALL : ~T_WALL;
				nr.push_back(r1);

				splity(r0, r1, b1 ? ovr.y1 : ovr.y0);
				r0 = b1 ? r0 : r1;
				nr.push_back(r0);
				break;
			case 4:
				printf("SOMETHING IS WRONG\n");
			}

			Delete(i);
			// insert new rectangles
			// update existing entries
		}
		// IDEA
		// discard larger entries that overlap stuff,
		// there should be enough rects that dont enclose anything to fill
		// their place ?
		for (int j = (int)nr.size() - 1; j >= 0; j--) {
			for (int h = (int)nr.size() - 1; h >= 0; h--) {
				if (h == j)
					continue;
				if (nr[j].rec.enclose(nr[h].rec)) {
					// bool b0 = nr[h].rec.x0 == nr[j].rec.x0;
					// bool b1 = nr[h].rec.y0 == nr[j].rec.y0;
					// bool b2 = nr[h].rec.x1 == nr[j].rec.x1;
					// bool b3 = nr[h].rec.y1 == nr[j].rec.y1;
					// uint32_t mask =
					//(L_WALL * (b0)) |
					//(T_WALL * (b1)) |
					//(R_WALL * (b2)) |
					//(B_WALL * (b3));

					// nr[h].datum &= nr[j].datum | (~mask);
					printf("	MERGING NEW\n");
					nr[j] = nr.back();
					nr.pop_back();
					break;
				}
			}
		}
		// IDEA
		// there are still cases where rectangles are intersecting
		// at this point, is there a simpler axis clipping we can do ? ?

		// TODO
		// Need to absorb missing walls that are shared edges

		for (int j = (int)nr.size() - 1; j >= 0; j--) {
			for (int h = (int)nr.size() - 1; h >= 0; h--) {
				if (h == j)
					continue;
				rect_t& r0 = nr[h].rec;
				rect_t& r1 = nr[j].rec;

				bool b0 = (r0.x0 == r1.x0);
				bool b1 = (r0.y0 == r1.y0);
				bool b2 = (r0.x1 == r1.x1);
				bool b3 = (r0.y1 == r1.y1);
				bool b4 = nr[h].rec.width() <= nr[j].rec.width();
				bool b5 = nr[h].rec.height() <= nr[j].rec.height();

				uint32_t mask =
				    // not sure if this is sane
				    (L_WALL * (b0 & (b1 | b3) & b4))
				    | (T_WALL * (b1 & (b0 | b2) & b5))
				    | (R_WALL * (b2 & (b1 | b3) & b4))
				    | (B_WALL * (b3 & (b0 | b2) & b5));
				nr[h].datum &= nr[j].datum | (~mask);

				bool b6 = (r0.x0 == r1.x1);
				bool b7 = (r0.y0 == r1.y1);
				bool b8 = (r0.x1 == r1.x0);
				bool b9 = (r0.y1 == r1.y0);
				mask =
				    // i need bits shuffled such
				    // that right wall value is in left wall bit position
				    (R_WALL * (b6 & (b1 | b3) & b5))
				    | (B_WALL * (b7 & (b0 | b2) & b4))
				    | (L_WALL * (b8 & (b1 | b3) & b5))
				    | (T_WALL * (b9 & (b0 | b2) & b4));

				nr[h].datum &= bit_permute_step_simple(nr[j].datum | (~mask),
				                                       0x55555555, 1);
			}
		}

		for (const entry_t& e : nr) {
			Insert(e.datum, e.rec);
		}
	}
};

// TODO
// try new map idea here

/*
struct mapEntry_t {
        uint8_t tile;
        uint8_t props;// bitfield
        int8_t offX;
        int8_t offY;
        enum {
                // possible tile props
                NONE = 0x00,
                FLOOR = 0x01,
                WALL = 0x02,
        }
};
typedef mapTiles_t rtree<mapEntry_t, 8, 16>;

*/
