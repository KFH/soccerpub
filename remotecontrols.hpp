#pragma once
#include "buff.hpp"
#include "game.hpp"
#include "localcontrols.hpp"
#include "netapi.hpp"

#include <cstring>

namespace sim {
// TODO/IDEA
// record players inputs while game is running forward
// playback recording when resimulating
// NEEDS TO PLAYBACK NON DESTRUCTIVELY, eg dont use pop()
// would be nice if it could expose recorded sequence of inputs to network serialization

template <typename input_t>
class RingRecordStream : public InputStream<input_t>, public InputRecorder<input_t> {
    public:
	RingBuffer<input_t, MAXINPUT> buff;
	size_t                        m_off;

	RingRecordStream(){};

	virtual void push_back(const input_t& in) override { buff.push(in); }

	// free from head
	virtual void Free(int x) override
	{
		m_off = std::max(0, (int)m_off - x);
		while (x > 0) {
			x--;
			buff.pop();
		}
	}

	virtual void Rewind() override { m_off = 0; }

	virtual int Get(input_t& out) override
	{
		out = buff.getRel(m_off); // buff.pop();
		m_off++;
		return buff.size();
	};
	virtual int GetConst(input_t& out) const override
	{
		out = buff.getRel(m_off);
		return buff.size();
	};

	virtual int Remaining() override { return buff.size(); };
};

template <typename input_t>
class speculateStream : public InputStream<input_t>, public BinaryInputRecorder {
    public:
	typedef typename vInstance<input_t>::ctrlOverride_t ctrlOverride_t;

	InputRecorder<input_t>* rec;
	ctrlOverride_t          ctrl;

	speculateStream(int new_id, InputRecorder<input_t>* r)
	{
		rec        = r;
		this->id   = new_id;
		_type_size = sizeof(ctrlOverride_t);
	}
	virtual int Get(input_t& out) override
	{
		out = ctrl.ctrl;
		return 1;
	}
	virtual int GetConst(input_t& out) const override
	{
		out = ctrl.ctrl;
		return 1;
	}
	// return estimate remaining, a network stream or local stream could just return 1
	virtual int Remaining() override { return 1; }

	virtual void push_back(const char* in) override
	{
		// pray
		// ctrl = *(input_t*)in;
		memcpy(&ctrl, in, _type_size);
		if (rec)
			rec->push_back(ctrl.ctrl);
	};
};

// template<typename input_t>
// class SizedPipeWrapper : public sized_pipe_t {
// };

// rube goldberg machine
// read() function reads from input pipe then copies to recorders and to out
template <size_t S> class SizedTeePipe_t : public sized_pipe_t {
    public:
	sized_pipe_t*              src;
	std::vector<sized_pipe_t*> rec;

	SizedTeePipe_t() { mtype_size = S; }

	virtual int read(char* out) override
	{
		src->read(out);
		for (auto r : rec) {
			r->append(out);
		}
		return src->remaining();
	}
	virtual int read_const(char* out) const override { return src->read_const(out); }
	virtual int remaining() { return src->remaining(); }

	// unsure if this is good behavior
	virtual void append(const char* in) override { src->append(in); }
	virtual void free(int x) { src->free(x); }
	virtual void rewind() { src->rewind(); }
};
/*
template<size_t S>
class SizedRingPipe_t : public sized_pipe_t {

        RingBuffer<char, S * MAXINPUT> buff;
        size_t                         m_off;

        public:

        SizedRingPipe_t() {
                mtype_size = S;
        }
        virtual void append(const char* in) override {
                buff.push(*(input_t*)in);
                return;
        }
        virtual int  read(char* out) override {
                auto* output = (input_t*)out;
                *out = buff.getRel(m_off);
                m_off++;
                return buff.size();
        }
        virtual int  read_const(char* out) const override {
                auto* output = (input_t*)out;
                *out = buff.getRel(m_off);
                return buff.size();
        }
        virtual int  remaining() {
                return buff.size();
        }
        virtual void free(int x) {
                m_off = std::max(0, (int)m_off - x);
                while (x > 0) {
                        x--;
                        buff.pop();
                }
        }
        virtual void rewind() {
                m_off = 0;
        }
};
*/
} // namespace sim
