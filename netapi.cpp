
#include "netapi.hpp"
#include "net.hpp"
#include <algorithm>
#include <vector>

namespace net {

int                   CONNQUALITY = 0;
const int             IPV4PROTO   = AF_INET;
const int             IPV6PROTO   = AF_INET6;
std::vector<Context*> ctxPtrs;
char*                 PublicPunchAddr;
const char*           _defaultPublicPunchAddr = DEFAULT_PUNCH;
int                   isNetworkUp             = 0;

int Init()
{
	// TODO
	// debug local punch server, instead use a domain name
	// and resolve to an ip address
	PublicPunchAddr = (char*)_defaultPublicPunchAddr;

#ifdef USEPUNCH
	addrinfo     hints, *result, *rp;
	sockaddr_in* h;
	char*        a;
	int          s;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family   = AF_INET; /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
	hints.ai_flags    = 0;
	hints.ai_protocol = 0; /* Any protocol */
	if (getaddrinfo("k-2.ca", "8888", &hints, &result) != 0) {
		return 0;
	}
	for (rp = result; rp != NULL; rp = rp->ai_next) {
		switch (rp->ai_addr->sa_family) {
		case AF_INET:
			h = (sockaddr_in*)rp->ai_addr;
			a = inet_ntoa(h->sin_addr);
			printf("ADRESS FOUND %s\n", a);
			PublicPunchAddr = (char*)calloc(strlen(a) + 6, sizeof(char));
			sprintf(PublicPunchAddr, "%s:8888", a);
			break;
		case AF_INET6:
			fprintf(stderr, "UNSUPPORTED NET FAMILY\n");
			break;
		}
		break;
	}
	freeaddrinfo(result);
	printf("PUBLIC PUNCH ADDR %s\n", PublicPunchAddr);
#endif
	isNetworkUp = 1;

	return sockInit();
}

int IsInit() { return isNetworkUp; }

void Exit()
{

	// TODO
	// for all open contexts and connections free resources
	isNetworkUp = 0;
	for (auto c : ctxPtrs) {
		FreeContext(c);
	}
	ctxPtrs.resize(0);
	sockQuit();
}

CTX_h OpenContext(uint16_t port, int fam)
{
	(void)fam;
	if (!isNetworkUp)
		return nullptr;
	printf("CREATED NET CONTEXT\n");
	Context* cpt = new Context(port);
	if (!cpt->msock.IsOpen()) {
		delete cpt;
		return nullptr;
	}

	ctxPtrs.push_back(cpt);
	return cpt;
}

void FreeContext(CTX_h ctx)
{
	printf("FREED NET CONTEXT\n");
	Context* cpt = (Context*)ctx;
	cpt->Free();
	// unordered remove with std is ugly
	std::swap(*std::find(ctxPtrs.begin(), ctxPtrs.end(), cpt), ctxPtrs.back());
	ctxPtrs.pop_back();
	delete cpt;
	ctx = NET_BADVAL;
}
int ConnectTo(CTX_h ctx, const char* addr)
{
	// TODO
	// parse string to addr
	Address newaddr;
	if (newaddr.SetStrIPv4(addr) == EOF) {
		fprintf(stderr, "Failed to parse address\n");
		return -1;
	}

	Context* cpt           = (Context*)ctx;
	int      connectionUid = 1234; // TODO not sure if i need this uid stuff anymore
	// TODO
	// check if connection already exists for this address and port
	Connection* nc = new Connection(*cpt, newaddr, connectionUid);
	cpt->AddConnection(nc);

	return 1;
}

void CloseAllConnections(CTX_h ctx)
{
	if (ctx) {
		for (auto& conn : ctx->conns) {
			if (conn) {
				conn->Close();
			}
		}
		ctx->Cleanup();
	}
}

int CloseConnection(CTX_h ctx, CXN_h conn)
{
	conn->Close();
	return 0;
}
int SendAndRecieve(CTX_h ctx)
{

	// if context set to use
	ctx->Announce();
	ctx->Send();
	ctx->Recieve();

	return 0;
}

int Recieve(CTX_h ctx)
{
	ctx->Recieve();
	return 0;
}

int FlushConn(CTX_h ctx, CXN_h conn)
{
	conn->Send();
	return 1;
}

std::string GetPublicAddress(CTX_h ctx) { return ctx->publicAddr.ToStrinIPv4(); }

int    GetNumConnecitons(CTX_h ctx) { return ctx->OpenConnections(); }
CXN_h* GetConnections(CTX_h ctx)
{

	Context*           cpt = (Context*)ctx;
	thread_local CXN_h opencons[NET_MAXCONNECTIONS];

	int i = 0;
	for (Connection* c : cpt->conns) {
		if (c) {
			opencons[i] = c;
			i++;
		}
	}
	return opencons;
}

void GetConnAddr(CTX_h ctx, CXN_h conn, char* dst) { conn->maddr.WriteV4(dst); }

int SendAll(CTX_h ctx, void* data, int len)
{
	for (Connection* c : ctx->conns) {
		if (c) {
			c->AddData(data, len);
		}
	}
	return 1;
}

int Send(CTX_h ctx, CXN_h conn, void* data, int data_s) { return conn->AddData(data, data_s); }

int GetSpace(CTX_h ctx, CXN_h conn) { return conn->GetSpace(); }

void SetSerializer(CTX_h ctx, CXN_h conn, deserializer* des, bool ordered)
{
	if (ordered) {
		conn->orderDst = des;
	} else {
		conn->unorderDst = des;
	}
}
deserializer* GetSerializer(CTX_h ctx, CXN_h conn, bool ordered)
{
	if (ordered)
		return conn->orderDst;
	return conn->unorderDst;
}
connStats GetConnStats(CTX_h ctx, CXN_h conn) { return conn->stats; }
void      SetConnOpenCallback(CTX_h ctx, ConnectionChangeCB cb)
{
	Context* c = (Context*)ctx;
	c->OpenCB  = cb;
}
void SetConnCloseCallback(CTX_h ctx, ConnectionChangeCB cb)
{
	Context* c = (Context*)ctx;
	c->CloseCB = cb;
}
} // namespace net
