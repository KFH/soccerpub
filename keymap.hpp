
template <typename K, typename V> class keymap {
	std::vector<K> keys;
	std::vector<V> values;

    public:
	keymap()
	    : keys()
	    , values()
	{
		// init
	}

	void reserve(size_t newsize)
	{
		keys.reserve(newsize);
		values.reserve(newsize);
	}
	void resize(size_t newsize)
	{
		keys.resize(newsize);
		values.resize(newsize);
	}

	int find(K key)
	{
		int ix = std::find(keys.begin(), keys.end(), key) - keys.begin();
		return ix < keys.size() ? ix : -1;
	}
	void insert(K key, V val)
	{
		int index = find(key);
		if (index < 0) {
			keys.push_back(key);
			values.push_back(val);
			return;
		}
		values[index] = val;
	}
	void insertAt(int index, const V val) { values[index] = val; }
	void insertKetAt(int index, const K key) { keys[index] = key; }
	void remove(const K key)
	{
		int index = find(key);
		if (index >= 0) {
			keys[index]   = keys.back();
			values[index] = values.back();
			keys.pop_back();
			values.pop_back();
		}
	}

	const K& getKey(int i) { return keys[i]; }
	const V& getVal(int i) { return values[i]; }
	void     reset()
	{
		keys.resize(0);
		values.resize(0);
	}
	int size() { return keys.size(); }
};
