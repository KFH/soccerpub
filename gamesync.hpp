
#include "netapi.hpp"
#include "remotecontrols.hpp"
#include "snap.hpp"
#include "timeslew.hpp"

#define NETMAGIC 170

namespace sim {

// TODO
// define some serialize formats/data types for network transfer
//
// unorderd:
// player pos and velocity
// current frame
// current time

// ordered:
// controller inputs
// new, stop, pause ?
// serialized level state - - - networking needs a method to break this into chunks

typedef uint8_t dataDesc_t;
namespace RPCT {
	enum {
		//
		PLAYER_INPUT,
		PLAYER_POS,
		RES,
		TIME,
		TIMEREQ,
		TIMESYNC,
		STATE
	};
};
template <typename T, dataDesc_t desc> struct chunk_t {
	dataDesc_t type;
	T          data;
	chunk_t(T d)
	{
		data = d;
		type = desc;
	}
};

struct snapshotDesc_t {
	uint64_t frame;
	uint32_t crc;
	uint32_t uid;
	uint32_t dsize;
};

#pragma pack(push, 1)

class snapShotIngestor {
    public:
	virtual void ApplySnapShot(ByteBuffer& buff, uint64_t frame) = 0;
};

// send this every 60 frame simulated snapshot
struct syncDesc_t {
	uint64_t time; // time from some kind of shared reference utc?
	uint64_t eframe; // local percieved frame ?
	uint64_t sframe; // fully simulated frame
	uint32_t crc; // checksum
};

// struct timeDesc_t {
// 	uint64_t frame;
// 	double   time;
// };
struct timeStamp_t {
	uint64_t timeSent;
	uint64_t localTime;
};
struct timeReq_t {
	uint64_t timeSent;
};

typedef chunk_t<timeReq_t, RPCT::TIMEREQ>   timeReqChunk;
typedef chunk_t<timeStamp_t, RPCT::TIME>    timeStampChunk;
typedef chunk_t<syncDesc_t, RPCT::TIMESYNC> syncDescChunk;

static_assert(sizeof(timeStampChunk) == 17, "struct not packed to correct size");
#pragma pack(pop)

class NoOrderDeserializer : public net::deserializer {
    public:
	timeSlewer* timeslew;
	bool        needStamp;
	uint64_t    needStampTime;

	size_t ctrl_override_size;

	NoOrderDeserializer(size_t ctrl_size, timeSlewer* ts)
	{
		timeslew           = ts;
		needStamp          = false;
		ctrl_override_size = ctrl_size;
	}
	virtual int read(net::CXN_h conn, const unsigned char* data, int len) override
	{

		ByteBuffer buff;
		buff.setData(data, len);
		dataDesc_t dtype = 0;
		while (buff.unread()) {
			buff.read(&dtype, sizeof(dtype));
			switch (dtype) {
			case RPCT::STATE: {
				// should consider disconnecting?
				snapshotDesc_t sd{};
				buff.read(&sd, sizeof(snapshotDesc_t));
				buff.read(nullptr, sd.dsize);
			} break;
			case RPCT::PLAYER_INPUT:
				buff.read(nullptr, ctrl_override_size);
				break;
			case RPCT::TIME:
				// set timeslew
				{
					timeStamp_t t;
					buff.read(&t, sizeof(t));
					double latency
					    = (double)timeslew->GetTimeInt() - (double)t.timeSent;
					timeslew->SetRef(t.localTime, latency * 0.5);
					printf("TIME STAMP %lu LOC %lu\n", t.timeSent,
					       timeslew->GetTimeInt());
				}
				break;
			case RPCT::TIMEREQ:
				// place req into some kind of timeslew queue thing ?
				needStamp = true;
				buff.read(&needStampTime, sizeof(needStampTime));
				printf("TIME REQ\n");
				break;
			case RPCT::TIMESYNC: {
				syncDesc_t sd;
				buff.read(&sd, sizeof(sd));
				timeslew->InsertCheckPoint(sd.eframe, sd.time);
				// TODO
				// compare syncDesc_t crc with local crcs to detect desync
				printf("TIME SYNC F:%ld T:%ld CRC:%08x\n", sd.eframe, sd.time,
				       sd.crc);
			} break;
			case NETMAGIC:
			default:
				printf("NETMAGIC\n");
				buff.readback(1);
				goto exit;
			}
		}
	exit:
		return len - buff.unread();
	}
};

class RpcDeserializer : public net::deserializer {
    public:
	sim::BinaryInputRecorder& in;
	timeSlewer&               timeslew;
	snapShotIngestor&         snapIngest;
	bool                      needStamp;
	uint64_t                  needStampTime;

	RpcDeserializer(sim::BinaryInputRecorder& pipe, timeSlewer& ts, snapShotIngestor& si)
	    : in(pipe)
	    , timeslew(ts)
	    , snapIngest(si)
	{

		needStamp     = false;
		needStampTime = 0;
	}
	virtual int read(net::CXN_h conn, const unsigned char* data, int len) override
	{

		ByteBuffer buff;
		buff.setData(data, len);
		dataDesc_t dtype = 0;
		// ctrlOverride co;
		char temp[128];
		while (buff.unread()) {
			buff.read(&dtype, sizeof(dtype));
			switch (dtype) {

			case RPCT::STATE:
				// TODO apply snapshot to game
				{
					snapshotDesc_t sd{};
					buff.read(&sd, sizeof(snapshotDesc_t));

					printf("SNAPSHOT recieved, frame:%lu crc %u size %u\n",
					       sd.frame, sd.crc, sd.dsize);

					SnapRecorder::snapshot snap;
					snap.frame = sd.frame;

					// copying would be redundant just wrap ptr
					size_t safe_size
					    = std::min(uint32_t(buff.unread()), sd.dsize);
					snap.buff.setData(buff.head(), safe_size);
					snap.crc = snap.CalcCRC();

					if (snap.crc != sd.crc) {
						fprintf(stderr,
						        "SNAP SHOT CRC %u DOES NOT MATCH %u\n",
						        snap.crc, sd.crc);
					} else {
						// TODO
						// GameSystem needs to reset all input buffers to 0?
						// apply game state
					}

					snapIngest.ApplySnapShot(snap.buff, snap.frame);

					// read to end
					buff.read(nullptr, safe_size);
				}
				break;
			case RPCT::PLAYER_INPUT:
				// place player input into an InputStream
				// that holds sequence of inputs
				// buff.read(&co, sizeof(ctrlOverride));
				// in->push_back(co.ctrl);

				// TODO quick hack this is crappy design/abstraction
				buff.read(temp, in.type_size());
				in.push_back(temp);

				break;
			case RPCT::TIME:
				buff.read(nullptr, sizeof(timeStamp_t));
				break;
			case RPCT::TIMEREQ:
				buff.read(nullptr, sizeof(needStampTime));
				break;
			case RPCT::TIMESYNC:
				buff.read(nullptr, sizeof(syncDesc_t));
				break;
				break;
			case NETMAGIC:
				printf("NETMAGIC\n");
			default:
				buff.readback(1);
				// fprintf(stderr, "unsupported data type %d\n", dtype);
				goto exit;
			}
		}
	exit:
		return len - buff.unread();
	}
};

} // namespace sim
