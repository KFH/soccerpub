#pragma once
// https://www.sebastiansylvan.com/post/r-trees-%E2%80%93-adapting-out-of-core-techniques-to-modern-memory-architectures/
// http://gdcvault.com/play/1012255/R-Trees-Adapting-out-of
// store bounds in parent node not child for cache
// try to make array of rects = 1 cache line / 64 bytes ? ? ?
// insert into node requiring least expansion
// refine tree periodicly using k-means clustering
// splitting hueristic isnt very important ? ? ?
// breadth search before depth search ? ? ?

#include "random.hpp"

struct rect_t {
	int32_t x0, y0, x1, y1;

	void merge(const rect_t& r)
	{
		x0 = r.x0 < x0 ? r.x0 : x0;
		y0 = r.y0 < y0 ? r.y0 : y0;
		x1 = r.x1 > x1 ? r.x1 : x1;
		y1 = r.y1 > y1 ? r.y1 : y1;
	};
	rect_t operator+(const rect_t& r) const
	{
		rect_t nr{ x0, y0, x1, y1 };
		nr.merge(r);
		return nr;
	}

	int cx() const { return x0 + (x1 - x0) / 2; }
	int cy() const { return y0 + (y1 - y0) / 2; }

	void expand(int x)
	{
		x0 -= x;
		x1 += x;
		y0 -= x;
		y1 += x;
	}

	void normalize()
	{
		// shuffle so top left small bottom right big
		int low = x0 < x1 ? x0 : x1;
		int hi  = x0 > x1 ? x0 : x1;
		x0      = low;
		x1      = hi;
		low     = y0 < y1 ? y0 : y1;
		hi      = y0 > y1 ? y0 : y1;
		y0      = low;
		y1      = hi;
	};

	// return a manhattan distance
	int mdist(const rect_t& rec) const
	{

		int hx0 = (x1 - x0) / 2;
		int hy0 = (y1 - y0) / 2;

		int cx0 = x0 + hx0;
		int cy0 = y0 + hy0;
		int hx1 = (rec.x1 - rec.x0) / 2;
		int hy1 = (rec.y1 - rec.y0) / 2;
		int cx1 = rec.x0 + hx1;
		int cy1 = rec.y0 + hy1;

		return abs(cx0 - cx1) * abs(cy0 - cy1);

		// int d = (abs(cx0 - cx1) + abs(cy0 - cy1) - ((hy0 + hy1) + (hx0 + hx1)));
		// return intersects(rec) ? 0 : d;
	};
	int cdist(int cx, int cy) const
	{
		int hx0 = (x1 - x0) / 2;
		int hy0 = (y1 - y0) / 2;
		int cx0 = x0 + hx0;
		int cy0 = y0 + hy0;
		return abs(cx0 - cx) + abs(cy0 - cy);
	};

	int width() const { return x1 - x0; }
	int height() const { return y1 - y0; }

	// NOTE area would overflow int32 type if side lengths exceed sqrt of 2^31 ?
	// could return a int64 to handle overflow?
	int64_t     area() const { return int64_t(x1 - x0) * int64_t(y1 - y0); }
	inline bool intersects(const rect_t& r) const
	{
		return !((r.x0 >= x1) | (r.x1 <= x0) | (r.y0 >= y1) | (r.y1 <= y0));
	}
	inline bool enclose(const rect_t& r) const
	{
		return (x0 <= r.x0) & (y0 <= r.y0) & (x1 >= r.x1) & (y1 >= r.y1);
	}
	inline bool enclose(int px, int py) const
	{
		return (x0 <= px) & (y0 <= py) & (x1 >= px) & (y1 >= py);
	}
	bool operator==(const rect_t& rhs) const
	{
		return x0 == rhs.x0 & y0 == rhs.y0 & x1 == rhs.x1 & y1 == rhs.y1;
	}
};

template <typename T> class queryCallback_t {
    public:
	virtual bool callback(int index, const rect_t& rec, const T& data) = 0;
};

template <typename T, int RMAX, int LMAX> class rtree {
    public:
	struct entry_t {
		rect_t rec;
		T      datum;
	};
	template <int maxChildren, bool isLeaf> struct node_t {
		static constexpr int  MAX    = maxChildren;
		static constexpr bool ISLEAF = isLeaf;
		static_assert((maxChildren % 2) == 0, "Node child size must be divisible by 2");
		static_assert((maxChildren <= 128), "Node maxChildren is too large");

		rect_t erects[MAX];
		int    eindex[MAX];
		rect_t rec;
		int8_t ecount;
		int8_t level;

		bool full() const { return ecount >= maxChildren; }

		void overwrite(int i, int index, const rect_t& r)
		{
			eindex[i] = index;
			erects[i] = r;
			rec.merge(r);
		}
		void add(int i, const rect_t& r)
		{
			if (full())
				return;
			overwrite(ecount, i, r);
			ecount++;
		}
		void remove(int i)
		{
			ecount--;
			erects[i] = erects[ecount];
			eindex[i] = eindex[ecount];
		}
		void reset()
		{
			for (auto ei : eindex) {
				ei = -1;
			}
			ecount = 0;
		}
		void recalcBounds()
		{
			rec = erects[0];
			for (int i = 1; i < ecount; i++) {
				rec.merge(erects[i]);
			}
		}
		// try to inferIndex within array
		int inferIndex(char* headptr, char* tailptr) const
		{
			char*   curptr   = (char*)this;
			int64_t sizediv  = sizeof(node_t);
			int64_t curindex = (curptr - headptr) / sizediv;
			curindex         = ((curindex >= 0) & (curptr <= tailptr)) ? curindex : -1;
			return curindex;
		}

		node_t<MAX, ISLEAF> naiveSplit()
		{
			node_t<MAX, ISLEAF> n = { {}, {}, rect_t{}, 0, 0 };
			for (int i = MAX / 2, h = 0; i < MAX; i++, h++) {
				n.erects[h] = erects[i];
				n.eindex[h] = eindex[i];
				eindex[i]   = 0;
			}
			ecount   = MAX / 2;
			n.ecount = MAX / 2;
			n.level  = level;
			n.recalcBounds();
			recalcBounds();
			return n;
		}
		node_t<MAX, ISLEAF> kmeanSplit()
		{
			static thread_local unsigned int mrs = 1;

			int8_t ls[MAX];
			int8_t rs[MAX];
			int8_t lc   = 1;
			int8_t rc   = 1;
			int    l    = rand_r_fill(&mrs) % MAX;
			int    r    = rand_r_fill(&mrs) % MAX;
			int    loop = 0;

			auto centroid = [&](int8_t stack[MAX], int count) {
				int cx = 0, cy = 0, ni = 0, dist = 0;
				for (int i = 0; i < count; i++) {
					cx += erects[stack[i]].cx();
					cy += erects[stack[i]].cy();
				}
				cx /= count;
				cy /= count;
				// printf("			CENTROID %dx%d\n", cx, cy);
				dist = erects[stack[0]].cdist(cx, cy);
				for (int i = 1; i < count; i++) {
					int ndist = erects[stack[i]].cdist(cx, cy);
					if (ndist < dist) {
						dist = ndist;
						ni   = i;
					}
				}
				return stack[ni];
			};
			// TODO should probably lower loop count
			for (; loop < 32; loop++) {
				auto& lr = erects[l];
				auto& rr = erects[r];
				lc       = 1;
				rc       = 1;
				ls[0]    = l;
				rs[0]    = r;
				for (int i = 0; i < MAX; i++) {
					if ((i == l) | (i == r))
						continue;
					int d0 = erects[i].mdist(lr);
					int d1 = erects[i].mdist(rr);
					// printf("	n%d=%dvs%d\n", eindex[i], d0, d1);
					if (d0 < d1) {
						ls[lc] = i;
						lc++;
					} else {
						rs[rc] = i;
						rc++;
					}
				}
				// pick new l and r closest to centroid
				// if l or r equal to previous l or r break
				int newl = centroid(ls, lc);
				int newr = centroid(rs, rc);
				if (r == newr && l == newl)
					break;

				l = newl;
				r = newr;
			}
			// WE NOW have two stacks of indexs use this to split node
			// right node
			node_t<MAX, ISLEAF> n = { {}, {}, rect_t{}, rc, level };
			for (int i = 0; i < rc; i++) {
				int h       = rs[i];
				n.erects[i] = erects[h];
				n.eindex[i] = eindex[h];
			}
			n.recalcBounds();
			// left node
			node_t<MAX, ISLEAF> n2 = { {}, {}, rect_t{}, lc, level };
			for (int i = 0; i < lc; i++) {
				int h        = ls[i];
				n2.erects[i] = erects[h];
				n2.eindex[i] = eindex[h];
			}
			n2.recalcBounds();
			*this = n2;

			// printf("			KMEANS %d-%d LOOPS %d \n", lc, rc, loop);

			return n;
		}
	};
	typedef node_t<RMAX, false> root_t;
	typedef node_t<LMAX, true>  leaf_t;

	struct split_t {
		int    index;
		rect_t rec;
	};

	root_t               m_root;
	std::vector<root_t>  roots;
	std::vector<leaf_t>  leaves;
	std::vector<entry_t> entries;
	std::vector<int>     freeEntries;

	rtree()
	    : roots()
	    , leaves()
	    , entries()
	{
		// leaves.reserve(2);
		leaves.push_back(leaf_t{ {}, {}, rect_t{ 0, 0, 0, 0 }, 0, 0 });
		memset(&leaves.back(), 0, sizeof(leaf_t));
		m_root = root_t{ {}, {}, rect_t{ 0, 0, 0, 0 }, 0, 0 };
		memset(&m_root, 0, sizeof(root_t));
		m_root.add(0, leaves.back().rec);

		// printf("RTREE\nroot_t sizeof %zu\nleaf_t sizeof %zu\n", sizeof(root_t),
		//       sizeof(leaf_t));
	}

	void Reset()
	{
		freeEntries.resize(0);
		roots.resize(0);
		leaves.resize(0);
		entries.resize(0);
		leaves.push_back(leaf_t{ { 0 }, { 0 }, rect_t{ 0, 0, 0, 0 }, 0, 0 });
		m_root = root_t{ { 0 }, { 0 }, rect_t{ 0, 0, 0, 0 }, 0, 0 };
		m_root.add(0, leaves.back().rec);
	}

    private:
	inline bool AddLeaf(leaf_t& node, int eid, const rect_t& rec, split_t (&out)[2])
	{
		// NOTE
		// NODE must infer index before leaves.push_back() because
		// memory location could change

		bool msplit = false;
		if (node.full()) {

			msplit = true;

			leaf_t newnode = node.naiveSplit(); // node.kmeanSplit(); //
			out[1].index   = leaves.size();
			out[1].rec     = newnode.rec;

			out[0].index = node.inferIndex((char*)leaves.data(), (char*)&leaves.back());
			node.rec.merge(rec);
			node.add(eid, rec);
			out[0].rec = node.rec;

			leaves.push_back(newnode);
			// printf("		SPLIT %d NEW TOTAL %zu\n", out[0].index,
			//       leaves.size());

		} else {
			node.rec.merge(rec);
			node.add(eid, rec);
		}
		return msplit;
	}
	bool NodeInsert(root_t& node, int eid, const rect_t& rec, split_t (&out)[2])
	{
		// printf("	ADDING TO ROOT %d id %d\n", node.level, eid);
		node.rec.merge(rec);
		int64_t harea = std::numeric_limits<int64_t>::max();
		int64_t sarea = std::numeric_limits<int64_t>::max();
		int     n = -1, i = 0;
		bool    msplit = false, childSplit = false;
		for (i = 0; i < node.ecount; i++) {
			// use heuristic of least expansion to enclose
			const rect_t& nrec      = node.erects[i];
			int64_t       newarea   = (nrec + rec).area() - nrec.area();
			int64_t       smallarea = nrec.area();
			if (newarea <= harea) {
				if (harea == 0) {
					if (smallarea < sarea) {
						sarea = smallarea;
						harea = newarea;
						n     = i;
					}
				} else {
					sarea = smallarea;
					harea = newarea;
					n     = i;
				}
			}
		}

		i = node.eindex[n];
		if (node.level == 0) {
			// printf("		ADDING TO LEAF %d id %d\n", i, eid);
			node.erects[n].merge(rec);
			childSplit = AddLeaf(leaves[i], eid, rec, out);
		} else {
			node.erects[n].merge(rec);
			childSplit = NodeInsert(roots[i], eid, rec, out);
		}

		if (childSplit && !node.full()) {
			node.overwrite(n, out[0].index, out[0].rec);
			node.add(out[1].index, out[1].rec);
		} else if (childSplit) {
			// split current node AND add the new childs out[0] and out[1] to
			// either
			// printf("SPLITTING ROOT\n");
			msplit       = true;
			auto newnode = node.kmeanSplit(); // node.naiveSplit();
			node.add(out[1].index, out[1].rec);
			out[0].index = node.inferIndex((char*)roots.data(), (char*)&roots.back());
			out[0].rec   = node.rec;
			// determine THIS nodes index by pointer distance from array[0] ptr?
			// m_root would be outside so return -1 for that instance
			out[1].index = roots.size();
			out[1].rec   = newnode.rec;
			roots.push_back(newnode);
		}
		return msplit;
	}

    public:
	void Insert(const T& datum, const rect_t& rec)
	{
		// printf("INSERTING %d,%dx%d,%d\n", rec.x0, rec.y0, rec.x1, rec.y1);
		split_t splits[2];
		int     eid = -1;
		if (freeEntries.size() > 0) {
			eid = freeEntries.back();
			freeEntries.pop_back();
			entries[eid] = entry_t{ rec, datum };
		} else {
			eid = entries.size();
			entries.push_back(entry_t{ rec, datum });
		}

		if (NodeInsert(m_root, eid, rec, splits)) {
			roots.push_back(m_root);
			m_root.reset();
			m_root.level++;
			m_root.add(splits[1].index, splits[1].rec);
			m_root.add(int(roots.size()) - 1, splits[0].rec);
			m_root.recalcBounds();
		}
	}

    private:
	template <class N>
	void SearchNode(const N& node, const rect_t& rec, queryCallback_t<T>& cb) const
	{

		int h = 0;
		int nstack[node.MAX];
		// uint8_t istack[node.MAX];
		for (int i = 0; i < node.ecount; i++) {
			if (node.erects[i].intersects(rec)) {
				nstack[h] = node.eindex[i];
				// istack[h] = i;
				h++;
			}
		}
		if (!node.ISLEAF) {
			for (h -= 1; h >= 0; h--) {
				int i = nstack[h];
				if (node.level == 0) {
					SearchNode(leaves[i], rec, cb);
				} else {
					SearchNode(roots[i], rec, cb);
				}
			}
		} else {
			for (h -= 1; h >= 0; h--) {
				int i = nstack[h];
				cb.callback(i, entries[i].rec, entries[i].datum);
			}
		}
	}

    public:
	void Search(const rect_t& rec, queryCallback_t<T>& cb) const
	{
		// recuse down tree searching for nodes
		SearchNode(m_root, rec, cb);
	}

    private:
	template <class N> rect_t SearchDelete(N& node, const rect_t& rec)
	{

		int h = 0;
		int nstack[node.MAX];
		// uint8_t istack[node.MAX];
		for (int i = 0; i < node.ecount; i++) {
			// must pass intersect and enclose for it to be a true parent
			if (node.erects[i].intersects(rec) & node.erects[i].enclose(rec)) {
				nstack[h] = node.eindex[i];
				// istack[h] = i;
				h++;
			}
		}
		for (h -= 1; h >= 0; h--) {
			int i = nstack[h];
			if (node.level == 0) {
				auto& l = leaves[i];
				for (int k = 0; k < l.ecount; k++) {
					if (rec == l.erects[k]) {
						// printf("REMOVING %d\n", k, l.eindex[k]);
						freeEntries.push_back(l.eindex[k]);
						l.remove(k);
					}
				}
			} else {
				node.erects[i] = SearchDelete(roots[i], rec);
			}
		}
		node.recalcBounds();
		return node.rec;
	}

    public:
	// traverse down tree find leaf that contains erect == rec and remove + index
	// add entry index to a list of freed entries for reuse
	void Delete(const rect_t& rec) { SearchDelete(m_root, rec); }
	void Delete(int eindex) { SearchDelete(m_root, entries[eindex].rec); }

    private:
	template <class N> void getRects(N& node, const rect_t& rec, queryCallback_t<rect_t>& cb)
	{
		for (int i = 0; i < node.ecount; i++) {
			if (node.erects[i].intersects(rec))
				cb.callback(i, node.erects[i], node.erects[i]);
		}
	}
	void searchRoot(const root_t& node, const rect_t& rec, queryCallback_t<rect_t>& cb)
	{
		for (int i = 0; i < node.ecount; i++) {
			if ((node.erects[i].intersects(rec) & (node.level != 0))) {
				cb.callback(i, node.erects[i], node.erects[i]);
				searchRoot(roots[node.eindex[i]], rec, cb);
			}
		}
	};
	void searchLeaves(const root_t& node, const rect_t& rec, queryCallback_t<rect_t>& cb)
	{
		for (int i = 0; i < node.ecount; i++) {
			if (node.erects[i].intersects(rec)) {
				if (node.level == 0) {
					cb.callback(i, node.erects[i], node.erects[i]);
				} else {
					searchLeaves(roots[node.eindex[i]], rec, cb);
				}
			}
		}
	};
	void searchEntries(const root_t& node, const rect_t& rec, queryCallback_t<rect_t>& cb)
	{
		for (int i = 0; i < node.ecount; i++) {
			if (node.erects[i].intersects(rec)) {
				if (node.level == 0) {
					getRects(leaves[node.eindex[i]], rec, cb);
				} else {
					searchEntries(roots[node.eindex[i]], rec, cb);
				}
			}
		}
	};

    public:
	size_t Size() { return entries.size(); }

	// retreive bounding boxes of node type for debugging, depth first for simplicity
	void DebugSearch(const rect_t& rec, queryCallback_t<rect_t>& cb, int nodetype)
	{

		switch (nodetype) {
		case 0:
			cb.callback(-1, m_root.rec, m_root.rec);
			searchRoot(m_root, rec, cb);
			break;
		case 1:
			searchLeaves(m_root, rec, cb);
			break;
		case 2:
			searchEntries(m_root, rec, cb);
			break;
		}
	}
	// TODO
	// function needs some kind of max time / iterations before interupt
	// or interupter flag / mutex of some kind ?
	// and a struct/stack of some kind to track progress refining the tree ? ? ?
	// would probably need to restart after an insert or delete
	struct refProg {
		int cRoot, cLeaf, cEntry;
		int flag; // ? ? ?
	};
	void Refine(refProg& rp) {}
};
