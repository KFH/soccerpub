#pragma once
// https://en.wikipedia.org/wiki/AA_tree
// https://ycpcs.github.io/cs350-fall2017/resources/index.html
// https://ycpcs.github.io/cs350-fall2017/lectures/AA-tree_lecture.pdf
// https://www.geeksforgeeks.org/aa-trees-set-1-introduction/
// http://www.eternallyconfuzzled.com/tuts/datastructures/jsw_tut_andersson.aspx

template <typename K, typename V> class tree_t {
    public:
	struct node_t;

	int16_t             rix;
	std::vector<node_t> nodes;
	std::vector<V>      values;

	// private:

	struct node_t {
		int16_t p;
		int16_t c0; // left
		int16_t c1; // right
		int16_t lvl;
		K       key;

		bool operator==(const node_t& other) const { return key == other.key; }
		bool operator!=(const node_t& other) const { return key != other.key; }

		void swap(int16_t oix, int16_t nix)
		{
			if (c0 == oix) {
				c0 = nix;
			} else if (c1 == oix) {
				c1 = nix;
			}
		}

		node_t* get(int16_t i, std::vector<node_t>& n)
		{
			if (i < 0)
				return nullptr;
			return &n[i];
		}
		node_t* parent(std::vector<node_t>& n) { return get(p, n); }
		node_t* left(std::vector<node_t>& n) { return get(c0, n); }
		node_t* right(std::vector<node_t>& n) { return get(c1, n); }
	};

	tree_t()
	    : nodes()
	    , values()
	{
		rix = -1;
	}
	~tree_t() {}

	int findRightSubEdge(int ix) const
	{
		while (ix > -1 && nodes[ix].c1 >= 0) {
			ix = nodes[ix].c1;
		}
		return ix;
	}

	int findLeftSubEdge(int ix) const
	{
		while (ix > -1 && nodes[ix].c0 >= 0) {
			ix = nodes[ix].c0;
		}
		return ix;
	}

	int find(K key) const
	{
		int16_t ix = rix;
		int16_t ox = rix;
		// while node probably exists
		while (ix >= 0) {
			auto& l = nodes[ix];
			if (key == l.key)
				return ix;
			ox = ix;
			if (key < l.key) {
				ix = l.c0;
			} else {
				ix = l.c1;
			}
		}
		return -1;
	}
	int findIndex(K key) const
	{
		int16_t ix = rix;
		int16_t ox = rix;
		// while node probably exists
		while (ix >= 0) {
			auto& l = nodes[ix];
			if (key == l.key)
				return ix;
			ox = ix;
			if (key < l.key) {
				ix = l.c0;
			} else {
				ix = l.c1;
			}
		}
		return ox;
	}

	inline int16_t skew(node_t& n, int16_t nix)
	{
		auto l = n.left(nodes);
		if (l && l->lvl == n.lvl) {
			if (n.p > -1) {
				n.parent(nodes)->swap(nix, n.c0);
			} else {
				rix = n.c0;
			}
			l->p = n.p;
			n.p  = n.c0;
			n.c0 = l->c1;
			if (l->c1 > -1)
				nodes[l->c1].p = nix;
			l->c1 = nix;
			return n.p;
		}
		return nix;
	}
	inline int16_t split(node_t& n, int16_t nix)
	{
		auto* r = n.right(nodes);
		if (r && r->right(nodes) && n.lvl == r->right(nodes)->lvl) {
			if (n.p > -1) {
				n.parent(nodes)->swap(nix, n.c1);
			} else {
				rix = n.c1;
			}
			// r = n.right(nodes);
			r->p = n.p;
			n.p  = n.c1;
			n.c1 = r->c0;
			if (r->c0 > -1)
				nodes[r->c0].p = nix;
			r->c0 = nix;
			r->lvl++;
			return n.p;
		}
		return nix;
	}

    public:
	K GetRoot() const
	{
		if (rix >= 0)
			return nodes[rix].key;
		return {};
	}
	bool GetParent(K& key) const
	{
		int ix = find(key);
		if (ix < 0)
			return false;
		if (nodes[ix].p >= 0) {
			key = nodes[nodes[ix].p].key;
			return true;
		}
		return false;
	}
	bool GetLeft(K& key) const
	{
		int ix = find(key);
		if (ix < 0)
			return false;
		if (nodes[ix].c0 >= 0) {
			key = nodes[nodes[ix].c0].key;
			return true;
		}
		return false;
	}
	bool GetRight(K& key) const
	{
		int ix = find(key);
		if (ix < 0)
			return false;
		if (nodes[ix].c1 >= 0) {
			key = nodes[nodes[ix].c1].key;
			return true;
		}
		return false;
	}

	V& operator[](int ix) { return values[ix]; }

	void Reset()
	{
		rix = -1;
		nodes.resize(0);
		values.resize(0);
	}

	size_t size() const { return nodes.size(); }

	bool Exists(K key) const { return find(key) >= 0; }

	K  GetKey(int ix) const { return nodes[ix].key; }
	V* GetVal(int ix) const { return values[ix]; }

	V* Get(K key)
	{
		int ix = find(key);
		if (ix < 0)
			return nullptr;
		return &values[ix];
	}
	const V* Get(K key) const
	{
		int ix = find(key);
		if (ix < 0)
			return nullptr;
		return &values[ix];
	}

	int Insert(const K& key, const V& val)
	{

		if (nodes.size() < 1) {
			rix = 0;
			values.push_back(val);
			nodes.push_back(node_t{ -1, -1, -1, 1, key });
			return 0;
		}

		int16_t pix = findIndex(key);
		int16_t ix  = nodes.size();
		values.push_back(val);
		nodes.push_back(node_t{ pix, -1, -1, 1, key });
		node_t* p = &nodes[pix];
		if (key == p->key) {
			values[pix] = val;
			return 0;
		}

		if (key < p->key) {
			p->c0 = ix;
		} else {
			p->c1 = ix;
		}

		// starting with current nodes parent
		// traveser up
		do {
			p = &nodes[pix];
			// attempt skew and split
			pix = skew(*p, pix);
			p   = &nodes[pix];
			pix = split(*p, pix);
			pix = nodes[pix].p;
		} while (pix > -1);

		return 0;
	}

	void Delete(K key)
	{
		const int16_t ix = find(key);
		if (ix < 0)
			return;

		auto*   p   = &nodes[ix];
		auto    pix = p->p;
		int16_t nix = -1;
		if (p->c0 < 0 && p->c1 < 0) {
			// if leaf remove from parent
			if (pix >= 0) {
				nodes[pix].swap(ix, -1);
				p = &nodes[pix];
			} else {
				return;
			}
		} else if (p->c0 < 0) {
			// if no left child
			// swap with leaf found by traversing from right child to left most
			nix = findLeftSubEdge(p->c1);
			nodes[nodes[nix].p].swap(nix, nodes[nix].c1);
			goto replace;
		} else {
			// else
			// swap with leaf found by traversing from left child to right most
			nix = findRightSubEdge(p->c0);
			nodes[nodes[nix].p].swap(nix, nodes[nix].c0);
		replace:
			// child parent link
			if (pix > -1)
				nodes[pix].swap(ix, nix);

			nodes[nix].p   = pix;
			nodes[nix].c0  = p->c0;
			nodes[nix].c1  = p->c1;
			nodes[nix].lvl = p->lvl; // ? ? ? ?
			nodes[nix].swap(nix, -1); // unlink to self just incase
			p = &nodes[nix];
			if (p->c0 > -1)
				nodes[p->c0].p = nix;
			if (p->c1 > -1)
				nodes[p->c1].p = nix;
			pix = nix;
		}

		if (pix < 0 || p == nullptr) {
			rix = -1;
			return;
		}
		if (pix > -1)
			rix = pix;
		while (true) {
			// traverse from node to root
			// decreasing level if appropriate
			// skew level
			// split level

			if ((p->c0 > -1 && nodes[p->c0].lvl < p->lvl - 1)
			    || (p->c1 > -1 && nodes[p->c1].lvl < p->lvl - 1)) {
				p->lvl--;
				if (p->c1 > -1 && nodes[p->c1].lvl < p->lvl)
					nodes[p->c1].lvl = p->lvl;

				pix   = skew(*p, pix);
				p     = &nodes[pix];
				p->c1 = skew(nodes[p->c1], p->c1);
				if (p->c1 > -1 && nodes[p->c1].c1 > -1)
					nodes[p->c1].c1
					    = skew(nodes[nodes[p->c1].c1], nodes[p->c1].c1);

				pix   = split(*p, pix);
				p     = &nodes[pix];
				p->c1 = split(nodes[p->c1], p->c1);
			}
			pix = p->p;
			if (pix < 0)
				break;
			rix = pix;
			p   = &nodes[pix];
		}
		// remove node from array swapping position with last node
		// and patch up any broken links
		if (nodes[ix] != nodes.back()) {
			int16_t oix = (int16_t)nodes.size() - 1;
			nodes[ix]   = nodes.back();
			values[ix]  = values.back();
			auto* n     = &nodes[ix];
			if (n->p >= 0)
				nodes[n->p].swap(oix, ix);
			if (n->c0 >= 0)
				nodes[n->c0].p = ix;
			if (n->c1 >= 0)
				nodes[n->c1].p = ix;
		}
		nodes.pop_back();
		values.pop_back();
	}

	struct treeitr_t {
		tree_t& tree;
		int16_t ix;

		void preOrderIncr()
		{
			if (ix < 0)
				return;
			if (tree.nodes[ix].c0 > -1) {
				ix = tree.nodes[ix].c0;
				return;
			}
			if (tree.nodes[ix].c1 > -1) {
				ix = tree.nodes[ix].c1;
				return;
			}
			int count = 0;
			while (tree.nodes[ix].p > -1) {
				auto* p = tree.nodes[ix].parent(tree.nodes);
				if (ix == p->c0 && p->c1 > -1) {
					ix = p->c1;
					return;
				}
				ix = tree.nodes[ix].p;
				count++;
				if (count >= tree.size())
					break;
			}
			ix = -1;
		}

		treeitr_t operator++()
		{
			preOrderIncr();
			return *this;
		}

		K        Key() const { return tree.nodes[ix].key; }
		V&       operator*() { return tree.values[ix]; }
		const V& operator*() const { return tree.values[ix]; }
		bool     operator==(const treeitr_t& other) const { return ix == other.ix; }
		bool     operator!=(const treeitr_t& other) const { return ix != other.ix; }
	};

	int SerializeReqSize() const
	{
		return nodes.size() * sizeof(node_t) + values.size() * sizeof(V) + sizeof(rix);
	}
	int Serialize(void* dst) const
	{
		size_t ofs  = nodes.size() * sizeof(node_t);
		size_t ofs2 = values.size() * sizeof(V);

		memcpy(dst, nodes.data(), ofs);
		memcpy((char*)dst + ofs, values.data(), ofs2);
		memcpy((char*)dst + ofs + ofs2, &rix, sizeof(rix));
		return ofs + ofs2 + sizeof(rix);
	}

	int Deserialize(void* src, size_t n)
	{
		int    len  = (n - sizeof(rix)) / (sizeof(node_t) + sizeof(V));
		size_t ofs0 = len * sizeof(node_t);
		size_t ofs1 = len * sizeof(V);

		nodes.resize(len);
		values.resize(len);
		memcpy(nodes.data(), src, ofs0);
		memcpy(values.data(), (char*)src + ofs0, ofs1);
		memcpy(&rix, (char*)src + ofs0 + ofs1, sizeof(rix));

		return 1;
	}

	bool Validate()
	{
		// iterate over all keys bound checking
		// then do preoder traversal and post order traversal?
		if (rix < 0 | rix >= nodes.size())
			return false;
		int s = nodes.size();
		for (const auto& node : nodes) {
			bool inRange = nodes.p >= 0 & nodes.c0 >= 0 & nodes.c1 >= 0 & nodes.p < s
			    & nodes.c0 < s & nodes.c1 < s;
			if (!inRange)
				return false;
		}
		int       count = 0;
		treeitr_t preOrder{ this, rix };
		while (preOrder.preOrderIncr() && preOrder.ix >= 0) {
			count++;
			if (count > nodes.size())
				break;
		}

		if (count != nodes.size())
			return false;

		return true;
	}

	// typedef T* iterator;
	// typedef const T* const_iterator;
	// iterators for range based for loop
	// const_iterator begin() const { return &m_data[0];}
	// const_iterator end() const { return &m_data[m_size];}

	struct simpleiter_t {
		const node_t* m_nodes;
		V*            m_values;
		int16_t       ix;
		simpleiter_t  operator++(int)
		{
			ix++;
			return *this;
		}
		simpleiter_t operator++()
		{
			ix++;
			return *this;
		}
		K    Key() const { return m_nodes[ix].key; }
		V*   operator->() const { return &m_values[ix]; }
		V&   operator*() const { return m_values[ix]; }
		bool operator==(const simpleiter_t& other) const { return ix == other.ix; }
		bool operator!=(const simpleiter_t& other) const { return ix != other.ix; }
	};
	simpleiter_t begin() { return simpleiter_t{ nodes.data(), values.data(), 0 }; }
	simpleiter_t end()
	{
		return simpleiter_t{ nodes.data(), values.data(), (int16_t)nodes.size() };
	}
	struct const_iter_t {
		const tree_t& tree;
		int16_t       ix;

		const_iter_t& operator++(int)
		{
			ix++;
			return *this;
		}
		const_iter_t& operator++()
		{
			ix++;
			return *this;
		}
		const K  Key() const { return tree.nodes[ix].key; }
		const V& operator*() const { return tree.values[ix]; }
		const V* operator->() const { return &tree.values[ix]; }
		bool     operator==(const const_iter_t& other) const { return ix == other.ix; }
		bool     operator!=(const const_iter_t& other) const { return ix != other.ix; }
	};
	const_iter_t begin() const { return const_iter_t{ *this, 0 }; }
	const_iter_t end() const { return const_iter_t{ *this, (int16_t)nodes.size() }; }

	class debugCallback {
	    public:
		virtual void callback(K key, int x, int y, bool c0, bool c1, int lvl) = 0;
	};

	void _layout(int16_t ix, int16_t depth, int& i, debugCallback& cb)
	{
		if (nodes[ix].c0 > -1)
			_layout(nodes[ix].c0, depth + 1, i, cb);
		cb.callback(nodes[ix].key, i, depth, nodes[ix].c0 > -1, nodes[ix].c1 > -1,
		            nodes[ix].lvl);
		i += 1;
		if (nodes[ix].c1 > -1)
			_layout(nodes[ix].c1, depth + 1, i, cb);
	};
	void KnuthDebug(debugCallback& cb)
	{
		if (nodes.size() < 1)
			return;
		int i = 0;
		_layout(rix, 0, i, cb);
	}

	/*
	void ExecDebugVisitor(debugCallback &cb) {
	        // TODO
	        // visit every node in tree breadth first to see its shape
	        struct pair_t {
	                int16_t ix;
	                int16_t depth;
	        };
	        thread_local static std::deque<int16_t> order;
	        if(nodes.size() < 1)
	                return;
	        order.resize(0);
	        order.push_back(rix);
	        while(order.size() > 0) {
	                auto p = order.pop_front();
	                const auto& n = nodes[p.ix];

	                //cb.callback(n.key, ix, n.p, n.lvl);
	                // push back its children if they exist
	                // order.push_back(c0, c1)
	                if(n.c0 > -1)
	                        order.push_back(pair_t{n.c0, p.depth+1});
	                if(n.c1 > -1)
	                        order.push_back(pair_t{n.c1, p.depth+1});

	        }

	}*/
};
