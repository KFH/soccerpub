#pragma once
static tileDesc_t const tiles_xpm_tiles[]
    = { { 0, 0, 16, 16, 0, 0 },  { 16, 0, 24, 8, 0, 0 },  { 0, 64, 96, 80, 0, 0 },
	{ 0, 16, 96, 64, 0, 0 }, { 40, 0, 56, 16, 0, 0 }, { 56, 0, 72, 16, 0, 0 },
	{ 16, 8, 24, 16, 0, 0 }, { 0, 80, 64, 144, 0, 0 } };
enum tiles_xpm_names {
	brick_pink  = 0,
	brick_noise = 1,
	wall_bottom = 2,
	wall_top    = 3,
	wall_left   = 4,
	wall_right  = 5,
	brick_green = 6,
	grass       = 7,
};
