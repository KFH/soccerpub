#pragma once
#include <cstdint>
#include "fpm.hpp"
#include "keys.hpp"
#include "idtags.hpp"

namespace sim {

struct ctrlAxis {
	// mapping of single axis of control
	int    itype;
	int    atype;
	int    pos;
	int    neg;
	double keyinc;
	double value;
	bool   actv;
	bool   pos_key_prev;

	enum {
		// itype enums
		NONE    = 0,
		KB      = 1,
		JS      = 2,
		JS_BTN  = 4,
		DISABLE = 0x08,
		// atype enums
		AXIS    = 0,
		PRESS   = 1,
		DOWN    = 2,
		RELEASE = 3,
	};

	void DrifCenter()
	{
		if (actv)
			return;

		double cval = value;
		double m    = keyinc * signof(value);
		value       = (std::abs(cval) <= std::abs(keyinc)) ? 0.f : cval - m;
	}
	void ApplyKeys(const bool keys[512])
	{
		actv = false;
		if (itype == KB) {

			switch (atype) {
			case AXIS:
				if (pos >= 0 && keys[pos]) {
					actv  = true;
					value = std::min(1.0, value + keyinc);
				} else if (neg >= 0 && keys[neg]) {
					actv  = true;
					value = std::max(-1.0, value - keyinc);
				}
				break;
			case DOWN:
				value = 0.0;
				if (pos >= 0 && keys[pos] && pos_key_prev == true)
					value = 1.0;
				break;
			case PRESS:
				value = 0.0;
				if (pos >= 0 && keys[pos] && pos_key_prev == false)
					value = 1.0;
				break;
			case RELEASE:
				value = 0.0;
				if (pos >= 0 && !keys[pos] && pos_key_prev == true)
					value = 1.0;
			}
			if (pos >= 0)
				pos_key_prev = keys[pos];
		}
	}
};
} // namespace sim
