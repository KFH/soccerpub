// TODO
// throw random utility functions here for rendering only

inline void rtDrawRect(renderContext& ctx, const sim::view_t& view, const rect_t& rec,
                       uint8_t color)
{
	int px0 = view.screenX(rec.x0), py0 = view.screenY(rec.y0);
	int px1 = view.screenX(rec.x1), py1 = view.screenY(rec.y1);
	ctx.drawLine(px0, py0, px0, py1, color);
	ctx.drawLine(px0, py0, px1, py0, color);
	ctx.drawLine(px1, py1, px1, py0, color);
	ctx.drawLine(px0, py1, px1, py1, color);
};
inline void rtDrawRectX(renderContext& ctx, const sim::view_t& view, const rect_t& rec,
                        uint8_t color)
{
	int px0 = view.screenX(rec.x0), py0 = view.screenY(rec.y0);
	int px1 = view.screenX(rec.x1), py1 = view.screenY(rec.y1);
	ctx.drawLine(px0, py0, px1, py1, color);
	ctx.drawLine(px0, py1, px1, py0, color);
};
inline rect_t GetViewRect(sim::view_t& view)
{
	int vx     = roundf(view.x);
	int vy     = roundf(view.y);
	int hvx    = view.w / 2;
	int hvy    = view.h / 2;
	int margin = 64;
	return rect_t{ vx - hvx - margin, vy - hvy - margin, vx + hvx + margin, vy + hvy + margin };
};
