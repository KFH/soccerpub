#pragma once

// platform detection

#define PLATFORM_WINDOWS 1
#define PLATFORM_MAC 2
#define PLATFORM_UNIX 3

#if defined(_WIN32)
#define PLATFORM PLATFORM_WINDOWS
#elif defined(__APPLE__)
#define PLATFORM PLATFORM_MAC
#else
#define PLATFORM PLATFORM_UNIX
#endif

#if PLATFORM == PLATFORM_WINDOWS
#define NOMINMAX
#include <winsock2.h>
#include <ws2tcpip.h>
//#include <ws2ipdef.h>
#pragma comment(lib, "wsock32.lib")

#elif PLATFORM == PLATFORM_MAC || PLATFORM == PLATFORM_UNIX
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

// types.h, netdb.h, inet.h arent needed for using sockets but for resolving names etc
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>

#else
#error unknown platform!
#endif

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <string>

namespace net {

#if PLATFORM == PLATFORM_WINDOWS
typedef int socklen_t;
#endif

#if PLATFORM == PLATFORM_WINDOWS
inline void nwait(float seconds) { Sleep((int)(seconds * 1000.0f)); };
#else
inline void nwait(float seconds) { usleep((int)(seconds * 1000000.0f)); };
#endif

inline bool sockInit(void)
{
#if PLATFORM == PLATFORM_WINDOWS
	WSADATA wsa_data;
	return WSAStartup(MAKEWORD(2, 2), &wsa_data) != NO_ERROR;
#else
	return true;
#endif
};

inline int sockQuit(void)
{
#if PLATFORM == PLATFORM_WINDOWS
	return WSACleanup();
#else
	return 0;
#endif
};

// hold both ipv6 & ipv4
/*struct ip_address {
        uint32_t sect[4];
};*/
union ip_address {
	uint8_t  chnks[16];
	uint16_t v6chnks[8];
	uint32_t v4[4];
	uint64_t v6[2];
};
// TODO
// Store ip and port in big endian/network byte order
// make funcs to convert back to little endian / human readable representations
class Address {
    public:
	ip_address ip;
	uint16_t   port = 0;

	Address()
	{
		ip.v6[0] = 0;
		ip.v6[1] = 0;
	};

	Address(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint16_t prt)
	{
		ip.chnks[0] = a;
		ip.chnks[1] = b;
		ip.chnks[2] = c;
		ip.chnks[3] = d;
		port        = htons(prt);
	};

	Address(uint32_t ipv4, uint16_t prt, bool nb = false)
	{
		if (nb) {
			SetNIPv4(ipv4, prt);
		} else {
			SetIPv4(ipv4, prt);
		}
	}
	Address(uint16_t ipv6[8], uint16_t prt, bool nb = false)
	{
		if (nb) {

		} else {
			SetIPv6(ipv6, prt);
		}
	}
	std::string ToStrinIPv4() const
	{
		char buff[32];
		sprintf(buff, "%u.%u.%u.%u:%hu", ip.chnks[0], ip.chnks[1], ip.chnks[2], ip.chnks[3],
		        port);
		return buff;
	}

	// net network order ipv4
	void SetNIPv4(uint32_t ipv4, uint16_t prt)
	{
		ip.v4[0] = ipv4;
		port     = prt;
	}

	// converts to network byte order
	void SetIPv4(uint32_t ipv4, uint16_t prt)
	{
		ip.v4[0] = htonl(ipv4);
		port     = htons(prt);
	}

	void SetIPv6(uint16_t ipv6[8], uint16_t prt)
	{
		for (int i = 7; i >= 0; i--) {
			ip.v6chnks[i] = htons(ipv6[i]);
		}
		port = htons(prt);
	}

	void SetNIPv6(uint16_t ipv6[8], uint16_t prt)
	{
		// already in network order
	}
	int SetStrIPv4(const char* a)
	{
		int rc = sscanf(a, "%hhu.%hhu.%hhu.%hhu:%hu", &ip.chnks[0], &ip.chnks[1],
		                &ip.chnks[2], &ip.chnks[3], &port);
		port   = htons(port);
		return rc;
	}
	void WriteV4(char* dst)
	{
		sprintf(dst, "%03hhu.%03hhu.%03hhu.%03hhu:%hu", ip.chnks[0], ip.chnks[1],
		        ip.chnks[2], ip.chnks[3], ntohs(port));
	}
	void PrintV4()
	{
		printf("IPv4 = %03hhu.%03hhu.%03hhu.%03hhu:%hu\n", ip.chnks[0], ip.chnks[1],
		       ip.chnks[2], ip.chnks[3], ntohs(port));
	}
	void PrintV6()
	{
		printf("IPv6 = [%hx:%hx:%hx:%hx:%hx:%hx:%hx:%hx]:%hu\n", ntohs(ip.v6chnks[0]),
		       ntohs(ip.v6chnks[1]), ntohs(ip.v6chnks[2]), ntohs(ip.v6chnks[3]),
		       ntohs(ip.v6chnks[4]), ntohs(ip.v6chnks[5]), ntohs(ip.v6chnks[6]),
		       ntohs(ip.v6chnks[7]), ntohs(port));
	}

	/*void SetIPv6Addr(in6_addr &ipv6addr) {
	        // reversing byte order
	        int h = 0;
	        for(int i = 15;i >= 0; i++) {
	                ip.chnks[i] = ipv6addr.s6_addr[h];
	                h++;
	        }
	}*/

	// overload equality for easier comparisons
	bool operator==(const Address& other) const
	{
		return ip.v6[0] == other.ip.v6[0] & ip.v6[1] == other.ip.v6[1] & port == other.port;
	}
};

class Socket {
    protected:
#if PLATFORM == PLATFORM_WINDOWS
	SOCKET sockt;
#else
	int sockt;
#endif
	unsigned short mport;
	int            fam; // AF_INET or AF_INET6 == ipv4 or ipv6

    public:
	enum Options { NonBlocking = 1, Broadcast = 2 };

	Socket() { sockt = 0; }
	~Socket() { Close(); }
	unsigned short GetPort() { return mport; }
	bool           IsOpen() const { return sockt != 0; }

	// open a port for listening
	// proto
	// AF_INET
	// AF_INET6
	bool Open(int proto, unsigned short port, int options = NonBlocking)
	{

		fam = proto;

		// create socket with ipv4-6 proto, datagram socket, using ip proto udp ???
		sockt = ::socket(fam, SOCK_DGRAM, IPPROTO_UDP);
		// if failed
		if (!IsOpen())
			return false;

		mport = htons((unsigned short)port);

		printf("Socket Open %hu\n", port);

		sockaddr_storage address;
		address.ss_family = fam;
		sockaddr_in6* a6;
		sockaddr_in*  a4;

		switch (fam) {
		case AF_INET6:
			a6            = (sockaddr_in6*)&address;
			a6->sin6_port = htons((unsigned short)port);
			a6->sin6_addr = in6addr_any;
			break;
		case AF_INET:
			a4                  = (sockaddr_in*)&address;
			a4->sin_port        = htons((unsigned short)port);
			a4->sin_addr.s_addr = INADDR_ANY;
		}

		if (bind(sockt, (const sockaddr*)&address, sizeof(sockaddr_storage)) < 0) {
			printf("failed to bind socket\n");
			Close();
			return false;
		}

		if (options & NonBlocking) {
#if PLATFORM == PLATFORM_WINDOWS
			DWORD nonBlocking = 1;
			if (ioctlsocket(sockt, FIONBIO, &nonBlocking) != 0) {
				printf("failed to set non-blocking socket\n");
				Close();
				return false;
			}
#else
			int nonBlocking = 1;
			if (fcntl(sockt, F_SETFL, O_NONBLOCK, nonBlocking) == -1) {
				printf("failed to set non-blocking socket\n");
				Close();
				return false;
			}
#endif
		}

		if (options & Broadcast) {
			int enable = 1;
			if (setsockopt(sockt, SOL_SOCKET, SO_BROADCAST, (const char*)&enable,
			               sizeof(enable))
			    < 0) {
				printf("failed to set socket to broadcast\n");
				Close();
				return false;
			}
		}
		return true;
	}
	// close open port
	void Close()
	{
		if (!IsOpen())
			return;

// only if OPEN
#if PLATFORM == PLATFORM_WINDOWS
		closesocket(sockt);
#else
		close(sockt);
#endif
		sockt = 0;
	}
	// send binary data
	bool Send(Address& dst, void* data, int data_s)
	{
		if (!IsOpen())
			return false;

		sockaddr_storage dstaddr;
		socklen_t        dstaddr_s;
		sockaddr_in6*    a6;
		sockaddr_in*     a4;

		switch (fam) {
		case AF_INET6:
			a6              = (sockaddr_in6*)&dstaddr;
			a6->sin6_family = AF_INET6;
			memcpy(a6->sin6_addr.s6_addr, dst.ip.chnks, sizeof(ip_address));
			a6->sin6_port = dst.port;
			dstaddr_s     = sizeof(sockaddr_in6);
			break;
		case AF_INET:
			a4                  = (sockaddr_in*)&dstaddr;
			a4->sin_family      = AF_INET;
			a4->sin_addr.s_addr = dst.ip.v4[0]; // htons(dst.ip);
			a4->sin_port        = dst.port; // htonl(dst.port);
			dstaddr_s           = sizeof(sockaddr_in);
		};

		int sent_bytes
		    = sendto(sockt, (const char*)data, data_s, 0, (sockaddr*)&dstaddr, dstaddr_s);

		return sent_bytes == data_s;
	}
	// receive binary data
	// pre allocate data to max packet size
	int Receive(Address& src, void* data, int data_s)
	{

		if (!IsOpen())
			return false; // !? what the

		sockaddr_storage from;
		socklen_t        fromLength = sizeof(from);
		sockaddr_in6*    a6;
		sockaddr_in*     a4;

		int received_bytes
		    = recvfrom(sockt, (char*)data, data_s, 0, (sockaddr*)&from, &fromLength);
		if (received_bytes <= 0)
			return 0;

		// maybe check fam too?
		switch (from.ss_family) {
		case AF_INET6:
			a6 = (sockaddr_in6*)&from;
			src.SetNIPv6((uint16_t*)a6->sin6_addr.s6_addr, a6->sin6_port);
			break;
		case AF_INET:
			a4 = (sockaddr_in*)&from;
			src.SetNIPv4(a4->sin_addr.s_addr, a4->sin_port);
		}
		return received_bytes;
	}
};
}; // namespace net
