#pragma once
#define FIXARRLEN(x) sizeof(x) / sizeof(x[0])
template <typename T> class arr_view {
	size_t   _size;
	const T* data;

    public:
	arr_view(const T* p, size_t len)
	    : data(p)
	    , _size(len)
	{
	}

	size_t size() const { return _size; }

	constexpr const T& operator[](size_t i) const { return data[i]; }
};
