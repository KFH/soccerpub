#pragma once
#include "game.hpp"

namespace sim {

template <class input_t> class TeeStream : public sim::InputStream<input_t> {
    public:
	InputStream<input_t>*                in;
	std::vector<InputRecorder<input_t>*> rec;

	TeeStream(){};
	~TeeStream(){};

	virtual int Get(input_t& out) override
	{
		in->Get(out);
		for (auto r : rec) {
			r->push_back(out);
		}
		return in->Remaining();
	}
	virtual int GetConst(input_t& out) const override { return in->GetConst(out); };
	virtual int Remaining() override { return in->Remaining(); }
};

// records sequence of inputs from a another InputStream
// and passes through controls when recording end is reached
// TODO
// switch std::vector inputs to RingBuffer ? ? ?
// or implement another class that behaves similarly
template <class input_t> class LocalRecordedStream : public sim::InputStream<input_t> {

	std::vector<input_t> inputs;
	size_t               m_off;

    public:
	InputStream<input_t>* target;

	LocalRecordedStream(InputStream<input_t>* t)
	{
		target = t;
		if (target)
			this->id = target->ID();
		m_off = 0;
	};

	virtual int Get(input_t& out) override
	{

		if (m_off < inputs.size())
			return read(out);

		if (target) {
			int i = target->Get(out);
			push_back(out);
			m_off = inputs.size();
			return i;
		}
		return read(out);
	};
	virtual int GetConst(input_t& out) const override { return target->GetConst(out); };

	virtual int Remaining() override
	{
		if (m_off < inputs.size())
			return unread();
		if (target) {
			return target->Remaining();
		}
		return unread();
	};

	virtual void push_back(const input_t& in)
	{
		inputs.push_back(in);
		//	return inputs.size();
	}
	void pop_back(int n) { inputs.resize(std::max(0, int(inputs.size()) - n)); }

	void reset() { m_off = 0; }
	void free()
	{
		m_off = 0;
		inputs.resize(0);
	}
	int read(input_t& out)
	{
		if (m_off < inputs.size()) {
			out = inputs[m_off];
			m_off++;
			return inputs.size() - m_off;
		} else {
			out = input_t{};
			return -1;
		}
	}
	int unread() const { return (int)inputs.size() - (int)m_off; }
	int len() const { return inputs.size(); };

	int Decompress(ByteBuffer& buff) { return 0; }
	int Compress(ByteBuffer& buff) { return 0; }
};

} // namespace sim
