#include "keys.hpp"
#include <X11/XKBlib.h>
short int keycodes[512];

// Mouse
MouseButtons_t mouse_button_maps[16]
    = { MouseButtons_t::NONE,

	MouseButtons_t::LEFT,     MouseButtons_t::MIDDLE,     MouseButtons_t::RIGHT,

	MouseButtons_t::SCROLLUP, MouseButtons_t::SCROLLDOWN,

	MouseButtons_t::NONE, // ? ? ?
	MouseButtons_t::NONE, // ? ? ?

	MouseButtons_t::BACKWARD, MouseButtons_t::FORWARD };

// Translate an X11 key code to a GLFW key code.
// Taken from GLFW
//
static int translateKeyCode(int scancode, Display* xdisplay)
{
	int keySym;

	// Valid key code range is  [8,255], according to the Xlib manual
	if (scancode < 8 || scancode > 255)
		return KEY_UNKNOWN;

	// Try secondary keysym, for numeric keypad keys
	// Note: This way we always force "NumLock = ON", which is intentional
	// since the returned key code should correspond to a physical
	// location.
	keySym = XkbKeycodeToKeysym(xdisplay, scancode, 0, 1);
	switch (keySym) {
	case XK_KP_0:
		return KEY_KP_0;
	case XK_KP_1:
		return KEY_KP_1;
	case XK_KP_2:
		return KEY_KP_2;
	case XK_KP_3:
		return KEY_KP_3;
	case XK_KP_4:
		return KEY_KP_4;
	case XK_KP_5:
		return KEY_KP_5;
	case XK_KP_6:
		return KEY_KP_6;
	case XK_KP_7:
		return KEY_KP_7;
	case XK_KP_8:
		return KEY_KP_8;
	case XK_KP_9:
		return KEY_KP_9;
	case XK_KP_Separator:
	case XK_KP_Decimal:
		return KEY_KP_DECIMAL;
	case XK_KP_Equal:
		return KEY_KP_EQUAL;
	case XK_KP_Enter:
		return KEY_KP_ENTER;
	default:
		break;
	}

	// Now try primary keysym for function keys (non-printable keys)
	// These should not depend on the current keyboard layout
	keySym = XkbKeycodeToKeysym(xdisplay, scancode, 0, 0);

	int     dummy;
	KeySym* keySyms;

	keySyms = XGetKeyboardMapping(xdisplay, scancode, 1, &dummy);
	keySym  = keySyms[0];
	XFree(keySyms);

	switch (keySym) {
	case XK_Escape:
		return KEY_ESCAPE;
	case XK_Tab:
		return KEY_TAB;
	case XK_Shift_L:
		return KEY_LEFT_SHIFT;
	case XK_Shift_R:
		return KEY_RIGHT_SHIFT;
	case XK_Control_L:
		return KEY_LEFT_CONTROL;
	case XK_Control_R:
		return KEY_RIGHT_CONTROL;
	case XK_Meta_L:
	case XK_Alt_L:
		return KEY_LEFT_ALT;
	case XK_Mode_switch: // Mapped to Alt_R on many keyboards
	case XK_ISO_Level3_Shift: // AltGr on at least some machines
	case XK_Meta_R:
	case XK_Alt_R:
		return KEY_RIGHT_ALT;
	case XK_Super_L:
		return KEY_LEFT_SUPER;
	case XK_Super_R:
		return KEY_RIGHT_SUPER;
	case XK_Menu:
		return KEY_MENU;
	case XK_Num_Lock:
		return KEY_NUM_LOCK;
	case XK_Caps_Lock:
		return KEY_CAPS_LOCK;
	case XK_Print:
		return KEY_PRINT_SCREEN;
	case XK_Scroll_Lock:
		return KEY_SCROLL_LOCK;
	case XK_Pause:
		return KEY_PAUSE;
	case XK_Delete:
		return KEY_DELETE;
	case XK_BackSpace:
		return KEY_BACKSPACE;
	case XK_Return:
		return KEY_ENTER;
	case XK_Home:
		return KEY_HOME;
	case XK_End:
		return KEY_END;
	case XK_Page_Up:
		return KEY_PAGE_UP;
	case XK_Page_Down:
		return KEY_PAGE_DOWN;
	case XK_Insert:
		return KEY_INSERT;
	case XK_Left:
		return KEY_LEFT;
	case XK_Right:
		return KEY_RIGHT;
	case XK_Down:
		return KEY_DOWN;
	case XK_Up:
		return KEY_UP;
	case XK_F1:
		return KEY_F1;
	case XK_F2:
		return KEY_F2;
	case XK_F3:
		return KEY_F3;
	case XK_F4:
		return KEY_F4;
	case XK_F5:
		return KEY_F5;
	case XK_F6:
		return KEY_F6;
	case XK_F7:
		return KEY_F7;
	case XK_F8:
		return KEY_F8;
	case XK_F9:
		return KEY_F9;
	case XK_F10:
		return KEY_F10;
	case XK_F11:
		return KEY_F11;
	case XK_F12:
		return KEY_F12;
	case XK_F13:
		return KEY_F13;
	case XK_F14:
		return KEY_F14;
	case XK_F15:
		return KEY_F15;
	case XK_F16:
		return KEY_F16;
	case XK_F17:
		return KEY_F17;
	case XK_F18:
		return KEY_F18;
	case XK_F19:
		return KEY_F19;
	case XK_F20:
		return KEY_F20;
	case XK_F21:
		return KEY_F21;
	case XK_F22:
		return KEY_F22;
	case XK_F23:
		return KEY_F23;
	case XK_F24:
		return KEY_F24;
	case XK_F25:
		return KEY_F25;

	// Numeric keypad
	case XK_KP_Divide:
		return KEY_KP_DIVIDE;
	case XK_KP_Multiply:
		return KEY_KP_MULTIPLY;
	case XK_KP_Subtract:
		return KEY_KP_SUBTRACT;
	case XK_KP_Add:
		return KEY_KP_ADD;

	// These should have been detected in secondary keysym test above!
	case XK_KP_Insert:
		return KEY_KP_0;
	case XK_KP_End:
		return KEY_KP_1;
	case XK_KP_Down:
		return KEY_KP_2;
	case XK_KP_Page_Down:
		return KEY_KP_3;
	case XK_KP_Left:
		return KEY_KP_4;
	case XK_KP_Right:
		return KEY_KP_6;
	case XK_KP_Home:
		return KEY_KP_7;
	case XK_KP_Up:
		return KEY_KP_8;
	case XK_KP_Page_Up:
		return KEY_KP_9;
	case XK_KP_Delete:
		return KEY_KP_DECIMAL;
	case XK_KP_Equal:
		return KEY_KP_EQUAL;
	case XK_KP_Enter:
		return KEY_KP_ENTER;

	// Last resort: Check for printable keys (should not happen if the XKB
	// extension is available). This will give a layout dependent mapping
	// (which is wrong, and we may miss some keys, especially on non-US
	// keyboards), but it's better than nothing...
	case XK_a:
		return KEY_A;
	case XK_b:
		return KEY_B;
	case XK_c:
		return KEY_C;
	case XK_d:
		return KEY_D;
	case XK_e:
		return KEY_E;
	case XK_f:
		return KEY_F;
	case XK_g:
		return KEY_G;
	case XK_h:
		return KEY_H;
	case XK_i:
		return KEY_I;
	case XK_j:
		return KEY_J;
	case XK_k:
		return KEY_K;
	case XK_l:
		return KEY_L;
	case XK_m:
		return KEY_M;
	case XK_n:
		return KEY_N;
	case XK_o:
		return KEY_O;
	case XK_p:
		return KEY_P;
	case XK_q:
		return KEY_Q;
	case XK_r:
		return KEY_R;
	case XK_s:
		return KEY_S;
	case XK_t:
		return KEY_T;
	case XK_u:
		return KEY_U;
	case XK_v:
		return KEY_V;
	case XK_w:
		return KEY_W;
	case XK_x:
		return KEY_X;
	case XK_y:
		return KEY_Y;
	case XK_z:
		return KEY_Z;
	case XK_1:
		return KEY_1;
	case XK_2:
		return KEY_2;
	case XK_3:
		return KEY_3;
	case XK_4:
		return KEY_4;
	case XK_5:
		return KEY_5;
	case XK_6:
		return KEY_6;
	case XK_7:
		return KEY_7;
	case XK_8:
		return KEY_8;
	case XK_9:
		return KEY_9;
	case XK_0:
		return KEY_0;
	case XK_space:
		return KEY_SPACE;
	case XK_minus:
		return KEY_MINUS;
	case XK_equal:
		return KEY_EQUAL;
	case XK_bracketleft:
		return KEY_LEFT_BRACKET;
	case XK_bracketright:
		return KEY_RIGHT_BRACKET;
	case XK_backslash:
		return KEY_BACKSLASH;
	case XK_semicolon:
		return KEY_SEMICOLON;
	case XK_apostrophe:
		return KEY_APOSTROPHE;
	case XK_grave:
		return KEY_GRAVE_ACCENT;
	case XK_comma:
		return KEY_COMMA;
	case XK_period:
		return KEY_PERIOD;
	case XK_slash:
		return KEY_SLASH;
	case XK_less:
		return KEY_WORLD_1; // At least in some layouts...
	default:
		break;
	}

	// No matching translation was found
	return KEY_UNKNOWN;
}

inline void initKeycodeTable(Display* xdisplay)
{
	memset(keycodes, 0, sizeof(keycodes));

	// Use XKB to determine physical key locations independently of the current
	// keyboard layout
	int        scancode, key;
	char       name[XkbKeyNameLength + 1];
	XkbDescPtr desc = XkbGetMap(xdisplay, 0, XkbUseCoreKbd);
	XkbGetNames(xdisplay, XkbKeyNamesMask, desc);

	// Find the X11 key code -> GLFW key code mapping
	for (scancode = desc->min_key_code; scancode <= desc->max_key_code; scancode++) {
		memcpy(name, desc->names->keys[scancode].name, XkbKeyNameLength);
		name[XkbKeyNameLength] = '\0';

		// Map the key name to a GLFW key code. Note: We only map printable
		// keys here, and we use the US keyboard layout. The rest of the
		// keys (function keys) are mapped using traditional KeySym
		// translations.
		// clang-format off
		if (strcmp(name, "TLDE") == 0) key = KEY_GRAVE_ACCENT;
		else if (strcmp(name, "AE01") == 0) key = KEY_1;
		else if (strcmp(name, "AE02") == 0) key = KEY_2;
		else if (strcmp(name, "AE03") == 0) key = KEY_3;
		else if (strcmp(name, "AE04") == 0) key = KEY_4;
		else if (strcmp(name, "AE05") == 0) key = KEY_5;
		else if (strcmp(name, "AE06") == 0) key = KEY_6;
		else if (strcmp(name, "AE07") == 0) key = KEY_7;
		else if (strcmp(name, "AE08") == 0) key = KEY_8;
		else if (strcmp(name, "AE09") == 0) key = KEY_9;
		else if (strcmp(name, "AE10") == 0) key = KEY_0;
		else if (strcmp(name, "AE11") == 0) key = KEY_MINUS;
		else if (strcmp(name, "AE12") == 0) key = KEY_EQUAL;
		else if (strcmp(name, "AD01") == 0) key = KEY_Q;
		else if (strcmp(name, "AD02") == 0) key = KEY_W;
		else if (strcmp(name, "AD03") == 0) key = KEY_E;
		else if (strcmp(name, "AD04") == 0) key = KEY_R;
		else if (strcmp(name, "AD05") == 0) key = KEY_T;
		else if (strcmp(name, "AD06") == 0) key = KEY_Y;
		else if (strcmp(name, "AD07") == 0) key = KEY_U;
		else if (strcmp(name, "AD08") == 0) key = KEY_I;
		else if (strcmp(name, "AD09") == 0) key = KEY_O;
		else if (strcmp(name, "AD10") == 0) key = KEY_P;
		else if (strcmp(name, "AD11") == 0) key = KEY_LEFT_BRACKET;
		else if (strcmp(name, "AD12") == 0) key = KEY_RIGHT_BRACKET;
		else if (strcmp(name, "AC01") == 0) key = KEY_A;
		else if (strcmp(name, "AC02") == 0) key = KEY_S;
		else if (strcmp(name, "AC03") == 0) key = KEY_D;
		else if (strcmp(name, "AC04") == 0) key = KEY_F;
		else if (strcmp(name, "AC05") == 0) key = KEY_G;
		else if (strcmp(name, "AC06") == 0) key = KEY_H;
		else if (strcmp(name, "AC07") == 0) key = KEY_J;
		else if (strcmp(name, "AC08") == 0) key = KEY_K;
		else if (strcmp(name, "AC09") == 0) key = KEY_L;
		else if (strcmp(name, "AC10") == 0) key = KEY_SEMICOLON;
		else if (strcmp(name, "AC11") == 0) key = KEY_APOSTROPHE;
		else if (strcmp(name, "AB01") == 0) key = KEY_Z;
		else if (strcmp(name, "AB02") == 0) key = KEY_X;
		else if (strcmp(name, "AB03") == 0) key = KEY_C;
		else if (strcmp(name, "AB04") == 0) key = KEY_V;
		else if (strcmp(name, "AB05") == 0) key = KEY_B;
		else if (strcmp(name, "AB06") == 0) key = KEY_N;
		else if (strcmp(name, "AB07") == 0) key = KEY_M;
		else if (strcmp(name, "AB08") == 0) key = KEY_COMMA;
		else if (strcmp(name, "AB09") == 0) key = KEY_PERIOD;
		else if (strcmp(name, "AB10") == 0) key = KEY_SLASH;
		else if (strcmp(name, "BKSL") == 0) key = KEY_BACKSLASH;
		else if (strcmp(name, "LSGT") == 0) key = KEY_WORLD_1;
		else key = KEY_UNKNOWN;
		// clang-format on
		if ((scancode >= 0) && (scancode < 256))
			keycodes[scancode] = key;
	}

	XkbFreeNames(desc, XkbKeyNamesMask, True);
	XkbFreeKeyboard(desc, 0, True);

	for (scancode = 0; scancode < 256; scancode++) {
		// Translate the un-translated key codes using traditional X11 KeySym
		// lookups
		if (keycodes[scancode] < 0)
			keycodes[scancode] = translateKeyCode(scancode, xdisplay);
	}
}
