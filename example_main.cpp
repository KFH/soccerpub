
#include <algorithm>

#include <cmath>
#include <mutex>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "gamecontrols.hpp"
#include "game.hpp"
#include "gamerend.hpp"
#include "render.hpp"

#include "gfx.hpp"
#include "keys.hpp"
#include "netapi.hpp"

#include "localcontrols.hpp"
#include "snap.hpp"
#include "system.hpp"
#include "threadwork.hpp"
#include "menu.hpp"
#include "interpolate.hpp"

// sprite sheets
extern "C" {
#include "font4x6.xpm"
}

// TODO
// write own game instance and DrawGame func
class empty_instance : public sim::vInstance {
    public:
	virtual int  Init() override { return 0; };
	virtual void Step(int frameNum, std::vector<sim::ctrlOverride>& in) override{};
	virtual void Serialize(ByteBuffer& buff) const override{};
	virtual void DeSerialize(ByteBuffer& buff) override{};
};

void DrawGame(renderContext& ctx, sim::view_t& mainView, sim::Game& mainGame, Messanger& msger,
              sim::playerIX LocalPlayerIX)
{
	// TODO  Draw game

	ctx.blank(0);
}

bool displayDebug = true;
int  needExit     = 0;
int  wantQuit() { return needExit | gfx::NeedExit(); }

tw::threadPool_t threadPool;
std::mutex       gs_lock;
empty_instance   mainInst;
sim::Game        mainGame(&mainInst);
sim::GameSystem  gs(mainGame);
SnapRecorder*    snapRec = &gs.snapRec;
m::menuManager   mainMenuManager;

ByteBuffer snapshotBuff[2];
uint64_t   frame;

PlayerControllerStream P1Menu(0);
PlayerControllerStream P1Ctrl(0);
LocalRecordedStream    P1Rec(&P1Ctrl);
sim::playerIX          LocalPlayerIX = 0;
sim::playerID          LocalPlayerID = 0;
net::CTX_h&            netctx        = gs.nctx;

char     hostaddr[24]    = { "127.0.0.1:1234_" };
uint16_t listenPort      = 1234;
int      debugPacketLoss = 0;

sim::view_t mainView{ 0, 0, 640, 480 };

v3   debugViewPos = { 0, 0, 0 };
bool debugView    = false;

extern uint64_t defaultMap asm("defaultMap");
extern int      defaultMap_size asm("defaultMap_size");

float SimSpeedMult    = 1.f;
float SimSpeedMultPos = 1.f;

bool doDisplayMenu() { return gs.GetNumPlayers() < 1 && debugView == false; }

class EndReplayStreamCalllback : public sim::InputStreamExhaustionCB {
	virtual int Callback(sim::Game& me, sim::InputStream* stream) override
	{
		printf("EXHAUSTION CALLBACK\n");
		me.m_cb         = 0;
		P1Rec.target    = &P1Ctrl;
		mainGame.paused = true;
		return -1;
	}
} endReplayCB;

ByteBuffer& GetSnapshotBuffer()
{
	int i = 0; // ((mainGame.GetFrameCount()-1) / 10) % 2;
	return snapshotBuff[i];
}

void SnapShot()
{
	ByteBuffer& buff = GetSnapshotBuffer();
	buff.resize(0);
	mainGame.SnapShot(buff, frame);
	P1Rec.free();
	printf("Snapshot size %zu bytes\n", buff.len());
}
void ApplySnapshot()
{
	ByteBuffer& buff = GetSnapshotBuffer();
	if (buff.len() < 1)
		return;
	printf("Reload\n");
	buff.readback(buff.len());
	P1Rec.free();
	mainGame.SetState(buff, frame);
	mainGame.paused = false;
}
void InitReplay()
{
	ByteBuffer& buff = GetSnapshotBuffer();
	if (buff.len() < 1)
		return;
	printf("REPLAY\n");
	buff.readback(buff.len());
	P1Rec.reset();
	P1Rec.target = 0;
	mainGame.SetState(buff, frame);
	mainGame.m_cb   = &endReplayCB;
	mainGame.paused = false;
}

void StartNewGame()
{
	P1Rec.free();
	mainGame.Reset();
}
void TogglePlayerOne()
{
	if (mainGame.GetInputStream(0)) {
		// mainGame.SetInputStream(0, NULL);
		gs.DelLocalPlayer(0);
	} else {
		// mainGame.SetInputStream(0, &P1Rec);
		gs.AddLocalPlayer(0, &P1Ctrl);
	}
}

void ConnCloseCB(net::CTX_h ctx, net::CXN_h conn)
{
	// TODO handle this
	printf("Connection Closed\n");
	mainGame.paused = true;
	gs.DelLocalPlayer(0);
	gs.DelLocalPlayer(1);
}
void ConnOpenCB(net::CTX_h ctx, net::CXN_h conn)
{
	// TODO
	printf("Connection Open\n");
	// net::SetSerializer(ctx, conn, new sim::RemoteDeserializer(), false);

	gs.game.Reset();
	gs.DelLocalPlayer(0);
	mainGame.paused = false;
	SnapShot();
	gs.ForceSnap();
	gs.ResetSlew();
	gs.SendAllSync();

	if (gs.isHost) {
		LocalPlayerIX = 0;
		gs.AddLocalPlayer(0, &P1Ctrl);
		gs.AddRemotePlayer(1, conn); // rr, spec, deserializer);
	} else {
		LocalPlayerIX = 1;
		gs.AddLocalPlayer(1, &P1Ctrl);
		gs.AddRemotePlayer(0, conn); // rr, spec, deserializer);
	}
}

class netPumpJob_t : public tw::job_t {
	virtual void Exec() override
	{
		// TODO acquire gs mutex
		/// --- SEND AND RECEIVE ALL NETWORK STUFF

		if (gs_lock.try_lock()) {
			gs.PumpNetwork(1.0);
			gs_lock.unlock();
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		return;
	}
} netPumpJob;

void ToggleNet()
{
	if (netctx == NET_BADVAL) {
		netctx = net::OpenContext(listenPort, net::IPV4PROTO);
		if (netctx != NET_BADVAL) {
			net::SetConnOpenCallback(netctx, ConnOpenCB);
			net::SetConnCloseCallback(netctx, ConnCloseCB);
		} else {
			fprintf(stderr, "FAILED TO OPEN NETWORK\n");
		}
	} else {
		net::FreeContext(netctx);
	}
}
void ConnectToHost()
{
	if (netctx == NET_BADVAL)
		ToggleNet();

	if (netctx == NET_BADVAL)
		return;
	gs.isHost     = false;
	LocalPlayerIX = 1;
	if (net::ConnectTo(netctx, hostaddr) < 0) {
		printf("Failed to init connection\n");
	}
}

void StartHosting()
{
	StartNewGame();
	TogglePlayerOne();
	ToggleNet();
}

void DoNetStuff() { return; }

void RollBack()
{
	mainGame.paused = true;
	uint64_t f      = mainGame.GetFrameCount();
	snapRec->RollBack(mainGame);
	int n = f - mainGame.GetFrameCount();
	// dump n recorded inputs
	P1Rec.pop_back(n);
}

void keyboardEvent(int keycode, int event, const char* str)
{

	if (doDisplayMenu()) {
		mainMenuManager.Keyevent(keycode, event, str);
	}

	// put debug key reaction crap here
	if (event == KEY_EVENT_PRESS) {

		switch (keycode) {
		case KEY_F1:
			displayDebug ^= 1;
			break;
		case KEY_F2:
			SnapShot();
			break;
		case KEY_F3:
			ApplySnapshot();
			break;
		case KEY_F4:
			InitReplay();
			break;
		case KEY_F5:
			RollBack();
			break;
		case KEY_F6:
			TogglePlayerOne();
			break;
		case KEY_F7:
			//
			break;
		case KEY_F8:
			debugView ^= 1;
			break;
		case KEY_F10:
			gs.ResetSlew();
			break;
		case KEY_F11:
			ConnectToHost();
			break;
		case KEY_F12:
			ToggleNet();
			break;
		case KEY_ESCAPE:
			needExit = 1;
			break;
		case KEY_P:
			mainGame.paused ^= 1;
			printf("PAUSED\n");
			break;
		case KEY_LEFT_BRACKET:
			mainGame.doSingleStep = true;
			mainGame.paused       = true;
			break;
		case KEY_MINUS:
			SimSpeedMultPos = std::max(SimSpeedMultPos - 0.1f, 0.f);
			break;
		case KEY_EQUAL:
			SimSpeedMultPos = std::min(SimSpeedMultPos + 0.1f, 2.f);
			break;
		default:
			// unhandled
			break;
		}

	} else {
	}
}

struct {
	int act = 0, x = 0, y = 0, btn = 0;

} mousePos;
void (*mousePressCB)(int act, int x, int y, int btn)   = 0;
void (*mouseReleaseCB)(int act, int x, int y, int btn) = 0;

void mouseEvent(int act, int x, int y, int btn)
{

	if (btn == 1) {
		if (act == gfx::MBTN_PRESS && mousePressCB)
			mousePressCB(act, x, y, btn);
		if (act == gfx::MBTN_RELEASE && mouseReleaseCB)
			mouseReleaseCB(act, x, y, btn);
	}

	mousePos.act = act;
	mousePos.x   = x;
	mousePos.y   = y;
	mousePos.btn = btn;
};

void pasteEvent(char* buff, int len) { printf("PASTED: %.*s\n", len, buff); }

void printHelp()
{
	printf("COMMANDS:\n");
	printf("-p set network port\n");
	printf("-h set network host 'ip:port'\n");
	printf("-q set network quality 0-100");
	printf("-s set seed\n");
}

// menu callbacks
void Quit_cb() { needExit = 1; }

void SinglePlayerGame_cb()
{
	StartNewGame();
	gs.DelLocalPlayer(0);
	gs.DelLocalPlayer(1);
	gs.AddLocalPlayer(0, &P1Ctrl);
}

int main(int argc, char* argv[])
{
	if (argc > 2) {
		for (int i = 0; i < argc; i++) {
			printf("%s\n", argv[i]);
		}
		for (int i = 1; i + 1 < argc; i += 2) {
			if (strcmp("-p", argv[i]) == 0) {
				// set listen port
				sscanf(argv[i + 1], "%hu", &listenPort);
			} else if (strcmp("-h", argv[i]) == 0) {
				// set a host ip
				snprintf(hostaddr, 24, "%s", argv[i + 1]);
			} else if (strcmp("-q", argv[i]) == 0) {
				// set packet loss out of 100
				sscanf(argv[i + 1], "%d", &debugPacketLoss);
				debugPacketLoss  = std::min(100, std::max(0, debugPacketLoss));
				net::CONNQUALITY = debugPacketLoss;
			} else if (strcmp("-s", argv[i]) == 0) {
				// set seed
				int seedval = 1;
				sscanf(argv[i + 1], "%d", &seedval);
				srand(seedval);
			} else {
				printHelp();
				return 0;
			}
		}
	} else if (argc > 1) {
		printHelp();
		return 0;
	}

	printf("INIT\n");

	/// --- JOB POOL INIT
	// Minimum number of threads
	// 1 network, 1 render, 1 audio, 1 extra?
	int pool_size = std::min(8, std::max(4, (int)std::thread::hardware_concurrency() - 1));
	threadPool.CreatePool(pool_size);
	printf("THREADS %d\n", pool_size);
	std::vector<renderJob> jobs;

	/// --- NETWORK INIT
	// TODO/NOTE
	// lack of net connection should not end play, if netinit failes
	// retry when/if player attempts to start online game
	// TODO
	// Init can block for several seconds while resolving names
	// call it from a seperate thread ?
	if (!net::Init()) {
		fprintf(stderr, "NETPLAY NOT AVAILABLE\n");
	}

	/// --- GRAPHICS INIT
	if (!gfx::Init())
		return -1;
	gfx::SetKBCallback(keyboardEvent);
	gfx::SetMouseCallback(mouseEvent);
	gfx::SetPasteCallback(pasteEvent);

	dst_t         screen = {};
	renderContext ctx;
	palette_t     newpalette
	    = { { 0x00000000, 0xff000000, 0xff25009e, 0xffae0083, 0xffff1600, 0xff193ba9,
		  0xff5031c5, 0xffa04000, 0xff534b01, 0xff1ea95c, 0xff145d96, 0xff119464,
		  0xff545753, 0xff7f61df, 0xffffffff, 0xff5486da, 0xff7c8e28, 0xffff6767,
		  0xfff55fd4, 0xff8e918d, 0xfffd8739, 0xff50b48d, 0xffbb94fc, 0xff84b85a,
		  0xffd8ac4e, 0xffd6b638, 0xff86befe, 0xffced2cc, 0xff87f3b7, 0xffffd86f,
		  0xffb5f189, 0xfffffd45 } };
	ctx.palette = newpalette;
	ctx.palette.GenNames();
	gfx::SetPalette(newpalette.color);

	sprite_t     fntsrc = { 1, 1, NULL };
	paletteMap_t fntmap = {};
	if (!pixmapToSprite((const unsigned char**)font4x6_xpm, ARRSIZE(font4x6_xpm), ctx.palette,
	                    fntsrc, fntmap)) {
		fprintf(stderr, "Failed to parse font\n");
		return -1;
	}
	printf("fntsrc %d %d\n", fntsrc.width, fntsrc.height);
	monofont  fnt{ fntsrc, ctx.palette, {}, 4, 6, 32 };
	Messanger msger(ctx, fnt);

	// TODO
	// remove hack
	SnapShot();

	/// --- DEBUG STATS
	double       frameTime = 0.0;
	double       simTime   = 0.0;
	double       rendTime  = 0.0;
	double       orendTime = 0.0;
	unsigned int frameNum  = 0;
	gfx::ResetTimer();

	// start paused
	mainGame.paused = false;

	/// --- NETWORK
	gs.ForceSnap();
	gs.ResetSlew();
	threadPool.SubJob(&netPumpJob, tw::LOOP, 0);

	m::menu TestMenu;
	m::menu netMenu;

	TestMenu.focuscolor = ctx.palette.WHITE;
	TestMenu.textcolor  = ctx.palette.BLUE;
	TestMenu.AddMenu(new m::menuFunc("PLAY", SinglePlayerGame_cb));
	TestMenu.AddMenu(new m::menuSel("ONLINE", &netMenu, mainMenuManager));
	// TestMenu.AddMenu(new m::menuFunc("OPTIONS", nullptr));
	TestMenu.AddMenu(new m::menuFunc("QUIT", Quit_cb));

	netMenu.focuscolor = ctx.palette.WHITE;
	netMenu.textcolor  = ctx.palette.BLUE;
	netMenu.AddMenu(new m::menuIP(hostaddr));
	netMenu.AddMenu(new m::menuFunc("CONNECT", ConnectToHost));
	netMenu.AddMenu(new m::menuFunc("HOST", StartHosting));
	netMenu.AddMenu(new m::menuSel("EXIT", &TestMenu, mainMenuManager));

	mainMenuManager.menuAvail.push_back(&TestMenu);
	mainMenuManager.menuAvail.push_back(&netMenu);

	while (wantQuit() == 0) {

		frameNum++;
		gfx::SwapBuffers();
		gfx::GetCurrentBuffer((void**)&screen.raw32, &screen.width, &screen.height);

		/// --- SEND AND RECEIVE ALL NETWORK STUFF
		// gs.PumpNetwork(frameTime);

		frameTime = gfx::GetTimeElapsed();
		gfx::ResetTimer();

		ctx.dst = screen;
		jobs.resize(0);

		gs_lock.lock();
		/// --- STEP SIMULATION
		double t0         = gfx::GetTimeElapsed();
		SimSpeedMult      = std::pow(SimSpeedMultPos, 3.f);
		int simstepstaken = gs.StepGame(frameTime * SimSpeedMult);
		gs_lock.unlock();

		simTime   = gfx::GetTimeElapsed();
		rendTime  = simTime;
		orendTime = (rendTime * 0.2 + orendTime * 0.8);
		simTime   = simTime - t0;

		// TODO
		// camera needs moveTo to be called by game or overridden?
		// mainView.moveTo(ent->pos);
		v3 np      = InterpolatePos(mainView.pos, mainView.vel, mainGame.Tmult());
		mainView.x = np.x;
		mainView.y = np.y;

		// TODO
		// rework this interface/construction
		DrawGame(ctx, mainView, mainGame, msger, LocalPlayerIX);

		{
			int w, h, x, y;
			gfx::GetScreenDim(w, h);
			x = (float)mousePos.x / (float)w * (float)mainView.w;
			y = (float)mousePos.y / (float)h * (float)mainView.h;
			// TODO
			// remove debug visuals for editing/seeing mouse pos
			ctx.drawCircle(x, y, 2, ctx.palette.RED, true);
		}

		// TODO/NOTE
		// try and pick some sane job division based on cores
		int numjobs = std::max(2, std::min(12, pool_size - 2));
		// int tilesize = 480 / numjobs;
		// for (int i = 0; i < numjobs; i++) {
		//  ctx.drawLine(100, tilesize * i, 540, tilesize * i, ctx.palette.WHITE);
		// }

		/// --- RENDERS/BLIT out all the main sprites
		FillJobList(ctx, numjobs, jobs);
		for (renderJob& rj : jobs) {
			// rj.Exec();
			threadPool.SubJob(&rj, 0, 1);
		}
		threadPool.WaitBatch(1);

		ctx.reset();
		if (displayDebug) {
			msger.BeginMsg(10, 10, 10, 200);
			msger.Write("FTIME %03.3fms SIM %03.3fms STEPS %d REND %03.3fms", frameTime,
			            simTime, simstepstaken, orendTime);
		}
		msger.WriteOut();
		msger.Clear();
	}

	free(fntsrc.raw32);

	threadPool.Join();

	gfx::Exit();
	net::Exit();
	// nix::Exit();
	printf("EXIT\n");
	return 0;
}
