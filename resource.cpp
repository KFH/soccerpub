
#include <vector>
#include <iostream>
#include <fstream>
#include <filesystem>
#include <unordered_map>
#include <sys/stat.h>

#include "resource.hpp"

void resource_manager_t::init(std::string fname)
{

	std::ifstream fs;
	size_t        len       = 0;
	char*         leak_buff = nullptr;
	fs.open(fname, std::ios::in | std::ios::binary);

	fs.seekg(0, std::ios::end);

	len       = fs.tellg();
	leak_buff = new char[len];

	fs.seekg(0, std::ios::beg);
	fs.read(leak_buff, len);

	init(leak_buff, len);
}

void resource_manager_t::init(const char* data, size_t data_size)
{

	res_index = (const resource_index_t*)data;
	res       = (const resource_t*)(data + res_index->res_ptr);

	raw = data; //+ res_index->data_ptr;

	for (size_t i = 0lu; i < res_index->res_len; i++) {
		res_map.insert({ std::string(data + res[i]._ptr_name, res[i]._name_size), i });
	}

	printf("res_ptr   %zu\n", res_index->res_ptr);
	printf("res_len   %zu\n", res_index->res_len);
	printf("names_ptr %zu\n", res_index->names_ptr);
	printf("names_len %zu\n", res_index->names_len);
	printf("data_ptr  %zu\n", res_index->data_ptr);
	printf("data_len  %zu\n", res_index->data_len);
	printf("FOUND %zu ENTRIES:\n", res_index->res_len);

	for (const auto pair : res_map) {
		printf("%s\n", pair.first.c_str());

		const auto& rec = res[pair.second];
		printf("ptr %zu size %zu\n", rec._ptr_data, rec._data_size);

		printf("\n%s\n", get(pair.first).c_str());
	}
}

data_t resource_manager_t ::getNewest(const std::string& name)
{
	// TODO
	// if path set compare modified stat with stored val

	std::string fname     = root + name;
	size_t      fname_mod = 0lu;
	auto        it        = res_map.find(name);

	if (it == res_map.end())
		return data_t{ nullptr, 0 };

	const auto& rec = res[it->second];

	if (root.size() > 0) {

		struct stat sbuff;
		int         status;
		status = stat((fname).c_str(), &sbuff);

		if (sbuff.st_size > 0 && status == 0
		    && size_t(sbuff.st_mtim.tv_sec) > rec._modified) {

			printf("LOADING FROM DISK\n");
			char*         leak_data = new char[sbuff.st_size];
			std::ifstream fs;
			fs.open(fname, std::ios::in | std::ios::binary);
			fs.read(leak_data, sbuff.st_size);
			fs.close();

			return data_t{ leak_data, size_t(sbuff.st_size) };
		}
	}

	return { raw + rec._ptr_data, rec._data_size };
}

std::string resource_manager_t::get(const std::string& name)
{

	auto d = getNewest(name);

	return std::string(d._data, d._size);
}

std::unordered_map<std::string, resource_manager_t*>* res_manager_map = nullptr;
int init_resource_manager(const std::string& name, const char* data, size_t data_size)
{

	resource_manager_t* m = new resource_manager_t(data, data_size);
	if (!res_manager_map)
		res_manager_map = new std::unordered_map<std::string, resource_manager_t*>();
	res_manager_map->insert_or_assign(name, m);
	return 0;
}
resource_manager_t& get_resource_manager(const std::string& name)
{
	return *(*res_manager_map)[name];
}
