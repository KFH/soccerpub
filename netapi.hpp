#pragma once
#include "buff.hpp"
#include <stdint.h>
#include <string>
namespace net {

#define MAGICNUM 43690
#define CLOSENUM 21845
#define FRAGNUM 64170

#define NET_BADVAL 0
#define NET_MAXCONNECTIONS 4
#define MAX_RPC 16
#define MAX_BURST 8
#define BUFFERSIZE 2048 // number of packets to hold in buffer
#define MAXPACKET_S 1024 // keep MTU small larger packets can be silently dropped?
#define MSGHEADERSIZE 16
#define MAXTRANSMISSIBLE (BUFFERSIZE * (MAXPACKET_S - MSGHEADERSIZE))
#define MAXCOUNT 20 * 4 // after 20 packets per sec x 4 sec kill connection
#define MAXINPUT 60
#define MAXWAIT 5 // roughly 330ms latency
// #define CONNQUALITY 0
#define DEFAULT_PUNCH "127.0.0.1:8888"

extern int CONNQUALITY;
// 170 is 8bit version ?

// opaque handles could be used to identify connections and contexts/ports while minimizing
class Context;
class Connection;
typedef Connection* CXN_h;
typedef Context*    CTX_h;

extern const int IPV4PROTO;
extern const int IPV6PROTO;

typedef void (*ConnectionChangeCB)(CTX_h ctx, CXN_h conn);

class deserializer {
    public:
	virtual int read(CXN_h conn, const unsigned char* data, int len) = 0;
	virtual ~deserializer(){};
};

int  Init();
int  IsInit();
void Exit();
// every time a packet is recieved and has matching connection call this
typedef void (*NetPacketCallback_t)(int uid, void* packetdata, int size);
extern NetPacketCallback_t NetPacketCallback;
// if buffer for sending is full and connection isnt working good call this
typedef void (*NetStallCallback_t)(int uid, int amount, double time);
extern NetStallCallback_t NetStallCallback;

// open a port for listening
CTX_h OpenContext(uint16_t port, int fam);

void FreeContext(CTX_h ctx);

void SetConnOpenCallback(CTX_h ctx, ConnectionChangeCB cb);
void SetConnCloseCallback(CTX_h ctx, ConnectionChangeCB cb);

std::string GetPublicAddress(CTX_h ctx);

struct connStats {
	int64_t bitRate;
	double  latency;
	int64_t unackedPacket; // distance to older message or oldest?
	int64_t oldestUnacked;
	int64_t subMessagesOUT; // how many sub messages appended
	int64_t subMessagesIN;
	int64_t recvPackets;
	int64_t sentPackets;
	int64_t droppedPackets;
	int64_t stall;
	int64_t edge;
	int64_t orderBuffSize;

	int64_t orderEdge[10];
	bool    orderEdgeRdy[10];
	int64_t orderEdgeSize;

	void Reset() { memset(this, 0, sizeof(connStats)); }
};

struct transfer_progress_t {

	size_t byte_size;
	size_t num_packets_recieved;
	size_t num_packets;

	double complete() { return double(num_packets_recieved) / double(num_packets); }
};

// Pumps contexts connections data in and out
// will also open new connections if they are found
int SendAndRecieve(CTX_h ctx);

int Recieve(CTX_h ctx);

int ConnectTo(CTX_h, const char* addr);

void CloseAllConnections(CTX_h ctx);

int CloseConnection(CTX_h ctx, CXN_h conn);

int SendAll(CTX_h ctx, void* data, int data_s);

int Send(CTX_h ctx, CXN_h conn, void* data, int data_s);

int FlushConn(CTX_h ctx, CXN_h conn);

// return space on avaliable in current buffer
int GetSpace(CTX_h ctx, CXN_h conn);

void SetSerializer(CTX_h ctx, CXN_h conn, deserializer* des, bool ordered);

deserializer* GetSerializer(CTX_h ctx, CXN_h conn, bool ordered);

int GetNumConnecitons(CTX_h ctx);

CXN_h* GetConnections(CTX_h ctx);

connStats GetConnStats(CTX_h ctx, CXN_h conn);

void GetConnAddr(CTX_h ctx, CXN_h conn, char* dst);
} // namespace net
