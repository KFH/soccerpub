#version 300 es
//------------------
uniform sampler2D tex;
uniform sampler2D pal;

uniform highp vec2 pic_res;
uniform highp vec2 win_res;

in highp vec2 vUV;
out lowp vec4 fragColor;

lowp vec2 parallaxMap(vec2 texCoord, vec3 viewDir) {
	lowp float heightScale = 0.0;
	lowp float currentDepth = 0.0;
	lowp float depth = (texture(tex, texCoord).x );
	lowp vec2 P = (viewDir.xy / viewDir.z)  * (depth * heightScale);

	return texCoord - P;
	//lowp float currentDepthVal = texture(tex, texCoord).x;
	//while(currentDepth < currentDepthVal) {
	//}
}



lowp vec2 distort(vec2 p)
{
    lowp float barrelpower = 1.2;
    lowp float theta  = atan(p.y, p.x);
    lowp float radius = length(p);
    radius = pow(radius, barrelpower);
    p.x = radius * cos(theta);
    p.y = radius * sin(theta);
    return 0.5 * (p + 1.0);
}

void main(){

	/*

	highp float mul = 1.0;
	highp vec2 nUV;
	nUV = vUV;


	nUV = distort(vUV * 2.0 - 1.0);
	lowp vec3 viewPos = vec3(0.5,0.5,-1.0);
	lowp vec3 surfPos = vec3(nUV,0.0);
	lowp vec3 viewDir = normalize(viewPos - surfPos );

	nUV = parallaxMap(nUV, viewDir);
	// nUV = (nUV * 1.5) - 0.25;
	// 

	if(nUV.x < 0.0 || nUV.x > 1.0 || nUV.y < 0.0 || nUV.y > 1.0 ) {
		discard;
	}

	nUV *= pic_res;

	//lowp vec2 ts = vec2(textureSize(pal, 0)) * mul;
	lowp vec4 ix = texelFetch(tex, ivec2(nUV), 0);
	//ix.x = ceil(ix.x * ts.x) / ts.x;
	*/
		
	lowp vec4 ix = texelFetch(tex, ivec2(vUV * pic_res), 0);
	fragColor = texture(pal, ix.xz).bgra;
	//fragColor = (ix.xxxx * 8.0);
}
