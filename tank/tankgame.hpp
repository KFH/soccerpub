#pragma once
#include "game.hpp"
#include "map.hpp"
#include "binarytree.hpp"
#include "buff.hpp"

namespace sim {

struct maptile_t {
	uint8_t gfx;
	uint8_t type;
	enum maptiletypes { floor = 0x0, wall = 0x01 };
};
typedef rtree<maptile_t, 8, 16> fieldMap_t;

struct player_ctrl_t {
	float accel;
	float turn;
	float aim;
	uint8_t action;

	enum {
		NONE,
		SHOOT,
		ROTATE
	};
};

struct shell_t {
	v3 pos;
	v3 vel;
	fp32 radius;
	fp32 time;
	entityID emitter;
};

struct tank_t {
	v3 pos;
	v3 aim_norm;
	v3 drive_norm;
	v3 vel;

	fp32 throttle;
	fp32 loading;
	fp32 ammoing;
	fp32 rotating;

	uint32_t ammo;
	fp32     gas;
	
	entityID driver;
	entityID gunner;
	entityID loader;
	entityID fueler;

	entityID hopper[5];
};

struct prisoner_t {
	v3 pos;
	v3 vel;
	fp32      term;
	uint32_t crime;
	bool free;
};

struct ai_t {
	entityID self;
	entityID target;
	v3 watch_post;

	bool operator==(const entityID& id) const { return self == id; }
};


class tankInstance : public sim::vInstance<player_ctrl_t> {
    public:
	g_entityTracker              IDs;
	tree_t<entityID, tank_t>     tanks;
	tree_t<entityID, prisoner_t> prisoners;
	RingBuffer<shell_t, 32>      shells;
	std::vector<ai_t>            AI;

	fieldMap_t map;


#pragma pack(push, 1)
	struct ctrlMap {
		playerIX player_id;
		entityID entity_id;
		bool     isPaired;
		bool     operator==(const ctrlMap& other) const { return player_id == other.player_id; }
		bool     operator==(const playerIX& id) const { return player_id == id; }
		bool     operator==(const entityID& id) const { return entity_id == id; }
	};
#pragma pack(pop)
	std::vector<ctrlMap> inmap;
	int nextIx = 0;

	// standard functions must be implemented
	virtual int  Init()                                            override;
	virtual void Step(int frameNum, tankInstance::input_vec_t& in) override;

	// TODO
	// implement serialization and deserialization
	virtual void Serialize(ByteBuffer& buff) const                 override {};
	virtual void DeSerialize(ByteBuffer& buff)                     override {};

	// custom
	tank_t*  GetPlayerTank(playerIX ix);
};
}
