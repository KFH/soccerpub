#include <cstdint>

#include "idtags.hpp"
#include "game.hpp"
#include "gamerend.hpp"
#include "interpolate.hpp"
#include "rtree.hpp"
#include "util.hpp"

#include "tankgame.hpp"

#include "draw.hpp"

#include "pig.xpm"

typedef entityQuant<sim::tank_t> tankQuant_t;
typedef entityQuant<sim::shell_t> shellQuant_t;
typedef entityQuant<sim::prisoner_t> prisonerQuant_t;
std::vector<tankQuant_t> tankpos;

class debugTreeDrawer : public queryCallback_t<sim::maptile_t> {
    public:
	sim::view_t*   gview;
	rect_t         view;
	renderContext* ctx    = 0;
	uint8_t        color  = 0;
	int            margin = 0;

	debugTreeDrawer() {}
	virtual bool callback(int index, const rect_t& ir, const sim::maptile_t& mt) override
	{
		rect_t r = ir;
		r.expand(margin);
		rtDrawRect(*ctx, *gview, r, color);
		return 0;
	}
};
debugTreeDrawer mapDrawer;

bool rendering_init = false;
uint8_t sprite_ix = 0;
uint8_t map_ix    = 0;

void InitDrawing(renderContext& ctx) {

	if(rendering_init)
		return;
	rendering_init = true;

	sprite_ix = ctx.sprites.size();
	map_ix    = ctx.maps.size();

	ctx.sprites.emplace_back();
	ctx.maps.emplace_back();

	if (!pixmapToSprite((const unsigned char**)pig_xpm, ARRSIZE(pig_xpm), ctx.palette,
				ctx.sprites.back(), ctx.maps.back())) {
		fprintf(stderr, "Failed to parse pixmap 1\n");
		return;
	}
}

void drawSprite(renderContext& ctx, i3 pt) {
	ctx.blit(sprite_ix, map_ix, 0, 0, 32, 32, pt.x, pt.y);
};


void DrawGame(renderContext& ctx, 
		sim::view_t& mainView, 
		sim::Game<sim::player_ctrl_t>& mainGame, 
		Messanger& msger,
		sim::playerIX LocalPlayerIX) { 

	InitDrawing(ctx);

	ctx.blank(0);
	sim::tankInstance& inst = mainGame.CurInst<sim::tankInstance>();

	/// --- MOVE VIEW 
	// TODO this is bad
	sim::playerIX pix = 0;
	auto it = std::find(inst.inmap.begin(), inst.inmap.end(), pix);
	sim::tank_t* ptank = nullptr;
	if(it != inst.inmap.end()) {
		ptank = inst.tanks.Get(it->entity_id);
		mainView.moveTo(ptank->pos);
	}

	/// --- SETUP
	tankpos.resize(0);

	InterpolateViewPos<sim::tank_t, decltype(inst.tanks)>(mainView, inst.tanks, tankpos, mainGame.Tmult());
	// sort top to bottom
	sort(tankpos.begin(), tankpos.end(),
	     [](const tankQuant_t& a, const tankQuant_t& b) { return a.y < b.y; });

	for (const auto& eq : tankpos) {
		const auto tank = inst.tanks.Get(eq.uid);
		ctx.drawCircle(eq.x, eq.y, 8, ctx.palette.RED, false);
		ctx.drawLine(
			eq.x, eq.y, 
			eq.x + tank->drive_norm.x * 8, 
			eq.y + tank->drive_norm.y * 8, 
			ctx.palette.RED);
		ctx.drawLine(
			eq.x, eq.y, 
			eq.x + tank->aim_norm.x * 16, 
			eq.y + tank->aim_norm.y * 16, 
			ctx.palette.WHITE);
	}

	for(const auto& shell : inst.shells) {
		shellQuant_t eq;
		InterpolateViewPos<sim::shell_t>(mainView, shell, eq, mainGame.Tmult());
		ctx.drawCircle(eq.x, eq.y, 3, ctx.palette.GREEN, false);
	}

	rect_t viewRect = GetViewRect(mainView);
	mapDrawer.gview  = &mainView;
	mapDrawer.view   = viewRect;
	mapDrawer.ctx    = &ctx;
	mapDrawer.color  = ctx.palette.RED;
	mapDrawer.margin = 0;
	inst.map.Search(viewRect, mapDrawer);


	for(const auto& p : inst.prisoners) {
		prisonerQuant_t eq;
		InterpolateViewPos<sim::prisoner_t>(mainView, p, eq, mainGame.Tmult());	
		ctx.drawCircle(eq.x, eq.y, 3, ctx.palette.YELLOW, true);
		ctx.drawCircle(eq.x, eq.y+6, 5, ctx.palette.YELLOW, true);
	}


	/// --- draw main players tank gui shit
	if(ptank != nullptr)
	{
		
		// prisoner hopper gui
		int offx = 100;
		int offy = 20;
		int x = 0;
		int y = 0;
		i3 c = {offx-16,offy-16};
		for(auto prisoner : ptank->hopper) {
			ctx.drawCircle(offx + x, offy + y,13, ctx.palette.RED, false);
			if(prisoner != sim::NULL_ID) {
				drawSprite(ctx, c + i3{x,y} );		
			}
			x += 32;
		}

		// crew gui
		offx = 60;
		offy = 80;
		fp32 m = std::min(1.f,std::max(0.f,ptank->rotating / 0.25f)) * M_PI * 0.5;
		fp32 f = M_PI * 0.5;
		fp32 r = 32.f;
		c = {offx-16,offy-16, 0};
		bool staffed = false;

		x = int(r * cos(m));
		y = int(r * sin(m));
		staffed = ptank->gunner != sim::NULL_ID;
		ctx.drawCircle(offx + x ,offy + y, 13, staffed ? ctx.palette.GREEN : ctx.palette.BLUE, false);
		if(staffed)
			drawSprite(ctx, c + i3{x,y});

		x = int(r * cos(m+f));
		y = int(r * sin(m+f));
		staffed = ptank->driver != sim::NULL_ID;
		ctx.drawCircle(offx + x, offy + y, 13, staffed ? ctx.palette.GREEN : ctx.palette.BLUE, false);
		if(staffed)
			drawSprite(ctx, c + i3{x,y});

		x = int(r * cos(m+f*2.f));
		y = int(r * sin(m+f*2.f));
		staffed = ptank->fueler != sim::NULL_ID;
		ctx.drawCircle(offx + x, offy + y, 13, staffed ? ctx.palette.GREEN : ctx.palette.BLUE, false);
		if(staffed)
			drawSprite(ctx, c + i3{x,y});

		x = int(r * cos(m+f*3.f));
		y = int(r * sin(m+f*3.f));
		staffed = ptank->loader != sim::NULL_ID;
		ctx.drawCircle(offx + x, offy + y, 13, staffed ? ctx.palette.GREEN : ctx.palette.BLUE, false);
		if(staffed)
			drawSprite(ctx, c + i3{x,y});

		
		msger.BeginMsg(offx-32, offy+20, 10, 200);
		msger.Write("FUEL");

		msger.BeginMsg(offx+32, offy+20, 10, 200);
		msger.Write("GUN");

		msger.BeginMsg(offx, offy+32+20, 10, 200);
		msger.Write("DRIVE");

		msger.BeginMsg(offx, offy-32+20, 10, 200);
		msger.Write("LOAD");
	


	
		// fuel gui
		offx = 20;
		offy = 160;
		int gas_d = int( 100.f * std::min(1.f, std::max(0.f,ptank->gas/10.f)));
		ctx.drawLine(offx, offy, offx + gas_d, offy, ctx.palette.YELLOW);
		msger.BeginMsg(offx,offy+2,10, 200);
		msger.Write("FUEL");

		// reload gui
		offy = 180;
		int loading_d = int( 100.f * std::min(1.f, std::max(0.f,ptank->loading/0.5f)));
		ctx.drawLine(offx, offy, offx + loading_d, offy, ctx.palette.YELLOW);
		msger.BeginMsg(offx,offy+2,10, 200);
		msger.Write("RELOAD");
	

		// ammo gui
		offx = 20;
		offy = 200;
		int a = ptank->ammo;
		while(a--) { 
			ctx.drawCircle(offx + a * 10, offy, 5, ctx.palette.YELLOW);
		}	
		msger.BeginMsg(offx,offy+10,10, 200);
		msger.Write("AMMO");
	}

}
