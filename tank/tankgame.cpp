#include "tankgame.hpp"

const double STEPVAL = 0.016;

namespace sim {
int  tankInstance::Init() {
	// TODO
	// instantiate some tanks
	tanks.Reset();
	IDs.Reset();
	prisoners.Reset();
	for(int i = 0; i < 5; i++) {
		auto id = IDs.Create();
		v3 pos = {i * 64, 0, 0};
		v3 vel = {0,0,0};
		v3 aim_norm = {0,1,0};
		v3 drive_norm = {0,1,0};
		float throttle = 0.0;
		float loading = 0.0;
		float ammoing = 0.0;
		float rotating = 0.0;
		uint32_t ammo = 10;
		fp32 gas = 10.0;

		auto driver_id = IDs.Create();
		auto filler_id = NULL_ID;

		tanks.Insert(id, {pos, aim_norm, drive_norm, vel, 
			throttle, loading, ammoing, rotating, ammo, gas, 
			driver_id, filler_id, filler_id, filler_id
				
				});		
	
	}	

	for(int i = 0; i < 20; i++) {
		auto id = IDs.Create();
		fp32 mag = 600.f;
		v3 pos = {randf() * mag, randf() * mag, 0 };
		v3 vel = {};
		fp32 term = 0.f;
		uint32_t crime = 0;
		bool free = true;
		prisoners.Insert(id, {pos, vel , term, crime, free});
	}

	for(const auto& tank : tanks) {
		v3 pos = {99999,999999,9};
		v3 vel = {};
		fp32 term = 0.f;
		uint32_t crime = 0;
		bool free = false;	
		prisoners.Insert(tank.driver, {pos, vel , term, crime, free});
	}

	return 0;
};

inline rect_t rtBoxPoint(v3 p, int margin)
{
	v3  np = p.round();
	int x  = int(np.x);
	int y  = int(np.y);
	return rect_t{ x - margin, y - margin, x + margin, y + margin };
}

class shellCollisionQuery : public queryCallback_t<maptile_t> {
    public:
	shell_t& me;
	int     x, y;
	v3      npos;
	fp32    mx, my, zpos;
	bool    hasHit;
	shellCollisionQuery(shell_t& ball)
	    : me(ball)
	{
		fp32 margin = fp32(0.f);
		x           = !signfp(me.vel.x);
		y           = !signfp(me.vel.y);
		npos        = me.pos + me.vel;
		mx          = fp32(x) * margin;
		my          = fp32(y) * margin;
		npos.x += mx;
		npos.y += my;
		hasHit = false;
		zpos   = npos.z;
	}
	// TODO should determine a deflection point and plane/planes
	// based on all returned intersections
	virtual bool callback(int index, const rect_t& ir, const maptile_t& i) override
	{
		if (hasHit)
			return 0;
		if (i.type == maptile_t::floor)
			return 0;

		fp32 x0  = x ? fp32(ir.x0) : fp32(ir.x1);
		fp32 x1  = x ? fp32(ir.x1) : fp32(ir.x0);
		fp32 y0  = y ? fp32(ir.y0) : fp32(ir.y1);
		fp32 y1  = y ? fp32(ir.y1) : fp32(ir.y0);
		v3   p0  = v3{ x0, y0, 0 };
		v3   hp0 = v3{}, hp1 = v3{};
		bool b0 = false, b1 = false;

		// left right
		if (Intersects(me.pos, npos, p0, v3{ x0, y1, 0 }, hp0)) {
			b0 = true;
		}

		// up down
		if (Intersects(me.pos, npos, p0, v3{ x1, y0, 0 }, hp1)) {
			b1 = true;
		}
		// determine time of impact
		if (b0 & b1) {
			// fp32 d = hp0.dist(me.pos);
			// fp32 m = (d - npos.dist(me.pos)) / d;
			// me.vel = me.vel * v3{ fp32(-1), fp32(-1), fp32(1) };
			// me.pos = hp0; // + (me.vel * m);
			// me.pos.x += mx;
			// me.pos.y += my;
			me.time = 0.0;
		} else if (b0) {
			v3 d = (p0 - v3{ x0, y1, 0 }).normal();
			if(absfp(me.vel.normal().dot(d)) < 0.5)
				me.time = 0.0;

			me.vel = me.vel * v3{ fp32(-1), fp32(1), fp32(1) };
			me.pos = hp0; // + (me.vel * m);
			me.pos.x += mx;

		} else if (b1) {
			v3 d = (p0 - v3{ x1, y0, 0 }).normal();
			if(absfp(me.vel.normal().dot(d)) < 0.5)
				me.time = 0.0;

			me.vel = me.vel * v3{ fp32(1), fp32(-1), fp32(1) };
			me.pos = hp1; // + (me.vel * m);
			me.pos.y += my;
		}
		if (b0 | b1) {
			me.vel   = me.vel * fp32(0.95f);
			me.pos.z = zpos;
		}

		hasHit |= b0 | b1;

		return 0;
	}
};


inline v3 rectCenter(const rect_t& ir) {
	return  v3{fp32(ir.x0 + ir.x1), fp32(ir.y0 + ir.y1), 0.f} * 0.5f;
}

class tankCollisionQuery_t : public queryCallback_t<maptile_t> {
	public:	
	tank_t& me;
	fp32 margin;
	 int     x, y;

	tankCollisionQuery_t(tank_t& tank) : me(tank) {
		margin = fp32(9.f);	
		x = !signfp(me.vel.x);
		y = !signfp(me.vel.y);
	}

	virtual bool callback(int index, const rect_t& ir, const maptile_t& i) override
	{
		fp32 x0  = x ? fp32(ir.x0) : fp32(ir.x1);
		fp32 x1  = x ? fp32(ir.x1) : fp32(ir.x0);
		fp32 y0  = y ? fp32(ir.y0) : fp32(ir.y1);
		fp32 y1  = y ? fp32(ir.y1) : fp32(ir.y0);
		v3   p0  = v3{ x0, y0, 0 };

		v3 hit_pt0 = ClosestPtOnLine(p0, v3{ x1, y0, 0 }, me.pos);
		v3 hit_pt1 = ClosestPtOnLine(p0, v3{ x0, y1, 0 }, me.pos);
		v3 np0, np1;

		v3 cent_dir = (rectCenter(ir) - me.pos).normal();

		bool updown = (me.pos.dist(hit_pt0) < margin);
		bool leftright = (me.pos.dist(hit_pt1) < margin);
		if(updown) {
			v3 dir = (hit_pt0 - me.pos).normal();
			dir = dir.dot(cent_dir) < 0.f ? dir * -1.f : dir;
			me.vel = me.vel * v3{ 1.f, 0.f, 0.f };
			me.pos = hit_pt0 + (dir * -margin); 
		} 
		if(leftright) {
			v3 dir = (hit_pt1 - me.pos).normal();
			dir = dir.dot(cent_dir) < 0.f ? dir * -1.f : dir;
			me.pos = hit_pt1 + dir * -margin;
			me.vel = me.vel * v3{ 0.f, 1.f, 0.f};
		}          
          
		return 0;	
	}
};

void tankInstance::Step(int frameNum, tankInstance::input_vec_t& in) {
	// iterate all shells and tanks updating their position based on vel etc

	// marking all control input mappings as unpaired then checking if matching ctrlOverride
	// for each player still exists
	for(auto& m : inmap) {
		m.isPaired = false;
	}
	for (auto& c : in) {
		auto it = std::find(inmap.begin(), inmap.end(), c.player_id);
		if (it != inmap.end()) {
			// convert input ctrl uid to from player_id to the corresponding entity_id cause
			// i cant think of a better design atm
			c.entity_id       = it->entity_id;
			it->isPaired = true;
		} else {
			// find a pairing
			for(size_t i = 0; i < tanks.size(); i++) {
				nextIx = (nextIx+1) % tanks.size();
				auto id = tanks.GetKey(nextIx);
				auto itr_map = std::find_if(inmap.begin(), inmap.end(),
				                        [&id](auto const& cm) {
					                        return cm.entity_id == id;
				                        });
				// if tank is not mapped to someone elses controller create mapping
				if (itr_map == inmap.end()) {
					inmap.push_back(ctrlMap{ c.player_id, id, true });		
				}
			}

		}
	}
	// remove unpaired mappings / player must have left
	for(size_t i = 0; i < inmap.size(); i ++) {
		if(!inmap[i].isPaired) {
			inmap[i] = inmap.back();
			inmap.pop_back();
		}
	}

	// step ai or apply player controls
	for (auto tank = tanks.begin(); tank != tanks.end(); tank++) {
		auto ctrl_it = std::find(in.begin(), in.end(), tank.Key());
		if (ctrl_it != in.end()) {
	
			// aim	
			if(tank->gunner != NULL_ID) 
				tank->aim_norm = (tank->aim_norm + v3{0,0,1}.cross(tank->aim_norm) * ctrl_it->ctrl.aim * 0.1).normal();

			// steer
			if(tank->driver != NULL_ID) {
				tank->drive_norm = (tank->drive_norm + v3{0,0,1}.cross(tank->drive_norm) * ctrl_it->ctrl.turn * 0.06).normal();
				tank->throttle = ctrl_it->ctrl.accel;
			}

			// SHOOT
			if(tank->loading <= 1e-4 && tank->gunner != NULL_ID && tank->ammo > 0 && ctrl_it->ctrl.action == player_ctrl_t::SHOOT) {
				tank->loading = 0.5;
				fp32 radius = 3.0;
				fp32 time = 3.0;
				tank->ammo--;
				shells.push(shell_t{ tank->pos + tank->aim_norm*16.f , tank->aim_norm * 3.0, radius, time, tank.Key() });
			}

			// ROTATE prisoners
			if(tank->rotating <= 1e-4 && ctrl_it->ctrl.action == player_ctrl_t::ROTATE) {
				tank->rotating = 0.25;
	
				auto temp = tank->driver;
				tank->driver = tank->fueler;
				tank->fueler = tank->loader;
				tank->loader = tank->gunner;
				tank->gunner = temp;

				printf("%u %u %u %u\n", 
					tank->driver.id,
					tank->fueler.id,
					tank->loader.id,
					tank->gunner.id);

				if(tank->loader == NULL_ID && tank->hopper[0] != NULL_ID) {
					printf("%u => %u\n", tank->loader.id, tank->hopper[0].id);
					prisoners.Get(tank->hopper[0])->term = 30.f;
					tank->loader = tank->hopper[0];
					for(int i = 0; i < 4; i++) {
						tank->hopper[i] = tank->hopper[i+1];
					}
				}


			}
		} else {
			// TODO
			// step tank ai	
		}
	}
	for(auto& ai : AI) {
		if(ai.target == NULL_ID)
			continue;
		auto* me = tanks.Get(ai.self);	
		auto* target = tanks.Get(ai.target);
		//tank->aim_norm = (target.pos - me.pos).normal();
	}

	// FOR EACH PRISONER
	for(auto& p : prisoners) {
		p.term = std::max(0.0, p.term - STEPVAL);
	}

	// FOR EACH TANK
	for(auto& tank : tanks) {

		tank.rotating = std::max(0.0, tank.rotating - STEPVAL);
		tank.gas      = std::max(0.0, tank.gas      - STEPVAL);


		if(tank.loader != NULL_ID) {
			tank.loading = std::max(0.0, tank.loading - STEPVAL);
			tank.ammoing = std::max(0.0, tank.ammoing - STEPVAL);
			if(tank.ammoing < 1e-4 && tank.ammo < 10) {
				tank.ammoing = 2.f;
				tank.ammo++;	
			}
		}
		if(tank.fueler != NULL_ID)
			tank.gas = std::min(10.0, tank.gas + 0.15);

		if(tank.gas < 1e-4)
			tank.throttle = 0.f;

		v3 opos = tank.pos;
		v3 ovel = tank.vel;
		v3 drive = tank.drive_norm * tank.throttle * 0.15;
		tank.vel = ovel * 0.9 + drive;
		tank.pos = tank.pos + tank.vel;

		tankCollisionQuery_t cb(tank);
		map.Search(rtBoxPoint(tank.pos, tank.vel.len() + 16.f), cb);

	}
	
	// FOR EACH BULLET
	for(auto& shell : shells) {
		shell.time -= STEPVAL;		
		shell.pos = shell.pos + shell.vel;

		shellCollisionQuery cb(shell);
		map.Search(rtBoxPoint(shell.pos, int(shell.vel.len()) + 8.0), cb);
	}

	while(shells.size() > 0 && shells.getRel(0).time <= 0.0) {
		shells.pop();
	}


	/// --- TANK PRISONER COLLISION
	int z = 0;
	for(auto& tank : tanks) {
		for(auto p = prisoners.begin(); p != prisoners.end(); p++) {
			z++;
			if(!p->free) {
				continue;
			}
			v3 d = p->pos - tank.pos; 
			if(d.len2() > 200.f) {
				continue;
			}
			
			if(d.len() > 18.f) {
				continue;
			}
			p->free = false;
			p->pos = v3{9999,9999,0};

			// sizeof hooper
			for(int i = 0; i < 5; i++) {
				if(tank.hopper[i] == NULL_ID) {
					tank.hopper[i] = p.Key();
					break;
				} 
			}
		}
	}

	/// --- TANK TANK COLLISION
	for (auto tank = tanks.begin(); tank != tanks.end(); tank++) {
		auto other = tank;
		for (other++; other != tanks.end(); other++) {
			v3 d = other->pos - tank->pos;
			// arbitraryish large number
			if(d.len2() > 200)
				continue;
			fp32 l = d.len();
			if (l > 16.f)
				continue;
			v3 n = d.normal();
			if (n.sum() == 0)
				n = v3{ 0.5, 0.5, 0 };

			fp32 vl0 = other->vel.len();
			fp32 vl1 = tank->vel.len();

			fp32 d0 = std::max(0.f, other->vel.normal().dot(n));
			fp32 d1 = std::max(0.f, tank->vel.normal().dot(n));

			v3 f0 = (n * vl0 * -0.3 * d0);
			v3 f1 = (n * vl1 * 0.3 * d1);

			other->vel = (other->vel * (0.7 + 0.3 * (1.f - d0))) + f1;
			tank->vel = (tank->vel * (0.7 + 0.3 * (1.f - d1))) + f0;

			other->vel = other->vel + n * (l - 16.f) * -0.05;
			tank->vel = tank->vel + n * (l - 16.f) * 0.05;
		}
	}


};

tank_t* tankInstance::GetPlayerTank(playerIX ix) {
	auto it = std::find(inmap.begin(), inmap.end(), ix);
	if(it != inmap.end()) {
		return tanks.Get(it->entity_id);	
	}
	return nullptr;
}

}
