
#include <assert.h>
#include <emmintrin.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <vector>

#include "arrview.hpp"

#define ARRSIZE(x) (sizeof(x) / sizeof(x[0]))
static constexpr int MAX_COLORS = 32;
#pragma once
struct src16C {
	uint32_t* colors;
	uint8_t*  src;
	int       width;
	int       height;
	uint32_t  Get(int x) { return colors[src[x]]; }
};

typedef uint8_t paletteSet_t[MAX_COLORS];
struct paletteMap_t {
	union {
		uint8_t offset[MAX_COLORS];
		__m128i m128i[2];
	};
	inline auto operator[](const uint8_t i) const { return offset[i]; }
	void        debug()
	{
		for (int i = 0; i < MAX_COLORS; i++) {
			printf("%hhu\n", offset[i]);
		}
	}
};
struct color_t {
	float a, r, g, b;
	color_t(uint32_t v)
	{
		a = (v & 0xff000000) >> 24;
		r = (v & 0x00ff0000) >> 16;
		g = (v & 0x0000ff00) >> 8;
		b = (v & 0x000000ff);
		a /= 255.f;
		r /= 255.f;
		g /= 255.f;
		b /= 255.f;
	}
	// return distance from other color
	float dist(color_t& x) const
	{
		float s0 = (x.a - a);
		float s1 = (x.r - r);
		float s2 = (x.g - g);
		float s3 = (x.b - b);
		s0 *= 0.1f; // matching alpha not important
		return s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3;
	}
};
struct palette_t {
	// should probably sort palette from 0 to white ? ? ?
	uint32_t color[MAX_COLORS];

	uint8_t WHITE;
	uint8_t BLACK;
	uint8_t RED;
	uint8_t GREEN;
	uint8_t BLUE;

	uint8_t YELLOW;
	uint8_t PINK;
	uint8_t TEAL;
	uint8_t PURPLE;

	void GenNames()
	{
		WHITE  = Match(0xffffffff);
		BLACK  = Match(0xff000000);
		RED    = Match(0xffff0000);
		GREEN  = Match(0xff00ff00);
		BLUE   = Match(0xff0000ff);
		YELLOW = Match(0xffffff00);
		PINK   = Match(0xfff55fd4);
		TEAL   = Match(0xff55ffaa);
		PURPLE = Match(0xff7f61df);
	}

	// TODO find closest match binary search ?
	uint8_t Match(uint32_t in) const
	{

		float   d = 999999999.f;
		color_t c0(in);
		// printf("INCOLOR %08x\n", in);
		uint8_t v = 0;
		for (uint8_t i = 0; i < MAX_COLORS; i++) {
			color_t c1(color[i]);
			float   nd = c0.dist(c1);
			if (nd < d) {
				d = nd;
				v = i;
			}
		}
		// printf("MACOLOR %08x DIST %f\n", color[v], d);
		return v;
	}
};

extern palette_t defaultPalette;

template <typename T> struct renderTarget {
	int width;
	int height;
	T*  raw32;
};

// define sprite locations on a sprite map
struct spriteLoc_t {
	int16_t x0;
	int16_t y0;
	int16_t x1;
	int16_t y1;
	int     width() const { return std::abs(x1 - x0); }
	int     height() const { return std::abs(y1 - y0); }
};
struct tileDesc_t {
	int16_t     x0;
	int16_t     y0;
	int16_t     x1;
	int16_t     y1;
	int16_t     offx;
	int16_t     offy;
	uint32_t    flags;
	int         width() const { return std::abs(x1 - x0); }
	int         height() const { return std::abs(y1 - y0); }
	spriteLoc_t spriteL() const { return spriteLoc_t{ x0, y0, x1, y1 }; }
};

typedef uint8_t               dstFormat;
typedef renderTarget<uint8_t> sprite_t;
typedef renderTarget<uint8_t> dst_t;

struct renderProxie {
	int        yoff;
	int        height;
	dstFormat* raw32;
};

static inline uint32_t argb(uint32_t a, uint32_t r, uint32_t g, uint32_t b)
{
	return a << 24 | r << 16 | g << 8 | b;
}

int pixmapToSprite(const unsigned char* pixmap[], int pixmapSize, const palette_t& p, sprite_t& tex,
                   paletteMap_t& map);

int pixmapToU32(const char* pixmap[], int pixmapSize, uint32_t** out, int* out_w, int* out_h);

struct monofont {
	sprite_t&    src;
	palette_t&   pal;
	paletteMap_t map;
	int          fwidth;
	int          fheight;
	int          lchars; // characters per line
	void         WriteTo(dst_t& dst, uint8_t fg, uint8_t bg, int x, int y, const char* msg);
};

struct blitcmd {
	uint8_t sprite_i;
	uint8_t map_i;
	int     srcx0, srcy0, srcx1, srcy1;
	int     dstx0, dsty0;
};
struct tilecmd {
	uint8_t     sprite_i;
	uint8_t     map_i;
	spriteLoc_t sl;
	// int      srcx0, srcy0;
	// int16_t  sw, sh;
	int      dstx0, dsty0;
	uint16_t dw, dh;
};
struct chkrcmd {
	int     x0, y0, x1, y1;
	int     offx, offy;
	uint8_t c0, c1;
	uint8_t cw, ch;
};

struct linecmd {
	int     x0, y0, x1, y1;
	uint8_t color;
};
struct circcmd {
	int     cx, cy, r;
	uint8_t color;
	bool    fill;
};
struct blnkcmd {
	uint8_t color;
};

union cmdunion {
	blitcmd blit;
	tilecmd tile;
	circcmd circ;
	blnkcmd blnk;
	linecmd line;
	chkrcmd chkr;
	// initializers
	cmdunion(){};
	cmdunion(const blitcmd& b)
	    : blit(b){};
	cmdunion(const tilecmd& t)
	    : tile(t){};
	cmdunion(const blnkcmd& b)
	    : blnk(b){};
	cmdunion(const circcmd& c)
	    : circ(c){};
	cmdunion(const linecmd& l)
	    : line(l){};
	cmdunion(const chkrcmd& c)
	    : chkr(c){};
};
struct cmdtoken {
	uint8_t  ctype;
	cmdunion cmd;

	enum { BLIT, TILE, LINE, CIRC, MASK, BLNK, CHKR };
};

class renderContext {

	std::vector<cmdtoken> commands;

    public:
	dst_t                             dst;
	std::vector<sprite_t>             sprites;
	std::vector<arr_view<tileDesc_t>> tileDesc;
	std::vector<paletteMap_t>         maps;
	palette_t                         palette;

	renderContext()
	    : dst{}
	{
		maps.push_back(paletteMap_t{ { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,
		                               11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
		                               22, 23, 24, 25, 26, 27, 28, 29, 30, 31 } });
	}

	~renderContext()
	{
		for (auto& s : sprites) {
			free(s.raw32);
		}
	}

	void reset() { commands.resize(0); }

	void blit(uint8_t sprite_i, uint8_t map_i, int srcx0, int srcy0, int srcx1, int srcy1,
	          int dstx0, int dsty0)
	{

		commands.push_back(cmdtoken{
		    cmdtoken::BLIT,
		    blitcmd{ sprite_i, map_i, srcx0, srcy0, srcx1, srcy1, dstx0, dsty0 } });
	}
	void tile(uint8_t sprite_i, uint8_t map_i, spriteLoc_t sl, int dstx0, int dsty0, int dstx1,
	          int dsty1)
	{
		commands.push_back(
		    cmdtoken{ cmdtoken::TILE,
		              tilecmd{ sprite_i, map_i, sl, dstx0, dsty0,
		                       // must be positive
		                       (uint16_t)(dstx1 - dstx0), (uint16_t)(dsty1 - dsty0) } });
	}

	void blank(uint8_t color)
	{
		commands.push_back(cmdtoken{ cmdtoken::BLNK, blnkcmd{ color } });
	}
	void drawCircle(int cx, int cy, int radius, uint8_t color, bool fill = false)
	{
		commands.push_back(
		    cmdtoken{ cmdtoken::CIRC, circcmd{ cx, cy, radius, color, fill } });
	}
	void drawLine(int x0, int y0, int x1, int y1, uint8_t color)
	{
		commands.push_back(cmdtoken{ cmdtoken::LINE, linecmd{ x0, y0, x1, y1, color } });
	}
	void checker(int x0, int y0, int x1, int y1, uint8_t cw, uint8_t ch, int offx, int offy,
	             uint8_t c0, uint8_t c1)
	{
		commands.push_back(cmdtoken{
		    cmdtoken::CHKR, chkrcmd{ x0, y0, x1, y1, offx, offy, c0, c1, cw, ch } });
	}

	void proxieProcess(const renderProxie& rp);
};

#include "job.hpp"
class renderJob : public tw::job_t {
	renderContext* ctx;
	renderProxie   rp;

    public:
	renderJob(){};
	renderJob(renderContext* context, renderProxie proxie)
	    : ctx(context)
	    , rp(proxie){};

	virtual void Exec() override
	{
		// TODO
		// process all of a contexts cmdtoken relative to slice of
		// screen
		ctx->proxieProcess(rp);
	}
};

static inline void FillJobList(renderContext& ctx, int threads, std::vector<renderJob>& jobs)
{

	int          hm   = ctx.dst.height / threads;
	int          poff = (ctx.dst.width * hm);
	renderProxie rp   = { 0, hm, ctx.dst.raw32 };
	for (int i = 0; i < threads; i++) {
		jobs.push_back(renderJob(&ctx, rp));
		rp.yoff += hm;
		rp.raw32 += poff;
	}
}
// TODO
// design is kinda crude, shouldn't this be implemented in renderContext?

// NOTE
// must call Messanger::BeginMsg to set position and line spacing of subsequent
// Messanger::Write commands
class Messanger {

	static constexpr int MAXLEN = 78;
	struct Item {
		char    msg[MAXLEN];
		uint8_t fg, bg;
	};
	struct MessagePane {
		int    x, y, h, w;
		size_t num;
	};
	renderContext&           ctx;
	monofont&                fnt;
	std::vector<Item>        items;
	std::vector<MessagePane> panes;

    public:
	uint8_t fgColor, bgColor;
	Messanger(renderContext& r, monofont& f)
	    : ctx(r)
	    , fnt(f)
	{
		fgColor = 26;
		bgColor = 1;
	}

	// TODO
	// use width arg to wrap lines that are too long
	void BeginMsg(int x, int y, int lineheight, int width)
	{
		panes.push_back(MessagePane{ x, y, lineheight, width, 0 });
	}
	void Write(const char* fmt, ...)
	{
		panes.back().num++;
		items.emplace_back();
		Item&   item = items.back();
		va_list args;
		va_start(args, fmt);
		vsnprintf(item.msg, MAXLEN, fmt, args);
		va_end(args);
		item.fg = fgColor;
		item.bg = bgColor;
	}
	void WriteOut()
	{
		std::vector<MessagePane>::iterator p = panes.begin();
		if (p == panes.end())
			return;
		int total = p->num;
		for (Item& i : items) {
			int r = total - p->num;
			fnt.WriteTo(ctx.dst, i.fg, i.bg, p->x, p->y + (p->h * r), i.msg);
			p->num--;
			if (p->num == 0) {
				p++;
				if (p == panes.end())
					return;
				total = p->num;
			}
		}
	}
	void Clear()
	{
		items.resize(0);
		panes.resize(0);
	}
};
